package vn.tcbs.analytic.service;

import java.util.List;

public interface AnaUserDAO {
	public List<AnaUserDTO> getOpenAccountStatistic(String type, String date);

	public AnaUserStatisticDTO getUserStatistic();
}
