package vn.tcbs.analytic.service;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SQLQuery;
import org.hibernate.internal.SessionImpl;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import vn.tcbs.tool.base.BaseDAO;

@Repository("anaUserDAO")
@Transactional
public class AnaUserDAOImpl extends BaseDAO implements AnaUserDAO {
	private static int ORACLE_CURSOR_TYPE = -10;
	private static Logger logger = LoggerFactory.getLogger(AnaUserDAOImpl.class);

	@Override
	public List<AnaUserDTO> getOpenAccountStatistic(String type, String date) {
		try {
			SessionImpl sessionImpl = (SessionImpl) getSession();
			CallableStatement cs = sessionImpl.connection()
					.prepareCall("call TCBS_ANA_OPENACCOUNT_STATISTIC(?, ?, ?) ");
			cs.registerOutParameter(1, ORACLE_CURSOR_TYPE);
			cs.setString(2, type);
			cs.setString(3, date);
			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(1);
			List<AnaUserDTO> results = new ArrayList<AnaUserDTO>();
			while (rs.next()) {
				AnaUserDTO obj = new AnaUserDTO();
				obj.setTotal(rs.getLong(1));
				obj.setActivated(rs.getLong(2));
				obj.setQueue(rs.getLong(3));
				obj.setReportDate(rs.getString(4));
				results.add(obj);
			}
			rs.close();
			cs.close();
			return results;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public AnaUserStatisticDTO getUserStatistic() {
		String qStr = "select count(*) as total, sum(case when user_id is not null then 1 else 0 end ) as activated from tcbs_user_openaccount_queue";
		SQLQuery q = getSession().createSQLQuery(qStr);
		q.setResultTransformer(new AliasToBeanResultTransformer(AnaUserStatisticDTO.class));
		return (AnaUserStatisticDTO) q.list().get(0);
	}

}
