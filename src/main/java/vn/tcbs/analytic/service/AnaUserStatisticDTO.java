package vn.tcbs.analytic.service;

import java.io.Serializable;
import java.math.BigDecimal;

public class AnaUserStatisticDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private BigDecimal TOTAL;
	private BigDecimal ACTIVATED;

	public BigDecimal getTotal() {
		return TOTAL;
	}

	public void setTotal(BigDecimal total) {
		this.TOTAL = total;
	}

	public BigDecimal getActivated() {
		return ACTIVATED;
	}

	public void setActivated(BigDecimal activated) {
		this.ACTIVATED = activated;
	}

}
