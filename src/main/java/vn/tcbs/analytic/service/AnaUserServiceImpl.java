package vn.tcbs.analytic.service;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("anaUserService")
public class AnaUserServiceImpl implements AnaUserService {
	@Autowired
	private AnaUserDAO anaUserDAO;

	public List<AnaUserDTO> getOpenAccountStatisticByDate(DateTime date) {

		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
		return anaUserDAO.getOpenAccountStatistic("M", dtf.print(new DateTime()));
	}

	public List<AnaUserDTO> getOpenAccountStatisticByMonth(DateTime date) {
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
		return anaUserDAO.getOpenAccountStatistic("Y", dtf.print(new DateTime()));
	}

	@Override
	public AnaUserStatisticDTO getUserStatistic() {
		return anaUserDAO.getUserStatistic();
	}
}