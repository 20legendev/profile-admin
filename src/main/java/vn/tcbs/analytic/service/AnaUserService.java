package vn.tcbs.analytic.service;

import java.util.List;

import org.joda.time.DateTime;

public interface AnaUserService {
	public List<AnaUserDTO> getOpenAccountStatisticByDate(DateTime date);

	public List<AnaUserDTO> getOpenAccountStatisticByMonth(DateTime date);

	public AnaUserStatisticDTO getUserStatistic();
}