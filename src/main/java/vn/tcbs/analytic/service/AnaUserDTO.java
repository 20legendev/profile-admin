package vn.tcbs.analytic.service;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.NamedNativeQuery;
import javax.persistence.QueryHint;

@NamedNativeQuery(name = "getOpenAccountStatistic", query = "{ call TCBS_ANA_OPENACCOUNT_STATISTIC(?,:mode, :date) }", resultClass = AnaUserDTO.class, hints = {
		@QueryHint(name = "org.hibernate.callable", value = "true") })

public class AnaUserDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long total;
	private Long activated;
	private Long queue;
	private String reportDate;

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Long getActivated() {
		return activated;
	}

	public void setActivated(Long activated) {
		this.activated = activated;
	}

	public Long getQueue() {
		return queue;
	}

	public void setQueue(Long queue) {
		this.queue = queue;
	}

}
