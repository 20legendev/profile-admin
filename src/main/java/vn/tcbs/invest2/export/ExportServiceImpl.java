package vn.tcbs.invest2.export;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import vn.tcbs.cas.profile.admin.dto.AdminSearchDTO;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.service.ApplicationService;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.tool.base.TcbsUtils;

@Service("exportService")
public class ExportServiceImpl implements ExportService {

	@Value("${tcbs.folder}")
	private String tcbsFolder;
	@Autowired
	private UserService userService;
	@Autowired
	private ApplicationService applicationService;
	private static Logger logger = LoggerFactory.getLogger(ExportServiceImpl.class);

	public List<String> exportSearch(AdminSearchDTO search)
			throws FileNotFoundException, IOException, InvalidFormatException {
		List<TcbsUser> listUser = userService.find(search);
		DateTimeFormatter dtf = DateTimeFormat.forPattern("YYYYMMdd");
		DateTimeFormatter out = DateTimeFormat.forPattern("dd-MM-YYYY");

		XSSFWorkbook wb = new XSSFWorkbook(OPCPackage.open(tcbsFolder + File.separator + "tcbs" + File.separator
				+ "template" + File.separator + "user-template.xlsx"));
		XSSFSheet sheet = wb.getSheetAt(0);

		Row row = sheet.getRow(8);
		Cell cell = row.createCell(5);
		cell.setCellValue(out.print(new DateTime()));

		TcbsUser user;
		int fromLine = 12;
		for (int i = 0; i < listUser.size(); i++) {
			user = listUser.get(i);
			String custodyCd = applicationService.getCustodyCd(user.getId());

			row = sheet.createRow(i + fromLine);
			cell = row.createCell(0);
			cell.setCellValue(i + 1);
			cell = row.createCell(1);
			cell.setCellValue(user.getLastname() + " " + user.getFirstname());
			cell = row.createCell(2);
			cell.setCellValue(user.getEmail());
			cell = row.createCell(3);
			cell.setCellValue(user.getPhone());
			cell = row.createCell(4);
			if (custodyCd != null) {
				cell.setCellValue(custodyCd);
			}
			cell = row.createCell(5);
			if (user.getVsdStatus().equals(0)) {
				cell.setCellValue("Not activated");
			} else {
				cell.setCellValue("Activated");
			}
		}
		String fileName = TcbsUtils.generateRandom(12) + ".xlsx";
		String filePath = tcbsFolder + File.separator + "generated" + File.separator + dtf.print(new DateTime())
				+ File.separator + fileName;
		File outputF = new File(filePath);
		if (!outputF.exists()) {
			outputF.getParentFile().mkdirs();
			outputF.getParentFile().setWritable(true);
			outputF.getParentFile().setReadable(true);
		}
		FileOutputStream outputStream = new FileOutputStream(outputF);
		wb.write(outputStream);
		List<String> result = new ArrayList<String>();
		result.add(fileName);
		result.add(filePath);
		return result;
	}
}