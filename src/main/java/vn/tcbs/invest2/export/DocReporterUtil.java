package vn.tcbs.invest2.export;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;


import com.fasterxml.jackson.databind.ObjectMapper;

import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;

public class DocReporterUtil {

	private static final String TCBS = "tcb";
	private static Logger logger = LoggerFactory.getLogger(DocReporterUtil.class);

	/**
	 * Process Document Report Docx With FileStream Input
	 * 
	 * @param filePath
	 * @param outputFile
	 * @param context
	 * @return
	 */
	public static String processDocumentReportDocx(File filePath, String outputFile, Object contractInfo) {
		try {
			if (filePath.isFile()) {
				InputStream in = new FileInputStream(filePath);
				IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
				OutputStream out = new FileOutputStream(new File(outputFile));
				IContext context = report.createContext();
				context.put(TCBS, contractInfo);
				report.process(context, out);
				return outputFile;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Process Document Report Docx With File Path Input
	 * 
	 * @param filePath
	 * @param outputFile
	 * @param context
	 * @return
	 */
	public static String processDocumentReportDocx(String filePath, String outputFile, IContext context) {
		try {
			logger.info(filePath + "/" + outputFile + "/" + new ObjectMapper().writeValueAsString(context.getContextMap()));
			InputStream in = new FileInputStream(new File(filePath));
			IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
			File outputF = new File(outputFile);
			if (!outputF.exists()) {
				outputF.getParentFile().mkdirs();
				outputF.getParentFile().setWritable(true);
				outputF.getParentFile().setReadable(true);
			}
			OutputStream out = new FileOutputStream(outputF);
			report.process(context, out);
			return outputFile;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// public void convert(String documentObjet) {
	// try {
	// if (documentObjet != null) {
	// Gson gson = new Gson();
	// RegisterInfo documentJquest = gson.fromJson(documentObjet,
	// RegisterInfo.class);
	// HashMap<String, Object> map = new HashMap<String, Object>();
	// if (documentJquest != null) {
	// List<String> listOutputString = new ArrayList<String>();
	// HttpServletRequest req = context.getHttpServletRequest();
	// if (req == null) {
	// return null;
	// }
	// StringBuffer path = new StringBuffer(req
	// .getServletContext().getRealPath(File.separator))
	// .append(TEMPLATE)
	// .append(File.separator)
	// .append(DocumentUtil
	// .getFolderTemplate(documentJquest.getType()))
	// .append(File.separator);
	//
	// StringBuffer outPath = new StringBuffer(req
	// .getServletContext().getRealPath(File.separator))
	// .append(SAVE_FILE).append(File.separator);
	//
	// File[] lsFiles = new File(path.toString()).listFiles();
	// if (lsFiles != null && lsFiles.length > 0) {
	// for (File objFile : lsFiles) {
	// String name = objFile.getName();
	// int pos = name.lastIndexOf(".");
	// if (pos > 0) {
	// name = name.substring(0, pos);
	// }
	// StringBuffer namefile = new StringBuffer(name)
	// .append("_").append(
	// System.currentTimeMillis());
	// String outPut = new StringBuffer(outPath.toString())
	// .append(namefile).append(DOCX).toString();
	//
	//
	// DocReporterUtil.processDocumentReportDocx(objFile,
	// outPut, map);
	//
	//
	//
	// File outputFile = new File(outPut);
	// if (outputFile.isFile()) {
	// // listOutputString.add(namefile.toString());
	// ResponseBuilder response = Response
	// .ok((Object) outputFile);
	// response.header(
	// "Content-Disposition",
	// "attachment; filename="
	// + namefile.toString() + ".docx");
	// return response.build();
	// }
	// }
	// }
	// return Response.ok(gson.toJson(listOutputString)).build();
	//
	// }
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// }

}
