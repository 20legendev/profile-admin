package vn.tcbs.invest2.export;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import vn.tcbs.cas.profile.admin.dto.AdminSearchDTO;

public interface ExportService {
	public List<String> exportSearch(AdminSearchDTO search) throws FileNotFoundException, IOException, InvalidFormatException;
}