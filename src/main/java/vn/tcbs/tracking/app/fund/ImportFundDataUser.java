package vn.tcbs.tracking.app.fund;

import java.io.Serializable;
import java.util.Date;

public class ImportFundDataUser implements Serializable {
	private String fullname;
	private String idnumber;
	private Date iddate;
	private String idplace;
	private Integer gender;
	private Date birthday;
	private String address;
	private String phone;
	private String email;
	private String bankaccountnumber;
	private String bankaccountname;
	private String custodyCd;

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getIdnumber() {
		return idnumber;
	}

	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}

	public Date getIddate() {
		return iddate;
	}

	public void setIddate(Date iddate) {
		this.iddate = iddate;
	}

	public String getIdplace() {
		return idplace;
	}

	public void setIdplace(String idplace) {
		this.idplace = idplace;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBankaccountnumber() {
		return bankaccountnumber;
	}

	public void setBankaccountnumber(String bankaccountnumber) {
		this.bankaccountnumber = bankaccountnumber;
	}

	public String getBankaccountname() {
		return bankaccountname;
	}

	public void setBankaccountname(String bankaccountname) {
		this.bankaccountname = bankaccountname;
	}

	public String getCustodyCd() {
		return custodyCd;
	}

	public void setCustodyCd(String custodyCd) {
		this.custodyCd = custodyCd;
	}

}
