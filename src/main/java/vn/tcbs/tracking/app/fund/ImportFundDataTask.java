package vn.tcbs.tracking.app.fund;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service("importFundService")
public class ImportFundDataTask {

	public List<ImportFundDataUser> test(String filePath) {
		try {
			List<ImportFundDataUser> data = readFile(filePath);
			System.out.println("Tìm thấy: " + data.size() + " người");
			return data;
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Dữ liệu có chứa lỗi");
		}
		return null;
	}

	public List<ImportFundDataUser> start(String filePath) {
		List<ImportFundDataUser> data = test(filePath);
		if (data != null) {
			return data;
		}
		return null;
	}

	public List<ImportFundDataUser> readFile(String filePath) throws FileNotFoundException, IOException {
		List<ImportFundDataUser> persons = new ArrayList<ImportFundDataUser>();
		Workbook wb;
		try {
			wb = new HSSFWorkbook(new FileInputStream(filePath));
		} catch (Exception ex) {
			wb = new XSSFWorkbook(new FileInputStream(filePath));
		}
		Sheet sheet = wb.getSheetAt(0);
		Row row;
		Cell cell;
		int rows;
		rows = sheet.getPhysicalNumberOfRows();
		int cols = 0;
		int tmp = 0;
		for (int i = 0; i < 10 || i < rows; i++) {
			row = sheet.getRow(i);
			if (row != null) {
				tmp = sheet.getRow(i).getPhysicalNumberOfCells();
				if (tmp > cols)
					cols = tmp;
			}
		}
		DateTimeFormatter dtf = DateTimeFormat.forPattern("dd/MM/yyyy");
		for (int r = 1; r < rows; r++) {
			ImportFundDataUser user = new ImportFundDataUser();
			row = sheet.getRow(r);
			if (row != null) {
				cell = row.getCell(1);
				if (cell != null) {
					user.setFullname(cell.getStringCellValue());
				}
				cell = row.getCell(2);
				if (cell != null) {
					user.setIdnumber(cell.getStringCellValue());
				}

				cell = row.getCell(4);
				if (cell != null) {
					if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
						user.setIddate(cell.getDateCellValue());
					}
				}
				cell = row.getCell(5);
				if (cell != null) {
					user.setIdplace(cell.getStringCellValue());
				}
				cell = row.getCell(6);
				if (cell != null) {
					String gender = cell.getStringCellValue();
					user.setGender(gender != null && gender.equals("1-Nam") ? 1 : 0);
				}
				cell = row.getCell(7);
				if (cell != null) {
					if (cell.getStringCellValue() != null && !cell.getStringCellValue().equals("")) {
						user.setBirthday(dtf.parseDateTime(cell.getStringCellValue()).toDate());
					}
				}
				cell = row.getCell(12);
				if (cell != null) {
					user.setAddress(cell.getStringCellValue());
				}
				cell = row.getCell(14);
				if (cell != null) {
					user.setPhone(cell.getStringCellValue());
				}
				cell = row.getCell(15);
				if (cell != null) {
					user.setEmail(cell.getStringCellValue().toLowerCase());
				}
				cell = row.getCell(20);
				if (cell != null) {
					user.setCustodyCd(cell.getStringCellValue().toUpperCase());
				}
				cell = row.getCell(23);
				if (cell != null) {
					user.setBankaccountnumber(cell.getStringCellValue().toLowerCase());
				}
				persons.add(user);
			}
		}
		System.out.println(new ObjectMapper().writeValueAsString(persons));
		return persons;
	}

}
