package vn.tcbs.cas.exception;

/**
 * 
 * @author Kien Nguyen
 *
 */
public class FlexException extends Exception {
	private static final long serialVersionUID = 1L;

	public FlexException(String message) {
		super(message);
	}

	public FlexException() {
		super();
	}

	public FlexException(Throwable cause) {
		super(cause);
	}

	public FlexException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
