package vn.tcbs.cas.exception;

/**
 * 
 * @author NguyenNam
 *
 */
public class NetworkException extends Exception {
	public NetworkException(String message) {
		super(message);
	}
	
	public NetworkException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NetworkException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NetworkException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
