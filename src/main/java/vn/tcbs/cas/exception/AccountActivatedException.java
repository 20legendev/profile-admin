package vn.tcbs.cas.exception;

/**
 * 
 * @author NguyenNam
 *
 */
public class AccountActivatedException extends Exception {
	public AccountActivatedException(String message) {
		super(message);
	}
	
	public AccountActivatedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountActivatedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public AccountActivatedException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
