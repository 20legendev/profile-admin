package vn.tcbs.cas.exception;

/**
 * 
 * @author NguyenNam
 *
 */
public class ActiveAccountException extends Exception {
	public ActiveAccountException(String message) {
		super(message);
	}
	
	public ActiveAccountException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ActiveAccountException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ActiveAccountException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
