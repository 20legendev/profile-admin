package vn.tcbs.cas.exception;

/**
 * 
 * @author NguyenNam
 *
 */
public class TokenNotFoundException extends Exception{
	public TokenNotFoundException(){
		
	}
	
	public TokenNotFoundException(Throwable cause) {
		super(cause);
	}

	public TokenNotFoundException(String message) {
		super(message);
	}
	
	public TokenNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
