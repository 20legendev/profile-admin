package vn.tcbs.cas.profile.flex;

import java.io.Serializable;
import java.math.BigDecimal;

public class Cf1001 implements Serializable {

	private static final long serialVersionUID = 1L;
	private String currDate;
	private String txDate;
	private String custId;
	private String fullName;
	private String idCode;
	private String idDate;
	private String idPlace;
	private String address;
	private String mobile;
	private String custodyCd;
	private String afAcctNo;
	private String symbol;
	private String bondName;
	
	private BigDecimal seBalance;
	private BigDecimal trade;
	private BigDecimal endTradeBal;
	private BigDecimal blocked;
	private BigDecimal endBlockedBal;
	private BigDecimal mortage;
	private BigDecimal endMortageBal;
	private BigDecimal netting;
	private BigDecimal endNettingBal;
	private BigDecimal standing;
	private BigDecimal endStandingBal;
	private BigDecimal receiving;
	private BigDecimal endReceivingBal;
	private BigDecimal caReceiving;
	private BigDecimal withdraw;
	private BigDecimal endWithdrawBal;
	private BigDecimal ckChoGd;
	public String getCurrDate() {
		return currDate;
	}
	public void setCurrDate(String currDate) {
		this.currDate = currDate;
	}
	public String getTxDate() {
		return txDate;
	}
	public void setTxDate(String txDate) {
		this.txDate = txDate;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getIdCode() {
		return idCode;
	}
	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}
	public String getIdDate() {
		return idDate;
	}
	public void setIdDate(String idDate) {
		this.idDate = idDate;
	}
	public String getIdPlace() {
		return idPlace;
	}
	public void setIdPlace(String idPlace) {
		this.idPlace = idPlace;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getCustodyCd() {
		return custodyCd;
	}
	public void setCustodyCd(String custodyCd) {
		this.custodyCd = custodyCd;
	}
	public String getAfAcctNo() {
		return afAcctNo;
	}
	public void setAfAcctNo(String afAcctNo) {
		this.afAcctNo = afAcctNo;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getBondName() {
		return bondName;
	}
	public void setBondName(String bondName) {
		this.bondName = bondName;
	}
	public BigDecimal getSeBalance() {
		return seBalance;
	}
	public void setSeBalance(BigDecimal seBalance) {
		this.seBalance = seBalance;
	}
	public BigDecimal getTrade() {
		return trade;
	}
	public void setTrade(BigDecimal trade) {
		this.trade = trade;
	}
	public BigDecimal getEndTradeBal() {
		return endTradeBal;
	}
	public void setEndTradeBal(BigDecimal endTradeBal) {
		this.endTradeBal = endTradeBal;
	}
	public BigDecimal getBlocked() {
		return blocked;
	}
	public void setBlocked(BigDecimal blocked) {
		this.blocked = blocked;
	}
	public BigDecimal getEndBlockedBal() {
		return endBlockedBal;
	}
	public void setEndBlockedBal(BigDecimal endBlockedBal) {
		this.endBlockedBal = endBlockedBal;
	}
	public BigDecimal getMortage() {
		return mortage;
	}
	public void setMortage(BigDecimal mortage) {
		this.mortage = mortage;
	}
	public BigDecimal getEndMortageBal() {
		return endMortageBal;
	}
	public void setEndMortageBal(BigDecimal endMortageBal) {
		this.endMortageBal = endMortageBal;
	}
	public BigDecimal getNetting() {
		return netting;
	}
	public void setNetting(BigDecimal netting) {
		this.netting = netting;
	}
	public BigDecimal getEndNettingBal() {
		return endNettingBal;
	}
	public void setEndNettingBal(BigDecimal endNettingBal) {
		this.endNettingBal = endNettingBal;
	}
	public BigDecimal getStanding() {
		return standing;
	}
	public void setStanding(BigDecimal standing) {
		this.standing = standing;
	}
	public BigDecimal getEndStandingBal() {
		return endStandingBal;
	}
	public void setEndStandingBal(BigDecimal endStandingBal) {
		this.endStandingBal = endStandingBal;
	}
	public BigDecimal getReceiving() {
		return receiving;
	}
	public void setReceiving(BigDecimal receiving) {
		this.receiving = receiving;
	}
	public BigDecimal getEndReceivingBal() {
		return endReceivingBal;
	}
	public void setEndReceivingBal(BigDecimal endReceivingBal) {
		this.endReceivingBal = endReceivingBal;
	}
	public BigDecimal getCaReceiving() {
		return caReceiving;
	}
	public void setCaReceiving(BigDecimal caReceiving) {
		this.caReceiving = caReceiving;
	}
	public BigDecimal getWithdraw() {
		return withdraw;
	}
	public void setWithdraw(BigDecimal withdraw) {
		this.withdraw = withdraw;
	}
	public BigDecimal getEndWithdrawBal() {
		return endWithdrawBal;
	}
	public void setEndWithdrawBal(BigDecimal endWithdrawBal) {
		this.endWithdrawBal = endWithdrawBal;
	}
	public BigDecimal getCkChoGd() {
		return ckChoGd;
	}
	public void setCkChoGd(BigDecimal ckChoGd) {
		this.ckChoGd = ckChoGd;
	}
	

}
