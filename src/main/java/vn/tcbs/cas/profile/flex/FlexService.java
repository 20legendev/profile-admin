package vn.tcbs.cas.profile.flex;

import java.util.HashMap;
import java.util.List;

import vn.tcbs.cas.exception.FlexException;
import vn.tcbs.cas.profile.dto.CfmastDTO;

public interface FlexService {
	public CfmastDTO getUserInfo(String idNumber) throws FlexException;

	public List<HashMap> getStock(String custodyCd);
}
