package vn.tcbs.cas.profile.flex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.cas.exception.FlexException;
import vn.tcbs.cas.http.context.HttpContext;
import vn.tcbs.cas.profile.dto.CfmastDTO;
import vn.tcbs.cas.profile.service.JsonService;
import vn.tcbs.library.http.ResponseData;

@Service("flexService")
public class FlexServiceImpl implements FlexService {

	@Autowired
	private HttpContext httpContext;
	@Value("${url.api.flexservice}")
	private String flexUrl;

	private static Logger logger = LoggerFactory.getLogger(FlexServiceImpl.class);

	public CfmastDTO getUserInfo(String idNumber) throws FlexException {
		List<NameValuePair> data = new ArrayList<NameValuePair>();
		data.add(new BasicNameValuePair("idcode", idNumber));
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(flexUrl + "account/get-detail");
		try {
			post.setEntity(new UrlEncodedFormEntity(data));
			HttpResponse response = client.execute(post);
			String resStr = EntityUtils.toString(response.getEntity(), "UTF-8");
			ObjectMapper om = JsonService.getInstance().getObjectMapper();
			ResponseData<CfmastDTO> resData = om.readValue(resStr, new TypeReference<ResponseData<CfmastDTO>>() {
			});
			if (resData.getStatus() == 0) {
				return resData.getData();
			}
			return null;
		} catch (Exception ex) {
			throw new FlexException("FLEX_CONNECTION_ERROR");
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<HashMap> getStock(String custodyCd) {
		String url = flexUrl + "balance/by-custody";
		Map<String, String> headers = new HashMap<String, String>(1);
		Map<String, String> requestParams = new HashMap<String, String>();
		requestParams.put("startDate", "01/01/1970");
		requestParams.put("custodyCd", custodyCd);
		List<HashMap> data = new ArrayList<HashMap>();
		try {
			vn.tcbs.cas.http.context.HttpResponse response = httpContext.getHttpUtil().post(url, headers,
					requestParams);
			if (response != null) {
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					ObjectMapper om = JsonService.getInstance().getObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					@SuppressWarnings("unchecked")
					ResponseData<List<HashMap>> result = om.readValue(responseString, ResponseData.class);
					if (result.getStatus() == 0) {
						if (result.getData() != null) {
							return result.getData();
						}
					} else {
						logger.error("GET_STOCK_ERROR: DATA_STATUS={}", result.getStatus());
					}
				} else {
					logger.error("GET_STOCK_ERROR: STATUS={}", response.getStatus());
				}
			} else {
				logger.error("GET_STOCK_ERROR: response_null");
			}
		} catch (Exception e) {
			logger.error("GET_STOCK_ERROR: {}", e.getMessage());
			e.printStackTrace();
		}
		return data;
	}
}
