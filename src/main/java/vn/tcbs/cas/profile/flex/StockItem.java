package vn.tcbs.cas.profile.flex;

import java.io.Serializable;

public class StockItem implements Serializable {
	private static final long serialVersionUID = 1L;
	private String symbol;
	private Integer seBalance;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Integer getSeBalance() {
		return seBalance;
	}

	public void setSeBalance(Integer seBalance) {
		this.seBalance = seBalance;
	}

}