package vn.tcbs.cas.profile.object;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.tool.base.TcbsUtils;

public class OpenAccountMessageValidator implements Validator {

	public boolean supports(Class clazz) {
		return OpenAccountMessage.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		OpenAccountMessage msg = (OpenAccountMessage) target;

		validateEmpty(msg.getLastname(), "lastname", errors);
		validateSpecialCharacter(msg.getLastname(), "lastname", errors);

		validateSpecialCharacter(msg.getFirstname(), "firstname", errors);
		validateEmpty(msg.getFirstname(), "firstname", errors);

		validateEmpty(msg.getIdnumber(), "idnumber", errors);
		validateSpecialCharacter(msg.getIdplace(), "idplace", errors);
		validateEmpty(msg.getIdplace(), "idplace", errors);

		validateSpecialCharacter(msg.getAddress(), "address", errors);
		validateEmpty(msg.getAddress(), "address", errors);

		if (TcbsUtils.validatePhone(msg.getPhone()) == false) {
			errors.rejectValue("phone", "Wrong format");
		}
		if (TcbsUtils.validateEmail(msg.getEmail()) == false) {
			errors.rejectValue("email", "Email wrong format");
		}
		validateEmpty(msg.getProvince(), "province", errors);
		if (msg.getProvince() != null && msg.getProvince().equals("-1")) {
			errors.rejectValue("province", "Province is blank");
		}
		/*
		 * if (TcbsUtils.validateDate(msg.getIddate(), "dd/mm/yyyy") == false) {
		 * errors.rejectValue("iddate", "Wrong format"); }
		 */
		if (msg.getIsbank() == 1) {
			validateEmpty(msg.getBankaccountnumber(), "bankaccountnumber", errors);
			validateEmpty(msg.getBankaccountname(), "bankaccountname", errors);
			validateEmpty(msg.getBankname(), "bankname", errors);
			validateEmpty(msg.getBankcode(), "bankcode", errors);
			if (msg.getBankname() == null || msg.getBankname().equals("-1")) {
				errors.rejectValue("bankname", "Wrong format");
			}
			if (msg.getBankcode() == null || msg.getBankcode().equals("-1")) {
				errors.rejectValue("bankcode", "Wrong format");
			}
		}
		if (msg.getGender() == null || msg.getGender().equals("")) {
			errors.rejectValue("gender", "Missing value");
		}
		if (msg.getPassword() != null) {
			TcbsUtils.validatePassword(msg.getPassword(), errors);
		}
	}

	private void validateEmpty(String s, String val, Errors errors) {
		if (s == null || s.trim().isEmpty()) {
			errors.rejectValue(val, "Can't be empty");
		}
	}

	private void validateSpecialCharacter(String s, String val, Errors errors) {
		if (s == null) {
			errors.rejectValue(val, "Name can't have special characters");
			return;
		}
		Pattern p = Pattern.compile("[$&+:;=?@#|'\\\\]");
		Matcher m = p.matcher(s);
		boolean b = m.find();
		if (b) {
			errors.rejectValue(val, "Name can't have special characters");
		}
	}

}
