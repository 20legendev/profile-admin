package vn.tcbs.cas.profile.object;

import java.io.Serializable;

import vn.tcbs.cas.profile.entity.TcbsUser;

public class CachedUser implements Serializable {
	private static final long serialVersionUID = 1L;
	private TcbsUser user;
	private String appItegration;
	private String urlRedirect;
	private String userInAppId;

	public TcbsUser getUser() {
		return user;
	}

	public String getUserInAppId() {
		return userInAppId;
	}

	public void setUserInAppId(String userInAppId) {
		this.userInAppId = userInAppId;
	}

	public void setUser(TcbsUser user) {
		this.user = user;
	}

	public String getAppItegration() {
		return appItegration;
	}

	public void setAppItegration(String appItegration) {
		this.appItegration = appItegration;
	}

	public String getUrlRedirect() {
		return urlRedirect;
	}

	public void setUrlRedirect(String urlRedirect) {
		this.urlRedirect = urlRedirect;
	}
}