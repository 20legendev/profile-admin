package vn.tcbs.cas.profile.object;

import java.io.Serializable;

public class RmOpenAccount implements Serializable {
	private String rmid;

	private String redirecturl;

	public RmOpenAccount() {
	}

	public String getRmid() {
		return rmid;
	}

	public void setRmid(String rmid) {
		this.rmid = rmid;
	}

	public String getRedirecturl() {
		return redirecturl;
	}

	public void setRedirecturl(String redirecturl) {
		this.redirecturl = redirecturl;
	}

}