package vn.tcbs.cas.profile.object;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.tool.base.TcbsUtils;

public class OpenAccountValidator implements Validator {

	@Autowired
	private UserService userService;

	public boolean supports(Class clazz) {
		return TcbsUser.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		TcbsUser user = (TcbsUser) target;
		if (user != null) {
			if (user.getUsername() == null
					&& (user.getIdentifications().size() == 0 || user
							.getIdentifications().get(0).getIdNumber() == null)) {
				errors.rejectValue("username", "MISSING_DATA");
			}
			if (user.getIdentifications().size() == 0) {
				errors.reject("citizenid", "MISSING_CITIZEN_ID");
			}
		} else {
			errors.rejectValue("error", "NO_DATA_RECEIVED");
		}

	}

	private void validateEmpty(String s, String val, Errors errors) {
		if (s == null || s.trim().isEmpty()) {
			errors.rejectValue(val, "Can't be empty");
		}
	}

	private void validateSpecialCharacter(String s, String val, Errors errors) {
		if (s == null) {
			errors.rejectValue(val, "Name can't have special characters");
			return;
		}
		Pattern p = Pattern.compile("[$&+:;=?@#|'\\\\]");
		Matcher m = p.matcher(s);
		boolean b = m.find();
		if (b) {
			errors.rejectValue(val, "Name can't have special characters");
		}
	}

}
