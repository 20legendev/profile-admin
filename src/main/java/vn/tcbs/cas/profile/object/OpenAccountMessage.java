package vn.tcbs.cas.profile.object;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import vn.tcbs.cas.profile.service.JsonService;
import vn.tcbs.tool.base.TcbsUtils;

public class OpenAccountMessage {
	private String referenceid;
	@NotNull
	private String firstname;
	@NotNull
	private String lastname;
	@NotNull
	private Integer gender;
	@NotNull
	private String idnumber;
	@NotNull
	private String idtype;
	@NotNull
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private Date iddate;
	@NotNull
	private String idplace;
	@NotNull
	private String email;
	@NotNull
	private String phone;
	@NotNull
	private String address = "";
	@NotNull
	private String province = "";
	private int isbank;
	private String bankaccountnumber;
	private String bankaccountname;
	private String bankname;
	private String bankbranch;
	private String bankcode;
	private String custodycode;
	private Long referer;
	private String captcha;
	private String mode;
	private String bankplace;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private Date birthday;

	private String fullname;
	private String password;

	public OpenAccountMessage() {
		this.referenceid = TcbsUtils.generateRandom(16);
	}

	public void normalizeData() {
		this.firstname = this.firstname.toUpperCase();
		this.lastname = this.lastname.toUpperCase();
		this.email = this.email.toLowerCase();
	}

	public String getBankplace() {
		return bankplace;
	}

	public void setBankplace(String bankplace) {
		this.bankplace = bankplace;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getReferenceid() {
		return referenceid;
	}

	public void setReferenceid(String referenceid) {
		this.referenceid = referenceid;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getIdnumber() {
		return idnumber;
	}

	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}

	public String getIdtype() {
		return idtype;
	}

	public void setIdtype(String idtype) {
		this.idtype = idtype;
	}

	public Date getIddate() {
		return iddate;
	}

	public void setIddate(Date iddate) {
		this.iddate = iddate;
	}

	public String getIdplace() {
		return idplace;
	}

	public void setIdplace(String idplace) {
		this.idplace = idplace;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getBankaccountnumber() {
		return bankaccountnumber;
	}

	public void setBankaccountnumber(String bankaccountnumber) {
		this.bankaccountnumber = bankaccountnumber;
	}

	public String getBankaccountname() {
		return bankaccountname;
	}

	public void setBankaccountname(String bankaccountname) {
		this.bankaccountname = bankaccountname;
	}

	public String getBankname() {
		return bankname;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}

	public String getBankbranch() {
		return bankbranch;
	}

	public void setBankbranch(String bankbranch) {
		this.bankbranch = bankbranch;
	}

	public String getBankcode() {
		return bankcode;
	}

	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

	public String getCustodycode() {
		return custodycode;
	}

	public void setCustodycode(String custodycode) {
		this.custodycode = custodycode;
	}

	public Long getReferer() {
		return referer;
	}

	public void setReferer(Long referer) {
		this.referer = referer;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public int getIsbank() {
		return isbank;
	}

	public void setIsbank(int isbank) {
		this.isbank = isbank;
	}

	public String toString() {
		try {
			return JsonService.getInstance().getObjectMapper().writeValueAsString(this);
		} catch (Exception ex) {
			return "";
		}
	}
}
