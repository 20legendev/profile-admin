package vn.tcbs.cas.profile.bond;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsUser;

public interface BondService {
	public void sendToBond(TcbsUser user);
	public List<BondDTO> getBond(Long userId);
}