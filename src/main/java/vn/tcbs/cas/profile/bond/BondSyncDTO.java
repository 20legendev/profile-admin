package vn.tcbs.cas.profile.bond;

import java.io.Serializable;

public class BondSyncDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String CustomerName;
	private String IdentityCard;
	private String IdentityBy;
	private String IdentityDate;
	private String Email;
	private String Phone;
	private String Birthday;
	private String Address;
	private String BankCode;
	private Integer AgencyId;
	private String addressCity;
	private Integer gender;
	private String custodyCode;
	private String bankCity;
	private String bankName;
	private String branchName;
	private String branchCode;
	private String tcbsid;
	

	public String getTcbsid() {
		return tcbsid;
	}

	public void setTcbsid(String tcbsid) {
		this.tcbsid = tcbsid;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getIdentityCard() {
		return IdentityCard;
	}

	public void setIdentityCard(String identityCard) {
		IdentityCard = identityCard;
	}

	public String getIdentityBy() {
		return IdentityBy;
	}

	public void setIdentityBy(String identityBy) {
		IdentityBy = identityBy;
	}

	public String getIdentityDate() {
		return IdentityDate;
	}

	public void setIdentityDate(String identityDate) {
		IdentityDate = identityDate;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public String getBirthday() {
		return Birthday;
	}

	public void setBirthday(String birthday) {
		Birthday = birthday;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getBankCode() {
		return BankCode;
	}

	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public Integer getAgencyId() {
		return AgencyId;
	}

	public void setAgencyId(Integer agencyId) {
		AgencyId = agencyId;
	}

	public String getAddressCity() {
		return addressCity;
	}

	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getCustodyCode() {
		return custodyCode;
	}

	public void setCustodyCode(String custodyCode) {
		this.custodyCode = custodyCode;
	}

	public String getBankCity() {
		return bankCity;
	}

	public void setBankCity(String bankCity) {
		this.bankCity = bankCity;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
}