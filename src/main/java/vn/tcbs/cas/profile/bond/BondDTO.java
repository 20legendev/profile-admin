package vn.tcbs.cas.profile.bond;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.cas.profile.service.JsonService;

public class BondDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String code;
	private Integer quantity;
	private Long price;
	private Integer agencyStatus;
	private Integer operationStatus;
	private Integer superVisorStatus;
	private Integer accountantStatus;
	private Integer customerStatus;

	public Integer getAgencyStatus() {
		return agencyStatus;
	}

	public void setAgencyStatus(Integer agencyStatus) {
		this.agencyStatus = agencyStatus;
	}

	public Integer getOperationStatus() {
		return operationStatus;
	}

	public void setOperationStatus(Integer operationStatus) {
		this.operationStatus = operationStatus;
	}

	public Integer getSuperVisorStatus() {
		return superVisorStatus;
	}

	public void setSuperVisorStatus(Integer superVisorStatus) {
		this.superVisorStatus = superVisorStatus;
	}

	public Integer getAccountantStatus() {
		return accountantStatus;
	}

	public void setAccountantStatus(Integer accountantStatus) {
		this.accountantStatus = accountantStatus;
	}

	public Integer getCustomerStatus() {
		return customerStatus;
	}

	public void setCustomerStatus(Integer customerStatus) {
		this.customerStatus = customerStatus;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public static BondDTO valueOf(String str) {
		try {
			ObjectMapper om = JsonService.getInstance().getObjectMapper();
			om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			return om.readValue(str, BondDTO.class);
		} catch (Exception ex) {
			return null;
		}
	}

	public static BondDTO fromHashMap(HashMap<String, Object> map) {
		BondDTO data = new BondDTO();
		data.agencyStatus = (Integer) map.get("agencyStatus");
		data.operationStatus = (Integer) map.get("operationStatus");
		data.superVisorStatus = (Integer) map.get("superVisorStatus");
		data.accountantStatus = (Integer) map.get("accountantStatus");
		data.customerStatus = (Integer) map.get("customerStatus");
		data.code = (String) map.get("code");
		data.quantity = (Integer) map.get("quantity");
		data.price = (Long) map.get("price");
		return data;
	}
}