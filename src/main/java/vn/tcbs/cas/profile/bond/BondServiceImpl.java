package vn.tcbs.cas.profile.bond;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;

import vn.tcbs.cas.http.context.HttpContext;
import vn.tcbs.cas.http.context.HttpResponse;
import vn.tcbs.cas.profile.entity.TcbsApplicationUser;
import vn.tcbs.cas.profile.entity.TcbsBankAccount;
import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.service.ApplicationService;
import vn.tcbs.cas.profile.service.JsonService;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.tool.base.ResponseData;
import vn.tcbs.tool.base.RetryHandler;

@Service("bondService")
public class BondServiceImpl extends RetryHandler implements BondService {

	@Value("${queue.bond.user}")
	private String queueBond;
	@Value("${queue.bond.user.key}")
	private String queueBondKey;
	@Value("${sync.queue.exchangename}")
	private String exchangeName;
	@Value("${url.api.bond}")
	private String bondUrl;
	@Autowired
	private UserService userService;
	@Autowired
	private ApplicationService applicationService;
	@Autowired
	private HttpContext httpContext;

	private static Logger logger = LoggerFactory.getLogger(BondServiceImpl.class);

	@Override
	public void sendToBond(TcbsUser user) {
		// logger.info("SEND_TO_BOND_QUEUE: {}", user.getId());
		BondSyncDTO map = buildRequest(user);
		try {
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(Include.NON_NULL);
			send(exchangeName, queueBondKey, om.writeValueAsString(map));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public BondSyncDTO buildRequest(TcbsUser user) {
		BondSyncDTO dto = new BondSyncDTO();
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		dto.setCustomerName(user.getLastname() + " " + user.getFirstname());
		TcbsIdentification i = user.getIdentifications().size() > 0 ? user.getIdentifications().get(0) : null;
		if (i != null) {
			dto.setIdentityCard(i.getIdNumber());
			dto.setIdentityBy(i.getIdPlace());
			dto.setIdentityDate(sf.format(i.getIdDate()));
		}
		// List<TcbsApplicationUser> custodyCd =
		// applicationService.getUserInAppByUserId(user.getId(), "FLEX");
		dto.setEmail(user.getEmail());
		dto.setPhone(user.getPhone());
		if (user.getBirthday() != null) {
			dto.setBirthday(sf.format(user.getBirthday()));
		}
		dto.setAgencyId(0);
		dto.setGender(user.getGender());
		dto.setCustodyCode(user.getUsername());
		if (user.getBanks().size() > 0) {
			TcbsBankAccount ba = user.getBanks().get(0);
			dto.setBankName(ba.getBankBranch());
			dto.setBranchCode(ba.getBankCode());
			dto.setBankCode(user.getBanks().get(0).getBankAccountNo());
			dto.setBankCity(ba.getBankProvince());
		}
		if (user.getAddress().size() > 0) {
			dto.setAddress(user.getAddress().get(0).getAddress());
		}
		if (user.getTcbsid() != null) {
			dto.setTcbsid(user.getTcbsid());
		}
		/*
		try {
			logger.info("BUILD_BOND_REQUEST: {}", JsonService.getInstance().getObjectMapper().writeValueAsString(dto));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		*/
		return dto;
	}

	@Override
	public void onMessage(Message msg, Channel channel) throws Exception {
		String msgBody = new String(msg.getBody());
		TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {
		};
		Map<String, String> requestParams = JsonService.getInstance().getObjectMapper().readValue(msgBody, typeRef);
		String url = bondUrl + "customer";
		Map<String, String> headers = new HashMap<String, String>(1);
		try {
			/*
			logger.info("SENT_TO_BOND: {}",
					JsonService.getInstance().getObjectMapper().writeValueAsString(requestParams));
			*/
			HttpResponse response = httpContext.getHttpUtil().post(url, headers, requestParams);
			if (response != null) {
				logger.info("SEND_TO_BOND_STATUS: {} {}", response.getStatus(), new String(response.getBody()));
				if (!response.isOk()) {
					notifyError("SEND_TO_BOND_ERROR", msgBody + " WITH ERROR " + response.getStatus());
				}
			} else {
				logger.error("SEND_TO_BOND_ERROR: {} response null", msgBody);
				notifyError("SEND_TO_BOND_ERROR", msgBody);
			}
		} catch (Exception e) {
			logger.error("SEND_TO_BOND_ERROR: {}", e.getMessage());
			notifyError("SEND_TO_BOND_ERROR", e.getMessage());
		}
	}

	@Override
	public List<BondDTO> getBond(Long userId) {
		List<TcbsApplicationUser> lau = applicationService.getUserInAppByUserId(userId, "TC_BOND");
		TcbsUser user = userService.getById(userId);
		String bondId = null;
		if (lau.size() == 0) {
			bondId = getBondProfileFromBondService(user);
			if (bondId != null) {
				applicationService.addUserInAppByUserId(user, applicationService.getByName("TC_BOND"), bondId);
			}
		} else {
			bondId = lau.get(0).getUserAppId();
		}
		// logger.info("BOND: GETTING_BOND_INFORMATION {}", userId);
		List<BondDTO> listBond = new ArrayList<BondDTO>();
		if (bondId != null) {
			listBond = getBondByBondId(bondId);
		}
		return listBond;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<BondDTO> getBondByBondId(String bondId) {
		List<BondDTO> listBond = new ArrayList<BondDTO>();
		String url = bondUrl + "trading/listtrading";
		Map<String, String> headers = new HashMap<String, String>(1);
		Map<String, String> requestParams = new HashMap<String, String>();
		requestParams.put("indexPage", "0");
		requestParams.put("offset", "0");
		requestParams.put("permission", "Customer");
		requestParams.put("customerId", bondId);
		try {
			HttpResponse response = httpContext.getHttpUtil().post(url, headers, requestParams);
			if (response != null) {
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					ObjectMapper om = JsonService.getInstance().getObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					// logger.info("BOND_RESPONSE: {}", responseString);
					ResponseData<List> result = om.readValue(responseString, ResponseData.class);
					if (result.getStatus() == 0) {
						if (result.getData() != null) {
							HashMap<String, Object> map = (HashMap<String, Object>) result.getData();
							List<HashMap> data = (List<HashMap>) map.get("lsTrading");
							for (int i = 0; i < data.size(); i++) {
								listBond.add(BondDTO.fromHashMap(data.get(i)));
							}
						}
					}
				} else {
					logger.info("BOND: status {}", response.getStatus());
				}
			} else {
				logger.info("BOND: response null");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Can not get bond:" + e.getMessage());
		}
		return listBond;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String getBondProfileFromBondService(TcbsUser user) {
		String idNumber = null;
		if (user != null) {
			if (user.getIdentifications().size() > 0) {
				idNumber = user.getIdentifications().get(0).getIdNumber();
			}
		}
		if (idNumber == null) {
			return null;
		}
		String url = bondUrl + "customer/" + idNumber;
		try {
			HttpResponse response = httpContext.getHttpUtil().get(url, new HashMap<String, String>(1),
					new HashMap<String, String>(1));
			if (response != null) {
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					// logger.info("GET_BOND_DATA: {}", responseString);
					ObjectMapper om = JsonService.getInstance().getObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					ResponseData<HashMap> result = om.readValue(responseString, ResponseData.class);
					if (result.getStatus() == 0) {
						if (result.getData() != null) {
							return String.valueOf(result.getData().get("customerId"));
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Can not verify token:" + e.getMessage());
			return null;
		}
		return null;

	}

}