package vn.tcbs.cas.profile.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import vn.tcbs.cas.profile.entity.TcbsApplicationUser;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.service.ApplicationService;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.cas.profile.service.token.TokenService;
import vn.tcbs.tool.base.TcbsUtils;

@Controller
@RequestMapping(value = "/invest")
public class IntegrationController extends BaseController {

	@Value("${web.link.flexlogintoken}")
	private String flexLoginUrl;
	@Value("${web.link.flexlogintoken.mobile}")
	private String flexLoginUrlMobile;
	@Autowired
	private UserService userService;
	@Autowired
	private ApplicationService applicationService;
	@Autowired
	private TokenService tokenService;

	@RequestMapping(value = "/sso", method = RequestMethod.GET)
	public ModelAndView loginInvestSuccess(HttpServletRequest req) {
		List<TcbsApplicationUser> list = applicationService.getUserInAppByUserId(Long.valueOf(req.getRemoteUser()),
				"FLEX");
		if (list.size() > 0) {
			String custodyCd = list.get(0).getUserAppId();
			if (custodyCd != null) {
				String token = tokenService.generateTokenOneTime(custodyCd);
				if (token != null) {
					if (TcbsUtils.isMobile(req)) {
						return new ModelAndView("redirect:" + flexLoginUrlMobile + "?tokenid=" + token);
					} else {
						return new ModelAndView(
								"redirect:" + flexLoginUrl + "?tokenid=" + token + "&returnUrl=/OnlineTrading/Home");
					}
				} else {
					return new ModelAndView("redirect:/error/500.html");
				}
			}
		}
		String url = "/u/tai-khoan.html";
		TcbsUser user = getUser(req);
		List<Integer> ready = userService.isFlexTradeReady(user);
		if (ready.size() == 0) {
			url = "/u/tai-khoan.html?tab=settings&mode=active-flex";
		} else {
			url = "/u/tai-khoan.html?tab=profile&mode=data-miss";
		}
		return new ModelAndView("redirect:" + url);
	}

}
