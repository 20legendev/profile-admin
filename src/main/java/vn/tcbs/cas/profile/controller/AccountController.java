package vn.tcbs.cas.profile.controller;

import java.text.SimpleDateFormat;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;
import vn.tcbs.cas.profile.object.CachedUser;
import vn.tcbs.cas.profile.service.AddressService;
import vn.tcbs.cas.profile.service.BankService;
import vn.tcbs.cas.profile.service.JsonService;
import vn.tcbs.cas.profile.service.UserQueueService;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.cas.profile.service.token.TokenService;
import vn.tcbs.tool.base.TcbsUtils;

@Controller
@RequestMapping(value = "/")
public class AccountController {

	@Autowired
	private AddressService addressService;
	@Value("${url.api.tokenservice}")
	private String tokenServiceUrl;
	@Value("${cas.server.public}")
	private String casUrl;
	@Value("${cas.server.mypublic}")
	private String myPublicUrl;
	@Autowired
	private TokenService tokenService;
	@Autowired
	private UserQueueService queueService;
	@Autowired
	private UserService userService;
	@Autowired
	private BankService bankService;

	private static Logger logger = LoggerFactory.getLogger(AccountController.class);

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest req, HttpServletResponse res) {
		req.getSession().invalidate();
		String url = "https://" + casUrl + "/logout?service=https://" + myPublicUrl + "/logout-success.html";
		return new ModelAndView("redirect:" + url);
	}

	@RequestMapping(value = "/logout-success", method = RequestMethod.GET)
	public ModelAndView logoutSuccess(HttpServletRequest req, HttpServletResponse res) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(req, res, auth);
		}
		req.getSession().invalidate();
		ModelAndView mv = new ModelAndView("account/logout-success");
		return mv;
	}

	@RequestMapping(value = "/login-failed", method = RequestMethod.GET)
	public ModelAndView loginFailed(HttpServletRequest req, HttpServletResponse res) {
		return new ModelAndView("account/login-failed");
	}

	@RequestMapping(value = "/mo-tai-khoan-thanh-cong", method = RequestMethod.GET)
	public ModelAndView registerSuccess(HttpServletRequest req, HttpSession sess) {
		ModelAndView mv = new ModelAndView("account/open-success");
		mv.addObject("title", "Chúc mừng bạn đã mở tài khoản thành công!");
		String email = (String) sess.getAttribute("OPEN_ACCOUNT:EMAIL");
		String emailProvider = "NA";
		if (email != null) {
			emailProvider = TcbsUtils.detectEmailProvider(email);
		}
		String emailLink = null;
		if (emailProvider.equals("GOOGLE")) {
			emailLink = "http://mail.google.com";
		} else if (emailProvider.equals("YAHOO")) {
			emailLink = "http://mail.yahoo.com";
		} else if (emailProvider.equals("MS")) {
			emailLink = "http://outlook.com";
		}
		mv.addObject("emaillink", emailLink);
		return mv;
	}

	@RequestMapping(value = "/mo-tai-khoan", method = RequestMethod.GET)
	public ModelAndView openAccount(HttpServletRequest req) {
		String token = req.getParameter("token");
		String ref = req.getParameter("ref");
		String edit = req.getParameter("edit");
		// mode = 1: Co password
		Integer mode = 1;
		ModelAndView mv = new ModelAndView("account/open");
		mv.addObject("template", "profile");
		HttpSession sess = req.getSession();

		sess.removeAttribute("referenceid");
		sess.removeAttribute("TCB_IDLIST");

		// Token = TCB = Co password
		boolean isTcbs = false;
		TcbsUser user = null;
		if (token != null) {
			isTcbs = true;
			mv.addObject("template", "default");
			String userStr = tokenService.verifyToken(token);
			if (userStr == null)
				return new ModelAndView("error/500");
			CachedUser cUser = null;
			try {
				cUser = JsonService.getInstance().getObjectMapper().readValue(userStr, CachedUser.class);
			} catch (Exception ex) {
				return new ModelAndView("error/500");
			}
			if (cUser == null)
				return new ModelAndView("error/500");
			user = cUser.getUser();
			mv.addObject("redirecturl", cUser.getUrlRedirect());
			mv.addObject("disableidentification", 1);
		}
		if (ref != null && ref.equals("referer")) {
			sess.setAttribute("referer", 1);
			mv.addObject("template", "referer");
			mode = 0;
		} else {
			sess.removeAttribute("referer");
		}
		if (edit != null) {
			String referenceid = tokenService.verifyToken(edit);
			sess.setAttribute("referenceid", referenceid);
			if (referenceid != null) {
				mode = 0;
				TcbsUserOpenaccountQueue queueUser = queueService.getByReferenceId(referenceid);
				if (queueUser != null) {
					mv.addObject("editprovince", queueUser.getProvince());
					user = userService.buildUserFromQueue(queueUser);
					if (user.getBanks().size() > 0) {
						HashMap<String, Object> bankDetail = bankService
								.getByBankCode(user.getBanks().get(0).getBankCode());
						try {
							mv.addObject("bankprovince", bankDetail.get("regional"));
							mv.addObject("banksys", bankDetail.get("banksys"));
							mv.addObject("bankcode", bankDetail.get("bankcode"));
						} catch (Exception ex) {
						}
					}
				}
			}
			mv.addObject("redirecturl", "referer/khach-hang.html");
		}
		if (user != null) {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			TcbsIdentification i = user.getIdentifications().size() > 0 ? user.getIdentifications().get(0) : null;
			if (i != null) {
				if (isTcbs) {
					sess.setAttribute("TCB_IDLIST", user.getIdentifications().get(0));
				}
				if (i.getIdDate() != null) {
					mv.addObject("iddateformated", df.format(i.getIdDate()));
				}
				mv.addObject("identification", i);
			}
			if (user.getBirthday() != null) {
				mv.addObject("birthdayformated", df.format(user.getBirthday()));
			}
			if (user.getBanks().size() > 0) {
				mv.addObject("bank", user.getBanks().get(0));
			}
			mv.addObject("user", user);
		}

		mv.addObject("havepassword", mode.equals(1) ? 1 : 0);
		mv.addObject("province", addressService.getByParentName("VN"));
		return mv;
	}

}
