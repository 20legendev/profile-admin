package vn.tcbs.cas.profile.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.cxf.helpers.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import vn.tcbs.cas.profile.entity.Contract;
import vn.tcbs.cas.profile.entity.TcbsUserUpload;
import vn.tcbs.cas.profile.service.ContractService;
import vn.tcbs.cas.profile.service.UserUploadService;

@Controller
@RequestMapping(value = "/file")
public class FileController extends BaseController {

	@Autowired
	private ContractService contractService;
	@Autowired
	private UserUploadService userUploadService;
	@Value("${tcbs.folder}")
	private String tcbsFolder;
	
	private static Logger logger = LoggerFactory.getLogger(FileController.class);

	@RequestMapping(value = "/public/{filename}", method = RequestMethod.GET)
	public ModelAndView downloadPublicFile(@PathVariable("filename") String fName, HttpServletRequest req,
			HttpServletResponse response) {
		String filePath = tcbsFolder + File.separator + "tcbs" + File.separator + "public" + File.separator + fName;
		logger.info("File download = {}", filePath);
		try {
			InputStream inputStream = new FileInputStream(new File(filePath));
			response.setContentType("application/force-download");
			response.setHeader("Content-Disposition", "attachment; filename=" + fName);
			IOUtils.copy(inputStream, response.getOutputStream());
			response.flushBuffer();
			inputStream.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/download-generated-file", method = RequestMethod.GET)
	public ModelAndView downloadGeneratedFile(HttpServletRequest req, HttpServletResponse response) {
		String filePath = (String) req.getSession().getAttribute("generated-file");
		String fileName = (String) req.getSession().getAttribute("generated-file-name");
		try {
			InputStream inputStream = new FileInputStream(new File(filePath));
			response.setContentType("application/force-download");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
			IOUtils.copy(inputStream, response.getOutputStream());
			response.flushBuffer();
			inputStream.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/download/{alias}", method = RequestMethod.GET)
	public ModelAndView dashboard(@PathVariable("alias") String alias, HttpServletRequest req,
			HttpServletResponse response) {
		Contract contract = contractService.getByAlias(alias);
		if (contract == null) {
			return new ModelAndView("error/500");
		}
		contract.setDownloadCount(contract.getDownloadCount() + 1);
		contractService.update(contract);
		String downloadName = "TCBS-Contract-" + alias + ".docx";
		String fileName = contract.getDocumentPath();
		try {
			InputStream inputStream = new FileInputStream(new File(fileName));
			response.setContentType("application/force-download");
			response.setHeader("Content-Disposition", "attachment; filename=" + downloadName);
			IOUtils.copy(inputStream, response.getOutputStream());
			response.flushBuffer();
			inputStream.close();
		} catch (Exception ex) {
			return new ModelAndView("error/500");
		}
		return null;
	}

	@RequestMapping(value = "/alias/{alias}", method = RequestMethod.GET)
	public ModelAndView aliasFile(@PathVariable("alias") String alias, HttpServletRequest req,
			HttpServletResponse response) {
		List<TcbsUserUpload> lua = userUploadService.getByAlias(alias);
		if (lua.size() > 0) {
			try {
				String fileName = lua.get(0).getFilePath();
				InputStream inputStream = new FileInputStream(new File(fileName));
				response.setContentType("application/force-download");
				response.setHeader("Content-Disposition", "attachment; filename=" + alias);
				IOUtils.copy(inputStream, response.getOutputStream());
				response.flushBuffer();
				inputStream.close();
			} catch (Exception ex) {
				return new ModelAndView("error/500");
			}
		}
		return null;
	}

	@RequestMapping(value = "/download/tc", method = RequestMethod.GET)
	public ModelAndView downloadTC(HttpServletResponse response) {
		String downloadName = "MB-TCBS-TC01.pdf";
		String fileName = tcbsFolder + File.separator + "tcbs" + File.separator + "template" + File.separator
				+ "MB-TCBS-TC01.pdf";
		try {
			InputStream inputStream = new FileInputStream(new File(fileName));
			response.setContentType("application/force-download");
			response.setHeader("Content-Disposition", "attachment; filename=" + downloadName);
			IOUtils.copy(inputStream, response.getOutputStream());
			response.flushBuffer();
			inputStream.close();
			return null;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

}