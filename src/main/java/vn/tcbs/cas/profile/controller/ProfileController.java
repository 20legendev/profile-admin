package vn.tcbs.cas.profile.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;

import vn.tcbs.cas.profile.bond.BondDTO;
import vn.tcbs.cas.profile.bond.BondService;
import vn.tcbs.cas.profile.entity.TcbsApplicationUser;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.flex.FlexService;
import vn.tcbs.cas.profile.flex.StockItem;
import vn.tcbs.cas.profile.service.ApplicationService;
import vn.tcbs.cas.profile.service.JsonService;
import vn.tcbs.cas.profile.service.UserService;

@Controller
@RequestMapping(value = "/u/")
public class ProfileController extends BaseController {

	@Autowired
	private UserService userService;
	@Autowired
	private FlexService flexService;
	@Autowired
	private ApplicationService applicationService;
	@Autowired
	private BondService bondService;

	private static Logger logger = LoggerFactory.getLogger(ProfileController.class);

	@RequestMapping(value = "/chuc-nang-dang-cap-nhat", method = RequestMethod.GET)
	public ModelAndView updating(HttpServletRequest req) throws JsonProcessingException {
		ModelAndView mv = new ModelAndView("profile/updating");
		mv.addObject("tag", "none");
		return mv;
	}

	@RequestMapping(value = "/tai-khoan", method = RequestMethod.GET)
	public ModelAndView dashboard(HttpServletRequest req) throws JsonProcessingException {
		String url = "/referer/home.html";
		return new ModelAndView("redirect:" + url);
		/*
		TcbsUser user = getUser(req);
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("user", user);
		model.put("userstr", JsonService.getInstance().getObjectMapper().writeValueAsString(user));
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		if (user.getBirthday() != null) {
			model.put("birthdayFormated", sf.format(user.getBirthday()));
		}
		model.put("relation", relationConfig.split(":"));
		String tab = req.getParameter("tab");
		String mode = req.getParameter("mode");
		if (mode != null) {
			model.put("mode", mode);
			if (mode.equals("data-miss")) {
				List<Integer> miss = userService.isFlexTradeReady(user);
				model.put("missing", miss);
			}
		}
		if (tab == null) {
			tab = "settings";
		}
		model.put("tab", tab);
		model.put("tag", "tai-khoan");
		return new ModelAndView("profile/summary", model);
		*/
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/tong-quan", method = RequestMethod.GET)
	public ModelAndView summary(HttpServletRequest req, HttpSession session) {
		Long userId = Long.valueOf(req.getRemoteUser());
		ModelAndView mv = new ModelAndView("profile/tong-quan");
		List<TcbsApplicationUser> lu = applicationService.getUserInAppByUserId(userId, "FLEX");
		if (lu.size() > 0) {
			List<HashMap> listStock = flexService.getStock(lu.get(0).getUserAppId());
			logger.info("NUMBER_OF_STOCK {}", listStock.size());
			List<StockItem> list = new ArrayList<StockItem>();
			for (int i = 0; i < listStock.size(); i++) {
				StockItem item = new StockItem();
				item.setSymbol((String) listStock.get(i).get("symbol"));
				item.setSeBalance((Integer) listStock.get(i).get("seBalance"));
				list.add(item);
			}
			try {
				mv.addObject("stockTicker", JsonService.getInstance().getObjectMapper().writeValueAsString(list));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		List<BondDTO> listBond = bondService.getBond(userId);
		logger.info("LIST_BOND: {}", listBond);
		mv.addObject("tag", "tong-quan");
		return mv;
	}

}
