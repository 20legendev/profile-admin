package vn.tcbs.cas.profile.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/error")
public class ErrorController {

	@RequestMapping(value = "/unauthorized")
	public ModelAndView unauthorized() {
		return new ModelAndView("error/unauthorized");
	}

	@RequestMapping(value = "/500")
	public ModelAndView systemError() {
		return new ModelAndView("error/500");
	}

	@RequestMapping(value = "/403")
	public ModelAndView denied() {
		return new ModelAndView("error/403");
	}

}