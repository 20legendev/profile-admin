package vn.tcbs.cas.profile.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserForgotPassword;
import vn.tcbs.cas.profile.service.PasswordService;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.tool.base.TcbsUtils;

@Controller
@RequestMapping(value = "/")
public class PasswordController extends BaseController {

	@Autowired
	private UserService userService;
	private static Logger logger = LoggerFactory.getLogger(PasswordController.class);
	
	@RequestMapping(value = "/quen-mat-khau", method = RequestMethod.GET)
	public ModelAndView forgotPassword(HttpServletRequest req) {
		HashMap<String, String> model = new HashMap<String, String>();
		String email = (String) req.getParameter("email");
		model.put("email", email != null && !email.equals("") ? email : "");
		return new ModelAndView("user/forgot-password", model);
	}

	@Autowired
	private PasswordService passwordService;

	@RequestMapping(value = "/xac-nhan-thay-doi-mat-khau")
	public ModelAndView forgotPasswordConfirm(HttpServletRequest req) {
		ModelAndView mv = new ModelAndView("user/forgot-password-confirm");
		String token = req.getParameter("change_password_token");
		TcbsUserForgotPassword fp = passwordService.verifyToken(token);
		if (fp == null) {
			return new ModelAndView("user/forgot-password-error");
		}
		TcbsUser user = userService.getById(fp.getUserId());
		if (req.getMethod().equalsIgnoreCase("post")) {
			if (user == null) {
				return new ModelAndView("redirect:/error/unauthorized.html");
			}
			String password = req.getParameter("password");
			String passwordRetype = req.getParameter("password-retype");
			if (!password.equals(passwordRetype)) {
				mv.addObject("error", "Mật khẩu không trùng nhau");
			} else {
				Boolean validated = TcbsUtils.validatePassword(passwordRetype, null);
				if (!validated) {
					mv.addObject("error", "Mật khẩu phải chứa chữ cái, số và có độ dài ít nhất 6 ký tự");
				} else {
					passwordService.updatePassword(user.getId(), password);
					fp.setStatus(0);
					passwordService.update(fp);
					req.getSession().removeAttribute("forgot_password_user");
					return new ModelAndView("redirect:/cap-nhat-mat-khau-thanh-cong.html");
				}
			}
		} else {
			passwordService.increaseAccessCount(fp);
			req.getSession().setAttribute("forgot_password_user", fp.getUserId());
		}
		logger.info("CUSTODYCD {}", user.getUsername());
		mv.addObject("custodycd", user.getUsername() != null ? user.getUsername() : null);
		return mv;
	}

	@RequestMapping(value = "/cap-nhat-mat-khau-thanh-cong")
	public ModelAndView changePasswordSuccess() {
		return new ModelAndView("user/forgot-password-confirm-success");
	}

	@RequestMapping(value = "/email", method = RequestMethod.GET)
	public ModelAndView preview() {
		HashMap<String, Object> hash = new HashMap<String, Object>();
		hash.put("email", "hey babe");
		return new ModelAndView("email/forgot-password", hash);
	}
}