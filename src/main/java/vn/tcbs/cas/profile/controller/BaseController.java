package vn.tcbs.cas.profile.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.service.UserService;

public class BaseController implements InitializingBean{

	@Autowired
	private UserService userService;
	private static Logger logger = LoggerFactory.getLogger(BaseController.class);

	protected TcbsUser getUser(HttpServletRequest req) {
		Long userId = Long.valueOf(req.getRemoteUser());
		logger.info("User đang đăng nhập {}", userId);
		if (userId == null)
			return null;
		return userService.getById(userId);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		/*
		logger.info("SET_TIME_ZONE_TO_HO_CHI_MINH");
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Ho_Chi_Minh"));
		*/
	}
}
