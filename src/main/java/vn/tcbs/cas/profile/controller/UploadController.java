package vn.tcbs.cas.profile.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import vn.tcbs.cas.profile.object.FileMeta;
import vn.tcbs.tool.base.TcbsFileUtils;

@RestController
@RequestMapping(value = "/upload")
public class UploadController {

	@Value("${tcbs.folder}")
	private String tcbsLocalFolder;

	LinkedList<FileMeta> files = new LinkedList<FileMeta>();
	FileMeta fileMeta = null;

	@RequestMapping(value = "/upload-file", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody LinkedList<FileMeta> upload(MultipartHttpServletRequest request,
			HttpServletResponse response) {
		Iterator<String> itr = request.getFileNames();
		MultipartFile mpf = null;
		while (itr.hasNext()) {
			mpf = request.getFile(itr.next());
			System.out.println(mpf.getOriginalFilename() + " uploaded! " + files.size());
			if (files.size() >= 10)
				files.pop();
			fileMeta = new FileMeta();
			fileMeta.setFileName(mpf.getOriginalFilename());
			fileMeta.setFileSize(mpf.getSize() / 1024 + " Kb");
			fileMeta.setFileType(mpf.getContentType());
			try {
				String uploadDir = tcbsLocalFolder + File.separator + "tcbs" + File.separator + "upload"
						+ File.separator + new SimpleDateFormat("yyyyMMdd").format(new Date()) + File.separator;
				fileMeta.setBytes(mpf.getBytes());
				String filePath = TcbsFileUtils.generateFilename(uploadDir, mpf.getOriginalFilename());
				FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(filePath));
			} catch (IOException e) {
				e.printStackTrace();
			}
			files.add(fileMeta);
		}
		response.setContentType("application/json");
		return files;
	}

	@RequestMapping(value = "/file", method = RequestMethod.POST)
	public ModelAndView uploadExcel(HttpServletRequest request, HttpServletResponse response) {
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		String uploadDir = tcbsLocalFolder + File.separator + "tcbs" + File.separator + "upload" + File.separator
				+ new SimpleDateFormat("yyyyMMdd").format(new Date()) + File.separator;
		File outputF = new File(uploadDir + ".metadata");
		if (!outputF.exists()) {
			outputF.getParentFile().mkdirs();
			outputF.getParentFile().setWritable(true);
			outputF.getParentFile().setReadable(true);
		}
		List<String> listFile = new ArrayList<String>();
		try {
			List<FileItem> multiparts = upload.parseRequest(request);
			for (FileItem item : multiparts) {
				if (!item.isFormField() && (TcbsFileUtils.getMimeType(item.getContentType()) == "IMAGETYPE_PNG"
						|| TcbsFileUtils.getMimeType(item.getContentType()) == "IMAGETYPE_JPEG")) {
					String filePath = TcbsFileUtils.generateFilename(uploadDir, item.getName());
					item.write(new File(filePath));
					listFile.add(filePath);
				}
			}
			request.getSession().setAttribute("excelfile", listFile.get(0));
			request.getSession().setAttribute("upload_message", 0);
			return new ModelAndView("redirect:/mo-tai-khoan-excel.html");
		} catch (Exception e) {
			request.getSession().setAttribute("upload_message", 1);
			return new ModelAndView("redirect:/mo-tai-khoan-excel.html");
		}
	}
}
