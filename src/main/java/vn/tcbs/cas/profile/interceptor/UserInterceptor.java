package vn.tcbs.cas.profile.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.tool.base.TcbsUtils;

public class UserInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	private UserService userService;

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		return true;
	}

	public void postHandle(HttpServletRequest req, HttpServletResponse response, Object handler, ModelAndView mv)
			throws Exception {
		String userId = req.getRemoteUser();
		if (userId != null) {
			String fullname = (String) req.getSession().getAttribute("_fullname");
			if (fullname == null) {
				TcbsUser user = userService.getById(Long.valueOf(userId));
				fullname = user.getLastname() + " " + user.getFirstname();
				req.getSession().setAttribute("_fullname", fullname);
			}
			if (mv != null) {
				mv.addObject("_fullname", fullname);
				mv.addObject("_is_admin", TcbsUtils.hasRole("ROLE_ADMIN"));
			}
		}
	}
}
