package vn.tcbs.cas.profile.permission.service;

import java.util.List;

import vn.tcbs.cas.profile.permission.InvGroup;

public interface GroupService {
	public Integer count(int groupid);

	public boolean updateGroup(InvGroup group);

	List<InvGroup> search(String query);

	public boolean removeGroup(Integer groupid);
	public List<InvGroup> getById(Integer groupid);
}