package vn.tcbs.cas.profile.permission.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.cas.http.context.HttpResponse;
import vn.tcbs.cas.profile.permission.InvSystem;
import vn.tcbs.tool.base.BaseESBService;
import vn.tcbs.tool.base.EsbDTO;

@Service("systemService")
public class SystemServiceImpl extends BaseESBService implements SystemService {

	private static Logger logger = LoggerFactory.getLogger(SystemServiceImpl.class);

	@Override
	public List<InvSystem> listAll() {
		String url = esbUrl + "authorization/system/list";
		Map<String, String> headers = new HashMap<String, String>(1);
		headers.put("accept", "application/json");
		Map<String, String> params = new HashMap<String, String>(1);
		try {
			HttpResponse response = httpContext.getHttpUtil().post(url, headers, params);
			if (response != null) {
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					ObjectMapper om = new ObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					EsbDTO<InvSystem> result = om.readValue(responseString, new TypeReference<EsbDTO<InvSystem>>() {
					});
					return result.getData("Entry");
				}
			}
		} catch (Exception e) {
			logger.error("ERROR={}", e.getMessage());
			e.printStackTrace();
		}
		return new ArrayList<InvSystem>();
	}
}