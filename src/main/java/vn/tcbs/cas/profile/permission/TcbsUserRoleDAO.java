package vn.tcbs.cas.profile.permission;

import java.util.List;

public interface TcbsUserRoleDAO {
	public List<TcbsUserRole> getByUserId(Long userId);
}
