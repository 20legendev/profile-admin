package vn.tcbs.cas.profile.permission;

public enum GroupType {
	IT(1), ORG(0);

	private final int levelCode;

	GroupType(int levelCode) {
		this.levelCode = levelCode;
	}

	public int getLevelCode() {
		return this.levelCode;
	}
}
