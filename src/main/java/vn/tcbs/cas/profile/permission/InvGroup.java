package vn.tcbs.cas.profile.permission;

import java.io.Serializable;

public class InvGroup implements Serializable {
	private static final long serialVersionUID = 1L;
	private GroupType grouptype;
	private Integer groupstatus;
	private Integer grouplevel;
	private Integer parentid;
	private String groupname;
	private String groupdesc;
	private Integer groupid;
	private Integer ownerid;

	public GroupType getGrouptype() {
		return grouptype;
	}

	public String getGroupdesc() {
		return groupdesc;
	}

	public void setGroupdesc(String groupdesc) {
		this.groupdesc = groupdesc;
	}

	public void setGrouptype(GroupType grouptype) {
		this.grouptype = grouptype;
	}

	public Integer getGroupstatus() {
		return groupstatus;
	}

	public void setGroupstatus(Integer groupstatus) {
		this.groupstatus = groupstatus;
	}

	public Integer getGrouplevel() {
		return grouplevel;
	}

	public void setGrouplevel(Integer grouplevel) {
		this.grouplevel = grouplevel;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public Integer getGroupid() {
		return groupid;
	}

	public void setGroupid(Integer groupid) {
		this.groupid = groupid;
	}

	public Integer getOwnerid() {
		return ownerid;
	}

	public void setOwnerid(Integer ownerid) {
		this.ownerid = ownerid;
	}

}