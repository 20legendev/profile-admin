package vn.tcbs.cas.profile.permission.service;

import java.util.List;

import vn.tcbs.cas.profile.permission.InvSystem;

public interface SystemService {
	public List<InvSystem> listAll();
}