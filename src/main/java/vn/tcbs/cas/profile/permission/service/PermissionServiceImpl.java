package vn.tcbs.cas.profile.permission.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opensaml.saml2.metadata.Company;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.cas.http.context.HttpResponse;
import vn.tcbs.cas.profile.permission.InvGroup;
import vn.tcbs.tool.base.BaseESBService;
import vn.tcbs.tool.base.EsbDTO;

@Service("permissionService")
public class PermissionServiceImpl extends BaseESBService implements PermissionService {

	private static Logger logger = LoggerFactory.getLogger(PermissionServiceImpl.class);

	public List<InvGroup> getGroupTree() {
		String url = esbUrl + "authorization/group/tree";
		Map<String, String> headers = new HashMap<String, String>(1);
		headers.put("accept", "application/json");
		try {
			HttpResponse response = httpContext.getHttpUtil().post(url, headers, new HashMap<String, String>(1));
			if (response != null) {
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					ObjectMapper om = new ObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					EsbDTO<InvGroup> result = om.readValue(responseString, new TypeReference<EsbDTO<InvGroup>>() {
					});
					return result.getData("Entry");
				}
			}
		} catch (Exception e) {
			logger.error("ERROR={}", e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

}