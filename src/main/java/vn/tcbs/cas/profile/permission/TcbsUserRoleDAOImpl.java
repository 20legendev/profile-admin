package vn.tcbs.cas.profile.permission;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import vn.tcbs.tool.base.BaseDAO;

@Repository("userRoleDAO")
@Transactional
public class TcbsUserRoleDAOImpl extends BaseDAO implements TcbsUserRoleDAO {

	private static Logger logger = LoggerFactory.getLogger(TcbsUserRoleDAOImpl.class);
	
	@Override
	public List<TcbsUserRole> getByUserId(Long userId) {
		logger.info("GETTING_ROLES {}", userId);
		Criteria cr = getSession().createCriteria(TcbsUserRole.class);
		cr.add(Restrictions.eq("userId", userId));
		return cr.list();
	}
}
