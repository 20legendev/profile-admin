package vn.tcbs.cas.profile.permission;

import java.io.Serializable;

public class InvSystem implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer systemid;
	private Integer systemstatus;
	private Integer systemname;

	public Integer getSystemid() {
		return systemid;
	}

	public void setSystemid(Integer systemid) {
		this.systemid = systemid;
	}

	public Integer getSystemstatus() {
		return systemstatus;
	}

	public void setSystemstatus(Integer systemstatus) {
		this.systemstatus = systemstatus;
	}

	public Integer getSystemname() {
		return systemname;
	}

	public void setSystemname(Integer systemname) {
		this.systemname = systemname;
	}

}