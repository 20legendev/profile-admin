package vn.tcbs.cas.profile.permission.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.cas.http.context.HttpResponse;
import vn.tcbs.cas.profile.permission.InvGroup;
import vn.tcbs.cas.profile.permission.InvGroupMemberCount;
import vn.tcbs.tool.base.BaseESBService;
import vn.tcbs.tool.base.EsbDTO;
import vn.tcbs.tool.base.UpdateRowCount;

@Service("groupService")
public class GroupServiceImpl extends BaseESBService implements GroupService {

	private static Logger logger = LoggerFactory.getLogger(GroupServiceImpl.class);

	@Override
	public Integer count(int groupid) {
		String url = esbUrl + "authorization/group/count-member";
		logger.info("url={}", url);
		Map<String, String> headers = new HashMap<String, String>(1);
		headers.put("accept", "application/json");
		Map<String, String> params = new HashMap<String, String>(1);
		params.put("groupid", String.valueOf(groupid));
		try {
			HttpResponse response = httpContext.getHttpUtil().post(url, headers, params);
			if (response != null) {
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					logger.info("group={} member={}", groupid, responseString);
					ObjectMapper om = new ObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					EsbDTO<InvGroupMemberCount> result = om.readValue(responseString,
							new TypeReference<EsbDTO<InvGroupMemberCount>>() {
							});
					return result.getData("Entry").get(0).getTotal();
				}
			}
		} catch (Exception e) {
			logger.error("ERROR={}", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<InvGroup> search(String query) {
		String url = esbUrl + "authorization/group/search";
		logger.info("url={}", url);
		Map<String, String> headers = new HashMap<String, String>(1);
		headers.put("accept", "application/json");
		Map<String, String> params = new HashMap<String, String>(1);
		params.put("groupname", query);
		try {
			HttpResponse response = httpContext.getHttpUtil().post(url, headers, params);
			if (response != null) {
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					ObjectMapper om = new ObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					EsbDTO<InvGroup> result = om.readValue(responseString, new TypeReference<EsbDTO<InvGroup>>() {
					});
					return result.getData("Entry");
				}
			}
		} catch (Exception e) {
			logger.error("ERROR={}", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public boolean updateGroup(InvGroup group) {
		String url = esbUrl + "authorization/group/update";
		Map<String, String> headers = new HashMap<String, String>(1);
		headers.put("accept", "application/json");
		Map<String, String> params = new HashMap<String, String>(1);
		if (group.getGroupid() > 0) {
			params.put("groupid", String.valueOf(group.getGroupid()));
		}
		params.put("groupdesc", String.valueOf(group.getGroupdesc()));
		params.put("groupname", String.valueOf(group.getGroupname()));
		if (group.getGroupstatus() != null) {
			params.put("groupstatus", String.valueOf(group.getGroupstatus()));
		}
		params.put("grouptype", String.valueOf(group.getGrouptype()));
		if (group.getOwnerid() != null) {
			params.put("ownerid", String.valueOf(group.getOwnerid()));
		}
		if (group.getParentid() != null) {
			params.put("parentid", String.valueOf(group.getParentid()));
		}
		try {
			HttpResponse response = httpContext.getHttpUtil().post(url, headers, params);
			if (response != null) {
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					logger.info("String={}", responseString);
					ObjectMapper om = new ObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					EsbDTO<InvGroupMemberCount> result = om.readValue(responseString,
							new TypeReference<EsbDTO<InvGroupMemberCount>>() {
							});
					return true;
				}
			}
		} catch (Exception e) {
			logger.error("ERROR={}", e.getMessage());
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean removeGroup(Integer groupid) {
		String url = esbUrl + "authorization/group/update-status";
		Map<String, String> headers = new HashMap<String, String>(1);
		headers.put("accept", "application/json");
		Map<String, String> params = new HashMap<String, String>(1);
		params.put("groupid", String.valueOf(groupid));
		params.put("status", "0");
		try {
			HttpResponse response = httpContext.getHttpUtil().post(url, headers, params);
			if (response != null) {
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					ObjectMapper om = new ObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					UpdateRowCount result = om.readValue(responseString, UpdateRowCount.class);
					if (result.getRowCount() > 0) {
						return true;
					}
					return false;
				}
			}
		} catch (Exception e) {
			logger.error("ERROR={}", e.getMessage());
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<InvGroup> getById(Integer groupid) {
		String url = esbUrl + "authorization/group/detail";
		Map<String, String> headers = new HashMap<String, String>(1);
		headers.put("accept", "application/json");
		Map<String, String> params = new HashMap<String, String>(1);
		params.put("groupid", String.valueOf(groupid));
		try {
			HttpResponse response = httpContext.getHttpUtil().post(url, headers, params);
			if (response != null) {
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					ObjectMapper om = new ObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					EsbDTO<InvGroup> result = om.readValue(responseString, new TypeReference<EsbDTO<InvGroup>>() {
					});
					return result.getData("Entry");
				}
			}
		} catch (Exception e) {
			logger.error("ERROR={}", e.getMessage());
			e.printStackTrace();
		}
		return new ArrayList<InvGroup>();
	}

}