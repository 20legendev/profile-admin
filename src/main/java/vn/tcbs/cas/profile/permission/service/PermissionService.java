package vn.tcbs.cas.profile.permission.service;

import java.util.List;

import vn.tcbs.cas.profile.permission.InvGroup;

public interface PermissionService {
	public List<InvGroup> getGroupTree();
}