package vn.tcbs.cas.profile.permission;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TCBS_USER_ROLE database table.
 * 
 */
@Entity
@Table(name="TCBS_USER_ROLE")
@NamedQuery(name="TcbsUserRole.findAll", query="SELECT t FROM TcbsUserRole t")
public class TcbsUserRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TCBS_USER_ROLE_ID_GENERATOR", sequenceName = "TCBS_USER_ROLE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_USER_ROLE_ID_GENERATOR")
	private Long id;

	@Column(name="\"ROLE\"")
	private int role;

	@Column(name="USER_ID")
	private Long userId;

	public TcbsUserRole() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getRole() {
		return this.role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}