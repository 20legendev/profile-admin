package vn.tcbs.cas.profile.dto;

import java.io.Serializable;

import javax.persistence.Column;

public class UserBankAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "BANK_ACCOUNT_NAME")
	private String bankAccountName;

	@Column(name = "BANK_ACCOUNT_NO")
	private String bankAccountNo;

	@Column(name = "BANK_CODE")
	private String bankCode;

	@Column(name = "BANK_NAME")
	private String bankName;

	@Column(name = "BANK_BRANCH")
	private String bankBranch;

	private String bankProvince;

	private Long id;

	private Long userId;

	private String bankSys;

	public UserBankAccount() {
	}

	public String getBankSys() {
		return bankSys;
	}

	public void setBankSys(String bankSys) {
		this.bankSys = bankSys;
	}

	public String getBankProvince() {
		return bankProvince;
	}

	public void setBankProvince(String bankProvince) {
		this.bankProvince = bankProvince;
	}

	public String getBankAccountName() {
		return this.bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getBankAccountNo() {
		return this.bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBankCode() {
		return this.bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

}