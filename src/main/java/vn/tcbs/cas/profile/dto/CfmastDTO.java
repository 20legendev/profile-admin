package vn.tcbs.cas.profile.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class CfmastDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String custid;

	private Date activedate;

	private String activests;

	private String address;

	private String afstatus;

	private String approveid;

	private String assetrange;

	private Date auditdate;

	private String auditorid;

	private String bankacctno;

	private String bankcode;

	private String brid;

	private String businesstype;

	private String careby;

	private Date cfclsdate;

	private String class_;

	private Long commrate;

	private String companyid;

	private String consultant;

	private String country;

	private String custatcom;

	private String custodycd;

	private String custtype;

	private Date dateofbirth;

	private String description;

	private String dmsts;

	private String education;

	private String email;

	private String experiencecd;

	private String experiencetype;

	private String fax;

	private String focustype;

	private String fullname;

	private String grinvestor;

	private String idcode;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private Date iddate;

	private Date idexpired;

	private String idplace;

	private String idtype;

	private String incomerange;

	private String internation;

	private String investmentexperience;

	private String investrange;

	private String investtype;

	private String isbanking;

	private String ischkonlimit;

	private String issuerid;

	private String language;

	private Timestamp lastChange;

	private String lastMkid;

	private String lastOfid;

	private Date lastdate;

	private String managetype;

	private String marginallow;

	private String married;

	private String mnemonic;

	private String mobile;

	private String mobilesms;

	private BigDecimal mrloanlimit;

	private String occupation;

	private BigDecimal olautoid;

	private BigDecimal onlinelimit;

	private String openvia;

	private Date opndate;

	private String orginf;

	private String pcustodycd;

	private String phone;

	private String pin;

	private String position;

	private String postcode;

	private String province;

	private String pstatus;

	private String refname;

	private String resident;

	private String risklevel;

	private String sector;

	private String sex;

	private String shortname;

	private String staff;

	private String status;

	private BigDecimal t0loanlimit;

	private String taxcode;

	private String timetojoin;

	private String tlid;

	private String tradefloor;

	private String tradeonline;

	private String tradetelephone;

	private String tradingcode;

	private Date tradingcodedt;

	private String username;

	private String valudadded;

	private String vat;

	public CfmastDTO() {
	}

	public String getCustid() {
		return this.custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public Date getActivedate() {
		return this.activedate;
	}

	public void setActivedate(Date activedate) {
		this.activedate = activedate;
	}

	public String getActivests() {
		return this.activests;
	}

	public void setActivests(String activests) {
		this.activests = activests;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAfstatus() {
		return this.afstatus;
	}

	public void setAfstatus(String afstatus) {
		this.afstatus = afstatus;
	}

	public String getApproveid() {
		return this.approveid;
	}

	public void setApproveid(String approveid) {
		this.approveid = approveid;
	}

	public String getAssetrange() {
		return this.assetrange;
	}

	public void setAssetrange(String assetrange) {
		this.assetrange = assetrange;
	}

	public Date getAuditdate() {
		return this.auditdate;
	}

	public void setAuditdate(Date auditdate) {
		this.auditdate = auditdate;
	}

	public String getAuditorid() {
		return this.auditorid;
	}

	public void setAuditorid(String auditorid) {
		this.auditorid = auditorid;
	}

	public String getBankacctno() {
		return this.bankacctno;
	}

	public void setBankacctno(String bankacctno) {
		this.bankacctno = bankacctno;
	}

	public String getBankcode() {
		return this.bankcode;
	}

	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

	public String getBrid() {
		return this.brid;
	}

	public void setBrid(String brid) {
		this.brid = brid;
	}

	public String getBusinesstype() {
		return this.businesstype;
	}

	public void setBusinesstype(String businesstype) {
		this.businesstype = businesstype;
	}

	public String getCareby() {
		return this.careby;
	}

	public void setCareby(String careby) {
		this.careby = careby;
	}

	public Date getCfclsdate() {
		return this.cfclsdate;
	}

	public void setCfclsdate(Date cfclsdate) {
		this.cfclsdate = cfclsdate;
	}

	public String getClass_() {
		return this.class_;
	}

	public void setClass_(String class_) {
		this.class_ = class_;
	}

	public Long getCommrate() {
		return this.commrate;
	}

	public void setCommrate(Long commrate) {
		this.commrate = commrate;
	}

	public String getCompanyid() {
		return this.companyid;
	}

	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}

	public String getConsultant() {
		return this.consultant;
	}

	public void setConsultant(String consultant) {
		this.consultant = consultant;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCustatcom() {
		return this.custatcom;
	}

	public void setCustatcom(String custatcom) {
		this.custatcom = custatcom;
	}

	public String getCustodycd() {
		return this.custodycd;
	}

	public void setCustodycd(String custodycd) {
		this.custodycd = custodycd;
	}

	public String getCusttype() {
		return this.custtype;
	}

	public void setCusttype(String custtype) {
		this.custtype = custtype;
	}

	public Date getDateofbirth() {
		return this.dateofbirth;
	}

	public void setDateofbirth(Date dateofbirth) {
		this.dateofbirth = dateofbirth;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDmsts() {
		return this.dmsts;
	}

	public void setDmsts(String dmsts) {
		this.dmsts = dmsts;
	}

	public String getEducation() {
		return this.education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getExperiencecd() {
		return this.experiencecd;
	}

	public void setExperiencecd(String experiencecd) {
		this.experiencecd = experiencecd;
	}

	public String getExperiencetype() {
		return this.experiencetype;
	}

	public void setExperiencetype(String experiencetype) {
		this.experiencetype = experiencetype;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getFocustype() {
		return this.focustype;
	}

	public void setFocustype(String focustype) {
		this.focustype = focustype;
	}

	public String getFullname() {
		return this.fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getGrinvestor() {
		return this.grinvestor;
	}

	public void setGrinvestor(String grinvestor) {
		this.grinvestor = grinvestor;
	}

	public String getIdcode() {
		return this.idcode;
	}

	public void setIdcode(String idcode) {
		this.idcode = idcode;
	}

	public Date getIddate() {
		return this.iddate;
	}

	public void setIddate(Date iddate) {
		this.iddate = iddate;
	}

	public Date getIdexpired() {
		return this.idexpired;
	}

	public void setIdexpired(Date idexpired) {
		this.idexpired = idexpired;
	}

	public String getIdplace() {
		return this.idplace;
	}

	public void setIdplace(String idplace) {
		this.idplace = idplace;
	}

	public String getIdtype() {
		return this.idtype;
	}

	public void setIdtype(String idtype) {
		this.idtype = idtype;
	}

	public String getIncomerange() {
		return this.incomerange;
	}

	public void setIncomerange(String incomerange) {
		this.incomerange = incomerange;
	}

	public String getInternation() {
		return this.internation;
	}

	public void setInternation(String internation) {
		this.internation = internation;
	}

	public String getInvestmentexperience() {
		return this.investmentexperience;
	}

	public void setInvestmentexperience(String investmentexperience) {
		this.investmentexperience = investmentexperience;
	}

	public String getInvestrange() {
		return this.investrange;
	}

	public void setInvestrange(String investrange) {
		this.investrange = investrange;
	}

	public String getInvesttype() {
		return this.investtype;
	}

	public void setInvesttype(String investtype) {
		this.investtype = investtype;
	}

	public String getIsbanking() {
		return this.isbanking;
	}

	public void setIsbanking(String isbanking) {
		this.isbanking = isbanking;
	}

	public String getIschkonlimit() {
		return this.ischkonlimit;
	}

	public void setIschkonlimit(String ischkonlimit) {
		this.ischkonlimit = ischkonlimit;
	}

	public String getIssuerid() {
		return this.issuerid;
	}

	public void setIssuerid(String issuerid) {
		this.issuerid = issuerid;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Timestamp getLastChange() {
		return this.lastChange;
	}

	public void setLastChange(Timestamp lastChange) {
		this.lastChange = lastChange;
	}

	public String getLastMkid() {
		return this.lastMkid;
	}

	public void setLastMkid(String lastMkid) {
		this.lastMkid = lastMkid;
	}

	public String getLastOfid() {
		return this.lastOfid;
	}

	public void setLastOfid(String lastOfid) {
		this.lastOfid = lastOfid;
	}

	public Date getLastdate() {
		return this.lastdate;
	}

	public void setLastdate(Date lastdate) {
		this.lastdate = lastdate;
	}

	public String getManagetype() {
		return this.managetype;
	}

	public void setManagetype(String managetype) {
		this.managetype = managetype;
	}

	public String getMarginallow() {
		return this.marginallow;
	}

	public void setMarginallow(String marginallow) {
		this.marginallow = marginallow;
	}

	public String getMarried() {
		return this.married;
	}

	public void setMarried(String married) {
		this.married = married;
	}

	public String getMnemonic() {
		return this.mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMobilesms() {
		return this.mobilesms;
	}

	public void setMobilesms(String mobilesms) {
		this.mobilesms = mobilesms;
	}

	public BigDecimal getMrloanlimit() {
		return this.mrloanlimit;
	}

	public void setMrloanlimit(BigDecimal mrloanlimit) {
		this.mrloanlimit = mrloanlimit;
	}

	public String getOccupation() {
		return this.occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public BigDecimal getOlautoid() {
		return this.olautoid;
	}

	public void setOlautoid(BigDecimal olautoid) {
		this.olautoid = olautoid;
	}

	public BigDecimal getOnlinelimit() {
		return this.onlinelimit;
	}

	public void setOnlinelimit(BigDecimal onlinelimit) {
		this.onlinelimit = onlinelimit;
	}

	public String getOpenvia() {
		return this.openvia;
	}

	public void setOpenvia(String openvia) {
		this.openvia = openvia;
	}

	public Date getOpndate() {
		return this.opndate;
	}

	public void setOpndate(Date opndate) {
		this.opndate = opndate;
	}

	public String getOrginf() {
		return this.orginf;
	}

	public void setOrginf(String orginf) {
		this.orginf = orginf;
	}

	public String getPcustodycd() {
		return this.pcustodycd;
	}

	public void setPcustodycd(String pcustodycd) {
		this.pcustodycd = pcustodycd;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPin() {
		return this.pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPostcode() {
		return this.postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getPstatus() {
		return this.pstatus;
	}

	public void setPstatus(String pstatus) {
		this.pstatus = pstatus;
	}

	public String getRefname() {
		return this.refname;
	}

	public void setRefname(String refname) {
		this.refname = refname;
	}

	public String getResident() {
		return this.resident;
	}

	public void setResident(String resident) {
		this.resident = resident;
	}

	public String getRisklevel() {
		return this.risklevel;
	}

	public void setRisklevel(String risklevel) {
		this.risklevel = risklevel;
	}

	public String getSector() {
		return this.sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getShortname() {
		return this.shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	public String getStaff() {
		return this.staff;
	}

	public void setStaff(String staff) {
		this.staff = staff;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getT0loanlimit() {
		return this.t0loanlimit;
	}

	public void setT0loanlimit(BigDecimal t0loanlimit) {
		this.t0loanlimit = t0loanlimit;
	}

	public String getTaxcode() {
		return this.taxcode;
	}

	public void setTaxcode(String taxcode) {
		this.taxcode = taxcode;
	}

	public String getTimetojoin() {
		return this.timetojoin;
	}

	public void setTimetojoin(String timetojoin) {
		this.timetojoin = timetojoin;
	}

	public String getTlid() {
		return this.tlid;
	}

	public void setTlid(String tlid) {
		this.tlid = tlid;
	}

	public String getTradefloor() {
		return this.tradefloor;
	}

	public void setTradefloor(String tradefloor) {
		this.tradefloor = tradefloor;
	}

	public String getTradeonline() {
		return this.tradeonline;
	}

	public void setTradeonline(String tradeonline) {
		this.tradeonline = tradeonline;
	}

	public String getTradetelephone() {
		return this.tradetelephone;
	}

	public void setTradetelephone(String tradetelephone) {
		this.tradetelephone = tradetelephone;
	}

	public String getTradingcode() {
		return this.tradingcode;
	}

	public void setTradingcode(String tradingcode) {
		this.tradingcode = tradingcode;
	}

	public Date getTradingcodedt() {
		return this.tradingcodedt;
	}

	public void setTradingcodedt(Date tradingcodedt) {
		this.tradingcodedt = tradingcodedt;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getValudadded() {
		return this.valudadded;
	}

	public void setValudadded(String valudadded) {
		this.valudadded = valudadded;
	}

	public String getVat() {
		return this.vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

}