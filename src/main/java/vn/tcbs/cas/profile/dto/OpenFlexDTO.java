package vn.tcbs.cas.profile.dto;

import java.io.Serializable;

public class OpenFlexDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	// private Long userid;
	private String referenceid;
	// private String idnumber;
	/*
	public String getIdnumber() {
		return idnumber;
	}

	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}
	*/
	public String getReferenceid() {
		return referenceid;
	}

	public void setReferenceid(String referenceid) {
		this.referenceid = referenceid;
	}

}