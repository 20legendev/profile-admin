package vn.tcbs.cas.profile.dto;

import java.io.Serializable;

public class ConfirmFlexDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String referenceid;

	public String getReferenceid() {
		return referenceid;
	}

	public void setReferenceid(String referenceid) {
		this.referenceid = referenceid;
	}

}