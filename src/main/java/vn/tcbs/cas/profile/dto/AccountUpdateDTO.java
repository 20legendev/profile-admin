package vn.tcbs.cas.profile.dto;

import java.io.Serializable;

public class AccountUpdateDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String fullname;
	private String birthday;
	private Integer gender;
	private String email;
	private String phone;
	private String idnumber;
	private String idplace;
	private String iddate;
	private String address;

	public String getIdnumber() {
		return idnumber;
	}

	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}

	public String getIdplace() {
		return idplace;
	}

	public void setIdplace(String idplace) {
		this.idplace = idplace;
	}

	public String getIddate() {
		return iddate;
	}

	public void setIddate(String iddate) {
		this.iddate = iddate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

}