package vn.tcbs.cas.profile.dto;

import java.io.Serializable;

public class UserContact implements Serializable {
	private static final long serialVersionUID = 1L;
	private String email;
	private String phone;
	private String firstname;
	private String lastname;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

}