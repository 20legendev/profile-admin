package vn.tcbs.cas.profile.dto;

import java.io.Serializable;

public class SyncPasswordDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String idnumber;
	private String password;

	public String getIdnumber() {
		return idnumber;
	}

	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}