package vn.tcbs.cas.profile.dao;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsUserUpload;

public interface UserUploadDAO {
	public void addNew(TcbsUserUpload userUpload);
	public List<TcbsUserUpload> getByAlias(String alias);
}
