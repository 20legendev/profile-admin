package vn.tcbs.cas.profile.dao;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUser;

public interface IdentificationDAO {
	public List<TcbsIdentification> getByIdNumber(String idNumber);
	/*
	public Long getUserId(String identificationNumber);
	 */
	public TcbsUser getUserInfo(String idNumber);

	public TcbsIdentification getUser(String idNumber);
	public List<TcbsIdentification> isDuplicated(String idNumber, Long userId);
}
