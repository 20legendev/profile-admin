package vn.tcbs.cas.profile.dao;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsApplication;

public interface ApplicationDAO {
	public List<TcbsApplication> getByName(String appName);
	public TcbsApplication getAppByName(String appName);
}
