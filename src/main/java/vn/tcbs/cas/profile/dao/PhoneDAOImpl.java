package vn.tcbs.cas.profile.dao;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import vn.tcbs.cas.profile.entity.TcbsUserPhone;
import vn.tcbs.tool.base.BaseDAO;

@Repository("phoneDAO")
@Transactional
public class PhoneDAOImpl extends BaseDAO implements PhoneDAO {

	public void deleteByUser(Long userId) {

	}

	public TcbsUserPhone getById(Long id) {
		return (TcbsUserPhone) getSession().load(TcbsUserPhone.class, id);
	}

}