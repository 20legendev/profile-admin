package vn.tcbs.cas.profile.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.tool.base.BaseDAO;

@Repository("identificationDAO")
@Transactional
public class IdentificationDAOImpl extends BaseDAO implements IdentificationDAO {

	private static Logger logger = Logger.getLogger(IdentificationDAOImpl.class);

	public List<TcbsIdentification> getByIdNumber(String idNumber) {
		Criteria cr = getSession().createCriteria(TcbsIdentification.class);
		cr.add(Restrictions.eq("idNumber", idNumber));
		return cr.list();
	}

	/*
	 * public Long getUserId(String idNumber) { List<TcbsIdentification> list =
	 * this.getByIdNumber(idNumber); if (list.size() > 0) { TcbsUser user =
	 * list.get(0).getUser(); if (user != null) { try { logger.info(new
	 * ObjectMapper().writeValueAsString(user)); } catch
	 * (JsonProcessingException e) { e.printStackTrace(); } return user.getId();
	 * } return null; } return null; }
	 */

	public TcbsIdentification getUser(String idNumber) {
		List<TcbsIdentification> list = this.getByIdNumber(idNumber);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	public TcbsUser getUserInfo(String idNumber) {
		List<TcbsIdentification> list = this.getByIdNumber(idNumber);
		if (list.size() > 0) {
			return list.get(0).getUser();
		}
		return null;
	}

	@Override
	public List<TcbsIdentification> isDuplicated(String idNumber, Long userId) {
		Criteria cr = getSession().createCriteria(TcbsIdentification.class);
		cr.add(Restrictions.eq("idNumber", idNumber));
		if (userId != null) {
			cr.createAlias("user", "user");
			cr.add(Restrictions.ne("user.id", userId));
		}
		return cr.list();
	}

}
