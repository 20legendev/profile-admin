package vn.tcbs.cas.profile.dao;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsBankAccount;

public interface BankDAO {
	public List<TcbsBankAccount> getBankByUser(Long userId);
	public TcbsBankAccount getById(Long id);
	public void remove(Long id);
}
