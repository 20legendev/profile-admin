package vn.tcbs.cas.profile.dao;

import java.util.List;

import vn.tcbs.cas.profile.admin.dto.AdminSearchDTO;
import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUser;

public interface UserDAO {
	public List<TcbsUser> findByEmail(String email, Integer status);

	public Boolean checkLoginEmail(String email);

	public void updatePassword(Long userId, String password);

	public TcbsUser getById(Long id);
	public TcbsUser getByUserCode(String usercode);

	public List<TcbsUser> getByLoginPhone(String phone);

	public List<TcbsUser> getByUsername(String username);

	public List<TcbsUser> getByMainEmail(String email, Integer status);

	public void update(TcbsUser user);

	public void add(TcbsUser user);

	public List<TcbsUser> listAll();

	public List<TcbsUser> getByIdNumber(String idNumber, Integer status);

	public List<TcbsIdentification> find(String idNumber, Integer status);

	public List<TcbsUser> findByLoginKey(String key);

	public List<TcbsUser> find(Integer page, Integer perpage, String email, String idNumber, String fullname,
			String phone);
	public List<TcbsUser> find(AdminSearchDTO searchDTO);

	public List<TcbsUser> findByPhone(String phone, Integer status);

	public boolean isEmailDuplicated(String email, Long userId);

	public boolean isPhoneDuplicated(String phone, Long userId);

	public boolean isUsernameDuplicated(String username, Long userId);

	public void remove(TcbsUser user);

}
