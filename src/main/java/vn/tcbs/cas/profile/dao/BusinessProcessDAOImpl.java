package vn.tcbs.cas.profile.dao;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import vn.tcbs.tool.base.BaseDAO;

@Repository("businessProcessDAO")
@Transactional
public class BusinessProcessDAOImpl extends BaseDAO implements
		BusinessProcessDAO {

}
