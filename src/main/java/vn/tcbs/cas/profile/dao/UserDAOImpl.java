package vn.tcbs.cas.profile.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import vn.tcbs.cas.profile.admin.dto.AdminSearchDTO;
import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.tool.base.BaseDAO;

@Repository("userDAO")
@Transactional
public class UserDAOImpl extends BaseDAO implements UserDAO {

	private static Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

	public List<TcbsUser> findByEmail(String email, Integer status) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		cr.add(Restrictions.eq("email", email.toLowerCase()));
		if (status != null) {
			cr.add(Restrictions.eq("status", status));
		}
		return cr.list();
	}

	public void handUpdate(String email, Integer status, Long id, String password) {
		Query q = getSession().createSQLQuery(
				"UPDATE tcbs_user SET email='" + email + "', status = 1, password='" + password + "' WHERE id=" + id);
		q.executeUpdate();
	}

	public TcbsUser getById(Long userId) {
		TcbsUser user = (TcbsUser) getSession().get(TcbsUser.class, userId);
		return user;
	}

	public void updatePassword(Long userId, String password) {
		TcbsUser user = getById(userId);
		if (user != null) {
			user.setPassword(password);
			getSession().update(user);
		}
	}

	public void update(TcbsUser user) {
		getSession().update(user);
	}

	public void add(TcbsUser user) {
		getSession().save(user);
	}

	public List<TcbsUser> listAll() {
		return getSession().createCriteria(TcbsUser.class).list();
	}

	public List<TcbsUser> getByUsername(String username) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		cr.add(Restrictions.eq("username", username.toLowerCase()));
		return cr.list();
	}

	public List<TcbsUser> getByMainEmail(String email, Integer status) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		cr.add(Restrictions.eq("email", email));
		if (status != null) {
			cr.add(Restrictions.eq("status", status));
		}
		return cr.list();
	}

	public Boolean checkLoginEmail(String email) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		cr.add(Restrictions.eq("email", email.toLowerCase()));
		List<TcbsUser> result = cr.list();
		if (result.size() > 0) {
			return true;
		}
		return false;

	}

	public List<TcbsUser> getByIdNumber(String idNumber, Integer status) {
		DetachedCriteria subQuery = DetachedCriteria.forClass(TcbsIdentification.class, "i")
				.add(Restrictions.eq("idNumber", idNumber).ignoreCase()).createAlias("user", "user")
				.setProjection(Projections.property("user.id"));
		Criteria cr = getSession().createCriteria(TcbsUser.class, "u").add(Subqueries.propertyIn("u.id", subQuery));
		if (status != null) {
			cr.add(Restrictions.eq("status", status));
		}
		return cr.list();
	}

	public List<TcbsIdentification> find(String idNumber, Integer status) {
		DetachedCriteria subQuery = DetachedCriteria.forClass(TcbsUser.class).setProjection(Projections.property("id"));
		if (status != null) {
			subQuery.add(Restrictions.eq("status", status));
		}
		Criteria cr = getSession().createCriteria(TcbsIdentification.class);
		cr.add(Restrictions.eq("idNumber", idNumber));
		cr.createAlias("user", "tcbsUser");
		cr.add(Subqueries.propertyIn("tcbsUser.id", subQuery));
		return cr.list();
	}

	public List<TcbsUser> findByPhone(String phone, Integer status) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		cr.add(Restrictions.eq("phone", phone));
		if (status != null) {
			cr.add(Restrictions.eq("status", status));
		}
		return cr.list();
	}

	@Override
	public List<TcbsUser> findByLoginKey(String key) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		Disjunction or = Restrictions.disjunction();
		or.add(Restrictions.eq("email", key));
		or.add(Restrictions.eq("username", key));
		or.add(Restrictions.eq("phone", key));
		cr.add(or);
		return cr.list();
	}

	@Override
	public List<TcbsUser> getByLoginPhone(String phone) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		cr.add(Restrictions.eq("phone", phone));
		return cr.list();
	}

	@Override
	public List<TcbsUser> find(Integer page, Integer perpage, String email, String idNumber, String fullname,
			String phone) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		if (email != null && !email.equals("")) {
			cr.add(Restrictions.ilike("email", email, MatchMode.ANYWHERE));
		}
		if (fullname != null && !fullname.equals("")) {
			cr.add(Restrictions.disjunction()
					.add(Restrictions.ilike("lastname", fullname.toLowerCase(), MatchMode.ANYWHERE))
					.add(Restrictions.ilike("firstname", fullname.toLowerCase(), MatchMode.ANYWHERE)));

		}
		if (phone != null && !phone.equals("")) {
			cr.add(Restrictions.like("phone", phone.toLowerCase(), MatchMode.ANYWHERE));
		}
		cr.addOrder(Order.asc("lastname"));
		cr.setFirstResult(page * perpage);
		cr.setMaxResults(perpage);
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<TcbsUser> find(AdminSearchDTO searchDTO) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		if (searchDTO.getEmail() != null && !searchDTO.getEmail().equals("")) {
			cr.add(Restrictions.ilike("email", searchDTO.getEmail(), MatchMode.ANYWHERE));
		}
		if (searchDTO.getFullname() != null && !searchDTO.getFullname().equals("")) {
			cr.add(Restrictions.disjunction()
					.add(Restrictions.ilike("lastname", searchDTO.getFullname().toLowerCase(), MatchMode.ANYWHERE))
					.add(Restrictions.ilike("firstname", searchDTO.getFullname().toLowerCase(), MatchMode.ANYWHERE)));
		}
		if (searchDTO.getPhone() != null && !searchDTO.getPhone().equals("")) {
			cr.add(Restrictions.ilike("phone", searchDTO.getPhone().toLowerCase(), MatchMode.ANYWHERE));
		}
		if (searchDTO.getUsername() != null && !searchDTO.getUsername().equals("")) {
			cr.add(Restrictions.ilike("username", searchDTO.getUsername(), MatchMode.ANYWHERE));
		}
		if (searchDTO.getIdnumber() != null && !searchDTO.getIdnumber().equals("")) {
			String idNumber = searchDTO.getIdnumber();
			DetachedCriteria subQuery = DetachedCriteria.forClass(TcbsIdentification.class, "i")
					.add(Restrictions.ilike("idNumber", idNumber, MatchMode.ANYWHERE));
			subQuery.createAlias("user", "user");
			subQuery.setProjection(Projections.property("user.id"));
			cr.add(Subqueries.propertyIn("id", subQuery));
		}
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			if (searchDTO.getFromdate() != null && !searchDTO.getFromdate().equals("")) {
				cr.add(Restrictions.ge("createdDate", sf.parse(searchDTO.getFromdate())));
			}
			if (searchDTO.getTodate() != null && !searchDTO.getTodate().equals("")) {
				cr.add(Restrictions.le("createdDate", sf.parse(searchDTO.getTodate())));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (searchDTO.getVsdStatus() != null && !searchDTO.getVsdStatus().equals(-1)) {
			cr.add(Restrictions.eq("vsdStatus", searchDTO.getVsdStatus()));
		}
		cr.addOrder(Order.desc("createdDate"));
		Integer page = searchDTO.getPage();
		if (page == null || !page.equals(-1)) {
			cr.setMaxResults(20).setFirstResult(20 * searchDTO.getPage());
		}
		return cr.list();
	}

	public boolean isEmailDuplicated(String email, Long userId) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		cr.add(Restrictions.eq("email", email));
		cr.add(Restrictions.ne("id", userId));
		List<TcbsUser> lu = cr.list();
		if (lu.size() > 0) {
			return true;
		}
		return false;
	}

	public boolean isPhoneDuplicated(String phone, Long userId) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		cr.add(Restrictions.eq("phone", phone));
		cr.add(Restrictions.ne("id", userId));
		List<TcbsUser> lu = cr.list();
		if (lu.size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isUsernameDuplicated(String username, Long userId) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		cr.add(Restrictions.eq("username", username));
		cr.add(Restrictions.ne("id", userId));
		List<TcbsUser> lu = cr.list();
		if (lu.size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public void remove(TcbsUser user) {
		getSession().delete(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public TcbsUser getByUserCode(String tcbsid) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		cr.add(Restrictions.eq("tcbsid", tcbsid));
		List<TcbsUser> lu = cr.list();
		if (lu.size() > 0) {
			return lu.get(0);
		}
		return null;
	}
}
