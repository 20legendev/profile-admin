package vn.tcbs.cas.profile.dao;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsUserForgotPassword;

public interface ForgotPasswordDAO{
	public void addNew(TcbsUserForgotPassword fp);
	public void addNew(Long userId, String token);
	public List<TcbsUserForgotPassword> findByEmail(String email);
	void update(TcbsUserForgotPassword fp);
	
	public List<TcbsUserForgotPassword> checkToken(String token);
}
