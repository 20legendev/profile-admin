package vn.tcbs.cas.profile.dao;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import vn.tcbs.cas.profile.entity.TcbsUpdateLog;
import vn.tcbs.tool.base.BaseDAO;

@Repository("updateLogDAO")
@Transactional
public class UpdateLogDAOImpl extends BaseDAO implements UpdateLogDAO {

	public void addNew(TcbsUpdateLog log) {
		getSession().save(log);
	}
}
