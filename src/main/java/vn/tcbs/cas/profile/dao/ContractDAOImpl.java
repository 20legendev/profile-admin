package vn.tcbs.cas.profile.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import vn.tcbs.cas.profile.entity.Contract;
import vn.tcbs.tool.base.BaseDAO;
import vn.tcbs.tool.base.TcbsUtils;

@Repository("contractDAO")
@Transactional
public class ContractDAOImpl extends BaseDAO implements ContractDAO {

	@Override
	public List<Contract> getByAlias(String alias) {
		Criteria cr = getSession().createCriteria(Contract.class);
		cr.add(Restrictions.eq("documentAlias", alias));
		return cr.list();
	}

	@Override
	public Contract createNew(Contract c) {
		c.setContractNumber(TcbsUtils.generateContractId(this));
		c.setDocumentAlias(TcbsUtils.generateRandom(24));
		c.setCreatedDate(new Date());
		c.setDownloadCount(0);
		getSession().saveOrUpdate(c);
		return c;
	}

	@Override
	public Contract checkContract(String contractNumber) {
		Criteria cr = getSession().createCriteria(Contract.class);
		cr.add(Restrictions.eq("contractNumber", contractNumber));
		List<Contract> result = cr.list();
		if (result.size() > 0) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public List<Contract> getByIdNumber(String idNumber) {
		Criteria cr = getSession().createCriteria(Contract.class);
		cr.add(Restrictions.eq("idNumber", idNumber));
		return cr.list();
	}

	@Override
	public Contract update(Contract c) {
		if(c.getContractNumber() == null){
			c.setContractNumber(TcbsUtils.generateContractId(this));
		}
		if(c.getDownloadCount() == null){
			c.setDownloadCount(0);
		}
		getSession().saveOrUpdate(c);
		return c;
	}

	@Override
	public List<Contract> getByReferenceId(String referenceId) {
		Criteria cr = getSession().createCriteria(Contract.class);
		cr.add(Restrictions.eq("referenceId", referenceId));
		return cr.list();
	}
}
