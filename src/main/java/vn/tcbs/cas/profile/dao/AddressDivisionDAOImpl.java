package vn.tcbs.cas.profile.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import vn.tcbs.cas.profile.entity.TcbsAddressDivision;
import vn.tcbs.tool.base.BaseDAO;

@Repository("addressDivisionDAO")
@Transactional
public class AddressDivisionDAOImpl extends BaseDAO implements AddressDivisionDAO {

	public List<TcbsAddressDivision> getByParent(Long parentId) {
		Criteria cr = getSession().createCriteria(TcbsAddressDivision.class);
		cr.add(Restrictions.eq("parentId", parentId));
		return cr.list();
	}

	public List<TcbsAddressDivision> getByParentName(String parentName) {
		DetachedCriteria subQuery = DetachedCriteria.forClass(TcbsAddressDivision.class, "pr")
				.add(Restrictions.eq("divisionCode", parentName).ignoreCase())
				.setProjection(Projections.property("pr.id"));
		Criteria cr = getSession().createCriteria(TcbsAddressDivision.class, "ad")
				.add(Subqueries.propertyIn("ad.parentId", subQuery)).addOrder(Order.desc("ad.ranking"));
		return cr.list();
	}

	public List<TcbsAddressDivision> getByCode(String divisionCode) {
		Criteria cr = getSession().createCriteria(TcbsAddressDivision.class);
		cr.add(Restrictions.eq("divisionCode", divisionCode));
		return cr.list();
	}
}
