package vn.tcbs.cas.profile.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import vn.tcbs.cas.profile.entity.TcbsErrorLog;
import vn.tcbs.tool.base.BaseDAO;

@Repository("errorDAO")
@Transactional
public class ErrorDAOImpl extends BaseDAO implements ErrorDAO {

	@Override
	public void addNew(TcbsErrorLog log) {
		getSession().save(log);
	}

	@Override
	public void resolve(Long id) {
		String hql = "UPDATE TcbsErrorLog set status = 1 WHERE id = :id";
		Query query = getSession().createQuery(hql);
		query.setParameter("id", id);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TcbsErrorLog> get(Integer page, Integer perpage, String type, Integer status, String data) {
		Criteria cr = getSession().createCriteria(TcbsErrorLog.class);
		if (type != null) {
			cr.add(Restrictions.ilike("errorType", type, MatchMode.ANYWHERE));
		}
		if (status != null) {
			cr.add(Restrictions.eq("status", status));
		}
		if (data != null) {
			cr.add(Restrictions.ilike("data", data, MatchMode.ANYWHERE));
		}
		cr.addOrder(Order.desc("since"));
		cr.setMaxResults(perpage).setFirstResult(page * perpage);
		return cr.list();
	}

	@Override
	public int count(String type, Integer status, String data) {
		Criteria cr = getSession().createCriteria(TcbsErrorLog.class);
		if (type != null) {
			cr.add(Restrictions.ilike("errorType", type, MatchMode.ANYWHERE));
		}
		if (status != null) {
			cr.add(Restrictions.eq("status", status));
		}
		if (data != null) {
			cr.add(Restrictions.ilike("data", data, MatchMode.ANYWHERE));
		}
		return cr.setProjection(Projections.rowCount()).uniqueResult().hashCode();
	}

}
