package vn.tcbs.cas.profile.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;
import vn.tcbs.tool.base.BaseDAO;

@Repository("userQueueDAO")
@Transactional
public class UserQueueDAOImpl extends BaseDAO implements UserQueueDAO {

	@Override
	public void addNew(TcbsUserOpenaccountQueue userQueue) {
		getSession().save(userQueue);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TcbsUserOpenaccountQueue> getByReferenceId(String referenceid) {
		Criteria cr = getSession().createCriteria(TcbsUserOpenaccountQueue.class);
		cr.add(Restrictions.eq("referenceid", referenceid));
		return cr.list();
	}

	@Override
	public void update(TcbsUserOpenaccountQueue userQueue) {
		getSession().update(userQueue);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TcbsUserOpenaccountQueue> getByIdNumberAndStatus(String idNumber, Integer status) {
		Criteria cr = getSession().createCriteria(TcbsUserOpenaccountQueue.class);
		cr.add(Restrictions.eq("idnumber", idNumber));
		cr.add(Restrictions.eq("status", status));
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TcbsUserOpenaccountQueue> findUser(String idNumber) {
		Criteria cr = getSession().createCriteria(TcbsUserOpenaccountQueue.class);
		cr.add(Restrictions.eq("idnumber", idNumber));
		cr.add(Restrictions.ne("status", 0));
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TcbsUserOpenaccountQueue> find(String idNumber, Long referer) {
		Criteria cr = getSession().createCriteria(TcbsUserOpenaccountQueue.class);
		if (idNumber != null) {
			cr.add(Restrictions.eq("idnumber", idNumber));
		}
		if (referer != null) {
			cr.add(Restrictions.eq("referer", referer));
		}
		cr.addOrder(Order.desc("createdDate"));
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TcbsUserOpenaccountQueue> listQueue(Integer page, Integer perpage, String email, String phone,
			String idNumber, String fullname) {
		Criteria cr = getSession().createCriteria(TcbsUserOpenaccountQueue.class);
		if (email != null && !email.equals("")) {
			cr.add(Restrictions.ilike("email", email, MatchMode.ANYWHERE));
		}
		if (phone != null && !phone.equals("")) {
			cr.add(Restrictions.ilike("phone", phone, MatchMode.ANYWHERE));
		}
		if (idNumber != null && !idNumber.equals("")) {
			cr.add(Restrictions.ilike("idnumber", idNumber, MatchMode.ANYWHERE));
		}
		if (fullname != null && !fullname.equals("")) {
			cr.add(Restrictions.disjunction()
					.add(Restrictions.ilike("lastname", fullname.toLowerCase(), MatchMode.ANYWHERE))
					.add(Restrictions.ilike("firstname", fullname.toLowerCase(), MatchMode.ANYWHERE)));

		}
		cr.addOrder(Order.desc("createdDate"));
		cr.setMaxResults(perpage);
		cr.setFirstResult(page * perpage);
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<TcbsUserOpenaccountQueue> getByReferer(Long referer) {
		Criteria cr = getSession().createCriteria(TcbsUserOpenaccountQueue.class);
		cr.add(Restrictions.eq("referer", referer));
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	public List<TcbsUserOpenaccountQueue> getByRefererRestricted(Long referer) {
		Criteria cr = getSession().createCriteria(TcbsUserOpenaccountQueue.class);
		cr.add(Restrictions.eq("referer", referer));
		cr.addOrder(Order.desc("createdDate"));
		return cr.list();
	}

}
