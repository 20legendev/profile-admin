package vn.tcbs.cas.profile.dao;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsApplicationUser;

public interface ApplicationUserDAO {
	public List<TcbsApplicationUser> get(Long userId, Long appId);

	public List<TcbsApplicationUser> getApplicationUser(String idNumber, String appName);

	public List<TcbsApplicationUser> getApplicationUserByEmail(String email, String appName);

	public List<TcbsApplicationUser> getApplicationUserByPhone(String phone, String appName);

	public void add(TcbsApplicationUser appUser);

	public void update(TcbsApplicationUser appUser);

	public TcbsApplicationUser getByUserAndAppId(Long userId, Long appId);

	public List<TcbsApplicationUser> getByUserAndAppName(Long userId, String appName);

	public TcbsApplicationUser getByApp(Long appId, String userInAppId);
}
