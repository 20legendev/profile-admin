package vn.tcbs.cas.profile.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import vn.tcbs.cas.profile.entity.TcbsBankAccount;
import vn.tcbs.tool.base.BaseDAO;

@Repository("bankDAO")
@Transactional
public class BankDAOImpl extends BaseDAO implements BankDAO {

	@Override
	public List<TcbsBankAccount> getBankByUser(Long userId) {
		String qStr = "select * from TCBS_BANK_ACCOUNT where USER_ID = " + userId;
		SQLQuery q = getSession().createSQLQuery(qStr);
		q.addEntity(TcbsBankAccount.class);
		return q.list();
	}

	@Override
	public TcbsBankAccount getById(Long id) {
		return (TcbsBankAccount) getSession().load(TcbsBankAccount.class, id);
	}

	@Override
	public void remove(Long id) {
		getSession().delete(getById(id));
	}

}
