package vn.tcbs.cas.profile.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import vn.tcbs.cas.profile.entity.TcbsApplication;
import vn.tcbs.tool.base.BaseDAO;

@Repository("applicationDAO")
@Transactional
public class ApplicationDAOImpl extends BaseDAO implements ApplicationDAO {

	@Cacheable("map-with-ttl")
	public List<TcbsApplication> getByName(String appName) {
		Criteria cr = getSession().createCriteria(TcbsApplication.class);
		cr.add(Restrictions.eq("appname", appName));
		return cr.list();
	}

	@Cacheable("map-with-ttl")
	public TcbsApplication getAppByName(String appName) {
		List<TcbsApplication> apps = this.getByName(appName);
		if (apps != null && apps.size() > 0) {
			return apps.get(0);
		}
		return null;
	}

}
