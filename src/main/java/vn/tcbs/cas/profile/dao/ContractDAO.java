package vn.tcbs.cas.profile.dao;

import java.util.List;

import vn.tcbs.cas.profile.entity.Contract;

public interface ContractDAO {

	public List<Contract> getByAlias(String alias);

	public Contract createNew(Contract c);

	public List<Contract> getByReferenceId(String referenceId);

	public Contract checkContract(String contractNumber);

	public List<Contract> getByIdNumber(String idNumber);

	public Contract update(Contract c);
}
