package vn.tcbs.cas.profile.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import vn.tcbs.cas.profile.entity.TcbsUserUpload;
import vn.tcbs.tool.base.BaseDAO;

@Repository("userUploadDAO")
@Transactional
public class UserUploadDAOImpl extends BaseDAO implements UserUploadDAO {

	private static Logger logger = LoggerFactory.getLogger(UserUploadDAOImpl.class);

	@Override
	public void addNew(TcbsUserUpload userUpload) {
		getSession().save(userUpload);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TcbsUserUpload> getByAlias(String alias) {
		Criteria cr = getSession().createCriteria(TcbsUserUpload.class);
		cr.add(Restrictions.eq("fileAlias", alias));
		return cr.list();
	}

}
