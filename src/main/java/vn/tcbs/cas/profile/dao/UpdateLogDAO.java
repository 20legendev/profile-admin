package vn.tcbs.cas.profile.dao;

import vn.tcbs.cas.profile.entity.TcbsUpdateLog;

public interface UpdateLogDAO {
	public void addNew(TcbsUpdateLog log);
}
