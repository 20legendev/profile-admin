package vn.tcbs.cas.profile.dao;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;

public interface UserQueueDAO {
	public void addNew(TcbsUserOpenaccountQueue userQueue);

	public List<TcbsUserOpenaccountQueue> getByReferenceId(String referenceid);

	public void update(TcbsUserOpenaccountQueue userQueue);

	public List<TcbsUserOpenaccountQueue> getByIdNumberAndStatus(String idNumber, Integer status);

	public List<TcbsUserOpenaccountQueue> findUser(String idNumber);

	public List<TcbsUserOpenaccountQueue> find(String idNumber, Long referer);

	public List<TcbsUserOpenaccountQueue> listQueue(Integer page, Integer perpage, String email, String phone,
			String idNumber, String fullname);

	public List<TcbsUserOpenaccountQueue> getByReferer(Long referer);

	public List<TcbsUserOpenaccountQueue> getByRefererRestricted(Long referer);
}
