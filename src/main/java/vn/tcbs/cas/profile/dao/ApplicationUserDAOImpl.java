package vn.tcbs.cas.profile.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import vn.tcbs.cas.profile.entity.TcbsApplicationUser;
import vn.tcbs.tool.base.BaseDAO;

@Repository("applicationUserDAO")
@Transactional
public class ApplicationUserDAOImpl extends BaseDAO implements ApplicationUserDAO {

	public List<TcbsApplicationUser> get(Long userId, Long appId) {
		String qStr = "select * from CASDB.TCBS_APPLICATION_USER au where au.USER_ID = " + userId + " AND au.APP_ID = "
				+ appId;
		SQLQuery q = getSession().createSQLQuery(qStr);
		q.addEntity(TcbsApplicationUser.class);
		return q.list();
	}

	public List<TcbsApplicationUser> getApplicationUser(String idNumber, String appName) {
		String qStr = "select au.ID,au.USER_ID,au.APP_ID,au.USER_APP_ID from CASDB.TCBS_APPLICATION_USER au INNER JOIN CASDB.TCBS_APPLICATION a on au.APP_ID = a.APPID INNER JOIN CASDB.TCBS_IDENTIFICATION i ON i.USER_ID = au.USER_ID WHERE i.ID_NUMBER = '"
				+ idNumber + "' AND a.APPNAME = '" + appName + "'";
		SQLQuery q = getSession().createSQLQuery(qStr);
		q.addEntity(TcbsApplicationUser.class);
		return q.list();
	}

	public List<TcbsApplicationUser> getApplicationUserByEmail(String email, String appName) {
		String qStr = "select au.ID,au.USER_ID,au.APP_ID,au.USER_APP_ID from CASDB.TCBS_APPLICATION_USER au INNER JOIN CASDB.TCBS_APPLICATION a on au.APP_ID = a.APPID INNER JOIN CASDB.TCBS_USER u ON u.ID = au.USER_ID WHERE u.EMAIL = '"
				+ email + "' AND a.APPNAME = '" + appName + "'";
		SQLQuery q = getSession().createSQLQuery(qStr);
		q.addEntity(TcbsApplicationUser.class);
		return q.list();
	}

	@Override
	public List<TcbsApplicationUser> getApplicationUserByPhone(String phone, String appName) {
		String qStr = "select au.ID,au.USER_ID,au.APP_ID,au.USER_APP_ID from CASDB.TCBS_APPLICATION_USER au INNER JOIN CASDB.TCBS_APPLICATION a on au.APP_ID = a.APPID INNER JOIN CASDB.TCBS_USER u ON u.ID = au.USER_ID WHERE u.PHONE = '"
				+ phone + "' AND a.APPNAME = '" + appName + "'";
		SQLQuery q = getSession().createSQLQuery(qStr);
		q.addEntity(TcbsApplicationUser.class);
		return q.list();
	}

	public TcbsApplicationUser getByUserAndAppId(Long userId, Long appId) {
		List<TcbsApplicationUser> list = this.get(userId, appId);
		if (list.size() > 0)
			return list.get(0);
		return null;
	}

	public List<TcbsApplicationUser> getByUserAndAppName(Long userId, String appName) {
		Criteria cr = getSession().createCriteria(TcbsApplicationUser.class);
		cr.createAlias("user", "user");
		cr.add(Restrictions.eq("user.id", userId));
		cr.createAlias("application", "application");
		cr.add(Restrictions.eq("application.appname", appName));
		return cr.list();
	}

	public void add(TcbsApplicationUser appUser) {
		getSession().save(appUser);
	}

	public void update(TcbsApplicationUser appUser) {
		getSession().update(appUser);
	}

	public TcbsApplicationUser getByApp(Long appId, String userInAppId) {
		Criteria cr = getSession().createCriteria(TcbsApplicationUser.class);
		cr.add(Restrictions.eq("application.appid", appId));
		cr.add(Restrictions.eq("userAppId", userInAppId));
		List<TcbsApplicationUser> appUser = cr.list();
		if (appUser.size() > 0) {
			return appUser.get(0);
		}
		return null;
	}

}
