package vn.tcbs.cas.profile.dao;

import vn.tcbs.cas.profile.entity.TcbsUserPhone;

public interface PhoneDAO {
	public void deleteByUser(Long userId);
	public TcbsUserPhone getById(Long id);
}
