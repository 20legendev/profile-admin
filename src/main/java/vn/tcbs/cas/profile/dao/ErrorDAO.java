package vn.tcbs.cas.profile.dao;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsErrorLog;

public interface ErrorDAO {
	public void addNew(TcbsErrorLog log);

	public void resolve(Long id);

	public List<TcbsErrorLog> get(Integer page, Integer perpage, String type, Integer status, String data);
	public int count(String type, Integer status, String data);
}
