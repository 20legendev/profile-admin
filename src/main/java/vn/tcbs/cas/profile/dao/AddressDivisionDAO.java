package vn.tcbs.cas.profile.dao;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsAddressDivision;

public interface AddressDivisionDAO {
	public List<TcbsAddressDivision> getByParent(Long parentId);

	public List<TcbsAddressDivision> getByParentName(String parentName);

	public List<TcbsAddressDivision> getByCode(String divisionCode);
}
