package vn.tcbs.cas.profile.publicapi;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import vn.tcbs.cas.profile.dto.ChangePasswordDTO;
import vn.tcbs.cas.profile.dto.ForgotPasswordDTO;
import vn.tcbs.cas.profile.entity.TcbsApplicationUser;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.object.OpenAccountMessage;
import vn.tcbs.cas.profile.service.OpenAccountService;
import vn.tcbs.cas.profile.service.PasswordService;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.library.crypto.CryptoUtils;
import vn.tcbs.tool.base.BaseWebService;
import vn.tcbs.tool.base.Constant;
import vn.tcbs.tool.base.ResponseData;
import vn.tcbs.tool.base.TcbsUtils;

@WebService
@Path(value = "/account")
public class AccountWebServiceImpl extends BaseWebService implements AccountWebService {

	@Autowired
	private UserService userService;
	@Autowired
	private OpenAccountService openAccountService;
	@Autowired
	private PasswordService passwordService;

	private static Logger logger = LoggerFactory.getLogger(AccountWebServiceImpl.class);

	@POST
	@Path("/forgot-pwd")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response requestChangePassword(@FormParam("") ForgotPasswordDTO msg, @Context HttpServletRequest request) {
		ResponseData<String> res = null;
		Integer isSent = passwordService.forgotPassword(msg.getEmail());
		String[] errors = { "SUCCESS", "USER_NOT_FOUND", "EMAIL_ERROR", "ERROR" };
		res = new ResponseData<String>(isSent, "", errors[isSent]);
		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	@POST
	@Path("/update-pwd")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response updatePassword(MultivaluedMap<String, Object> msg, @Context HttpServletRequest request) {
		ResponseData<String> res = null;
		Long userId = (Long) request.getSession().getAttribute("forgot_password_user");
		if (userId == null) {
			res = new ResponseData<String>(1, "", "ERROR");
		} else {
			String password = (String) msg.get("password").get(0);
			passwordService.updatePassword(userId, password);
			res = new ResponseData<String>(0, "", "SUCCESS");
		}
		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	@POST
	@Path("/change-pwd")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response changePassword(@FormParam("") ChangePasswordDTO passwordDTO, @Context HttpServletRequest req) {
		HttpSession sess = req.getSession();
		ResponseData<String> res = null;
		if (!TcbsUtils.preventAttach(sess)) {
			res = new ResponseData<String>(1, "Hey don't attach me plz", "ERROR");
			return generateResponse(res);
		}
		TcbsUser user = this.getUser(req);
		if (user == null) {
			res = new ResponseData<String>(2, "", "NOT_LOGGIN");
			return generateResponse(res);
		}
		String passwordEncrypted = CryptoUtils.encryptPassword(passwordDTO.getPassword(), Constant.saltKey);
		if (!passwordEncrypted.equals(user.getPassword())) {
			res = new ResponseData<String>(3, "", "WRONG_PASSWORD");
			return generateResponse(res);
		}
		if (!TcbsUtils.validatePassword(passwordDTO.getNewpassword(), null)) {
			res = new ResponseData<String>(4, "", "PASSWORD_NOT_GOOD");
			return generateResponse(res);
		}
		passwordService.updatePassword(user.getId(), passwordDTO.getNewpassword());
		res = new ResponseData<String>(0, "", "SUCCESS");
		return generateResponse(res);
	}

	@POST
	@Path("/detail")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response getById(MultivaluedMap<String, Object> msg, @Context HttpServletRequest req) {
		Long userId = Long.valueOf((String) msg.get("id").get(0));
		if (userId == null) {
			return generateResponse(new ResponseData<String>(1, "", "NOT_AUTHORIZED"));
		} else {
			TcbsUser user = userService.getById(userId);
			if (user != null) {
				return generateResponse(new ResponseData<TcbsUser>(0, user, "SUCCESS"));
			} else {
				return generateResponse(new ResponseData<String>(2, "", "NO_DATA"));
			}
		}
	}

	@POST
	@Path("/detail-by-app")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response getDetailByApp(MultivaluedMap<String, Object> msg, @Context HttpServletRequest req) {
		String appName = (String) msg.get("appname").get(0);
		String inAppId = ((String) msg.get("inappid").get(0));

		TcbsUser user = userService.getByAppId(appName, inAppId);
		return generateResponse(user != null ? new ResponseData<TcbsUser>(0, user, "SUCCESS")
				: new ResponseData<String>(2, "", "NO_DATA"));
	}

	@POST
	@Path("/is-registered")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response isUser(MultivaluedMap<String, Object> msg, @Context HttpServletRequest req) {
		String idNumber = (String) msg.get("idnumber").get(0);
		Boolean isRegistered = userService.isRegistered(idNumber, null);
		ResponseData<Integer> res;
		if (isRegistered) {
			res = new ResponseData<Integer>(0, 1, "SUCCESS");
		} else {
			res = new ResponseData<Integer>(0, 0, "SUCCESS");
		}
		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}

	@POST
	@Path("/open")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response createNew(@FormParam("") OpenAccountMessage msg, @Context HttpServletRequest req) {
		BindingResult errors = TcbsUtils.validateOpenAccount(msg);
		if (errors.hasErrors()) {
			return generateResponse(new ResponseData<List<ObjectError>>(3, errors.getAllErrors(), "ERROR"));
		}
		int errorCode = 0;
		try {
			openAccountService.openAccount(req, msg);
		} catch (Exception e) {
			logger.error("OPEN_ACCOUNT_ERROR: {}", e.getMessage());
			e.printStackTrace();
			return generateResponse(new ResponseData<String>(4, "", "SYSTEM_ERROR"));
		}
		switch (errorCode) {
		case 1:
		case -100098:
			errors.addError(new ObjectError("idnumber", "CMND này đã có tài khoản tại TCBS"));
			errors.addError(new ObjectError("error", "001"));
			break;
		case 2:
			errors.addError(new ObjectError("email", "Email này đã có tài khoản tại TCBS"));
			errors.addError(new ObjectError("error", "002"));
			break;
		case 3:
			errors.addError(new ObjectError("phone", "Số điện thoại đã được đăng ký"));
			errors.addError(new ObjectError("error", "003"));
			break;
		case 0:
			req.getSession().setAttribute("OPEN_ACCOUNT:EMAIL", msg.getEmail());
			return generateResponse(new ResponseData<String>(0, null, "SUCCESS"));
		default:
			logger.info("OPEN_ACCOUNT_ERROR: {}", errorCode);
			return generateResponse(new ResponseData<String>(4, null, "SYSTEM_ERROR"));
		}
		return generateResponse(new ResponseData<List<ObjectError>>(3, errors.getAllErrors(), "ERROR"));

	}

	@POST
	@Path("/sync-account")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response syncAccount(@FormParam("") OpenAccountMessage msg, @Context HttpServletRequest request) {
		String idNumber = msg.getIdnumber();

		Integer isCasUser = userService.findCasUser(idNumber, msg.getEmail().toLowerCase(), 1);
		TcbsUser user = null;
		Boolean success = false;
		String inAppId;
		switch (isCasUser) {
		case 1:
			inAppId = userService.getInAppId(idNumber, "FLEX");
			if (inAppId != null)
				success = true;
			break;
		case 2:
			TcbsApplicationUser au = userService.getApplicationUserByEmail(msg.getEmail(), "FLEX");
			if (au != null) {
				inAppId = au.getUserAppId();
				success = true;
			}
			break;
		}
		if (success) {
			return Response.ok(new ResponseData<List<ObjectError>>(0, null, "SUCCESS"))
					.header("Access-Control-Allow-Origin", "*").build();
		}
		isCasUser = userService.findCasUser(idNumber, msg.getEmail().toLowerCase(), 0);
		if (isCasUser == 0) {
			user = userService.getUserByIdNumber(idNumber);
		} else {
			user = userService.buildUserFromRequest(msg, null);
			if (user != null) {
				user.setCreatedDate(new Date());
			}
		}
		if (user != null) {
			user.setUsername(msg.getCustodycode());
			user.setStatus(1);
			userService.updateByIdNumber(msg.getIdnumber(), user);
			return Response.ok(new ResponseData<String>(0, null, "SUCCESS")).header("Access-Control-Allow-Origin", "*")
					.build();
		}
		return Response.ok(new ResponseData<List<ObjectError>>(2, null, "ERROR"))
				.header("Access-Control-Allow-Origin", "*").build();
	}

	@POST
	@Path("/get-user-app")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response checkAppAccount(MultivaluedMap<String, Object> msg, @Context HttpServletRequest req) {
		String idNumber = (String) msg.get("idnumber").get(0);
		String appName = (String) msg.get("app").get(0);

		Integer registered = userService.checkApp(idNumber, appName);
		if (registered == 1) {
			String inAppId = userService.getInAppId(idNumber, appName);
			return generateResponse(new ResponseData<String>(0, inAppId, "SUCCESS"));
		} else {
			String[] errors = { "NOT_REGISTERED", "REGISTERED", "NO_ACCOUNT", "NO_APP", "ERROR" };
			return generateResponse(new ResponseData<String>(registered, null, errors[registered]));
		}
	}

	@POST
	@Path("/add-user-app")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response addUserApp(MultivaluedMap<String, Object> msg, @Context HttpServletRequest req) {
		String idNumber = (String) msg.get("idnumber").get(0);
		String appName = (String) msg.get("app").get(0);
		String appId = (String) msg.get("appid").get(0);
		ResponseData<Integer> res = null;
		Integer registered = userService.checkApp(idNumber, appName);
		switch (registered) {
		case 3:
			res = new ResponseData<Integer>(2, 0, "NO_APP");
			break;
		case 2:
			res = new ResponseData<Integer>(1, 0, "NO_ACCOUNT");
			break;
		case 0:
			userService.addAppUser(idNumber, appName, appId);
			res = new ResponseData<Integer>(0, 0, "SUCCESS");
			break;
		case 1:
			userService.updateUserApp(idNumber, appName, appId);
			res = new ResponseData<Integer>(0, 0, "SUCCESS");
			break;
		default:
			res = new ResponseData<Integer>(3, 0, "ERROR");
			break;
		}
		return generateResponse(res);
	}

}