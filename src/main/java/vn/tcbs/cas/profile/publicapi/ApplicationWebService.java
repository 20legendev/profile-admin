package vn.tcbs.cas.profile.publicapi;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import vn.tcbs.cas.profile.dto.ForgotPasswordDTO;

public interface ApplicationWebService {

	public Response activeFlex(@Context HttpServletRequest req);

}
