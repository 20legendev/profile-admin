package vn.tcbs.cas.profile.publicapi;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;

import vn.tcbs.cas.profile.dto.AccountUpdateDTO;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.tool.base.BaseWebService;
import vn.tcbs.tool.base.ResponseData;

@WebService
@Path(value = "/user")
public class UserWebServiceImpl extends BaseWebService implements UserWebService {

	@Autowired
	private UserService userService;
	private static Logger logger = LoggerFactory.getLogger(UserWebServiceImpl.class);

	@POST
	@Path("/update")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response update(@FormParam("") AccountUpdateDTO accUpdate, @Context HttpServletRequest req) {
		TcbsUser user = getUser(req);
		if (user == null) {
			return generateResponse(new ResponseData<String>(1, "", "NOT_LOGIN"));
		}
		return generateResponse(userService.doUpdate(accUpdate, user));
	}

	@GET
	@Path("/me")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response me(@Context HttpServletRequest req) {
		TcbsUser user = getUser(req);
		if (user != null) {
			return generateResponse(new ResponseData<TcbsUser>(0, user, "SUCCESS"));
		} else {
			return generateResponse(new ResponseData<TcbsUser>(1, null, "ERROR"));
		}
	}

}