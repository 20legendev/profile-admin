package vn.tcbs.cas.profile.publicapi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.activation.DataHandler;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.google.common.io.Files;

import vn.tcbs.cas.profile.entity.TcbsUserUpload;
import vn.tcbs.cas.profile.object.FileMeta;
import vn.tcbs.cas.profile.service.UserUploadService;
import vn.tcbs.tool.base.BaseWebService;
import vn.tcbs.tool.base.ResponseData;
import vn.tcbs.tool.base.TcbsFileUtils;
import vn.tcbs.tool.base.TcbsUtils;

@WebService
@Path(value = "/upload")
public class UploadWebServiceImpl extends BaseWebService {

	@Value("${tcbs.folder}")
	private String tcbsLocalFolder;
	private static Logger logger = LoggerFactory.getLogger(UploadWebServiceImpl.class);

	@Autowired
	private UserUploadService userUploadService;

	@POST
	@Path("/upload-file")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response upload(List<Attachment> attachments, @Context HttpServletRequest req) {
		String uploadDir = tcbsLocalFolder + File.separator + "tcbs" + File.separator + "upload" + File.separator
				+ new SimpleDateFormat("yyyyMMdd").format(new Date());
		LinkedList<FileMeta> files = new LinkedList<FileMeta>();
		FileMeta fileMeta = null;

		for (Attachment attachment : attachments) {
			DataHandler handler = attachment.getDataHandler();
			try {
				InputStream stream = handler.getInputStream();
				MultivaluedMap<String, String> map = attachment.getHeaders();
				String filename = getFileName(map);
				String filePath = TcbsFileUtils.generateFilename(uploadDir, filename);
				TcbsFileUtils.setFolderWriteable(filePath);
				OutputStream out = new FileOutputStream(new File(filePath));

				int fileSize = stream.available();
				int read = 0;
				byte[] bytes = new byte[1024];
				while ((read = stream.read(bytes)) != -1) {
					out.write(bytes, 0, read);
				}
				String fileAlias = saveToDatabase(filePath, Long.valueOf(req.getRemoteUser()));
				fileMeta = new FileMeta();
				fileMeta.setFileName(filename);
				fileMeta.setFileSize(fileSize / 1024 + " Kb");
				fileMeta.setFileAlias(fileAlias);
				files.add(fileMeta);
				stream.close();
				out.flush();
				out.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return generateResponse(new ResponseData<LinkedList>(0, files, "SUCCESS"));
	}

	private String saveToDatabase(String filePath, Long userId) {
		String ext = Files.getFileExtension(filePath);
		String alias = TcbsUtils.generateRandom(16) + "." + ext;
		TcbsUserUpload userUpload = new TcbsUserUpload();
		userUpload.setUserId(userId);
		userUpload.setFileType("SALE_OFFICE_DOCUMENT");
		userUpload.setFilePath(filePath);
		userUpload.setFileAlias(alias);
		userUploadService.addNew(userUpload);
		return alias;
	}

	private String getFileName(MultivaluedMap<String, String> header) {
		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {
				String[] name = filename.split("=");
				String exactFileName = name[1].trim().replaceAll("\"", "");
				return exactFileName;
			}
		}
		return "unknown";
	}

}