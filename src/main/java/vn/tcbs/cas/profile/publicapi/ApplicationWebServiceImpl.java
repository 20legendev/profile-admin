package vn.tcbs.cas.profile.publicapi;

import java.util.HashMap;
import java.util.List;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedOperation;

import vn.tcbs.cas.profile.entity.TcbsApplicationUser;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;
import vn.tcbs.cas.profile.service.ApplicationService;
import vn.tcbs.cas.profile.service.JsonService;
import vn.tcbs.cas.profile.service.OpenAccountService;
import vn.tcbs.cas.profile.service.UserQueueService;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.tool.base.BaseWebService;
import vn.tcbs.tool.base.ResponseData;
import vn.tcbs.tool.base.UserUtils;

@WebService
@Path(value = "/app")
public class ApplicationWebServiceImpl extends BaseWebService implements ApplicationWebService {

	@Autowired
	private UserService userService;
	@Autowired
	private OpenAccountService openAccountService;
	@Autowired
	private UserQueueService userQueueService;
	@Autowired
	private ApplicationService applicationService;
	@Autowired
	private AmqpTemplate rabbitmqTemplate;
	@Value("${sync.queue.exchangename}")
	private String exchangeName;
	@Value("${queue.openaccount.cas.flex.routingkey}")
	private String openFlexAccountRoutingKey;

	private static Logger logger = LoggerFactory.getLogger(ApplicationWebServiceImpl.class);

	@POST
	@Path("/active-flex")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response activeFlex(@Context HttpServletRequest req) {
		ResponseData<String> res = null;
		if (!UserUtils.isLoggedIn(req)) {
			return generateResponse(new ResponseData<String>(1, null, "NOT_LOGGED_IN"));
		}
		TcbsUser user = getUser(req);
		List<TcbsApplicationUser> listAppUser = applicationService.getUserInAppByUserId(user.getId(), "FLEX");
		logger.info("FOUND_USER: {}", listAppUser.size());
		if (listAppUser.size() > 0) {
			return generateResponse(new ResponseData<String>(2, null, "ACCOUNT_EXIST"));
		}
		TcbsUserOpenaccountQueue uq = userQueueService
				.getUserByIdNumber(user.getIdentifications().get(0).getIdNumber());
		try {
			if (uq == null) {
				userQueueService.saveFromUser(user);
			}
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put("userid", String.valueOf(user.getId()));
			send(exchangeName, openFlexAccountRoutingKey,
					JsonService.getInstance().getObjectMapper().writeValueAsString(hash));
			res = new ResponseData<String>(0, "Kích hoạt tài khoản thành công", "SUCCESS");
			return generateResponse(res);
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Active Flex Error {}", ex.getMessage());
			res = new ResponseData<String>(3, "Có lỗi hệ thống", "SYSTEM_ERROR");
			return generateResponse(res);
		}
	}

	@ManagedOperation
	public void send(String exchange, String key, String text) {
		rabbitmqTemplate.convertAndSend(exchange, key, text);
	}

}