package vn.tcbs.cas.profile.publicapi;

import java.util.List;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import vn.tcbs.cas.profile.entity.TcbsAddressDivision;
import vn.tcbs.cas.profile.service.AddressService;
import vn.tcbs.library.http.ResponseData;
import vn.tcbs.tool.base.BaseWebService;

@WebService
@Path(value = "/flex")
public class FlexWSImpl extends BaseWebService implements FlexWS {

	@Value("${url.api.flexservice}")
	private String flexServiceUrl;
	private static Logger logger = Logger.getLogger(FlexWSImpl.class);
	@Autowired
	private AddressService addressService;

	@GET
	@Path("/province")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response provinceList(@Context HttpServletRequest req) {
		List<TcbsAddressDivision> a = addressService.getByParentName("VN");
		return generateResponse(new vn.tcbs.tool.base.ResponseData<List>(0, a, "SUCCESS"));
	}

	@GET
	@Path("/bank/province/{province}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response bankProvince(@PathParam("province") String province, @Context HttpServletRequest req) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(flexServiceUrl + "bank/by-province/" + province);
		try {
			HttpResponse response = client.execute(get);
			String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
			return Response.ok(responseString).header("Access-Control-Allow-Origin", "*").build();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return Response.ok(new ResponseData<String>(1, "", "ERROR")).header("Access-Control-Allow-Origin", "*").build();
	}

	@GET
	@Path("/bank/branch/{province}/{bank-code}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response bankCode(@PathParam("province") String province, @PathParam("bank-code") String bankCode,
			@Context HttpServletRequest req) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(flexServiceUrl + "bank/branch/" + province + "/" + bankCode);
		try {
			HttpResponse response = client.execute(get);
			String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
			return Response.ok(responseString).header("Access-Control-Allow-Origin", "*").build();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return Response.ok(new ResponseData<String>(1, "", "ERROR")).header("Access-Control-Allow-Origin", "*").build();
	}

	@GET
	@Path("/bank/by-code/{code}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response codeDetail(@PathParam("code") String code, @Context HttpServletRequest req) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(flexServiceUrl + "bank/by-code/" + code);
		try {
			HttpResponse response = client.execute(get);
			String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
			return Response.ok(responseString).header("Access-Control-Allow-Origin", "*").build();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return Response.ok(new ResponseData<String>(1, "", "ERROR")).header("Access-Control-Allow-Origin", "*").build();
	}
}