package vn.tcbs.cas.profile.publicapi;

import java.util.List;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import vn.tcbs.cas.profile.permission.service.PermissionService;
import vn.tcbs.tool.base.BaseWebService;
import vn.tcbs.tool.base.ResponseData;

@WebService
@Path(value = "/permission")
public class PermissionWebServiceImpl extends BaseWebService {

	@Autowired
	private PermissionService permissionService;

	private static Logger logger = LoggerFactory.getLogger(PermissionWebServiceImpl.class);

	@GET
	@Path("/group/tree")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response requestChangePassword(@Context HttpServletRequest req) {
		// TODO check permission
		ResponseData<List> res = new ResponseData<List>(0, permissionService.getGroupTree(), "SUCCESS");
		return Response.ok(res).header("Access-Control-Allow-Origin", "*").build();
	}
}