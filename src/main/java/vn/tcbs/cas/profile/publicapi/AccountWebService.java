package vn.tcbs.cas.profile.publicapi;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import vn.tcbs.cas.profile.dto.ForgotPasswordDTO;

public interface AccountWebService {

	public Response requestChangePassword(@FormParam("") ForgotPasswordDTO msg, @Context HttpServletRequest request);

	public Response updatePassword(MultivaluedMap<String, Object> msg, @Context HttpServletRequest request);

	public Response getById(MultivaluedMap<String, Object> msg, @Context HttpServletRequest req);

	public Response getDetailByApp(MultivaluedMap<String, Object> msg, @Context HttpServletRequest req);

	public Response isUser(MultivaluedMap<String, Object> msg, @Context HttpServletRequest req);

	public Response checkAppAccount(MultivaluedMap<String, Object> msg, @Context HttpServletRequest req);

	public Response addUserApp(MultivaluedMap<String, Object> msg, @Context HttpServletRequest req);

}
