package vn.tcbs.cas.profile.publicapi;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import vn.tcbs.cas.profile.dto.AccountUpdateDTO;
import vn.tcbs.cas.profile.dto.ForgotPasswordDTO;

public interface UserWebService {

	public Response update(@FormParam("") AccountUpdateDTO accUpdate, @Context HttpServletRequest request);
	public Response me(@Context HttpServletRequest request);

}
