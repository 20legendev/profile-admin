package vn.tcbs.cas.profile.publicapi;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

@Path(value = "/upload")
public interface UploadWebService {

	@POST
	@Path("/upload-file")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response upload(List<Attachment> attachments, @Context HttpServletRequest req);

}