package vn.tcbs.cas.profile.admin;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.cas.profile.dao.UserDAO;
import vn.tcbs.cas.profile.entity.TcbsAddress;
import vn.tcbs.cas.profile.entity.TcbsApplication;
import vn.tcbs.cas.profile.entity.TcbsApplicationUser;
import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.service.ApplicationService;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.tool.base.TcbsUtils;
import vn.tcbs.tracking.app.fund.ImportFundDataUser;

@Controller
@RequestMapping(value = "/test")
public class TestController {

	@Autowired
	UserService userService;
	@Autowired
	ApplicationService applicationService;
	@Autowired
	UserDAO userDAO;

	private boolean checkDuplicated(String email, String phone) {
		TcbsUser dp = userService.getUserByMainPhone(phone);
		TcbsUser de = userService.getUserByMainPhone(email);
		return true;
	}

	@RequestMapping(value = "/import-fund", method = RequestMethod.GET)
	private void importFund() {
		try {
			TcbsApplication app = applicationService.getByName("TC_FUND");
			List<ImportFundDataUser> data = readFile("/Users/kiennt/Desktop/customer_fund.xlsx");
			System.out.println("Tìm thấy: " + data.size() + " người");

			ImportFundDataUser importUser;
			for (int i = 0; i < data.size(); i++) {
				importUser = data.get(i);
				TcbsUser user = userService.getUserByIdNumber(importUser.getIdnumber());
				if (user != null) {
					// TRUNG CMND
					boolean duplicatedEmail = userService.isEmailDuplicated(importUser.getEmail(), user.getId());
					if (duplicatedEmail) {
						System.out.println(importUser.getIdnumber() + " email duplicated");
					} else {
						boolean duplicatedPhone = userService.isPhoneDuplicated(importUser.getPhone(), user.getId());
						if (duplicatedPhone) {
							System.out.println(importUser.getIdnumber() + " phone duplicated");
						} else {
							TcbsApplicationUser au = new TcbsApplicationUser();
							au.setUser(user);
							au.setUserAppId(importUser.getCustodyCd());
							au.setApps(app);
							user.getApps().add(au);
							System.out.println("DO_UPDATE: " + importUser.getIdnumber() + " - " + user.getId());
							userService.update(user);
						}
					}
				} else {
					user = userService.getByMainEmail(importUser.getEmail());
					if (user != null) {
						System.out.println(importUser.getIdnumber() + " trùng email " + importUser.getEmail());
					} else {
						user = userService.getUserByMainPhone(importUser.getPhone());
						if (user != null) {
							System.out.println(importUser.getIdnumber() + " trùng email " + importUser.getEmail());
						} else {
							user = new TcbsUser();
							List<String> d = userService.splitName(importUser.getFullname());
							user.setFirstname(d.get(1));
							user.setLastname(d.get(0));
							user.setBirthday(importUser.getBirthday());
							user.setEmail(importUser.getEmail());
							user.setGender(importUser.getGender());
							user.setPassword("14ca977e4e1cca34e82538e969fbd41fcf6c52e17f95cad3a29ecf0d52ca4d0f");
							user.setStatus(1);
							user.setTcbsid(TcbsUtils.generateUserCode(userDAO, null, importUser.getCustodyCd()));
							user.setCreatedDate(new Date());
							
							
							List<TcbsApplicationUser> la = new ArrayList<TcbsApplicationUser>();
							TcbsApplicationUser au = new TcbsApplicationUser();
							au.setUserAppId(importUser.getCustodyCd());
							au.setUser(user);
							au.setApps(app);
							la.add(au);
							user.setApps(la);
							
							List<TcbsAddress> laddr = new ArrayList<TcbsAddress>();
							TcbsAddress a = new TcbsAddress();
							a.setAddress(importUser.getAddress());
							a.setUser(user);
							laddr.add(a);
							user.setAddress(laddr);
							
							List<TcbsIdentification> li = new ArrayList<TcbsIdentification>();
							TcbsIdentification ident = new TcbsIdentification();
							ident.setUser(user);
							ident.setIdDate(importUser.getIddate());
							ident.setIdNumber(importUser.getIdnumber());
							ident.setIdPlace(importUser.getIdplace());
							li.add(ident);
							user.setIdentifications(li);
							System.out.println("ADD_NEW " + importUser.getIdnumber());
							userService.add(user);
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Dữ liệu có chứa lỗi");
		}
	}

	public List<ImportFundDataUser> readFile(String filePath) throws FileNotFoundException, IOException {
		List<ImportFundDataUser> persons = new ArrayList<ImportFundDataUser>();
		Workbook wb;
		try {
			wb = new HSSFWorkbook(new FileInputStream(filePath));
		} catch (Exception ex) {
			wb = new XSSFWorkbook(new FileInputStream(filePath));
		}
		Sheet sheet = wb.getSheetAt(0);
		Row row;
		Cell cell;
		int rows;
		rows = sheet.getPhysicalNumberOfRows();
		int cols = 0;
		int tmp = 0;
		for (int i = 0; i < 10 || i < rows; i++) {
			row = sheet.getRow(i);
			if (row != null) {
				tmp = sheet.getRow(i).getPhysicalNumberOfCells();
				if (tmp > cols)
					cols = tmp;
			}
		}
		DateTimeFormatter dtf = DateTimeFormat.forPattern("dd/MM/yyyy");
		for (int r = 1; r < rows; r++) {
			ImportFundDataUser user = new ImportFundDataUser();
			row = sheet.getRow(r);
			if (row != null) {
				cell = row.getCell(1);
				if (cell != null) {
					user.setFullname(cell.getStringCellValue());
				}
				cell = row.getCell(2);
				if (cell != null) {
					user.setIdnumber(cell.getStringCellValue());
				}

				cell = row.getCell(4);
				if (cell != null) {
					if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
						user.setIddate(cell.getDateCellValue());
					}
				}
				cell = row.getCell(5);
				if (cell != null) {
					user.setIdplace(cell.getStringCellValue());
				}
				cell = row.getCell(6);
				if (cell != null) {
					String gender = cell.getStringCellValue();
					user.setGender(gender != null && gender.equals("1-Nam") ? 1 : 0);
				}
				cell = row.getCell(7);
				if (cell != null) {
					if (cell.getStringCellValue() != null && !cell.getStringCellValue().equals("")) {
						user.setBirthday(dtf.parseDateTime(cell.getStringCellValue()).toDate());
					}
				}
				cell = row.getCell(12);
				if (cell != null) {
					user.setAddress(cell.getStringCellValue());
				}
				cell = row.getCell(14);
				if (cell != null) {
					user.setPhone(cell.getStringCellValue());
				}
				cell = row.getCell(15);
				if (cell != null) {
					user.setEmail(cell.getStringCellValue().toLowerCase());
				}
				cell = row.getCell(20);
				if (cell != null) {
					user.setCustodyCd(cell.getStringCellValue().toUpperCase());
				}
				cell = row.getCell(23);
				if (cell != null) {
					user.setBankaccountnumber(cell.getStringCellValue().toLowerCase());
				}
				persons.add(user);
			}
		}
		return persons;
	}

}
