package vn.tcbs.cas.profile.admin.api;

import java.util.List;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import vn.tcbs.cas.profile.permission.InvSystem;
import vn.tcbs.cas.profile.permission.service.GroupService;
import vn.tcbs.cas.profile.permission.service.SystemService;
import vn.tcbs.tool.base.BaseWebService;
import vn.tcbs.tool.base.ResponseData;

@WebService
@Path(value = "/system")
public class SystemWebServiceImpl extends BaseWebService {

	@Autowired
	private GroupService groupService;
	@Autowired
	private SystemService systemService;

	private static Logger logger = LoggerFactory.getLogger(SystemWebServiceImpl.class);

	@GET
	@Path("/list")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response listSystem(@Context HttpServletRequest req) {
		List<InvSystem> data = systemService.listAll();
		return generateResponse(new ResponseData<List>(0, data, "SUCCESS"));
	}

}