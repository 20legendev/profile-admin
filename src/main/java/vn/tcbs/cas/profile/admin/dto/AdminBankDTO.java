package vn.tcbs.cas.profile.admin.dto;

import java.io.Serializable;

public class AdminBankDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long userId;
	private String data;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}