package vn.tcbs.cas.profile.admin.api;

import java.util.List;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import vn.tcbs.cas.profile.admin.dto.AdminSearchDTO;
import vn.tcbs.invest2.export.ExportService;
import vn.tcbs.tool.base.BaseWebService;
import vn.tcbs.tool.base.ResponseData;

@WebService
@Path(value = "/export")
public class ExportWS extends BaseWebService {

	@Autowired
	private ExportService exportService;

	@POST
	@Path("/user/search")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response orderHistory(@FormParam("") AdminSearchDTO search, @Context HttpServletRequest req,
			@Context HttpServletResponse res) {
		if (!isLoggedIn(req)) {
			return generateResponse(new ResponseData<String>(1, "", "LOGIN_REQUIRED"));
		}
		try {
			List<String> result = exportService.exportSearch(search);
			req.getSession().setAttribute("generated-file", result.get(1));
			req.getSession().setAttribute("generated-file-name", result.get(0));
			return generateResponse(new ResponseData<String>(0, result.get(0), "SUCCESS"));
		} catch (Exception ex) {
			ex.printStackTrace();
			return generateResponse(new ResponseData<String>(2, "", "ERROR"));
		}
	}

}