package vn.tcbs.cas.profile.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import vn.tcbs.cas.profile.dao.ErrorDAO;
import vn.tcbs.cas.profile.entity.TcbsErrorLog;
import vn.tcbs.cas.profile.service.notification.NotificationService;

@Service("errorService")
public class ErrorServiceImpl implements ErrorService {

	@Autowired
	private ErrorDAO errorDAO;
	@Autowired
	private NotificationService notificationService;
	@Value("${email.error.sendto}")
	private String sendToEmail;
	@Value("${email.error.ccto}")
	private String ccToEmail;

	@Override
	public void addNew(TcbsErrorLog log) {
		notify(log.getErrorType() + log.getData());
		errorDAO.addNew(log);
	}

	@Override
	public void notify(String data) {
		try {
			notificationService.sendEmail("", sendToEmail, ccToEmail, "Lỗi trong hệ thống", data, "");
		} catch (Exception ex) {
		}
	}

	@Override
	public List<TcbsErrorLog> get(Integer page, Integer perpage, String type, Integer status, String data) {
		return errorDAO.get(page, perpage, type, status, data);
	}

	@Override
	public int count(String type, Integer status, String data) {
		return errorDAO.count(type, status, data);
	}

	@Override
	public void resolve(Long id) {
		errorDAO.resolve(id);
	}

}