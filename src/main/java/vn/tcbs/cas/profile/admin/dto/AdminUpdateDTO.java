package vn.tcbs.cas.profile.admin.dto;

import java.io.Serializable;
import java.util.Date;

public class AdminUpdateDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String email;
	private String phone;
	private String fullname;
	private String username;
	private Long userid;
	private String birthday;
	private String pwd;
	private String idcode;
	private String address;

	private Integer sendemail;
	private Integer sendsms;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIdcode() {
		return idcode;
	}

	public void setIdcode(String idcode) {
		this.idcode = idcode;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public Integer getSendemail() {
		return sendemail;
	}

	public void setSendemail(Integer sendemail) {
		this.sendemail = sendemail;
	}

	public Integer getSendsms() {
		return sendsms;
	}

	public void setSendsms(Integer sendsms) {
		this.sendsms = sendsms;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

}