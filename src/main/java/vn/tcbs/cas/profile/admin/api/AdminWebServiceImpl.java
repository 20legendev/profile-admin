package vn.tcbs.cas.profile.admin.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.velocity.app.VelocityEngine;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.analytic.service.AnaUserDTO;
import vn.tcbs.analytic.service.AnaUserService;
import vn.tcbs.cas.profile.admin.dto.AdminBankDTO;
import vn.tcbs.cas.profile.admin.dto.AdminSearchDTO;
import vn.tcbs.cas.profile.admin.dto.AdminUpdateDTO;
import vn.tcbs.cas.profile.admin.dto.OpenAppDTO;
import vn.tcbs.cas.profile.bond.BondService;
import vn.tcbs.cas.profile.dto.UserBankAccount;
import vn.tcbs.cas.profile.dto.UserContact;
import vn.tcbs.cas.profile.entity.TcbsAddress;
import vn.tcbs.cas.profile.entity.TcbsBankAccount;
import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUpdateLog;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;
import vn.tcbs.cas.profile.service.BankService;
import vn.tcbs.cas.profile.service.PasswordService;
import vn.tcbs.cas.profile.service.UpdateLogService;
import vn.tcbs.cas.profile.service.UserQueueService;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.cas.profile.service.notification.NotificationService;
import vn.tcbs.cas.profile.service.sync.UserSyncService;
import vn.tcbs.tool.base.BaseWebService;
import vn.tcbs.tool.base.ResponseData;
import vn.tcbs.tool.base.TcbsUtils;

@WebService
@Path(value = "/admin")
public class AdminWebServiceImpl extends BaseWebService {

	@Autowired
	private UserService userService;
	@Autowired
	private UserQueueService userQueueService;
	@Autowired
	private PasswordService passwordService;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private BankService bankService;
	@Autowired
	private VelocityEngine velocityEngine;
	@Value("${sms.content.password}")
	private String smsPasswordContent;
	@Autowired
	private BondService bondService;
	@Autowired
	private UpdateLogService updateLogService;
	@Autowired
	private AnaUserService anaUserService;
	@Autowired
	private UserSyncService userSyncService;

	private static Logger logger = LoggerFactory.getLogger(AdminWebServiceImpl.class);

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/search")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response search(@FormParam("") AdminSearchDTO search, @Context HttpServletRequest request) {
		List<TcbsUser> lu = userService.find(search);
		return generateResponse(new ResponseData<List>(0, lu, "SUCCESS"));
	}

	@SuppressWarnings("rawtypes")
	@GET
	@Path("/analytic/user/by-month")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response analyticByMonth(@Context HttpServletRequest request) {
		List<AnaUserDTO> results = anaUserService.getOpenAccountStatisticByMonth(new DateTime());
		return generateResponse(new ResponseData<List>(0, results, "SUCCESS"));
	}

	@SuppressWarnings("rawtypes")
	@POST
	@Path("/search-queue")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response searchQueue(@FormParam("") AdminSearchDTO search, @Context HttpServletRequest request) {
		List<TcbsUserOpenaccountQueue> lu = userQueueService.listQueue(0, 25, search.getEmail(), search.getPhone(),
				search.getIdnumber(), search.getFullname());
		return generateResponse(new ResponseData<List>(0, lu, "SUCCESS"));
	}

	@SuppressWarnings("rawtypes")
	@GET
	@Path("/user/get-bank-account/{userid}")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response getBankAccount(@PathParam("userid") Long userId, @Context HttpServletRequest req) {
		List<TcbsBankAccount> data = bankService.getByUser(userId);
		List<UserBankAccount> userBank = new ArrayList<UserBankAccount>();
		for (int i = 0; i < data.size(); i++) {
			UserBankAccount ub = new UserBankAccount();
			BeanUtils.copyProperties(data.get(i), ub);
			HashMap<String, Object> bankDetail = bankService.getByBankCode(ub.getBankCode());
			ub.setBankProvince((String) bankDetail.get("regional"));
			ub.setBankCode((String) bankDetail.get("bankcode"));
			ub.setBankSys((String) bankDetail.get("banksys"));
			userBank.add(ub);
		}
		return generateResponse(new ResponseData<List>(0, userBank, "SUCCESS"));
	}

	@POST
	@Path("/open-app")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response openApp(@FormParam("") OpenAppDTO data, @Context HttpServletRequest request) {
		if (data.getUserid() == null || data.getAppname() == null) {
			return generateResponse(new ResponseData<String>(1, "", "MISSING_DATA"));
		}
		if (data.getAppname().equals("BOND")) {
			Long userId = data.getUserid();
			TcbsUser user = userService.getById(userId);
			if (user == null) {
				return generateResponse(new ResponseData<String>(2, "", "NO_DATA"));
			}
			bondService.sendToBond(user);
		}
		return generateResponse(new ResponseData<String>(0, "", "SUCCESS"));
	}

	@POST
	@Path("/update-bank")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response updateBank(@FormParam("") AdminBankDTO update, @Context HttpServletRequest req) {
		Long userId = update.getUserId();
		if (userId == null) {
			return generateResponse(new ResponseData<String>(1, "", "ERROR"));
		}
		ObjectMapper om = new ObjectMapper();
		try {
			List<TcbsBankAccount> banks = om.readValue(update.getData(), new TypeReference<List<TcbsBankAccount>>() {
			});
			if (userService.updateBank(userId, banks) == 0) {

				TcbsUpdateLog log = updateLogService.fromRequest(req, true);
				log.setData(om.writeValueAsString(update));
				log.setUpdateType("UPDATE_BANK");
				log.setActorId(userId);
				updateLogService.addNew(log);

				return generateResponse(new ResponseData<String>(0, "", "SUCCESS"));
			}
		} catch (Exception ex) {
			logger.error("USER_UPDATE_BANK_ERROR: {}", ex.getMessage());
			ex.printStackTrace();
		}
		return generateResponse(new ResponseData<String>(1, "", "ERROR"));
	}

	@POST
	@Path("/delete-bank")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response updateBank(@FormParam("bankId") Long bankId, @Context HttpServletRequest req) {
		bankService.delete(bankId);
		TcbsBankAccount ba = bankService.getById(bankId);

		Long userId = ba.getUser().getId();
		TcbsUpdateLog log = updateLogService.fromRequest(req, true);
		log.setData("" + bankId);
		log.setActorId(userId);
		log.setUpdateType("DELETE_BANK");
		updateLogService.addNew(log);

		try {
			userSyncService.addSyncToFlex(userId, 1);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			logger.error("/delete-bank {}", e.getMessage());
		}
		return generateResponse(new ResponseData<String>(0, "", "SUCCESS"));
	}

	@POST
	@Path("/update-user")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response update(@FormParam("") AdminUpdateDTO update, @Context HttpServletRequest request) throws Exception {
		Long userId = update.getUserid();
		ObjectMapper om = new ObjectMapper();
		TcbsUser user;
		if (userId == null || (user = userService.getById(userId)) == null) {
			return generateResponse(new ResponseData<String>(1, "", "ERROR"));
		}
		String email = update.getEmail();
		if (update.getEmail() != null) {
			if (userService.isEmailDuplicated(email, userId)) {
				return generateResponse(new ResponseData<String>(2, "", "EMAIL_DUPLICATED"));
			}
			user.setEmail(email);
		}
		String phone = update.getPhone();
		if (phone != null) {
			if (userService.isPhoneDuplicated(phone, userId)) {
				return generateResponse(new ResponseData<String>(3, "", "PHONE_DUPLICATED"));
			}
			user.setPhone(phone);
		}
		String username = update.getUsername();
		if (username != null) {
			if (userService.isUsernameDuplicated(username, userId)) {
				return generateResponse(new ResponseData<String>(4, "", "USERNAME_DUPLICATED"));
			}
			user.setUsername(username);
		}
		String fullname = update.getFullname();
		if (fullname != null) {
			List<String> name = userService.splitName(fullname);
			user.setFirstname(name.size() > 0 ? name.get(1) : "");
			user.setLastname(name.size() > 0 ? name.get(0) : "");
		}
		if (update.getBirthday() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			user.setBirthday(sdf.parse(update.getBirthday()));
		}
		if (update.getIdcode() != null) {
			List<TcbsIdentification> old = user.getIdentifications();
			List<TcbsIdentification> ident = om.readValue(update.getIdcode(),
					new TypeReference<List<TcbsIdentification>>() {
					});
			for (int i = 0; i < old.size(); i++) {
				for (int j = 0; j < ident.size(); j++) {
					if (old.get(i).getId().equals(ident.get(i).getId())) {
						old.get(i).setIdDate(ident.get(i).getIdDate());
						old.get(i).setIdPlace(ident.get(i).getIdPlace());
						old.get(i).setIdNumber(ident.get(i).getIdNumber());
					}
				}
			}
		}
		if (update.getAddress() != null) {
			List<TcbsAddress> old = user.getAddress();

			List<TcbsAddress> addr = om.readValue(update.getAddress(), new TypeReference<List<TcbsAddress>>() {
			});
			for (int i = 0; i < old.size(); i++) {
				for (int j = 0; j < addr.size(); j++) {
					if (old.get(i).getId().equals(addr.get(i).getId())) {
						old.get(i).setAddress(addr.get(i).getAddress());
					}
				}
			}
		}
		try {
			TcbsUpdateLog log = updateLogService.fromRequest(request, true);
			log.setData(om.writeValueAsString(update));
			log.setUpdateType("UPDATE_USER");
			log.setActorId(userId);
			updateLogService.addNew(log);
		} catch (Exception ex) {
			logger.error("admin update user: {}", ex.getMessage());
		}
		userService.update(user);
		return generateResponse(new ResponseData<String>(0, "", "SUCCESS"));
	}

	@POST
	@Path("/update-pwd")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response updatePwd(@FormParam("") AdminUpdateDTO update, @Context HttpServletRequest request) {
		Long userId = update.getUserid();
		if (userId == null) {
			return generateResponse(new ResponseData<String>(1, "", "ERROR"));
		}
		TcbsUser user = userService.getById(userId);
		if (user == null) {
			return generateResponse(new ResponseData<String>(1, "", "ERROR"));
		}
		String pwd = TcbsUtils.randomPassword(6);
		passwordService.updatePassword(userId, pwd);
		if (update.getSendemail() != null && update.getSendemail().equals(1) && user.getEmail() != null) {
			HashMap<String, Object> model = new HashMap<String, Object>();
			model.put("email", user.getEmail());
			model.put("password", pwd);
			String emailText = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
					"META-INF/velocity/email/change-password-notification.vm", "utf-8", model);
			try {
				notificationService.sendEmail("", user.getEmail(), "", "[TechcomSecurities]", emailText, "");
			} catch (Exception e) {
				logger.info("SEND_EMAIL_ERROR: PASSWORD {}", user.getEmail());
			}
		}
		if (update.getSendsms() != null && update.getSendsms().equals(1) && user.getPhone() != null) {
			try {
				String smsContent = "QK da cap nhat mat khau thanh cong cho tai khoan tai TCBS. Mat khau moi cua quy khach la "
						+ pwd;
				notificationService.sendSMS(user.getPhone(), smsContent, "Gui thay password");
			} catch (Exception e) {
				logger.info("SEND_EMAIL_ERROR: PASSWORD {}", user.getEmail());
			}
		}
		return generateResponse(new ResponseData<String>(0, "", "SUCCESS"));
	}

	@POST
	@Path("/rmv-user")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response removeUser(@FormParam("") AdminUpdateDTO update, @Context HttpServletRequest request) {
		Long userId = update.getUserid();
		if (userId == null) {
			return generateResponse(new ResponseData<String>(1, "", "ERROR"));
		}
		TcbsUser user = userService.getById(userId);
		if (user == null) {
			return generateResponse(new ResponseData<String>(1, "", "ERROR"));
		}
		userService.remove(user);

		try {
			TcbsUpdateLog log = updateLogService.fromRequest(request, true);
			log.setData(new ObjectMapper().writeValueAsString(user));
			log.setActorId(userId);
			log.setUpdateType("REMOVE_USER");
			updateLogService.addNew(log);
		} catch (Exception ex) {
		}

		return generateResponse(new ResponseData<String>(0, "", "SUCCESS"));
	}

	@GET
	@Path("/user-detail/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response removeUser(@PathParam("id") Long userId, @Context HttpServletRequest request) {
		TcbsUser user = userService.getById(userId);
		UserContact contact = new UserContact();
		BeanUtils.copyProperties(user, contact);
		if (user != null) {
			return generateResponse(new ResponseData<UserContact>(0, contact, "SUCCESS"));
		} else {
			return generateResponse(new ResponseData<String>(0, "", "SUCCESS"));
		}
	}
}