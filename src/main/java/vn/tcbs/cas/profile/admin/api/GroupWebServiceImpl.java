package vn.tcbs.cas.profile.admin.api;

import java.util.List;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import vn.tcbs.cas.profile.permission.InvGroup;
import vn.tcbs.cas.profile.permission.service.GroupService;
import vn.tcbs.tool.base.BaseWebService;
import vn.tcbs.tool.base.ResponseData;

@WebService
@Path(value = "/group")
public class GroupWebServiceImpl extends BaseWebService {

	@Autowired
	private GroupService groupService;

	private static Logger logger = LoggerFactory.getLogger(GroupWebServiceImpl.class);

	@GET
	@Path("/count-member/{groupid}")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response countMember(@PathParam("groupid") Integer groupId, @Context HttpServletRequest req) {
		Integer count = groupService.count(groupId);
		if (count != null) {
			return generateResponse(new ResponseData<Integer>(0, count, "SUCESS"));
		} else {
			return generateResponse(new ResponseData<Integer>(1, count, "ERROR"));
		}
	}

	@GET
	@Path("/search/{query}")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response search(@PathParam("query") String query, @Context HttpServletRequest req) {
		List<InvGroup> data = groupService.search(query);
		if (data != null) {
			return generateResponse(new ResponseData<List>(0, data, "SUCESS"));
		} else {
			return generateResponse(new ResponseData<Integer>(1, 0, "ERROR"));
		}
	}

	@POST
	@Path("/update")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response updateGroup(@FormParam("") InvGroup group, @Context HttpServletRequest req) {
		logInfo("UpdateGroupData", group);
		boolean isSuccess = groupService.updateGroup(group);
		if (isSuccess) {
			return generateResponse(new ResponseData<Boolean>(0, true, "SUCESS"));
		} else {
			return generateResponse(new ResponseData<Boolean>(1, false, "ERROR"));
		}
	}

	@POST
	@Path("/update-parent")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response updateParent(@FormParam("") InvGroup group, @Context HttpServletRequest req) {
		List<InvGroup> data = groupService.getById(group.getGroupid());
		for(int i=0; i < data.size(); i++){
			data.get(i).setParentid(group.getParentid());
			groupService.updateGroup(data.get(i));
		}
		return generateResponse(new ResponseData<Boolean>(0, true, "SUCESS"));
	}

	@POST
	@Path("/remove")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response removeGroup(@FormParam("") InvGroup group, @Context HttpServletRequest req) {
		boolean isSuccess = groupService.removeGroup(group.getGroupid());
		if (isSuccess) {
			return generateResponse(new ResponseData<Boolean>(0, true, "SUCESS"));
		} else {
			return generateResponse(new ResponseData<Boolean>(1, false, "ERROR"));
		}
	}

}