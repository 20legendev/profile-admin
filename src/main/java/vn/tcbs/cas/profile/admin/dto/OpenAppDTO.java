package vn.tcbs.cas.profile.admin.dto;

import java.io.Serializable;

public class OpenAppDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long userid;
	private String appname;

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public String getAppname() {
		return appname;
	}

	public void setAppname(String appname) {
		this.appname = appname;
	}

}