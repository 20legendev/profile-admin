package vn.tcbs.cas.profile.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;

@Controller
@RequestMapping(value = "/referer")
public class RmController {

	@Value("${url.api.tokenservice}")
	private String tokenServiceUrl;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home(HttpServletRequest req, HttpSession sess) {
		ModelAndView mv = new ModelAndView("rm/home");
		return mv;
	}

	@RequestMapping(value = "/add-user", method = RequestMethod.GET)
	public ModelAndView addUser(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException {
		String url = "/mo-tai-khoan.html?ref=referer";
		return new ModelAndView("redirect:" + url);
	}

	@RequestMapping(value = "/khach-hang", method = RequestMethod.GET)
	public ModelAndView myCustomer(HttpServletRequest req, HttpSession sess) {
		req.getSession().setAttribute("RM_MODE", "MY_CUSTOMER");
		ModelAndView mv = new ModelAndView("rm/my-customer");
		return mv;
	}

	@RequestMapping(value = "/sale-office", method = RequestMethod.GET)
	public ModelAndView saleOffice(HttpServletRequest req, HttpSession sess) {
		req.getSession().setAttribute("RM_MODE", "SALE_OFFICE");
		ModelAndView mv = new ModelAndView("rm/sale-office");
		return mv;
	}

}
