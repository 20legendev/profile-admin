package vn.tcbs.cas.profile.admin;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsErrorLog;

public interface ErrorService {
	public void addNew(TcbsErrorLog log);

	public void notify(String data);

	public List<TcbsErrorLog> get(Integer page, Integer perpage, String type, Integer status, String data);

	public int count(String type, Integer status, String data);

	public void resolve(Long id);
}