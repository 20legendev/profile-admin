package vn.tcbs.cas.profile.admin.api;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedOperation;

import vn.tcbs.cas.profile.admin.dto.AdminSearchDTO;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;
import vn.tcbs.cas.profile.service.ContractService;
import vn.tcbs.cas.profile.service.JsonService;
import vn.tcbs.cas.profile.service.UserQueueService;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.cas.profile.service.token.TokenService;
import vn.tcbs.tool.base.BaseWebService;
import vn.tcbs.tool.base.ResponseData;

@WebService
@Path(value = "/rm")
public class RmWebServiceImpl extends BaseWebService {

	@Value("${tcbs.folder}")
	private String tcbsLocalFolder;
	@Autowired
	private UserService userService;
	@Autowired
	private UserQueueService userQueueService;
	@Autowired
	private ContractService contractService;

	@Value("${sync.queue.exchangename}")
	private String exchangeName;
	@Value("${queue.openaccount.cas.flex.routingkey}")
	private String openFlexAccountRoutingKey;
	@Autowired
	private AmqpTemplate rabbitmqTemplate;

	@Autowired
	private TokenService tokenService;

	private static Logger logger = LoggerFactory.getLogger(RmWebServiceImpl.class);

	@POST
	@Path("/check-id")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response byId(@FormParam("") AdminSearchDTO search, @Context HttpServletRequest req) {
		Long userId = null;
		String mode = search.getMode();
		if (mode != null && mode.equals("MY_CUSTOMER")) {
			userId = Long.valueOf(req.getRemoteUser());
		}
		String idNumber = search.getIdnumber();
		idNumber = idNumber == null || idNumber.equals("") ? null : idNumber;
		List<TcbsUserOpenaccountQueue> list = null;
		if (idNumber == null && mode == null) {
			list = new ArrayList<TcbsUserOpenaccountQueue>();
		} else {
			list = userQueueService.getByIdNumber(idNumber, userId);
		}
		byte[] base64String = null;
		try {
			base64String = Base64.encodeBase64(JsonService.getInstance().getObjectMapper().writeValueAsBytes(list));
		} catch (Exception ex) {
			logger.error("CONVERT_TO_JSON_BASE64_ERROR: {}", ex.getMessage());
		}
		return generateResponse(new ResponseData<String>(0, new String(base64String), "SUCCESS"));
	}

	@POST
	@Path("/print-contract")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response printContract(@FormParam("") AdminSearchDTO search, @Context HttpServletRequest req) {
		TcbsUserOpenaccountQueue acc = userQueueService.getByReferenceId(search.getReferenceid());
		if (acc == null) {
			return generateResponse(new ResponseData<String>(1, "", "NOT_FOUND"));
		}
		List<String> result = contractService.generateContractFile(acc);
		if (result.size() > 0) {
			req.getSession().setAttribute("generated-file", result.get(1));
			req.getSession().setAttribute("generated-file-name", result.get(0));
			return generateResponse(new ResponseData<String>(0, "", "SUCCESS"));
		} else {
			return generateResponse(new ResponseData<String>(1, "", "SYSTEM_ERROR"));
		}
	}

	@POST
	@Path("/edit-oar")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response editOpenAccountRequest(@FormParam("referenceid") String referenceid,
			@Context HttpServletRequest req) {
		String token = tokenService.generateToken(referenceid);
		String url = "mo-tai-khoan.html?edit=" + token;
		return generateResponse(new ResponseData<String>(0, url, "SUCCESS"));
	}

	@POST
	@Path("/confirm-open")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response confirmOpenAccount(@FormParam("") AdminSearchDTO search, @Context HttpServletRequest req) {
		try {
			userService.activeAccount(search.getReferenceid(), null);
			send(exchangeName, openFlexAccountRoutingKey, search.getReferenceid());
			return generateResponse(new ResponseData<String>(0, "", "SUCCESS"));
		} catch (Exception ex) {
			logger.error("CONFIRM_OPEN_ACCOUNT_FSS_ERROR {}", ex.getMessage());
			return generateResponse(new ResponseData<String>(2, null, "ERROR"));
		}
	}

	@POST
	@Path("/update-open")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response updateOpenAccount(@FormParam("") AdminSearchDTO search, @Context HttpServletRequest req) {
		if (search.getReferenceid() == null || search.getReferenceid().equals("")) {
			return generateResponse(new ResponseData<String>(1, "", "MISSING_DATA"));
		}
		String referenceid = search.getReferenceid();
		if (search.getUploaded() != null && !search.getUploaded().equals("")) {
			TcbsUserOpenaccountQueue queue = userQueueService.getByReferenceId(referenceid);
			if (queue != null) {
				queue.setUploaded(search.getUploaded());
				userQueueService.update(queue);
			} else {
				return generateResponse(new ResponseData<String>(2, "", "NOT_FOUND"));
			}
		}
		return generateResponse(new ResponseData<String>(0, "", "SUCCESS"));
	}

	@ManagedOperation
	private void send(String exchange, String key, String text) {
		rabbitmqTemplate.convertAndSend(exchange, key, text);
	}

	@POST
	@Path("/confirm-contract")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response confirmContract(@FormParam("") AdminSearchDTO search, @Context HttpServletRequest req) {
		boolean success = userQueueService.confirmContract(search.getReferenceid());
		if (success) {
			return generateResponse(new ResponseData<String>(0, "", "SUCCESS"));
		}
		return generateResponse(new ResponseData<String>(1, "", "ERROR"));
	}

}