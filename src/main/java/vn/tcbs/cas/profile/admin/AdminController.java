package vn.tcbs.cas.profile.admin;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.analytic.service.AnaUserDTO;
import vn.tcbs.analytic.service.AnaUserService;
import vn.tcbs.analytic.service.AnaUserStatisticDTO;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;
import vn.tcbs.cas.profile.service.UserQueueService;

@Controller
@RequestMapping(value = "/")
public class AdminController {

	@Value("${url.api.tokenservice}")
	private String tokenServiceUrl;
	@Autowired
	private AnaUserService anaUserService;
	@Autowired
	private UserQueueService userQueueService;

	private static Logger logger = LoggerFactory.getLogger(AdminController.class);

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home(HttpServletRequest req, HttpSession sess) {
		ModelAndView mv = new ModelAndView("admin/home");
		List<AnaUserDTO> data = anaUserService.getOpenAccountStatisticByDate(new DateTime());
		AnaUserStatisticDTO statistic = anaUserService.getUserStatistic();
		try {
			mv.addObject("lastmonth", new ObjectMapper().writeValueAsString(data));
			mv.addObject("statistic", new ObjectMapper().writeValueAsString(statistic));
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("/home {}", ex.getMessage());
		}
		return mv;
	}

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public ModelAndView userManager(HttpServletRequest req, HttpSession sess) {
		ModelAndView mv = new ModelAndView("admin/user");
		return mv;
	}

	@RequestMapping(value = "/user-flow/{userid}", method = RequestMethod.GET)
	public ModelAndView userFlow(@PathVariable("userid") Long userid, HttpServletRequest req) {
		logger.info("FLOW: {}", userid);
		ModelAndView mv = new ModelAndView("admin/user");
		return mv;
	}

	@RequestMapping(value = "/statistic", method = RequestMethod.GET)
	public ModelAndView statistic(HttpServletRequest req, HttpSession sess) {
		ModelAndView mv = new ModelAndView("admin/statistic");
		return mv;
	}

	@RequestMapping(value = "/user-queue", method = RequestMethod.GET)
	public ModelAndView userQueue(HttpServletRequest req, HttpSession sess) {
		ModelAndView mv = new ModelAndView("admin/user-queue");
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		List<TcbsUserOpenaccountQueue> listOpen = userQueueService.listQueue(0, 25, null, null, null, null);
		mv.addObject("listOpen", listOpen);
		mv.addObject("sf", sf);
		return mv;
	}
}
