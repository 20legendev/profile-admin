package vn.tcbs.cas.profile.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The persistent class for the TCBS_ADDRESS database table.
 * 
 */
@Entity
@Table(name = "TCBS_ADDRESS")
@NamedQuery(name = "TcbsAddress.findAll", query = "SELECT t FROM TcbsAddress t")
public class TcbsAddress implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TCBS_ADDRESS_ID_GENERATOR", sequenceName = "TCBS_ADDRESS_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_ADDRESS_ID_GENERATOR")
	private Long id;

	private String address;

	@Column(name = "ADDRESS_TYPE")
	private Long addressType;

	@Column(name = "DIVISION_1")
	private Long division1;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DIVISION_2")
	@JsonBackReference(value = "division2")
	private TcbsAddressDivision division2;

	@Column(name = "DIVISION_3")
	private Long division3;

	@Column(name = "DIVISION_4")
	private Long division4;

	@Column(name = "DIVISION_5")
	private Long division5;

	@Column(name = "DIVISION_6")
	private Long division6;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	@JsonBackReference(value = "user-address")
	private TcbsUser user;

	public TcbsAddress() {
	}

	public TcbsAddress(String str) {
		try {
			TcbsAddress address = new ObjectMapper().readValue(str, TcbsAddress.class);
			this.id = address.getId();
			this.address = address.getAddress();
			this.addressType = address.getAddressType();
			this.division1 = address.getDivision1();
			this.division2 = address.getDivision2();
			this.division3 = address.getDivision3();
			this.division4 = address.getDivision4();
			this.division5 = address.getDivision5();
			this.division6 = address.getDivision6();
			this.user = address.getUser();
		} catch (Exception ex) {
		}
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getAddressType() {
		return this.addressType;
	}

	public void setAddressType(Long addressType) {
		this.addressType = addressType;
	}

	public Long getDivision1() {
		return this.division1;
	}

	public void setDivision1(Long division1) {
		this.division1 = division1;
	}

	public TcbsAddressDivision getDivision2() {
		return this.division2;
	}

	public void setDivision2(TcbsAddressDivision division2) {
		this.division2 = division2;
	}

	public Long getDivision3() {
		return this.division3;
	}

	public void setDivision3(Long division3) {
		this.division3 = division3;
	}

	public Long getDivision4() {
		return this.division4;
	}

	public void setDivision4(Long division4) {
		this.division4 = division4;
	}

	public Long getDivision5() {
		return this.division5;
	}

	public void setDivision5(Long division5) {
		this.division5 = division5;
	}

	public Long getDivision6() {
		return this.division6;
	}

	public void setDivision6(Long division6) {
		this.division6 = division6;
	}

	public TcbsUser getUser() {
		return user;
	}

	public void setUser(TcbsUser user) {
		this.user = user;
	}

	public static TcbsAddress valueOf(String str) {
		try {
			return new ObjectMapper().readValue(str, TcbsAddress.class);
		} catch (Exception ex) {
			return null;
		}
	}

}