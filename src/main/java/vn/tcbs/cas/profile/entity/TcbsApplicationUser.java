package vn.tcbs.cas.profile.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the TCBS_APPLICATION_USER database table.
 * 
 */
@Entity
@Table(name = "TCBS_APPLICATION_USER")
@NamedQuery(name = "TcbsApplicationUser.findAll", query = "SELECT t FROM TcbsApplicationUser t")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class TcbsApplicationUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TCBS_APPLICATION_USER_ID_GENERATOR", sequenceName = "TCBS_APPLICATION_USER_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_APPLICATION_USER_ID_GENERATOR")
	private long id;

	@Column(name = "USER_APP_ID")
	private String userAppId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	@JsonBackReference(value = "user-apps")
	private TcbsUser user;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "APP_ID")
	@JsonBackReference(value = "application")
	private TcbsApplication application;

	public TcbsApplicationUser() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserAppId() {
		return this.userAppId;
	}

	public void setUserAppId(String userAppId) {
		this.userAppId = userAppId;
	}

	public TcbsUser getUser() {
		return user;
	}

	public void setUser(TcbsUser user) {
		this.user = user;
	}

	public TcbsApplication getApps() {
		return application;
	}

	public void setApps(TcbsApplication apps) {
		this.application = apps;
	}

}