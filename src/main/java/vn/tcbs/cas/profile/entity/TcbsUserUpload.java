package vn.tcbs.cas.profile.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the TCBS_USER_UPLOAD database table.
 * 
 */
@Entity
@Table(name = "TCBS_USER_UPLOAD")
@NamedQuery(name = "TcbsUserUpload.findAll", query = "SELECT t FROM TcbsUserUpload t")
public class TcbsUserUpload implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "FILE_PATH")
	private String filePath;

	@Column(name = "FILE_TYPE")
	private String fileType;

	@Column(name = "FILE_ALIAS")
	private String fileAlias;

	@Id
	@SequenceGenerator(name = "TCBS_USER_UPLOAD_ID_GENERATOR", sequenceName = "TCBS_USER_UPLOAD_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_USER_UPLOAD_ID_GENERATOR")
	private Long id;

	@Temporal(TemporalType.DATE)
	private Date since;

	@Column(name = "USER_ID")
	private Long userId;

	public TcbsUserUpload() {
		this.since = new Date();
	}

	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileType() {
		return this.fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getSince() {
		return this.since;
	}

	public void setSince(Date since) {
		this.since = since;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFileAlias() {
		return fileAlias;
	}

	public void setFileAlias(String fileAlias) {
		this.fileAlias = fileAlias;
	}

}