package vn.tcbs.cas.profile.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the TCBS_UPDATE_LOG database table.
 * 
 */
@Entity
@Table(name="TCBS_UPDATE_LOG")
@NamedQuery(name="TcbsUpdateLog.findAll", query="SELECT t FROM TcbsUpdateLog t")
public class TcbsUpdateLog implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TCBS_UPDATE_LOG_ID_GENERATOR", sequenceName="TCBS_UPDATE_LOG_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCBS_UPDATE_LOG_ID_GENERATOR")
	private Long id;

	@Column(name="ACTOR_ID")
	private Long actorId;

	@Temporal(TemporalType.DATE)
	@Column(name="CONFIRM_DATE")
	private Date confirmDate;

	@Column(name="CONFIRM_IP")
	private String confirmIp;

	@Column(name="CONFIRM_METHOD")
	private String confirmMethod;

	@Column(name="CONFIRM_UA")
	private String confirmUa;

	@Column(name="CONFIRM_USER")
	private Long confirmUser;

	@Column(name="\"DATA\"")
	private String data;

	@Temporal(TemporalType.DATE)
	@Column(name="REQUEST_DATE")
	private Date requestDate;

	@Column(name="REQUEST_IP")
	private String requestIp;

	@Column(name="REQUEST_UA")
	private String requestUa;

	@Column(name="REQUEST_USER")
	private Long requestUser;

	@Column(name="UPDATE_CODE")
	private String updateCode;

	@Column(name="UPDATE_TYPE")
	private String updateType;

	public TcbsUpdateLog() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getActorId() {
		return this.actorId;
	}

	public void setActorId(Long actorId) {
		this.actorId = actorId;
	}

	public Date getConfirmDate() {
		return this.confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getConfirmIp() {
		return this.confirmIp;
	}

	public void setConfirmIp(String confirmIp) {
		this.confirmIp = confirmIp;
	}

	public String getConfirmMethod() {
		return this.confirmMethod;
	}

	public void setConfirmMethod(String confirmMethod) {
		this.confirmMethod = confirmMethod;
	}

	public String getConfirmUa() {
		return this.confirmUa;
	}

	public void setConfirmUa(String confirmUa) {
		this.confirmUa = confirmUa;
	}

	public Long getConfirmUser() {
		return this.confirmUser;
	}

	public void setConfirmUser(Long confirmUser) {
		this.confirmUser = confirmUser;
	}

	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Date getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getRequestIp() {
		return this.requestIp;
	}

	public void setRequestIp(String requestIp) {
		this.requestIp = requestIp;
	}

	public String getRequestUa() {
		return this.requestUa;
	}

	public void setRequestUa(String requestUa) {
		this.requestUa = requestUa;
	}

	public Long getRequestUser() {
		return this.requestUser;
	}

	public void setRequestUser(Long requestUser) {
		this.requestUser = requestUser;
	}

	public String getUpdateCode() {
		return this.updateCode;
	}

	public void setUpdateCode(String updateCode) {
		this.updateCode = updateCode;
	}

	public String getUpdateType() {
		return this.updateType;
	}

	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}

}