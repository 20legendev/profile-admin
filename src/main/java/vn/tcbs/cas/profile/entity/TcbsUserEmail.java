package vn.tcbs.cas.profile.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * The persistent class for the TCBS_USER_EMAIL database table.
 * 
 */
@Entity
@Table(name = "TCBS_USER_EMAIL")
@NamedQuery(name = "TcbsUserEmail.findAll", query = "SELECT t FROM TcbsUserEmail t")
public class TcbsUserEmail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TCBS_USER_EMAIL_ID_GENERATOR", sequenceName = "TCBS_EMAIL_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_USER_EMAIL_ID_GENERATOR")
	private Long id;

	private String email;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	@JsonBackReference(value = "user-emails")
	private TcbsUser user;

	public TcbsUserEmail() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public TcbsUser getUser() {
		return user;
	}

	public void setUser(TcbsUser user) {
		this.user = user;
	}

}