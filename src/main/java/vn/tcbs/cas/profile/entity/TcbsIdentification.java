package vn.tcbs.cas.profile.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The persistent class for the TCBS_IDENTIFICATION database table.
 * 
 */
@Entity
@Table(name = "TCBS_IDENTIFICATION")
@NamedQuery(name = "TcbsIdentification.findAll", query = "SELECT t FROM TcbsIdentification t")
public class TcbsIdentification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TCBS_IDENTIFICATION_ID_GENERATOR", sequenceName = "TCBS_IDENTIFICATION_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_IDENTIFICATION_ID_GENERATOR")
	private Long id;

	@Column(name = "ID_DATE")
	private Date idDate;

	@Column(name = "ID_NUMBER")
	private String idNumber;

	@Column(name = "ID_PLACE")
	private String idPlace;

	@Column(name = "ID_TYPE")
	private String idType = "I";

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	@JsonBackReference(value = "user-identifications")
	private TcbsUser user;

	public TcbsIdentification() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getIdDate() {
		return this.idDate;
	}

	public void setIdDate(Date idDate) {
		this.idDate = idDate;
	}

	public String getIdNumber() {
		return this.idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIdPlace() {
		return this.idPlace;
	}

	public void setIdPlace(String idPlace) {
		this.idPlace = idPlace;
	}

	public String getIdType() {
		return this.idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public TcbsUser getUser() {
		return user;
	}

	public void setUser(TcbsUser user) {
		this.user = user;
	}

}