package vn.tcbs.cas.profile.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The persistent class for the TCBS_USER database table.
 * 
 */
@Entity
@Table(name = "TCBS_USER", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
@NamedQuery(name = "TcbsUser.findAll", query = "SELECT t FROM TcbsUser t")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "password" })
public class TcbsUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TCBS_USER_ID_GENERATOR", sequenceName = "TCBS_USER_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_USER_ID_GENERATOR")
	@Column(name = "ID")
	private Long id;

	private Date birthday;

	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	private String email;

	private String firstname;

	private Integer gender;

	private String lastname;

	private String password;
	private String tcbsid;

	private Long relationship;

	private Integer status;
	private String phone;

	@Column(name = "UPDATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updatedDate;

	private String username;

	@Column(name = "PROFILE_PICTURE")
	private String profilePicture;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@Cascade(CascadeType.SAVE_UPDATE)
	@JsonManagedReference(value = "user-phone")
	private List<TcbsUserPhone> phones = new ArrayList<TcbsUserPhone>();

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@Cascade(CascadeType.SAVE_UPDATE)
	@JsonManagedReference(value = "user-identifications")
	private List<TcbsIdentification> identifications = new ArrayList<TcbsIdentification>();

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@Cascade(CascadeType.SAVE_UPDATE)
	@JsonManagedReference(value = "user-emails")
	private List<TcbsUserEmail> emails = new ArrayList<TcbsUserEmail>();

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@Cascade(CascadeType.SAVE_UPDATE)
	@JsonManagedReference(value = "user-apps")
	private List<TcbsApplicationUser> apps = new ArrayList<TcbsApplicationUser>();

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@Cascade(CascadeType.SAVE_UPDATE)
	@JsonManagedReference(value = "user-address")
	private List<TcbsAddress> address = new ArrayList<TcbsAddress>();

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@Cascade(CascadeType.SAVE_UPDATE)
	@JsonManagedReference(value = "user-banks")
	private List<TcbsBankAccount> banks = new ArrayList<TcbsBankAccount>();

	@Column(name = "VSD_STATUS")
	private Integer vsdStatus;

	public TcbsUser() {
	}

	public TcbsUser(String str) {
		TcbsUser user;
		try {
			user = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).readValue(str,
					TcbsUser.class);
			this.birthday = user.getBirthday();
			this.apps = user.getApps();
			this.createdDate = user.getCreatedDate();
			this.email = user.getEmail();
			this.emails = user.getEmails();
			this.firstname = user.getFirstname();
			this.lastname = user.getLastname();
			this.gender = user.getGender();
			this.id = user.getId();
			this.identifications = user.getIdentifications();
			this.password = user.getPassword();
			this.phones = user.getPhones();
			this.profilePicture = user.getProfilePicture();
			this.relationship = user.getRelationship();
			this.status = user.getStatus();
			this.updatedDate = user.getUpdatedDate();
			this.username = user.getUsername();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getPhone() {
		return phone;
	}

	public String getTcbsid() {
		return tcbsid;
	}

	public void setTcbsid(String tcbsid) {
		this.tcbsid = tcbsid;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public Integer getGender() {
		return this.gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getRelationship() {
		return this.relationship;
	}

	public void setRelationship(Long relationship) {
		this.relationship = relationship;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<TcbsUserPhone> getPhones() {
		return this.phones;
	}

	public void setPhones(List<TcbsUserPhone> phones) {
		this.phones = phones;
	}

	public List<TcbsIdentification> getIdentifications() {
		return identifications;
	}

	public void setIdentifications(List<TcbsIdentification> identifications) {
		this.identifications = identifications;
	}

	public List<TcbsUserEmail> getEmails() {
		return emails;
	}

	public void setEmails(List<TcbsUserEmail> emails) {
		this.emails = emails;
	}

	public List<TcbsApplicationUser> getApps() {
		return apps;
	}

	public void setApps(List<TcbsApplicationUser> apps) {
		this.apps = apps;
	}

	public List<TcbsAddress> getAddress() {
		return address;
	}

	public void setAddress(List<TcbsAddress> address) {
		this.address = address;
	}

	public List<TcbsBankAccount> getBanks() {
		return banks;
	}

	public void setBanks(List<TcbsBankAccount> banks) {
		this.banks = banks;
	}

	public static TcbsUser valueOf(String str) {
		try {
			return new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).readValue(str,
					TcbsUser.class);
		} catch (Exception ex) {
			return null;
		}
	}

	public Integer getVsdStatus() {
		return vsdStatus;
	}

	public void setVsdStatus(Integer vsdStatus) {
		this.vsdStatus = vsdStatus;
	}

}