package vn.tcbs.cas.profile.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the TCBS_BUSINESS_PROCESS database table.
 * 
 */
@Entity
@Table(name = "TCBS_BUSINESS_PROCESS")
@NamedQuery(name = "TcbsBusinessProcess.findAll", query = "SELECT t FROM TcbsBusinessProcess t")
public class TcbsBusinessProcess implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer business;

	@Column(name = "BUSINESS_LEVEL")
	private Integer businessLevel;

	private Long id;

	private String referenceid;

	@Temporal(TemporalType.DATE)
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;

	public TcbsBusinessProcess() {
	}

	public Integer getBusiness() {
		return this.business;
	}

	public void setBusiness(Integer business) {
		this.business = business;
	}

	public Integer getBusinessLevel() {
		return this.businessLevel;
	}

	public void setBusinessLevel(Integer businessLevel) {
		this.businessLevel = businessLevel;
	}

	@Id
	@SequenceGenerator(name = "TCBS_BUSINESS_PROCESS_ID_GENERATOR", sequenceName = "TCBS_BUSINESS_PROCESS_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_BUSINESS_PROCESS_ID_GENERATOR")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReferenceid() {
		return this.referenceid;
	}

	public void setReferenceid(String referenceid) {
		this.referenceid = referenceid;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}