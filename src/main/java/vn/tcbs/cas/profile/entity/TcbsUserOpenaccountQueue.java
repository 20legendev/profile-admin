package vn.tcbs.cas.profile.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the TCBS_USER_OPENACCOUNT_QUEUE database table.
 * 
 */
@Entity
@Table(name = "TCBS_USER_OPENACCOUNT_QUEUE")
@NamedQuery(name = "TcbsUserOpenaccountQueue.findAll", query = "SELECT t FROM TcbsUserOpenaccountQueue t")
@JsonIgnoreProperties({ "openaccountdata" })
public class TcbsUserOpenaccountQueue implements Serializable {
	private static final long serialVersionUID = 1L;

	private String address;

	private String bankaccountname;

	private String bankaccountnumber;

	private String bankbranch;

	private String bankcode;

	private String bankname;
	private String bankprovince;

	@Temporal(TemporalType.DATE)
	private Date birthday;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	private String email;

	private String firstname;

	private Integer gender;
	private String uploaded;

	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "TCBS_USERQUEUE_ID_GENERATOR", sequenceName = "TCBS_USER_OAQ_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_USERQUEUE_ID_GENERATOR")
	private Long id;

	@Temporal(TemporalType.DATE)
	private Date iddate;

	private String idnumber;

	private String idplace;

	private String lastname;

	private String openaccountdata;

	private String password;

	private String passwordmd5;
	private String passwordblank;

	private String phone;

	private String province;

	private String referenceid;

	private Long referer;

	private Integer status;
	@Transient
	private String username;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	@NotFound(action = NotFoundAction.IGNORE)
	private TcbsUser user;

	public TcbsUserOpenaccountQueue() {
		this.status = 0;
		this.createdDate = new Date();
	}

	public String getUploaded() {
		return uploaded;
	}

	public void setUploaded(String uploaded) {
		this.uploaded = uploaded;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public TcbsUser getUser() {
		return user;
	}

	public void setUser(TcbsUser user) {
		this.user = user;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBankaccountname() {
		return this.bankaccountname;
	}

	public void setBankaccountname(String bankaccountname) {
		this.bankaccountname = bankaccountname;
	}

	public String getBankaccountnumber() {
		return this.bankaccountnumber;
	}

	public void setBankaccountnumber(String bankaccountnumber) {
		this.bankaccountnumber = bankaccountnumber;
	}

	public String getBankbranch() {
		return this.bankbranch;
	}

	public void setBankbranch(String bankbranch) {
		this.bankbranch = bankbranch;
	}

	public String getBankcode() {
		return this.bankcode;
	}

	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

	public String getBankname() {
		return this.bankname;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public Integer getGender() {
		return this.gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getIddate() {
		return this.iddate;
	}

	public void setIddate(Date iddate) {
		this.iddate = iddate;
	}

	public String getIdnumber() {
		return this.idnumber;
	}

	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}

	public String getIdplace() {
		return this.idplace;
	}

	public void setIdplace(String idplace) {
		this.idplace = idplace;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getOpenaccountdata() {
		return this.openaccountdata;
	}

	public void setOpenaccountdata(String openaccountdata) {
		this.openaccountdata = openaccountdata;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getReferenceid() {
		return this.referenceid;
	}

	public void setReferenceid(String referenceid) {
		this.referenceid = referenceid;
	}

	public Long getReferer() {
		return this.referer;
	}

	public void setReferer(Long referer) {
		this.referer = referer;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPasswordmd5() {
		return passwordmd5;
	}

	public void setPasswordmd5(String passwordmd5) {
		this.passwordmd5 = passwordmd5;
	}

	public String getPasswordblank() {
		return passwordblank;
	}

	public void setPasswordblank(String passwordblank) {
		this.passwordblank = passwordblank;
	}

	public String getBankprovince() {
		return bankprovince;
	}

	public void setBankprovince(String bankprovince) {
		this.bankprovince = bankprovince;
	}

}