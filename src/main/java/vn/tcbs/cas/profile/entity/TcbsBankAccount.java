package vn.tcbs.cas.profile.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.math.BigDecimal;

/**
 * The persistent class for the TCBS_BANK_ACCOUNT database table.
 * 
 */
@Entity
@Table(name = "TCBS_BANK_ACCOUNT")
@NamedQuery(name = "TcbsBankAccount.findAll", query = "SELECT t FROM TcbsBankAccount t")
public class TcbsBankAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "BANK_ACCOUNT_NAME")
	private String bankAccountName;

	@Column(name = "BANK_ACCOUNT_NO")
	private String bankAccountNo;

	@Column(name = "BANK_CODE")
	private String bankCode;

	@Column(name = "BANK_NAME")
	private String bankName;

	@Column(name = "BANK_BRANCH")
	private String bankBranch;

	private String bankProvince;

	@Id
	@SequenceGenerator(name = "TCBS_BANK_ACCOUNT_ID_GENERATOR", sequenceName = "TCBS_BANK_ACCOUNT_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_BANK_ACCOUNT_ID_GENERATOR")
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	@JsonBackReference(value = "user-banks")
	private TcbsUser user;

	public TcbsBankAccount() {
	}

	public String getBankProvince() {
		return bankProvince;
	}

	public void setBankProvince(String bankProvince) {
		this.bankProvince = bankProvince;
	}

	public String getBankAccountName() {
		return this.bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getBankAccountNo() {
		return this.bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBankCode() {
		return this.bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TcbsUser getUser() {
		return user;
	}

	public void setUser(TcbsUser user) {
		this.user = user;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

}