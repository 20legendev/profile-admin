package vn.tcbs.cas.profile.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the TCBS_APPLICATION database table.
 * 
 */
@Entity
@Table(name = "TCBS_APPLICATION")
@NamedQuery(name = "TcbsApplication.findAll", query = "SELECT t FROM TcbsApplication t")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class TcbsApplication implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TCBS_APPLICATION_APPID_GENERATOR", sequenceName = "TCBS_APPLICATION_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_APPLICATION_APPID_GENERATOR")
	private Long appid;

	private String appdesc;

	private String appname;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "APP_ID")
	@JsonBackReference
	private List<TcbsApplicationUser> appUser;

	public TcbsApplication() {
	}

	public Long getAppid() {
		return this.appid;
	}

	public void setAppid(Long appid) {
		this.appid = appid;
	}

	public String getAppdesc() {
		return this.appdesc;
	}

	public void setAppdesc(String appdesc) {
		this.appdesc = appdesc;
	}

	public String getAppname() {
		return this.appname;
	}

	public void setAppname(String appname) {
		this.appname = appname;
	}

}