package vn.tcbs.cas.profile.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The persistent class for the TCBS_USER_PHONE database table.
 * 
 */
@Entity
@Table(name = "TCBS_USER_PHONE", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
@NamedQuery(name = "TcbsUserPhone.findAll", query = "SELECT t FROM TcbsUserPhone t")
public class TcbsUserPhone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TCBS_USER_PHONE_ID_GENERATOR", sequenceName = "TCBS_USER_PHONE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_USER_PHONE_ID_GENERATOR")
	private Long id;

	private String phone;

	@Column(name = "PHONE_TYPE")
	private Integer phoneType = 0;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	@JsonBackReference(value = "user-phone")
	private TcbsUser user;

	public TcbsUserPhone() {
	}

	public TcbsUserPhone(String str) {
		try {
			TcbsUserPhone phone = new ObjectMapper().readValue(str, TcbsUserPhone.class);
			this.id = phone.getId();
			this.phone = phone.getPhone();
			this.phoneType = phone.getPhoneType();
			System.out.println("HERE 3");
		} catch (Exception ex) {
		}
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getPhoneType() {
		return this.phoneType;
	}

	public void setPhoneType(Integer phoneType) {
		this.phoneType = phoneType;
	}

	public TcbsUser getUser() {
		return user;
	}

	public void setUser(TcbsUser user) {
		this.user = user;
	}

	public static TcbsUserPhone valueOf(String str) {
		try {
			System.out.println("HERE");
			TcbsUserPhone phone = new ObjectMapper().readValue(str, TcbsUserPhone.class);
			return phone;
		} catch (Exception ex) {
			return null;
		}
	}
}