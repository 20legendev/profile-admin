package vn.tcbs.cas.profile.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the TCBS_ADDRESS_DIVISION database table.
 * 
 */
@Entity
@Table(name = "TCBS_ADDRESS_DIVISION")
@NamedQuery(name = "TcbsAddressDivision.findAll", query = "SELECT t FROM TcbsAddressDivision t")
public class TcbsAddressDivision implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "DIVISION_CODE")
	private String divisionCode;

	@Column(name = "DIVISION_LEVEL")
	private Long divisionLevel;

	@Column(name = "DIVISION_NAME")
	private String divisionName;

	@Column(name = "DIVISION_NAME_EN")
	private String divisionNameEn;

	@Id
	@SequenceGenerator(name = "TCBS_ADDRESS_DIVISION_GENERATOR", sequenceName = "TCBS_ADDRESS_DIVISION_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_ADDRESS_DIVISION_GENERATOR")
	private Long id;

	@Column(name = "PARENT_ID")
	private Long parentId;

	private Integer ranking;

	public Integer getRanking() {
		return ranking;
	}

	public void setRanking(Integer ranking) {
		this.ranking = ranking;
	}

	public TcbsAddressDivision() {
	}

	public String getDivisionCode() {
		return this.divisionCode;
	}

	public void setDivisionCode(String divisionCode) {
		this.divisionCode = divisionCode;
	}

	public Long getDivisionLevel() {
		return this.divisionLevel;
	}

	public void setDivisionLevel(Long divisionLevel) {
		this.divisionLevel = divisionLevel;
	}

	public String getDivisionName() {
		return this.divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public String getDivisionNameEn() {
		return this.divisionNameEn;
	}

	public void setDivisionNameEn(String divisionNameEn) {
		this.divisionNameEn = divisionNameEn;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return this.parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

}