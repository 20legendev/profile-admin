package vn.tcbs.cas.profile.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the TCBS_ERROR_LOG database table.
 * 
 */
@Entity
@Table(name = "TCBS_ERROR_LOG")
@NamedQuery(name = "TcbsErrorLog.findAll", query = "SELECT t FROM TcbsErrorLog t")
public class TcbsErrorLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "\"DATA\"")
	private String data;

	@Column(name = "ERROR_TYPE")
	private String errorType;

	@Id
	@SequenceGenerator(name = "TCBS_ERROR_LOG_ID_GENERATOR", sequenceName = "TCBS_ERROR_LOG_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_ERROR_LOG_ID_GENERATOR")
	private Long id;

	private Date since;

	private Integer status;

	public TcbsErrorLog() {
	}

	public TcbsErrorLog(String type, String data, Integer status) {
		this.errorType = type;
		this.data = data;
		this.status = status;
		this.since = new Date();
	}

	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getErrorType() {
		return this.errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getSince() {
		return this.since;
	}

	public void setSince(Date since) {
		this.since = since;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}