package vn.tcbs.cas.profile.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import vn.tcbs.cas.profile.admin.dto.AdminSearchDTO;
import vn.tcbs.cas.profile.bond.BondService;
import vn.tcbs.cas.profile.dao.ApplicationDAO;
import vn.tcbs.cas.profile.dao.ApplicationUserDAO;
import vn.tcbs.cas.profile.dao.IdentificationDAO;
import vn.tcbs.cas.profile.dao.UserDAO;
import vn.tcbs.cas.profile.dao.UserQueueDAO;
import vn.tcbs.cas.profile.dto.AccountUpdateDTO;
import vn.tcbs.cas.profile.entity.TcbsAddress;
import vn.tcbs.cas.profile.entity.TcbsAddressDivision;
import vn.tcbs.cas.profile.entity.TcbsApplication;
import vn.tcbs.cas.profile.entity.TcbsApplicationUser;
import vn.tcbs.cas.profile.entity.TcbsBankAccount;
import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserEmail;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;
import vn.tcbs.cas.profile.entity.TcbsUserPhone;
import vn.tcbs.cas.profile.object.OpenAccountMessage;
import vn.tcbs.cas.profile.service.sync.UserSyncService;
import vn.tcbs.library.crypto.CryptoUtils;
import vn.tcbs.tool.base.Constant;
import vn.tcbs.tool.base.ResponseData;
import vn.tcbs.tool.base.TcbsUtils;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;
	@Autowired
	private UserQueueDAO userQueueDAO;

	@Autowired
	private IdentificationDAO identificationDAO;
	@Autowired
	private ApplicationDAO applicationDAO;
	@Autowired
	private ApplicationUserDAO applicationUserDAO;
	@Autowired
	private AddressService addressService;
	@Autowired
	private BondService bondService;
	@Autowired
	private UserSyncService userSyncService;

	private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	public List<TcbsUser> getAll(Integer page, Integer perpage, String email, String idNumber, String fullname,
			String phone) {
		return userDAO.find(page, perpage, email, idNumber, fullname, phone);
	}

	public List<TcbsUser> find(AdminSearchDTO searchDTO) {
		return userDAO.find(searchDTO);
	}

	public List<TcbsUser> findByEmail(String email) {
		return userDAO.findByEmail(email, null);
	}

	public TcbsUser getById(Long id) {
		return userDAO.getById(id);
	}

	public void update(TcbsUser user) {
		userDAO.update(user);
		try {
			// logger.info("Update sync: {}", user.getId());
			userSyncService.addSyncToFlex(user.getId(), 0);
			bondService.sendToBond(user);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public Boolean isRegistered(String idNumber, String username) {
		List<TcbsIdentification> idents = identificationDAO.getByIdNumber(idNumber);
		if (idents != null && idents.size() > 0) {
			if (username != null) {
				List<TcbsUser> users = userDAO.getByUsername(username);
				if (users.size() > 0) {
					return true;
				}
				return false;
			}
			return true;
		}
		return false;
	}

	public TcbsUser getUserByIdNumber(String idNumber) {
		List<TcbsUser> lu = userDAO.getByIdNumber(idNumber, 1);
		if (lu.size() == 0)
			return null;
		return lu.get(0);
	}

	public TcbsUser getUserByMainPhone(String phone) {
		List<TcbsUser> lu = userDAO.getByLoginPhone(phone);
		if (lu.size() == 0)
			return null;
		return lu.get(0);
	}

	public List<TcbsUser> getUsersByIdNumber(String idNumber, Integer status) {
		return userDAO.getByIdNumber(idNumber, status);
	}

	public TcbsIdentification getByIdNumber(String idNumber) {
		List<TcbsIdentification> idents = identificationDAO.getByIdNumber(idNumber);
		if (idents != null && idents.size() > 0) {
			return idents.get(0);
		}
		return null;
	}

	public void add(TcbsUser user) {
		if (user.getCreatedDate() == null) {
			user.setCreatedDate(new Date());
		}
		user.setUpdatedDate(new Date());
		userDAO.add(user);
		bondService.sendToBond(user);
	}

	public boolean addWithDuplicateCheck(TcbsUser user) {
		if (isEmailDuplicated(user.getEmail(), null) || isPhoneDuplicated(user.getPhone(), null)) {
			return false;
		}
		if (user.getIdentifications().size() > 0) {
			if (isIdNumberDuplicated(user.getIdentifications().get(0).getIdNumber(), null)) {
				return false;
			}
		}
		add(user);
		return true;
	}

	public Integer checkApp(String idNumber, String appName) {
		List<TcbsUser> users = userDAO.getByIdNumber(idNumber, 1);
		if (users == null || users.size() == 0) {
			return 2;
		}
		TcbsApplication app = applicationDAO.getAppByName(appName);
		if (app == null) {
			return 3;
		}
		Long appId = app.getAppid();
		List<TcbsApplicationUser> data = applicationUserDAO.get(users.get(0).getId(), appId);

		if (data != null && data.size() > 0)
			return 1;
		return 0;
	}

	public Integer addAppUser(String idNumber, String appName, String inAppId) {
		List<TcbsUser> users = userDAO.getByIdNumber(idNumber, 1);
		if (users.size() == 0) {
			return -1;
		}
		TcbsApplication app = applicationDAO.getAppByName(appName);
		if (app == null) {
			return -2;
		}
		TcbsApplicationUser appUser = new TcbsApplicationUser();
		appUser.setUserAppId(inAppId);
		appUser.setApps(app);
		TcbsUser user = null;

		for (int i = 0; i < users.size(); i++) {
			user = users.get(i);
			List<TcbsApplicationUser> list = applicationUserDAO.get(user.getId(), app.getAppid());
			if (list.size() == 0) {
				appUser.setUser(users.get(i));
				applicationUserDAO.add(appUser);
			} else {
				for (int j = 0; j < list.size(); j++) {
					appUser = list.get(j);
					appUser.setUserAppId(inAppId);
					applicationUserDAO.update(appUser);
				}
			}
		}
		return 0;
	}

	public Integer updateUserApp(String idNumber, String appName, String inAppId) {
		List<TcbsUser> users = userDAO.getByIdNumber(idNumber, 1);
		if (users == null || users.size() == 0) {
			return -1;
		}
		TcbsApplication app = applicationDAO.getAppByName(appName);
		if (app == null) {
			return -2;
		}
		TcbsApplicationUser appUser = applicationUserDAO.getByUserAndAppId(users.get(0).getId(), app.getAppid());
		if (appUser == null) {
			return this.addAppUser(idNumber, appName, inAppId);
		}
		appUser.setUserAppId(inAppId);
		applicationUserDAO.update(appUser);
		return 0;
	}

	public List<TcbsUser> listAll() {
		return userDAO.listAll();
	}

	public String getInAppId(String idNumber, String appName) {
		TcbsApplicationUser appUser = this.getApplicationUser(idNumber, appName);
		if (appUser != null) {
			return appUser.getUserAppId();
		}
		return null;
	}

	public String getInAppIdByEmail(String email, String appName) {
		TcbsApplicationUser appUser = this.getApplicationUserByEmail(email, appName);
		if (appUser != null) {
			return appUser.getUserAppId();
		}
		return null;
	}

	public TcbsApplicationUser getApplicationUser(String idNumber, String appName) {
		List<TcbsApplicationUser> data = applicationUserDAO.getApplicationUser(idNumber, appName);
		if (data.size() > 0) {
			return data.get(0);
		}
		return null;
	}

	public TcbsApplicationUser getApplicationUserByEmail(String email, String appName) {
		List<TcbsApplicationUser> data = applicationUserDAO.getApplicationUserByEmail(email, appName);
		if (data.size() > 0) {
			return data.get(0);
		}
		return null;
	}

	public TcbsApplicationUser getApplicationUserByPhone(String phone, String appName) {
		List<TcbsApplicationUser> data = applicationUserDAO.getApplicationUserByPhone(phone, appName);
		if (data.size() > 0) {
			return data.get(0);
		}
		return null;
	}

	public TcbsUser getByAppId(String appName, String inAppId) {
		// TODO: change to join
		TcbsApplication app = applicationDAO.getAppByName(appName);
		if (app == null) {
			return null;
		}
		TcbsApplicationUser appUser = applicationUserDAO.getByApp(app.getAppid(), inAppId);
		if (appUser == null) {
			return null;
		}
		return userDAO.getById(appUser.getUser().getId());
	}

	public Boolean checkLoginEmail(String email) {
		return userDAO.checkLoginEmail(email);
	}

	public TcbsUser getByMainEmail(String email) {
		List<TcbsUser> users = userDAO.findByEmail(email, null);
		return users.size() > 0 ? users.get(0) : null;
	}

	public TcbsUser buildUserFromRequest(OpenAccountMessage msg, TcbsIdentification ident) {
		TcbsUser user = new TcbsUser();
		user.setFirstname(msg.getFirstname());
		user.setLastname(msg.getLastname());
		user.setBirthday(msg.getBirthday());
		// Address
		List<TcbsAddress> addressList = new ArrayList<TcbsAddress>();
		TcbsAddress address = new TcbsAddress();
		address.setUser(user);
		address.setAddress(msg.getAddress());
		List<TcbsAddressDivision> ads = addressService.getByCode(msg.getProvince());
		if (ads.size() > 0) {
			address.setDivision2(ads.get(0));
		}
		addressList.add(address);
		user.setAddress(addressList);

		// Email
		List<TcbsUserEmail> uel = new ArrayList<TcbsUserEmail>();
		TcbsUserEmail ue = new TcbsUserEmail();
		ue.setEmail(msg.getEmail());
		ue.setUser(user);
		uel.add(ue);
		user.setEmails(uel);
		user.setEmail(msg.getEmail());

		List<TcbsIdentification> il = new ArrayList<TcbsIdentification>();
		TcbsIdentification ti = new TcbsIdentification();
		if (ident != null) {
			ti.setIdDate(ident.getIdDate());
			ti.setIdPlace(ident.getIdPlace());
		} else {
			ti.setIdDate(msg.getIddate());
			ti.setIdPlace(msg.getIdplace());
		}
		ti.setIdNumber(msg.getIdnumber());
		ti.setIdType("I");
		ti.setUser(user);
		il.add(ti);
		user.setIdentifications(il);
		user.setGender(msg.getGender());

		// Phone
		List<TcbsUserPhone> lp = new ArrayList<TcbsUserPhone>();
		TcbsUserPhone up = new TcbsUserPhone();
		up.setPhone(msg.getPhone());
		up.setPhoneType(1);
		up.setUser(user);
		lp.add(up);
		user.setPhones(lp);
		user.setPhone(msg.getPhone());

		// Bank Account
		if (msg.getBankaccountnumber() != null) {
			List<TcbsBankAccount> lb = new ArrayList<TcbsBankAccount>();
			TcbsBankAccount ba = new TcbsBankAccount();
			ba.setBankAccountName(msg.getBankaccountname());
			ba.setBankAccountNo(msg.getBankaccountnumber());
			ba.setBankBranch(msg.getBankbranch());
			ba.setBankCode(msg.getBankcode());
			ba.setBankName(msg.getBankname());
			ba.setUser(user);
			lb.add(ba);
			user.setBanks(lb);
		}

		// Wait for job to get Flex open status
		user.setStatus(0);

		logger.info("PASSWORD: {}", msg.getPassword());

		if (msg.getPassword() != null && !msg.getPassword().equals("")) {
			user.setPassword(CryptoUtils.encryptPassword(msg.getPassword(), Constant.saltKey));
		}
		logger.info("PASSWORD: {}", user.getPassword());
		user.setCreatedDate(new Date());
		user.setUpdatedDate(new Date());
		return user;
	}

	public TcbsUser buildUserFromQueue(TcbsUserOpenaccountQueue queue) {
		TcbsUser user = new TcbsUser();
		user.setFirstname(queue.getFirstname());
		user.setLastname(queue.getLastname());
		user.setBirthday(queue.getBirthday());
		List<TcbsAddress> addressList = new ArrayList<TcbsAddress>();
		TcbsAddress address = new TcbsAddress();
		address.setUser(user);
		address.setAddress(queue.getAddress());
		List<TcbsAddressDivision> ads = addressService.getByCode(queue.getProvince());
		if (ads.size() > 0) {
			address.setDivision2(ads.get(0));
		}
		addressList.add(address);
		user.setAddress(addressList);

		List<TcbsUserEmail> uel = new ArrayList<TcbsUserEmail>();
		TcbsUserEmail ue = new TcbsUserEmail();
		ue.setEmail(queue.getEmail());
		ue.setUser(user);
		uel.add(ue);
		user.setEmails(uel);
		user.setEmail(queue.getEmail());

		List<TcbsIdentification> il = new ArrayList<TcbsIdentification>();
		TcbsIdentification ti = new TcbsIdentification();
		ti.setIdDate(queue.getIddate());
		ti.setIdPlace(queue.getIdplace());
		ti.setIdNumber(queue.getIdnumber());
		ti.setIdType("I");
		ti.setUser(user);
		il.add(ti);
		user.setIdentifications(il);
		user.setGender(queue.getGender());

		List<TcbsUserPhone> lp = new ArrayList<TcbsUserPhone>();
		TcbsUserPhone up = new TcbsUserPhone();
		up.setPhone(queue.getPhone());
		up.setPhoneType(1);
		up.setUser(user);
		lp.add(up);
		user.setPhones(lp);
		user.setPhone(queue.getPhone());

		if (queue.getBankaccountnumber() != null) {
			List<TcbsBankAccount> lb = new ArrayList<TcbsBankAccount>();
			TcbsBankAccount ba = new TcbsBankAccount();
			ba.setBankAccountName(queue.getBankaccountname());
			ba.setBankAccountNo(queue.getBankaccountnumber());
			ba.setBankBranch(queue.getBankbranch());
			ba.setBankCode(queue.getBankcode());
			ba.setBankName(queue.getBankname());
			ba.setBankProvince(queue.getBankprovince());
			ba.setUser(user);
			lb.add(ba);
			user.setBanks(lb);
		}
		user.setStatus(0);
		user.setPassword(queue.getPassword());
		user.setCreatedDate(new Date());
		user.setUpdatedDate(new Date());
		return user;
	}

	public TcbsUserOpenaccountQueue buildUserQueueFromRequest(OpenAccountMessage msg, TcbsIdentification ident) {
		TcbsUserOpenaccountQueue user = new TcbsUserOpenaccountQueue();
		user.setFirstname(msg.getFirstname());
		user.setLastname(msg.getLastname());
		user.setBirthday(msg.getBirthday());
		user.setEmail(msg.getEmail().toLowerCase());
		user.setIdnumber(msg.getIdnumber());
		user.setIddate(ident != null ? ident.getIdDate() : msg.getIddate());
		user.setIdplace(ident != null ? ident.getIdPlace() : msg.getIdplace());
		user.setPhone(msg.getPhone());
		user.setReferenceid(msg.getReferenceid());
		user.setGender(msg.getGender());
		user.setAddress(msg.getAddress());
		user.setProvince(msg.getProvince());
		user.setBankaccountnumber(msg.getBankaccountnumber());
		user.setBankaccountname(msg.getBankaccountname());
		user.setBankbranch(msg.getBankbranch());
		user.setBankcode(msg.getBankcode());
		user.setBankname(msg.getBankname());
		String password = msg.getPassword();
		if (password == null) {
			password = TcbsUtils.randomPassword(6);
			user.setPasswordblank(password);
		}
		if (msg.getReferer() != null) {
			user.setReferer(msg.getReferer());
		}
		user.setPassword(CryptoUtils.encryptPassword(password, Constant.saltKey));
		user.setPasswordmd5(TcbsUtils.MD5(password));
		user.setPasswordblank(password);
		user.setBankprovince(msg.getBankplace());
		return user;
	}

	public Integer isCasUser(String idNumber, String email, String phone) {
		if (idNumber != null && userDAO.find(idNumber, 1).size() > 0) {
			return 1;
		}
		if (email != null && userDAO.findByEmail(email, 1).size() > 0) {
			return 2;
		}
		if (phone != null && userDAO.findByPhone(phone, 1).size() > 0) {
			return 3;
		}
		return 0;
	}

	public int findCasUser(String idNumber, String email, Integer status) {
		List<TcbsIdentification> li = userDAO.find(idNumber, status);
		if (li.size() > 0) {
			return 1;
		} else if (userDAO.findByEmail(email, status).size() > 0) {
			return 2;
		}
		return 0;
	}

	@Override
	public void updateByIdNumber(String idNumber, TcbsUser user) {
		TcbsUser dbUser = this.getUserByIdNumber(idNumber);
		Boolean found = true;
		if (dbUser == null) {
			found = false;
			dbUser = user;
		} else {
			dbUser.setBirthday(user.getBirthday());
			dbUser.setEmail(user.getEmail());
			dbUser.setFirstname(user.getFirstname());
			dbUser.setGender(user.getGender());
			dbUser.setLastname(user.getLastname());
			dbUser.setPassword(user.getPassword());
			dbUser.setProfilePicture(user.getProfilePicture());
			dbUser.setRelationship(user.getRelationship());
			dbUser.setUsername(user.getUsername());
			// TODO: update more
		}
		dbUser.setUpdatedDate(new Date());
		if (!found) {
			userDAO.add(dbUser);
		} else {
			userDAO.update(dbUser);
		}
	}

	@Override
	public TcbsUser activeAccount(String referenceid, String idNumber) {
		List<TcbsUserOpenaccountQueue> lu = new ArrayList<TcbsUserOpenaccountQueue>();
		if (referenceid != null) {
			lu = userQueueDAO.getByReferenceId(referenceid);
		} else if (idNumber != null) {
			lu = userQueueDAO.getByIdNumberAndStatus(idNumber, 0);
		}
		if (lu.size() == 0)
			return null;
		TcbsUserOpenaccountQueue queuedUser = lu.get(0);

		TcbsUser user = getUserByIdNumber(queuedUser.getIdnumber());
		if (user == null) {
			user = buildUserFromQueue(queuedUser);
			user.setStatus(1);
			// user.setUsercode(TcbsUtils.generateUserCode(userDAO,
			// user.getPhone(), null));
			userDAO.add(user);
		} else {
			user.setFirstname(queuedUser.getFirstname());
			user.setLastname(queuedUser.getLastname());
			user.setBirthday(queuedUser.getBirthday());
			user.setGender(queuedUser.getGender());

			// ADDITIONAL DETAIL
			if (!queuedUser.getEmail().equals(user.getEmail())) {
				user.setEmail(queuedUser.getEmail());
				List<TcbsUserEmail> le = user.getEmails();
				TcbsUserEmail e = new TcbsUserEmail();
				e.setEmail(queuedUser.getEmail());
				e.setUser(user);
				le.add(e);
				user.setEmails(le);
			}
			if (!queuedUser.getPhone().equals(user.getPhone())) {
				user.setPhone(queuedUser.getPhone());
				List<TcbsUserPhone> lp = user.getPhones();
				TcbsUserPhone p = new TcbsUserPhone();
				p.setPhone(queuedUser.getPhone());
				p.setUser(user);
				lp.add(p);
				user.setPhones(lp);
			}
			List<TcbsAddress> lp = user.getAddress();
			TcbsAddress p = new TcbsAddress();
			p.setAddress(queuedUser.getAddress());
			p.setUser(user);
			lp.add(p);
			user.setAddress(lp);
			if (queuedUser.getBankaccountnumber() != null) {
				List<TcbsBankAccount> lb = user.getBanks();
				TcbsBankAccount b = new TcbsBankAccount();
				b.setBankAccountName(queuedUser.getBankaccountname());
				b.setBankAccountNo(queuedUser.getBankaccountnumber());
				b.setBankBranch(queuedUser.getBankbranch());
				b.setBankCode(queuedUser.getBankcode());
				b.setBankName(queuedUser.getBankname());
				b.setBankProvince(queuedUser.getBankprovince());
				b.setUser(user);
				lb.add(b);
				user.setBanks(lb);
			}
			user.setPassword(queuedUser.getPassword());
			update(user);
		}
		queuedUser.setUser(user);
		queuedUser.setStatus(1);
		userQueueDAO.update(queuedUser);
		bondService.sendToBond(user);
		return user;
	}

	@Override
	public TcbsUserOpenaccountQueue getByReferenceId(String referenceid) {
		List<TcbsUserOpenaccountQueue> lu = userQueueDAO.getByReferenceId(referenceid);
		return lu.size() > 0 ? lu.get(0) : null;
	}

	public Boolean isIdNumberDuplicated(String idNumber, Long userId) {
		List<TcbsIdentification> li = identificationDAO.isDuplicated(idNumber, userId);
		return li.size() > 0 ? true : false;
	}

	public Boolean isEmailDuplicated(String email, Long userId) {
		return userDAO.isEmailDuplicated(email, userId);
	}

	public Boolean isPhoneDuplicated(String phone, Long userId) {
		return userDAO.isPhoneDuplicated(phone, userId);
	}

	public Boolean isUsernameDuplicated(String username, Long userId) {
		return userDAO.isUsernameDuplicated(username, userId);
	}

	@Override
	public ResponseData<String> doUpdate(AccountUpdateDTO accUpdate, TcbsUser user) {
		Long userId = user.getId();
		if (accUpdate.getFullname() != null) {
			String fullName = accUpdate.getFullname();
			int pos = fullName.lastIndexOf(" ");
			user.setFirstname(pos >= 0 ? fullName.substring(pos + 1) : fullName);
			user.setLastname(pos > 0 ? fullName.substring(0, pos) : " ");
		}
		if (accUpdate.getBirthday() != null) {
			SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
			try {
				user.setBirthday(sf.parse(accUpdate.getBirthday()));
			} catch (ParseException e) {
				return new ResponseData<String>(2, "", "DATE_FORMAT_ERROR");
			}
		}
		if (accUpdate.getGender() != null) {
			user.setGender(accUpdate.getGender());
		}
		if (accUpdate.getEmail() != null) {
			String email = accUpdate.getEmail();
			if (userDAO.isEmailDuplicated(email, userId)) {
				return new ResponseData<String>(3, "", "EMAIL_DUPLICATED");
			}
			user.setEmail(email);
		}
		if (accUpdate.getPhone() != null) {
			if (userDAO.isPhoneDuplicated(accUpdate.getPhone(), userId)) {
				return new ResponseData<String>(4, "", "PHONE_DUPLICATED");
			}
			String phone = accUpdate.getPhone();
			user.setPhone(phone);
		}
		if (accUpdate.getAddress() != null) {
			if (user.getAddress().size() > 0) {
				user.getAddress().get(0).setAddress(accUpdate.getAddress());
			} else {
				List<TcbsAddress> la = new ArrayList<TcbsAddress>();
				TcbsAddress a = new TcbsAddress();
				a.setAddress(accUpdate.getAddress());
				a.setUser(user);
				la.add(a);
				user.setAddress(la);
			}
		}
		if (accUpdate.getIdnumber() != null && !accUpdate.getIdnumber().equals("")) {
			SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
			Date idDate = null;
			try {
				idDate = sf.parse(accUpdate.getIddate());
			} catch (Exception ex) {
				return new ResponseData<String>(1, "", "IDDATE_ERROR");
			}
			logger.info("ID DATE {}", idDate);
			if (this.isIdNumberDuplicated(accUpdate.getIdnumber(), user.getId())) {
				return new ResponseData<String>(1, "", "IDNUMBER_DUPLICATED");
			}
			if (user.getIdentifications().size() > 0) {
				user.getIdentifications().get(0).setIdNumber(accUpdate.getIdnumber());
				user.getIdentifications().get(0).setIdDate(idDate);
				user.getIdentifications().get(0).setIdPlace(accUpdate.getIdplace());
			} else {
				List<TcbsIdentification> li = new ArrayList<TcbsIdentification>();
				TcbsIdentification i = new TcbsIdentification();
				i.setIdDate(idDate);
				i.setIdPlace(accUpdate.getIdplace());
				i.setIdNumber(accUpdate.getIdnumber());
				i.setUser(user);
				li.add(i);
				user.setIdentifications(li);
			}
		}
		this.update(user);

		try {
			logger.info("USER {}", JsonService.getInstance().getObjectMapper().writeValueAsString(user));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ResponseData<String>(0, "", "SUCCESS");
	}

	public List<Integer> isFlexTradeReady(TcbsUser user) {
		List<Integer> result = new ArrayList<Integer>();
		if (user.getEmail() == null) {
			result.add(1);
		}
		if (user.getPhone() == null) {
			result.add(2);
		}
		if (user.getIdentifications().size() == 0) {
			result.add(3);
		}
		if (user.getAddress().size() == 0) {
			result.add(4);
		}
		return result;
	}

	@Override
	public void remove(TcbsUser user) {
		userDAO.remove(user);
	}

	@Override
	public List<String> splitName(String fullname) {
		List<String> list = new ArrayList<String>();
		if (fullname != null) {
			int pos = fullname.lastIndexOf(" ");
			list.add(pos >= 0 ? fullname.substring(0, pos) : fullname);
			list.add(pos >= 0 ? fullname.substring(pos + 1, fullname.length()) : "");
		}
		return list;
	}

	public int updateBank(Long userId, List<TcbsBankAccount> data) {
		TcbsUser user = getById(userId);
		if (user == null)
			return -1;
		List<TcbsBankAccount> banks = user.getBanks();
		for (int i = 0; i < data.size(); i++) {
			if (data.get(i).getId().equals(-1L)) {
				TcbsBankAccount ba = new TcbsBankAccount();
				ba.setBankAccountName(data.get(i).getBankAccountName());
				ba.setBankAccountNo(data.get(i).getBankAccountNo());
				ba.setBankBranch(data.get(i).getBankBranch());
				ba.setBankCode(data.get(i).getBankCode());
				ba.setBankName(data.get(i).getBankName());
				ba.setBankProvince(data.get(i).getBankProvince());
				ba.setUser(user);
				banks.add(ba);
			} else {
				for (int j = 0; j < banks.size(); j++) {
					if (data.get(i).getId().equals(banks.get(j).getId())) {
						banks.get(j).setBankAccountName(data.get(i).getBankAccountName());
						banks.get(j).setBankAccountNo(data.get(i).getBankAccountNo());
						banks.get(j).setBankBranch(data.get(i).getBankBranch());
						banks.get(j).setBankCode(data.get(i).getBankCode());
						banks.get(j).setBankName(data.get(i).getBankName());
						banks.get(j).setBankProvince(data.get(i).getBankProvince());
					}
				}
			}
		}
		user.setBanks(banks);
		update(user);
		try {
			userSyncService.addSyncToFlex(user.getId(), 1);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return 0;
	}
}