package vn.tcbs.cas.profile.service;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;

import vn.tcbs.cas.profile.dto.AdminCheckIDDTO;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;

public interface UserQueueService {
	public TcbsUserOpenaccountQueue getUserByIdNumber(String idNumber);

	public TcbsUserOpenaccountQueue getByReferenceId(String referenceid);

	public void setActive(String referenceId, TcbsUser user);

	public boolean confirmContract(String referenceId);

	public void update(TcbsUserOpenaccountQueue queue);

	public void saveFromUser(TcbsUser user) throws JsonProcessingException;

	public List<TcbsUserOpenaccountQueue> getByIdNumber(String idNumber, Long Long);

	public List<TcbsUserOpenaccountQueue> listQueue(Integer page, Integer perpage, String email, String phone,
			String idNumber, String fullname);

	public List<TcbsUserOpenaccountQueue> getByReferer(Long referer);
	public List<TcbsUserOpenaccountQueue> getByRefererRestricted(Long referer);

	public void addOpenAccountQueue(TcbsUserOpenaccountQueue userQueue);
}