package vn.tcbs.cas.profile.service;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcbs.cas.profile.dao.UpdateLogDAO;
import vn.tcbs.cas.profile.entity.TcbsUpdateLog;

@Service("updateLogService")
public class UpdateLogServiceImpl implements UpdateLogService {

	@Autowired
	private UpdateLogDAO updateLogDAO;

	public void addNew(TcbsUpdateLog log) {
		log.setRequestDate(new Date());
		updateLogDAO.addNew(log);
	}

	public TcbsUpdateLog fromRequest(HttpServletRequest req, boolean confirm) {
		TcbsUpdateLog log = new TcbsUpdateLog();
		log.setActorId(Long.valueOf(req.getRemoteUser()));
		if (confirm) {
			log.setConfirmDate(new Date());
			log.setConfirmIp(req.getRemoteAddr());
			log.setConfirmMethod("DIRECT");
			log.setConfirmUa(req.getHeader("User-Agent"));
			log.setConfirmUser(Long.valueOf(req.getRemoteUser()));
		}
		log.setRequestDate(new Date());
		log.setRequestIp(req.getRemoteAddr());
		log.setRequestUa(req.getHeader("User-Agent"));
		log.setRequestUser(Long.valueOf(req.getRemoteUser()));
		return log;
	}
}
