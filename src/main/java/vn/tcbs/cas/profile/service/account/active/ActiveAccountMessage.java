package vn.tcbs.cas.profile.service.account.active;

import com.fasterxml.jackson.annotation.JsonFilter;

@JsonFilter("myFilter")
public class ActiveAccountMessage {
	private String phone;
	private String referenceid;
	private String email;
	private int status = 0;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getReferenceid() {
		return referenceid;
	}

	public void setReferenceid(String referenceid) {
		this.referenceid = referenceid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		String delim = "|";
		builder.append("[Phone=");
		builder.append(phone);
		builder.append(delim);
		builder.append("Referenceid=");
		builder.append(referenceid);
		builder.append(delim);
		builder.append("Email=");
		builder.append(email);
		builder.append(delim);
		builder.append("Status=");
		builder.append(status);
		builder.append("]");
		return builder.toString();
	}
}
