package vn.tcbs.cas.profile.service.account.active;

import vn.tcbs.cas.exception.AccountActivatedException;
import vn.tcbs.cas.exception.ActiveAccountException;
import vn.tcbs.cas.exception.TokenNotFoundException;

public interface ActiveAccountService {
	// public void sendRequest(ActiveAccountMessage activeAccountMessage) throws ActiveAccountException, Exception;

	public String activeAccount(String token) throws ActiveAccountException,AccountActivatedException, TokenNotFoundException, Exception;

	public int confirmActive(String validationCode,String transactionID) throws ActiveAccountException, Exception;
}