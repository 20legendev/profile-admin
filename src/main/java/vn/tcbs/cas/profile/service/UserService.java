package vn.tcbs.cas.profile.service;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;

import vn.tcbs.cas.profile.admin.dto.AdminSearchDTO;
import vn.tcbs.cas.profile.dto.AccountUpdateDTO;
import vn.tcbs.cas.profile.entity.TcbsApplicationUser;
import vn.tcbs.cas.profile.entity.TcbsBankAccount;
import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;
import vn.tcbs.cas.profile.object.OpenAccountMessage;
import vn.tcbs.tool.base.ResponseData;

public interface UserService {
	// Start Admin
	public List<TcbsUser> getAll(Integer page, Integer perpage, String email, String idNumber, String fullname,
			String phone);

	public List<TcbsUser> find(AdminSearchDTO searchDTO);
	public int updateBank(Long userId, List<TcbsBankAccount> data);
	// End Admin

	public TcbsUser buildUserFromRequest(OpenAccountMessage msg, TcbsIdentification ident);

	public TcbsUserOpenaccountQueue buildUserQueueFromRequest(OpenAccountMessage msg, TcbsIdentification ident)
			throws JsonProcessingException;

	public TcbsUser buildUserFromQueue(TcbsUserOpenaccountQueue queue);

	public Integer isCasUser(String idNumber, String email, String phone);

	public int findCasUser(String idNumber, String email, Integer status);

	public List<TcbsUser> findByEmail(String email);

	public TcbsUser getById(Long id);

	public TcbsUser getByAppId(String appName, String appId);

	public TcbsUser getUserByIdNumber(String idNumber);

	public List<TcbsUser> getUsersByIdNumber(String idNumber, Integer status);

	public TcbsUser getByMainEmail(String email);
	public TcbsUser getUserByMainPhone(String phone);

	public void update(TcbsUser user);

	public void add(TcbsUser user);

	public boolean addWithDuplicateCheck(TcbsUser user);

	public Boolean isRegistered(String idNumber, String username);

	public TcbsIdentification getByIdNumber(String idNumber);

	public Integer checkApp(String idNumber, String appName);

	public String getInAppId(String idNumber, String appName);

	public Integer addAppUser(String idNumber, String appName, String appId);

	public Integer updateUserApp(String idNumber, String appName, String inAppId);

	public String getInAppIdByEmail(String email, String appName);

	public List<TcbsUser> listAll();

	public Boolean checkLoginEmail(String email);

	public TcbsApplicationUser getApplicationUser(String idNumber, String appName);

	public TcbsApplicationUser getApplicationUserByEmail(String email, String appName);

	public TcbsApplicationUser getApplicationUserByPhone(String phone, String appName);

	public void updateByIdNumber(String idNumber, TcbsUser user);

	public TcbsUserOpenaccountQueue getByReferenceId(String referenceid);

	public TcbsUser activeAccount(String referenceid, String idNumber);

	public Boolean isIdNumberDuplicated(String idNumber, Long userId);

	public Boolean isEmailDuplicated(String email, Long userId);

	public Boolean isPhoneDuplicated(String email, Long userId);

	public Boolean isUsernameDuplicated(String username, Long userId);

	public ResponseData<String> doUpdate(AccountUpdateDTO accUpdate, TcbsUser user);

	public List<Integer> isFlexTradeReady(TcbsUser user);

	public void remove(TcbsUser user);
	public List<String> splitName(String fullname);

}