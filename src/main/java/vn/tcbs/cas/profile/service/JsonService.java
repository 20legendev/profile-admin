package vn.tcbs.cas.profile.service;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("jsonService")
public class JsonService {

	private ObjectMapper om;

	public JsonService() {
		om = new ObjectMapper();
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
	}

	public ObjectMapper getObjectMapper() {
		return om;
	}

	private static JsonService singleton;

	public static JsonService getInstance() {
		if (singleton == null) {
			singleton = new JsonService();
		}
		return singleton;
	}
}