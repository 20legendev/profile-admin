package vn.tcbs.cas.profile.service;

import vn.tcbs.cas.profile.entity.TcbsUserPhone;

public interface PhoneService {
	public void deletePhoneByUser(Long userId);
	public TcbsUserPhone getById(Long id);
}