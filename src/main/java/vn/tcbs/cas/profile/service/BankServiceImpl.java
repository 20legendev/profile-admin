package vn.tcbs.cas.profile.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.cas.profile.dao.BankDAO;
import vn.tcbs.cas.profile.entity.TcbsBankAccount;
import vn.tcbs.cas.profile.object.Crbbanklist;
import vn.tcbs.tool.base.ResponseData;

@Service("bankService")
public class BankServiceImpl implements BankService {

	@Value("${url.api.flexservice}")
	private String bankServiceUrl;
	@Autowired
	private BankDAO bankDAO;

	public List<TcbsBankAccount> getByUser(Long userId) {
		return bankDAO.getBankByUser(userId);
	}

	@Override
	public List<Crbbanklist> listAll(String province) throws ClientProtocolException, IOException {
		HttpClient client = HttpClientBuilder.create().build();

		HttpGet req = new HttpGet(bankServiceUrl + "bank/by-province/" + province);
		HttpResponse response = client.execute(req);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		ObjectMapper mapper = new ObjectMapper();
		ResponseData<List<Crbbanklist>> banks = mapper.readValue(result.toString(), ResponseData.class);
		if (banks.getStatus() == 0) {
			return banks.getData();
		}
		return null;
	}

	public String byProvinceString(String province) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(bankServiceUrl + "bank/by-province/" + province);
		try {
			HttpResponse response = client.execute(get);
			String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
			return responseString;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public HashMap<String, Object> getByBankCode(String bankCode) {
		String url = bankServiceUrl + "bank/by-code/" + bankCode;
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet req = new HttpGet(url);
		HttpResponse response;
		try {
			response = client.execute(req);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			ObjectMapper mapper = new ObjectMapper();
			ResponseData<HashMap> banks = mapper.readValue(result.toString(), ResponseData.class);
			if (banks.getStatus() == 0) {
				return banks.getData();
			}
		} catch (IOException e) {
			return null;
		}
		return null;
	}

	@Override
	public void delete(Long id) {
		bankDAO.remove(id);
	}
	public TcbsBankAccount getById(Long id){
		return bankDAO.getById(id);
	}
}
