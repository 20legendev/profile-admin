package vn.tcbs.cas.profile.service;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsAddressDivision;

public interface AddressService {
	public List<TcbsAddressDivision> getByParentName(String parentName);
	public List<TcbsAddressDivision> getByCode(String code);
}