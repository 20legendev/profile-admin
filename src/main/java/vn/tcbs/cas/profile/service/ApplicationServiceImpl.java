package vn.tcbs.cas.profile.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcbs.cas.profile.dao.ApplicationDAO;
import vn.tcbs.cas.profile.dao.ApplicationUserDAO;
import vn.tcbs.cas.profile.entity.TcbsApplication;
import vn.tcbs.cas.profile.entity.TcbsApplicationUser;
import vn.tcbs.cas.profile.entity.TcbsUser;

@Service("applicationService")
public class ApplicationServiceImpl implements ApplicationService {

	@Autowired
	private ApplicationDAO applicationDAO;
	@Autowired
	private ApplicationUserDAO applicationUserDAO;

	public TcbsApplication getByName(String name) {
		return applicationDAO.getAppByName(name);
	}

	@Override
	public List<TcbsApplicationUser> getUserInAppByUserId(Long userId, String appName) {
		return applicationUserDAO.getByUserAndAppName(userId, appName);
	}

	public String getCustodyCd(Long userId) {
		List<TcbsApplicationUser> result = applicationUserDAO.getByUserAndAppName(userId, "FLEX");
		return result.size() > 0 ? result.get(0).getUserAppId() : null;
	}

	@Override
	public void updateApplicationUser(TcbsApplicationUser au) {
		applicationUserDAO.update(au);
	}

	@Override
	public Boolean isApplicationUserExist(Long userId, String appName) {
		return applicationUserDAO.getByUserAndAppName(userId, appName).size() > 0;
	}

	@Override
	public void addUserInAppByUserId(TcbsUser user, TcbsApplication app, String inAppId) {
		if (isApplicationUserExist(user.getId(), app.getAppname())) {
			List<TcbsApplicationUser> lau = getUserInAppByUserId(user.getId(), app.getAppname());
			for (int i = 0; i < lau.size(); i++) {
				TcbsApplicationUser au = lau.get(i);
				au.setUserAppId(inAppId);
				updateApplicationUser(au);
			}
			return;
		}
		TcbsApplicationUser au = new TcbsApplicationUser();
		au.setUser(user);
		au.setApps(app);
		au.setUserAppId(inAppId);
		applicationUserDAO.add(au);
	}

	@Override
	public void addUserInAppByUserId(TcbsUser user, String appName, String inAppId) {
		TcbsApplication app = getByName(appName);
		if (app != null) {
			addUserInAppByUserId(user, app, inAppId);
		}
	}

}