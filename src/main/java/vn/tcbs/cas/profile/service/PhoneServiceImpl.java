package vn.tcbs.cas.profile.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcbs.cas.profile.dao.PhoneDAO;
import vn.tcbs.cas.profile.entity.TcbsUserPhone;

@Service("phoneService")
public class PhoneServiceImpl implements PhoneService {

	@Autowired
	private PhoneDAO phoneDAO;

	public void deletePhoneByUser(Long userId) {

	}

	public TcbsUserPhone getById(Long id) {
		return phoneDAO.getById(id);
	}

}