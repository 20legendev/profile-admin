/**
 * 
 */
package vn.tcbs.cas.profile.service.authentication.result;

/**
 * @author ttn
 *
 */
public interface Enumerable {
	public abstract int value();
}
