package vn.tcbs.cas.profile.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import vn.tcbs.cas.profile.dao.ForgotPasswordDAO;
import vn.tcbs.cas.profile.dao.UserDAO;
import vn.tcbs.cas.profile.entity.TcbsApplicationUser;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserForgotPassword;
import vn.tcbs.cas.profile.service.notification.NotificationService;
import vn.tcbs.library.crypto.CryptoUtils;
import vn.tcbs.tool.base.Constant;
import vn.tcbs.tool.base.TcbsUtils;

@Service("passwordService")
public class PasswordServiceImpl implements PasswordService {

	@Value("${cas.server.mypublic}")
	private String myurl;
	@Value("${queue.password.change.key}")
	private String changePasswordQueueKey;
	@Value("${sync.queue.exchangename}")
	private String exchangeName;
	@Autowired
	private AmqpTemplate rabbitmqTemplate;

	@Autowired
	private VelocityEngine velocityEngine;
	@Autowired
	private ForgotPasswordDAO forgotPasswordDAO;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private UserDAO userDAO;
	@Autowired
	private ApplicationService applicationService;

	private static Logger logger = LoggerFactory.getLogger(PasswordServiceImpl.class);

	public void findByEmail(String email) {
		List<TcbsUserForgotPassword> list = forgotPasswordDAO.findByEmail(email);
		System.out.println(list);
	}

	public Integer forgotPassword(String email) {
		try {
			List<TcbsUser> listUser = userDAO.findByEmail(email, null);
			if (listUser.size() == 0) {
				return 1;
			}
			int sent = 0;
			for (int i = 0; i < listUser.size(); i++) {
				TcbsUser user = listUser.get(i);
				String token = CryptoUtils.tokenGenerator(user.getEmail());
				forgotPasswordDAO.addNew(user.getId(), token);
				HashMap<String, Object> model = new HashMap<String, Object>();
				model.put("email", email);
				model.put("link",
						"https://" + myurl + "/xac-nhan-thay-doi-mat-khau.html?change_password_token=" + token);
				String emailText = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
						"META-INF/velocity/email/forgot-password.vm", "utf-8", model);
				sent = notificationService.sendEmail("", email, "", "[TechcomSecurities] Yêu cầu thay đổi mật khẩu",
						emailText, "");
			}
			return sent;
		} catch (Exception ex) {
			ex.printStackTrace();
			return 3;
		}
	}

	public TcbsUserForgotPassword verifyToken(String token) {
		List<TcbsUserForgotPassword> list = forgotPasswordDAO.checkToken(token);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	public void increaseAccessCount(TcbsUserForgotPassword fp) {
		fp.setAccessCount(fp.getAccessCount() + 1);
		forgotPasswordDAO.update(fp);
	}

	public void updatePassword(Long userId, String password) {
		String passwordHash = CryptoUtils.encryptPassword(password, Constant.saltKey);
		userDAO.updatePassword(userId, passwordHash);
		List<TcbsApplicationUser> custodyCd = applicationService.getUserInAppByUserId(userId, "FLEX");
		logger.info("Change_password_flex_found: {} user", custodyCd.size());
		if (custodyCd.size() > 0) {
			logger.info("PASSWORD_CUSTODY_SIZE: {}", custodyCd.size());
			String pwd = custodyCd.get(0).getUserAppId() + "|" + TcbsUtils.MD5(password);
			logger.info("SEND_PASSWORD_TO_FLEX: {}", pwd);
			send(exchangeName, changePasswordQueueKey, pwd);
			logger.info("SEND_PASSWORD_TO_FLEX_COMPLETED: {}", pwd);
		}else{
			logger.info("PASSWORD_NO_FLEX_ACCOUNT_FOUND");
		}
	}

	public void update(TcbsUserForgotPassword fp) {
		forgotPasswordDAO.update(fp);
	}

	@ManagedOperation
	public void send(String exchange, String key, String text) {
		rabbitmqTemplate.convertAndSend(exchange, key, text);
	}

}