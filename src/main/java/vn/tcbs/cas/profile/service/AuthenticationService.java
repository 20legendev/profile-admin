package vn.tcbs.cas.profile.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.jasig.cas.client.validation.Assertion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.cas.userdetails.AbstractCasAssertionUserDetailsService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import vn.tcbs.cas.profile.dao.UserDAO;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.permission.TcbsUserRole;
import vn.tcbs.cas.profile.permission.TcbsUserRoleDAO;
import vn.tcbs.tool.base.TcbsUtils;

@Service
public class AuthenticationService extends AbstractCasAssertionUserDetailsService {

	@Autowired
	private TcbsUserRoleDAO userRoleDAO;
	@Autowired
	private UserDAO userDAO;

	private static Logger logger = LoggerFactory.getLogger(AuthenticationService.class);

	@Override
	protected UserDetails loadUserDetails(Assertion assertion) {
		Long userId = null;
		if (assertion.getPrincipal().getAttributes().get("id") != null) {
			try {
				userId = (Long) assertion.getPrincipal().getAttributes().get("id");
			} catch (Exception ex) {
				userId = Long.valueOf((String) assertion.getPrincipal().getAttributes().get("id"));
			}
		} else {
			String email = (String) assertion.getPrincipal().getAttributes().get("email");
			List<TcbsUser> users = userDAO.getByMainEmail(email, 1);
			logger.info("{}", users.size());
			
			if (users.size() > 0) {
				userId = users.get(0).getId();
			} else {
				String fullname = (String) assertion.getPrincipal().getAttributes().get("fullname");
				TcbsUser user = new TcbsUser();
				int pos = fullname.lastIndexOf(" ");
				user.setFirstname(fullname.substring(pos + 1));
				user.setLastname(fullname.substring(0, pos));
				user.setEmail(email);
				user.setStatus(1);
				user.setTcbsid(TcbsUtils.generateUserCode(userDAO, null, null));
				userDAO.add(user);
				userId = user.getId();
			}
		}
		logger.info("GETTING_USER_ROLES: {}", userId);
		List<GrantedAuthority> listPermission = getRoles(userId);
		return (UserDetails) new User(String.valueOf(userId), "", listPermission);
	}

	private List<GrantedAuthority> getRoles(Long id) {
		List<TcbsUserRole> listRole = userRoleDAO.getByUserId(id);
		logger.info("ROLE: {} {}", id, listRole.size());
		List<GrantedAuthority> listPermission = new ArrayList<GrantedAuthority>();
		listPermission.add(new SimpleGrantedAuthority("ROLE_USER"));
		for (int i = 0; i < listRole.size(); i++) {
			logger.info("ROLE_ITEM: {}", listRole.get(i).getRole());
			if (listRole.get(i).getRole() == 1) {
				listPermission.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			}
		}
		try {
			logger.info("USER_ROLE: {}",
					JsonService.getInstance().getObjectMapper().writeValueAsString(listPermission));
		} catch (Exception ex) {
			logger.info("DEBUG_ERROR_WRITE_TO_CONSOLE");
		}
		return listPermission;
	}

}