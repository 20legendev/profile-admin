package vn.tcbs.cas.profile.service.authentication.result;

/**
 * 
 * @author NguyenNam
 * 
 */
public class TokenWSResult<T> {
	private String resultCode;
	private T resultValue;
	private String referenceTransaction;

	public TokenWSResult(String resultCode, T resultValue,
			String referenceTransaction) {
		super();
		this.resultCode = resultCode;
		this.resultValue = resultValue;
		this.referenceTransaction = referenceTransaction;
	}

	public String getReferenceTransaction() {
		return referenceTransaction;
	}

	public void setReferenceTransaction(String referenceTransaction) {
		this.referenceTransaction = referenceTransaction;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public T getResultValue() {
		return resultValue;
	}

	public void setResultValue(T resultValue) {
		this.resultValue = resultValue;
	}

	public TokenWSResult() {
		super();
	}

	public TokenWSResult(String resultCode, T resultValue) {
		this.resultCode = resultCode;
		this.resultValue = resultValue;
	}
}
