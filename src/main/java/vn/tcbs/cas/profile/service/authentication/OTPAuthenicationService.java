package vn.tcbs.cas.profile.service.authentication;

import vn.tcbs.cas.exception.NetworkException;


/**
 * 
 * @author NguyenNam
 * 
 */

public interface OTPAuthenicationService {
	public String getTokenProfile(String tokenID);

	public int initValidation(String tokenID, String channelName, String transactionID, String questionTemplate) throws NetworkException;

	public int doValidateToken(String tokenID, String channelName, String transacionID, String validationCode) throws NetworkException;
}
