package vn.tcbs.cas.profile.service;

import java.util.Date;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import vn.tcbs.cas.profile.dao.UserQueueDAO;
import vn.tcbs.cas.profile.entity.TcbsAddress;
import vn.tcbs.cas.profile.entity.TcbsBankAccount;
import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;
import vn.tcbs.cas.profile.object.OpenAccountMessage;
import vn.tcbs.tool.base.Constant;
import vn.tcbs.tool.base.TcbsUtils;

@Service("userQueueService")
public class UserQueueServiceImpl implements UserQueueService {

	@Autowired
	private UserQueueDAO userQueueDAO;
	private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Override
	public void addOpenAccountQueue(TcbsUserOpenaccountQueue userQueue) {
		if (userQueue.getId() == null) {
			logger.info("ADD_USER_QUEUE: {}", userQueue.getEmail());
			userQueueDAO.addNew(userQueue);
		} else {
			logger.info("UPDATE_USER_QUEUE: {}", userQueue.getEmail());
			userQueueDAO.update(userQueue);
		}
	}

	@Override
	public TcbsUserOpenaccountQueue getUserByIdNumber(String idNumber) {
		List<TcbsUserOpenaccountQueue> list = userQueueDAO.findUser(idNumber);
		return list.size() > 0 ? list.get(0) : null;
	}

	@Override
	public TcbsUserOpenaccountQueue getByReferenceId(String referenceid) {
		List<TcbsUserOpenaccountQueue> lu = userQueueDAO.getByReferenceId(referenceid);
		return lu.size() > 0 ? lu.get(0) : null;
	}

	@Override
	public void saveFromUser(TcbsUser user) throws JsonProcessingException {
		TcbsUserOpenaccountQueue queue = new TcbsUserOpenaccountQueue();
		queue.setBirthday(user.getBirthday());
		queue.setCreatedDate(new Date());
		queue.setEmail(user.getEmail());
		queue.setFirstname(user.getFirstname());
		queue.setLastname(user.getLastname());
		TcbsIdentification i = user.getIdentifications().get(0);
		queue.setIddate(i.getIdDate());
		queue.setIdnumber(i.getIdNumber());
		queue.setIdplace(i.getIdPlace());
		queue.setPhone(user.getPhone());
		queue.setReferenceid(TcbsUtils.generateRandom(16));
		queue.setStatus(1);

		TcbsAddress ad = user.getAddress().get(0);
		TcbsBankAccount ba = user.getBanks().size() > 0 ? user.getBanks().get(0) : null;
		OpenAccountMessage msg = new OpenAccountMessage();
		msg.setAddress(ad.getAddress());
		if (ba != null) {
			msg.setBankaccountname(ba.getBankAccountName());
			msg.setBankaccountnumber(ba.getBankAccountNo());
			msg.setBankbranch(ba.getBankBranch());
			msg.setBankcode(ba.getBankCode());
			msg.setBankname(ba.getBankName());
			msg.setIsbank(1);
		}
		msg.setBirthday(user.getBirthday());
		msg.setEmail(msg.getEmail());
		msg.setFullname(user.getFirstname() + " " + user.getLastname());
		msg.setGender(user.getGender());
		msg.setPhone(user.getPhone());
		if (i != null) {
			msg.setIddate(i.getIdDate());
			msg.setIdnumber(i.getIdNumber());
			msg.setIdplace(i.getIdPlace());
		}
		msg.setPassword(TcbsUtils.MD5(Constant.defaultTradingPass));
		queue.setOpenaccountdata(JsonService.getInstance().getObjectMapper().writeValueAsString(msg));
		userQueueDAO.addNew(queue);
	}

	@Override
	public void setActive(String referenceId, TcbsUser user) {
		List<TcbsUserOpenaccountQueue> list = userQueueDAO.getByReferenceId(referenceId);
		TcbsUserOpenaccountQueue queue = null;
		for (int i = 0; i < list.size(); i++) {
			queue = list.get(i);
			queue.setStatus(1);
			queue.setUser(user);
			userQueueDAO.update(queue);
		}
	}

	@Override
	public void update(TcbsUserOpenaccountQueue queue) {
		userQueueDAO.update(queue);
	}

	@Override
	public List<TcbsUserOpenaccountQueue> getByIdNumber(String idNumber, Long referer) {
		return userQueueDAO.find(idNumber, referer);
	}

	public List<TcbsUserOpenaccountQueue> getByReferer(Long referer) {
		return userQueueDAO.getByReferer(referer);
	}

	public List<TcbsUserOpenaccountQueue> getByRefererRestricted(Long referer) {
		return userQueueDAO.getByRefererRestricted(referer);
	}

	@Override
	public boolean confirmContract(String referenceid) {
		TcbsUserOpenaccountQueue queue = getByReferenceId(referenceid);
		if (queue != null) {
			queue.setStatus(3);
			update(queue);
			return true;
		}
		return false;
	}

	@Override
	public List<TcbsUserOpenaccountQueue> listQueue(Integer page, Integer perpage, String email, String phone,
			String idNumber, String fullname) {
		return userQueueDAO.listQueue(page, perpage, email, phone, idNumber, fullname);
	}

}