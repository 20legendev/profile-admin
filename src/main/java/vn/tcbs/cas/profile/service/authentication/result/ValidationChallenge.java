package vn.tcbs.cas.profile.service.authentication.result;


/**
 * 
 * @author NguyenNam
 * 
 */
public class ValidationChallenge{
	private String channel;
	private String assignedTransaction;
	private String challengeQuestion;
	private String challengeQuestionType;
	private String challengeNotes;
	private String timestamp;

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getAssignedTransaction() {
		return assignedTransaction;
	}

	public void setAssignedTransaction(String assignedTransaction) {
		this.assignedTransaction = assignedTransaction;
	}

	public String getChallengeQuestion() {
		return challengeQuestion;
	}

	public void setChallengeQuestion(String challengeQuestion) {
		this.challengeQuestion = challengeQuestion;
	}

	public String getChallengeQuestionType() {
		return challengeQuestionType;
	}

	public void setChallengeQuestionType(String challengeQuestionType) {
		this.challengeQuestionType = challengeQuestionType;
	}

	public String getChallengeNotes() {
		return challengeNotes;
	}

	public void setChallengeNotes(String challengeNotes) {
		this.challengeNotes = challengeNotes;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
}
