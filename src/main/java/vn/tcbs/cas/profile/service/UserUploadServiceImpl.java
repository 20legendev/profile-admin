package vn.tcbs.cas.profile.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcbs.cas.profile.dao.UserUploadDAO;
import vn.tcbs.cas.profile.entity.TcbsUserUpload;

@Service("userUploadService")
public class UserUploadServiceImpl implements UserUploadService {

	@Autowired
	private UserUploadDAO userUploadDAO;

	@Override
	public void addNew(TcbsUserUpload userUpload) {
		userUploadDAO.addNew(userUpload);
	}

	@Override
	public List<TcbsUserUpload> getByAlias(String alias) {
		return userUploadDAO.getByAlias(alias);
	}

}