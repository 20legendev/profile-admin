package vn.tcbs.cas.profile.service;

import javax.servlet.http.HttpServletRequest;

import vn.tcbs.cas.profile.entity.TcbsUpdateLog;

public interface UpdateLogService {
	public void addNew(TcbsUpdateLog log);

	public TcbsUpdateLog fromRequest(HttpServletRequest req, boolean confirm);
}
