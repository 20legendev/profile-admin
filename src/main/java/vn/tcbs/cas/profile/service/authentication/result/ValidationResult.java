package vn.tcbs.cas.profile.service.authentication.result;



/**
 * 
 * @author NguyenNam
 *
 */
public enum ValidationResult implements Enumerable, ResultValue {
	VALIDATION_OK(0), INCORRECT_ANSWER(1), NEXT_TOKEN_REQUIRED(2),UNKNOWN(4);

	private final int value;

	/**
	 * Default constructor.
	 * 
	 * @param value
	 *            is the alphabet value.
	 */
	private ValidationResult(int value) {
		this.value = value;
	}

	/**
	 * Get the alphabet value.
	 * 
	 * @return the alphabet value.
	 */
	public int value() {
		return value;
	}

	/**
	 * Get the enum constant associated with specified value.
	 * 
	 * @param value
	 *            is the value associated with the <tt>Alphabet</tt> enum
	 *            constant.
	 * @return the associated enum constant.
	 * @throws IllegalArgumentException
	 *             if there is no associated enum constant for given value.
	 */
	public static ValidationResult valueOf(int value) throws IllegalArgumentException {
		for (ValidationResult val : values()) {
			if (val.value == value)
				return val;
		}
		throw new IllegalArgumentException(
				"No enum const ValidationResult with value " + value);
	}
	
	public static ValidationResult lookUp(String value)  throws IllegalArgumentException {
		for (ValidationResult val : values()) {
			if (val.name().equalsIgnoreCase(value))
				return val;
		}
		throw new IllegalArgumentException(
				"No enum const ValidationResult with value " + value);
	}
}
