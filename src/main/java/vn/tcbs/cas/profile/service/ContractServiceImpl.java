package vn.tcbs.cas.profile.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import vn.tcbs.cas.profile.dao.ContractDAO;
import vn.tcbs.cas.profile.entity.Contract;
import vn.tcbs.cas.profile.entity.TcbsAddress;
import vn.tcbs.cas.profile.entity.TcbsBankAccount;
import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;
import vn.tcbs.invest2.export.DocReporterUtil;
import vn.tcbs.tool.base.TcbsUtils;

@Service("contractService")
public class ContractServiceImpl implements ContractService {

	@Autowired
	private ContractDAO contractDAO;
	@Autowired
	private ApplicationService applicationService;
	@Value("${tcbs.folder}")
	private String tcbsLocalFolder;

	private static Logger logger = LoggerFactory.getLogger(ContractServiceImpl.class);

	@Override
	public Contract addNew(Contract c) {
		return contractDAO.createNew(c);
	}

	@Override
	public Contract getByAlias(String alias) {
		List<Contract> result = contractDAO.getByAlias(alias);
		if (result.size() > 0) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public Contract getByIdNumber(String idNumber) {
		List<Contract> result = contractDAO.getByIdNumber(idNumber);
		return result.size() > 0 ? result.get(0) : null;
	}

	public void update(Contract c) {
		contractDAO.update(c);
	}

	@Override
	public Contract getByReferenceId(String referenceId) {
		List<Contract> list = contractDAO.getByReferenceId(referenceId);
		return list.size() > 0 ? list.get(0) : null;
	}

	@Override
	public Contract generateContract(String referenceId) {
		Contract c = getByReferenceId(referenceId);
		if (c != null) {
			return c;
		}
		c = new Contract();
		c.setDownloadCount(0);
		c.setReferenceId(referenceId);
		c.setContractNumber(TcbsUtils.generateContractId(contractDAO));
		c.setDocumentAlias(TcbsUtils.generateRandom(24));
		addNew(c);
		return c;
	}

	public HashMap<String, Object> contractFileUser(TcbsUser user) {
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("bankcode_text", " ");
		data.put("fullname", user.getLastname() + " " + user.getFirstname());
		data.put("birthday", sf.format(user.getBirthday()));
		data.put("gender", user.getGender().equals(1) ? "Nam" : "Nữ");
		if (user.getIdentifications().size() > 0) {
			TcbsIdentification i = user.getIdentifications().get(0);
			data.put("idnumber", i.getIdNumber());
			data.put("iddate", sf.format(i.getIdDate()));
			data.put("idplace", i.getIdPlace());
		} else {
			data.put("idnumber", "");
			data.put("iddate", "");
			data.put("idplace", "");
		}
		if (user.getAddress().size() > 0) {
			TcbsAddress a = user.getAddress().get(0);
			data.put("address", a.getAddress());
		} else {
			data.put("address", "");
		}
		data.put("phone", user.getPhone());
		data.put("email", user.getEmail());
		if (user.getBanks().size() > 0) {
			TcbsBankAccount ba = user.getBanks().get(0);
			if (ba != null && ba.getBankBranch() != null) {
				data.put("bankcode_text", ba.getBankBranch());
			}
			data.put("bankaccountnumber", ba.getBankAccountNo() != null ? ba.getBankAccountNo() : "");
		} else {
			data.put("bankaccountnumber", "");
		}
		data.put("province_text", "");
		String custodyCd = applicationService.getCustodyCd(user.getId());
		if (custodyCd != null) {
			logger.info("CUSTODYCD: {}", custodyCd);
			char[] custodyCdArr = custodyCd.toCharArray();
			for (int i = 0; i < custodyCdArr.length; i++) {
				data.put("C" + i, Character.toString(custodyCdArr[i]));
			}
			data.put("contractnumber", custodyCd.substring(4));
		} else {
			for (int i = 0; i < 10; i++) {
				data.put("C" + i, "");
			}
			data.put("contractnumber", "");
		}
		return data;
	}

	public HashMap<String, Object> contractFileQueue(TcbsUserOpenaccountQueue queue) {
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("bankcode_text", " ");
		data.put("fullname", queue.getLastname() + " " + queue.getFirstname());
		data.put("birthday", sf.format(queue.getBirthday()));
		data.put("gender", queue.getGender().equals(1) ? "Nam" : "Nữ");
		data.put("idnumber", queue.getIdnumber() != null ? queue.getIdnumber() : "");
		data.put("iddate", queue.getIddate() != null ? sf.format(queue.getIddate()) : "");
		data.put("idplace", queue.getIdplace() != null ? queue.getIdplace() : "");
		data.put("address", queue.getAddress() != null ? queue.getAddress() : "");
		data.put("phone", queue.getPhone());
		data.put("email", queue.getEmail());
		data.put("bankaccountnumber", queue.getBankaccountnumber() != null ? queue.getBankaccountnumber() : "");
		data.put("bankcode_text", queue.getBankbranch() != null ? queue.getBankbranch() : "");
		data.put("province_text", queue.getProvince() != null ? queue.getProvince() : "");
		for (int i = 0; i < 10; i++) {
			data.put("C" + i, "");
		}
		data.put("contractnumber", "");
		return data;
	}

	public List<String> generateContractFile(TcbsUserOpenaccountQueue queue) {
		List<String> result = new ArrayList<String>();
		HashMap<String, Object> data = new HashMap<String, Object>();
		if (queue.getUser() != null) {
			data = contractFileUser(queue.getUser());
		} else {
			data = contractFileQueue(queue);
		}

		String folder = tcbsLocalFolder + File.separator + "tcbs" + File.separator;
		String path = folder + "template" + File.separator + "MB-TCBS-HD01.docx";
		String fileName = queue.getReferenceid() + ".docx";
		String output = folder + "generate" + File.separator + fileName;

		result.add(fileName);
		result.add(output);

		File templateFile = new File(path);
		try {
			InputStream in = new FileInputStream(templateFile);
			IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
			IContext context = report.createContext();
			context.putMap(data);
			DocReporterUtil.processDocumentReportDocx(path, output, context);
			return result;
		} catch (Exception ex) {
			logger.info("PRINT_DOCUMENT_ERROR: {}", ex.getMessage());
			return result;
		}
	}

}
