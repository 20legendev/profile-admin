package vn.tcbs.cas.profile.service.token;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.cas.http.context.HttpContext;
import vn.tcbs.cas.http.context.HttpResponse;
import vn.tcbs.cas.profile.service.JsonService;
import vn.tcbs.cas.profile.service.account.active.ActiveAccountServiceImpl;
import vn.tcbs.tool.base.ResponseData;

@Service("tokenService")
public class TokenServiceImpl implements TokenService {

	private static final Logger logger = LoggerFactory.getLogger(ActiveAccountServiceImpl.class);

	@Autowired
	private HttpContext httpContext;
	@Value("${url.api.tokenservice}")
	private String tokenUrl;
	@Value("${url.profile.token.timeout}")
	private String timeout;

	@Override
	public String generateTokenOneTime(String data) {
		String token = null;
		String url = tokenUrl + "token/generate";
		Map<String, String> headers = new HashMap<String, String>(1);
		Map<String, String> requestParams = new HashMap<String, String>();
		requestParams.put("timeout", "60000");
		requestParams.put("data", data);
		logger.info("Token Generate Request with " + requestParams);
		try {
			HttpResponse response = httpContext.getHttpUtil().post(url, headers, requestParams);
			if (response != null) {
				logger.info("Token Generate with status " + response.getStatus());
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					logger.info("Token Generate with value " + responseString);
					ObjectMapper om = new ObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					ResponseData<String> result = om.readValue(responseString, ResponseData.class);
					if (result.getStatus() == 0) {
						if (result.getData() != null) {
							token = result.getData();
						}
					} else {
						logger.error("Can not generate token:" + result.getMsg());
					}
				}
			} else {
				logger.error("Response token: null");
			}
		} catch (Exception e) {
			logger.error("Can not generate token:" + e.getMessage());
		}
		return token;
	}

	@Override
	public String generateToken(String data) {
		String token = null;
		String url = tokenUrl + "token/generate/v1";
		Map<String, String> headers = new HashMap<String, String>(1);
		Map<String, String> requestParams = new HashMap<String, String>();
		requestParams.put("timeout", timeout);
		requestParams.put("data", data);
		requestParams.put("type", "2");
		logger.info("Token Generate Request with " + requestParams);
		try {
			HttpResponse response = httpContext.getHttpUtil().post(url, headers, requestParams);
			if (response != null) {
				logger.info("Token Generate with status " + response.getStatus());
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					logger.info("Token Generate with value " + responseString);
					ObjectMapper om = new ObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					ResponseData<String> result = om.readValue(responseString, ResponseData.class);
					if (result.getStatus() == 0) {
						if (result.getData() != null) {
							token = result.getData();
						}
					} else {
						logger.error("Can not generate token:" + result.getMsg());
					}
				}
			} else {
				logger.error("Response token: null");
			}
		} catch (Exception e) {
			logger.error("Can not generate token:" + e.getMessage());
		}
		return token;
	}

	@Override
	public String verifyToken(String token) {
		String data = null;
		String url = tokenUrl + "token/verify";
		Map<String, String> headers = new HashMap<String, String>(1);
		Map<String, String> requestParams = new HashMap<String, String>();
		requestParams.put("token", token);
		logger.info("Token Verify Request with " + requestParams);
		try {
			HttpResponse response = httpContext.getHttpUtil().post(url, headers, requestParams);
			if (response != null) {
				logger.info("Token Verify with status " + response.getStatus());
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					logger.info("Token Verify with value " + responseString);
					ObjectMapper om = JsonService.getInstance().getObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					ResponseData<String> result = om.readValue(responseString, ResponseData.class);
					if (result.getStatus() == 0) {
						if (result.getData() != null) {
							data = result.getData();
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Can not verify token:" + e.getMessage());
		}
		return data;
	}

	@Override
	public boolean deleteToken(String token) {
		String url = tokenUrl + "token/delete";
		Map<String, String> headers = new HashMap<String, String>(1);
		Map<String, String> requestParams = new HashMap<String, String>();
		requestParams.put("token", token);
		logger.info("Token Verify Request with " + requestParams);
		try {
			HttpResponse response = httpContext.getHttpUtil().post(url, headers, requestParams);
			if (response != null) {
				logger.info("Token Verify with status " + response.getStatus());
				if (response.isOk()) {
					String responseString = new String(response.getBody());
					logger.info("Token Verify with value " + responseString);
					ObjectMapper om = new ObjectMapper();
					om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					ResponseData<String> result = om.readValue(responseString, ResponseData.class);
					if (result.getStatus() == 0) {
						return true;
					}
				}
			}
		} catch (Exception e) {
			logger.error("Can not verify token:" + e.getMessage());
		}
		return false;
	}
}
