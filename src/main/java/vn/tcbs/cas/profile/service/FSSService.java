package vn.tcbs.cas.profile.service;

import java.io.IOException;

import javax.xml.soap.SOAPException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import vn.tcbs.cas.profile.admin.dto.ConfirmFlexResult;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;
import vn.tcbs.cas.profile.object.OpenAccountMessage;

public interface FSSService {
	public int openAccount(OpenAccountMessage msg) throws SOAPException, IOException;

	public ConfirmFlexResult confirmOpeningAccount(String referenceId)
			throws SOAPException, JsonParseException, JsonMappingException, IOException;

	public void sendToConfirmQueue(String referenceId) throws JsonProcessingException;

	public int openAccount(TcbsUserOpenaccountQueue user) throws SOAPException, IOException;
}
