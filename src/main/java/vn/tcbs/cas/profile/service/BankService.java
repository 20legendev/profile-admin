package vn.tcbs.cas.profile.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import vn.tcbs.cas.profile.entity.TcbsBankAccount;
import vn.tcbs.cas.profile.object.Crbbanklist;

public interface BankService {
	public List<Crbbanklist> listAll(String province) throws ClientProtocolException, IOException;

	public HashMap<String, Object> getByBankCode(String bankCode);

	public String byProvinceString(String province);

	public List<TcbsBankAccount> getByUser(Long userId);
	public TcbsBankAccount getById(Long id);

	public void delete(Long id);
}
