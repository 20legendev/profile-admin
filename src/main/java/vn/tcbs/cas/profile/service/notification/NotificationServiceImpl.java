package vn.tcbs.cas.profile.service.notification;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.CharacterData;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import vn.tcbs.cas.algorithm.SimpleParser;
import vn.tcbs.cas.http.context.HttpContext;

/**
 * 
 * @author NguyenNam
 * 
 */

@Service("notificationService")
public class NotificationServiceImpl implements NotificationService, InitializingBean {
	private static final Logger logger = LoggerFactory.getLogger(NotificationServiceImpl.class);
	@Autowired
	private HttpContext httpContext;
	@Value("${url.notification.email}")
	private String emailUrl;
	@Value("${url.notification.sms}")
	private String smsUrl;

	private String buildSMSRequest(String phone, String content, String des, String uri) throws Exception {
		Map<String, String> valueMap = new HashMap<String, String>();
		valueMap.put("phone", StringEscapeUtils.escapeXml(phone));
		valueMap.put("content", StringEscapeUtils.escapeXml(content));
		valueMap.put("des", StringEscapeUtils.escapeXml(des));
		SimpleParser sp = new SimpleParser(getTemplate(uri));
		String xmlToSend = sp.parse(valueMap);
		return xmlToSend;
	}

	private String buildEmailRequest(String template, String receiver, String cc, String subject, String content,
			String file, String uri) throws Exception {
		Map<String, String> valueMap = new HashMap<String, String>();
		valueMap.put("template", StringEscapeUtils.escapeXml(template));
		valueMap.put("receiver", StringEscapeUtils.escapeXml(receiver));
		valueMap.put("cc", StringEscapeUtils.escapeXml(cc));
		valueMap.put("subject", StringEscapeUtils.escapeXml(subject));
		valueMap.put("content", StringEscapeUtils.escapeXml(content));
		valueMap.put("fileAttach", StringEscapeUtils.escapeXml(file));
		SimpleParser sp = new SimpleParser(getTemplate(uri));
		String xmlToSend = sp.parse(valueMap);
		return xmlToSend;
	}

	public String getTemplate(String wsUri) {
		String template = "";
		try {
			String templateFilename = SimpleParser.getTemplateName(wsUri);
			logger.info("Template file {}", templateFilename);
			URL fileURL = Thread.currentThread().getContextClassLoader().getResource(templateFilename);

			if (fileURL != null) {
				String filePath = URLDecoder.decode(fileURL.getFile(), "UTF-8");
				logger.debug("Reading soap template from file: " + filePath);
				template = SimpleParser.getTemplate(filePath);
			} else {
				logger.error("Soap template file: " + templateFilename + " could not be found in classpath");
			}
		} catch (IOException e) {
			logger.error("Can not load template for message. Exception: " + e.getMessage());
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return template;
	}

	private String parseSMSResult(String xmlResponse) {
		String result = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlResponse));
			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("ns1:sendSMSResponse");
			// iterate the anyType
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				NodeList message = element.getElementsByTagName("return");
				Element lineMessage = (Element) message.item(0);
				if (lineMessage != null) {
					result = getCharacterDataFromElement(lineMessage);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Error in while parsing response SMS: " + ex.getMessage());
		}
		return result;
	}

	private String getSMSResult(String xmlResponse) throws Exception {
		String result = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlResponse));
			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("ns1:sendSMSResponse");
			// iterate the anyType
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				NodeList message = element.getElementsByTagName("return");
				Element lineMessage = (Element) message.item(0);
				if (lineMessage != null) {
					result = getCharacterDataFromElement(lineMessage);
				}
			}
		} catch (Exception ex) {
			logger.error("Error in while parsing response SMS: " + ex.getMessage());
			throw new Exception("Send SMS error");
		}
		return result;
	}

	private String parseEmailResult(String xmlResponse) {
		String result = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlResponse));
			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("ns1:sendEmailResponse");
			// iterate the anyType
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				NodeList message = element.getElementsByTagName("return");
				Element lineMessage = (Element) message.item(0);
				if (lineMessage != null) {
					result = getCharacterDataFromElement(lineMessage);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Error in while parsing response SMS: " + ex.getMessage());
		}
		return result;
	}

	private static String getCharacterDataFromElement(Element e) throws UnsupportedEncodingException, DOMException {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return new String(cd.getData().getBytes(), "UTF-8");
		}
		return "";
	}

	@Override
	public int sendEmail(String template, String receiver, String cc, String subject, String content, String fileAttach)
			throws Exception {
		String response = "";
		try {
			String xmlToSend = buildEmailRequest(template, receiver, cc, subject, content, fileAttach, emailUrl);
			String xmlResponse = httpContext.httpPost(emailUrl, xmlToSend);
			response = parseEmailResult(xmlResponse);
			logger.info("Email:" + receiver + "@Response:" + response);
		} catch (Exception e) {
			logger.error("Error while send Email:" + receiver + "@Message:" + e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return 0;
	}

	@Override
	public String sendSMSTo(String userId, String content, String desc) {
		String response = "";
		try {
			String xmlToSend = buildSMSRequest(userId, content, desc, smsUrl);
			logger.info("Phone:" + userId + "@Request:" + xmlToSend);
			String xmlResponse = httpContext.httpPost(smsUrl, xmlToSend);
			response = parseSMSResult(xmlResponse);
			logger.info("Phone:" + userId + "@Response:" + response);
		} catch (Exception e) {
			logger.error("Error while send SMS:" + userId + "@Message:" + e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public String sendSMS(String phone, String content, String desc) throws Exception {
		String response = "";
		String xmlToSend = buildSMSRequest(phone, content, desc, smsUrl);
		logger.info("Phone:" + phone + "@Request:" + xmlToSend);
		String xmlResponse = httpContext.httpPost(smsUrl, xmlToSend);
		response = getSMSResult(xmlResponse);
		logger.info("Phone:" + phone + "@Response:" + response);
		return response;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		httpContext.init();
	}
}
