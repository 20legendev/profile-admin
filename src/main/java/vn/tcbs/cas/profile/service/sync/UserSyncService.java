package vn.tcbs.cas.profile.service.sync;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface UserSyncService {
	public void addSyncToFlex(Long userId, Integer withBank) throws JsonProcessingException;
}
