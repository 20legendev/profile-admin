package vn.tcbs.cas.profile.service;

import java.util.HashMap;
import java.util.List;

import vn.tcbs.cas.profile.entity.Contract;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;

public interface ContractService {
	public Contract generateContract(String referenceId);

	public List<String> generateContractFile(TcbsUserOpenaccountQueue queue);

	public Contract addNew(Contract c);

	public Contract getByAlias(String alias);

	public Contract getByReferenceId(String referenceId);

	public Contract getByIdNumber(String idNumber);

	public void update(Contract c);

	public HashMap<String, Object> contractFileQueue(TcbsUserOpenaccountQueue queue);

	public HashMap<String, Object> contractFileUser(TcbsUser user);
}
