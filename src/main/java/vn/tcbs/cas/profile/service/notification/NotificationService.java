package vn.tcbs.cas.profile.service.notification;

/**
 * 
 * @author NguyenNam
 * 
 */

public interface NotificationService {

	public int sendEmail(String template, String receiver, String cc, String subject, String content, String fileAttach)
			throws Exception;

	String sendSMSTo(String userId, String content, String desc);

	String sendSMS(String phone, String content, String desc) throws Exception;
}
