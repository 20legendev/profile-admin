package vn.tcbs.cas.profile.service.sync;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.cas.profile.entity.TcbsAddress;
import vn.tcbs.cas.profile.entity.TcbsBankAccount;
import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.service.ApplicationService;
import vn.tcbs.cas.profile.service.UserService;

@Service("userSyncService")
public class UserSyncServiceImpl implements UserSyncService {

	@Value("${sync.queue.exchangename}")
	private String exchangeName;
	@Value("${queue.user.updateinformation}")
	private String syncFlexQueueName;
	@Autowired
	protected AmqpTemplate rabbitmqTemplate;
	@Autowired
	private UserService userService;
	@Autowired
	private ApplicationService applicationService;
	private static Logger logger = LoggerFactory.getLogger(UserSyncServiceImpl.class);

	public void addSyncToFlex(Long userId, Integer withBank) throws JsonProcessingException {
		String custodyCd = applicationService.getCustodyCd(userId);
		if (custodyCd == null)
			return;
		TcbsUser user = userService.getById(userId);
		HashMap<String, Object> syncData = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		syncData.put("custodycd", custodyCd);
		syncData.put("firstname", user.getFirstname());
		syncData.put("lastname", user.getLastname());
		syncData.put("birthday", sdf.format(user.getBirthday()));
		syncData.put("email", user.getEmail());
		syncData.put("phone", user.getPhone());
		List<TcbsAddress> listAddress = user.getAddress();
		if (listAddress.size() > 0) {
			TcbsAddress addr = listAddress.get(listAddress.size() - 1);
			syncData.put("address", addr.getAddress());
		}
		List<TcbsIdentification> listId = user.getIdentifications();
		if (listId.size() > 0) {
			TcbsIdentification id = listId.get(0);
			syncData.put("idnumber", id.getIdNumber());
			syncData.put("iddate", sdf.format(id.getIdDate()));
			syncData.put("idplace", id.getIdPlace());
		}
		List<TcbsBankAccount> listBank;
		if (withBank.equals(1) && (listBank = user.getBanks()).size() > 0) {
			syncData.put("banks", new ObjectMapper().writeValueAsString(listBank));
		}
		send(syncFlexQueueName + ".key", new ObjectMapper().writeValueAsString(syncData));
	}

	@ManagedOperation
	public void send(String key, String text) {
		logger.info("sent to {} {} {}", exchangeName, key, text);
		rabbitmqTemplate.convertAndSend(exchangeName, key, text);
	}

	@ManagedOperation
	public void send(String key, Message message) {
		rabbitmqTemplate.send(exchangeName, key, message);
	}
}
