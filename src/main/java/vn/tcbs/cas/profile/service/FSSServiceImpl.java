package vn.tcbs.cas.profile.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.stereotype.Service;
import org.w3c.dom.NodeList;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.cas.profile.admin.dto.ConfirmFlexResult;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;
import vn.tcbs.cas.profile.object.OpenAccountMessage;
import vn.tcbs.tool.base.TcbsSoapUtils;
import vn.tcbs.tool.base.TcbsUtils;

@Service("fssService")
public class FSSServiceImpl implements FSSService {

	@Value("${register.default.agentid}")
	private String agentIdDefault;
	@Autowired
	private AmqpTemplate rabbitmqTemplate;
	@Value("${queue.openaccount.cas.flex.key.confirm}")
	private String openFlexConfirmKey;
	@Value("${sync.queue.exchangename}")
	private String exchangeName;

	@Value("${fss.wsdl.location}")
	private String service;
	private static String serverURI = "http://tempuri.org/";

	private static Logger logger = LoggerFactory.getLogger(FSSServiceImpl.class);

	@ManagedOperation
	public void send(String exchange, String key, String text) {
		rabbitmqTemplate.convertAndSend(exchange, key, text);
	}

	@Override
	public int openAccount(TcbsUserOpenaccountQueue user) throws SOAPException, IOException {
		String fullName = user.getLastname() + " " + user.getFirstname();
		HashMap<String, Object> data = new HashMap<String, Object>();
		String tradingPass = user.getPasswordmd5();
		tradingPass = tradingPass != null && !tradingPass.equals("") ? tradingPass : "";
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		data.put("referenceid", user.getReferenceid());
		data.put("fullname", fullName);
		data.put("gender", (user.getGender() != null && user.getGender().equals(1)) ? "001" : "002");
		data.put("idnumber", user.getIdnumber());
		data.put("idtype", "I");
		data.put("iddate", sf.format(user.getIddate()));
		data.put("idplace", user.getIdplace());
		data.put("email", user.getEmail());
		data.put("phone", user.getPhone());
		data.put("address", user.getAddress());
		data.put("province", user.getProvince());
		data.put("birthday", sf.format(user.getBirthday()));
		data.put("tradingpass", tradingPass);
		if (user.getBankaccountnumber() != null) {
			data.put("bankaccountnumber", user.getBankaccountnumber());
			data.put("bankaccountname", user.getBankaccountname());
			data.put("bankname", user.getBankname());
			data.put("bankbranch", user.getBankbranch());
			data.put("bankcode", user.getBankcode());
		} else {
			data.put("bankaccountnumber", "");
			data.put("bankaccountname", "");
			data.put("bankname", "");
			data.put("bankbranch", "");
			data.put("bankcode", "");
		}
		data.put("custodycode", "11111111");
		// data.put("agentid", agentIdDefault);
		data.put("agentid", "");
		return sendRegisterToFlex(data);
	}

	@Override
	public int openAccount(OpenAccountMessage msg) throws SOAPException, IOException {
		logger.info("SEND_TO_FLEX_DATA: {}", JsonService.getInstance().getObjectMapper().writeValueAsString(msg));

		String fullName = msg.getFullname();
		fullName = fullName == null || fullName.equals("") ? msg.getLastname() + " " + msg.getFirstname() : fullName;
		HashMap<String, Object> data = new HashMap<String, Object>();
		String tradingPass = msg.getPassword();
		tradingPass = tradingPass != null && !tradingPass.equals("") ? TcbsUtils.MD5(tradingPass) : "";
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		data.put("referenceid", msg.getReferenceid());
		data.put("fullname", fullName);
		data.put("gender", (msg.getGender() != null && msg.getGender().equals(1)) ? "001" : "002");
		data.put("idnumber", msg.getIdnumber());
		data.put("idtype", "I");

		data.put("iddate", sf.format(msg.getIddate()));
		data.put("idplace", msg.getIdplace());
		data.put("email", msg.getEmail());
		data.put("phone", msg.getPhone());
		data.put("address", msg.getAddress());
		data.put("province", msg.getProvince());
		data.put("birthday", sf.format(msg.getBirthday()));
		data.put("tradingpass", tradingPass);
		if (msg.getIsbank() == 1) {
			data.put("bankaccountnumber", msg.getBankaccountnumber());
			data.put("bankaccountname", msg.getBankaccountname());
			data.put("bankname", msg.getBankname());
			data.put("bankbranch", msg.getBankbranch());
			data.put("bankcode", msg.getBankcode());
		} else {
			data.put("bankaccountnumber", "");
			data.put("bankaccountname", "");
			data.put("bankname", "");
			data.put("bankbranch", "");
			data.put("bankcode", "");
		}
		data.put("custodycode", "11111111");
		if (msg.getReferer() != null) {
			data.put("agentid", agentIdDefault);
		} else {
			data.put("agentid", "");
		}
		return sendRegisterToFlex(data);
	}

	@Override
	public ConfirmFlexResult confirmOpeningAccount(String referenceId)
			throws SOAPException, JsonParseException, JsonMappingException, IOException {
		ConfirmFlexResult ret = new ConfirmFlexResult();

		SOAPConnection soapConnection = TcbsSoapUtils.getSoapConnection();
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("referenceid", referenceId);
		data.put("agentid", agentIdDefault);
		SOAPMessage soapMessage = TcbsSoapUtils.generateSoapMessage("soap/account-open-confirm.xml", data,
				serverURI + "IOnlineTradingWcf/FlexService");
		SOAPMessage soapResponse;
		soapResponse = soapConnection.call(soapMessage, this.service);
		soapConnection.close();
		SOAPBody body = soapResponse.getSOAPBody();
		NodeList nodes = body.getElementsByTagName("FlexServiceResult");
		String bodyText = nodes.item(0).getFirstChild().getNodeValue();
		logger.info("RETURN {}", bodyText);
		HashMap<String, Object> result = JsonService.getInstance().getObjectMapper().readValue(bodyText, HashMap.class);
		if (result.containsKey("p_err_code")) {
			Integer errorCode = Integer.valueOf((String) result.get("p_err_code"));
			ret.setErrorCode(errorCode);
			if (errorCode.equals("0")) {
				ret.setErrorMsg("SUCCESS");
				return ret;
			}
			ret.setErrorMsg((String) result.get("p_err_message"));
			return ret;
		}
		ret.setErrorCode(-1);
		ret.setErrorMsg("SYSTEM_ERROR");
		return ret;
	}

	public void sendToConfirmQueue(String referenceId) throws JsonProcessingException {
		ObjectMapper om = JsonService.getInstance().getObjectMapper();
		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put("referenceid", referenceId);
		send(exchangeName, openFlexConfirmKey, om.writeValueAsString(hash));
	}

	private Integer sendRegisterToFlex(HashMap<String, Object> data) throws SOAPException, IOException {
		SOAPConnection soapConnection = TcbsSoapUtils.getSoapConnection();
		SOAPMessage req = TcbsSoapUtils.generateSoapMessage("soap/account-open-request.xml", data,
				serverURI + "IOnlineTradingWcf/FlexService");
		SOAPMessage soapResponse = soapConnection.call(req, this.service);
		logger.info("FLEX_RESPONSE {}", soapResponse);
		soapConnection.close();
		SOAPBody body = soapResponse.getSOAPBody();
		/* for debug only */
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		soapResponse.writeTo(stream);
		String message = new String(stream.toByteArray(), "utf-8");
		logger.info("FLEX_OPEN_ACCOUNT_RECEIVED: " + message);
		/* end debug */
		NodeList nodes = body.getElementsByTagName("FlexServiceResult");
		String bodyText = nodes.item(0).getFirstChild().getNodeValue();
		HashMap<String, String> result = JsonService.getInstance().getObjectMapper().readValue(bodyText, HashMap.class);
		Integer errorCode = result.containsKey("p_err_code") ? Integer.valueOf(result.get("p_err_code"))
				: Integer.valueOf(result.get("P_ERR_CODE"));
		logger.error("FLEX_OPEN_ACCOUNT_ERROR: " + errorCode);
		Integer res = -1;
		if (errorCode != null) {
			if (errorCode.equals(0)) {
				res = 0;
			} else {
				res = errorCode;
			}
		}
		return res;
	}

}
