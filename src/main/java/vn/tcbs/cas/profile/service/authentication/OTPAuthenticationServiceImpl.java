package vn.tcbs.cas.profile.service.authentication;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.CharacterData;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import vn.tcbs.cas.algorithm.SimpleParser;
import vn.tcbs.cas.exception.NetworkException;
import vn.tcbs.cas.http.context.HttpContext;
import vn.tcbs.cas.profile.service.authentication.result.InitTokenResult;
import vn.tcbs.cas.profile.service.authentication.result.TokenWSResult;

import com.google.gson.GsonBuilder;

/**
 * 
 * @author NguyenNam
 * 
 */
@Service("otpAuthenticationService")
public class OTPAuthenticationServiceImpl implements OTPAuthenicationService {
	private static final Logger logger = Logger
			.getLogger(OTPAuthenticationServiceImpl.class);

	@Autowired
	private HttpContext httpContext;
	@Value("${url.authentication.otp.init}")
	private String initOtpUrl;
	@Value("${url.authentication.otp.validation}")
	private String validationOtpUrl;

	@Override
	public String getTokenProfile(String tokenID) {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public int initValidation(String tokenID, String channelName,
			String transactionID, String questionTemplate) throws NetworkException{
		int resultCode = -1;
		try {
			String xmlToSend = buildInitRequest(tokenID, channelName,
					transactionID, questionTemplate, initOtpUrl);
			logger.info("OTP Init:" + tokenID + "@Request:" + xmlToSend);
			String xmlResponse = httpContext.httpPost(initOtpUrl, xmlToSend);
			String response = parseInitResult(xmlResponse);
			logger.info("OTP Init:" + tokenID + "@Response:" + response);
			TokenWSResult<InitTokenResult> result =  new GsonBuilder().serializeNulls().create().fromJson(response, TokenWSResult.class);
			if(result != null && result.getResultCode().equalsIgnoreCase("OK")){
				resultCode = 1;
			}else{
				resultCode = -1;
			}
		} catch (Exception e) {
			logger.error("Error while Init OTP:" + tokenID + "@Message:" + e.getMessage());
			throw new NetworkException(e.getMessage());
		}
		return resultCode;
	}

	@SuppressWarnings("unchecked")
	@Override
	public int doValidateToken(String tokenID, String channelName,
			String transactionID, String validationCode) throws NetworkException{
		int resultCode = -1;
		try {
			String xmlToSend = buildValidationRequest(tokenID, channelName,
					transactionID, validationCode, validationOtpUrl);
			logger.info("OTP Validation:" + tokenID + "@Request:" + xmlToSend);
			String xmlResponse = httpContext.httpPost(validationOtpUrl,
					xmlToSend);
			String response = parseValidationResult(xmlResponse);
			logger.info("OTP Validation:" + tokenID + "@Response:" + response);
			TokenWSResult<String> result =  new GsonBuilder().serializeNulls().create().fromJson(response, TokenWSResult.class);
			if(result != null && result.getResultCode().equalsIgnoreCase("OK")){
				if(result.getResultValue().equals("VALIDATION_OK")){
					resultCode =  1;
				}
			}else{
				resultCode =  -1;
			}
		} catch (Exception e) {
			logger.error("Error while Validation OTP:" + tokenID + "@Message:" + e.getMessage());
			throw new NetworkException(e.getMessage());
		}
		return resultCode;
	}

	private String buildInitRequest(String tokenID, String channelName,
			String transactionID, String questionTemplate, String uri)
			throws Exception {
		Map<String, String> valueMap = new HashMap<String, String>();
		valueMap.put("tokenID", StringEscapeUtils.escapeXml(tokenID));
		valueMap.put("channelName", StringEscapeUtils.escapeXml(channelName));
		valueMap.put("transactionID",
				StringEscapeUtils.escapeXml(transactionID));
		valueMap.put("questionTemplate",
				StringEscapeUtils.escapeXml(questionTemplate));
		SimpleParser sp = new SimpleParser(getTemplate(uri));
		String xmlToSend = sp.parse(valueMap);
		return xmlToSend;
	}

	private String buildValidationRequest(String tokenID, String channelName,
			String transactionID, String validationCode, String uri)
			throws Exception {
		Map<String, String> valueMap = new HashMap<String, String>();
		valueMap.put("tokenID", StringEscapeUtils.escapeXml(tokenID));
		valueMap.put("channelName", StringEscapeUtils.escapeXml(channelName));
		valueMap.put("transactionID",
				StringEscapeUtils.escapeXml(transactionID));
		valueMap.put("validationCode",
				StringEscapeUtils.escapeXml(validationCode));
		SimpleParser sp = new SimpleParser(getTemplate(uri));
		String xmlToSend = sp.parse(valueMap);
		return xmlToSend;
	}

	public String getTemplate(String wsUri) {
		String template = "";
		try {
			String templateFilename = SimpleParser.getTemplateName(wsUri);
			URL fileURL = Thread.currentThread().getContextClassLoader()
					.getResource(templateFilename);

			if (fileURL != null) {
				String filePath = URLDecoder.decode(fileURL.getFile(), "UTF-8");
				logger.debug("Reading soap template from file: " + filePath);
				template = SimpleParser.getTemplate(filePath);
			} else {
				logger.error("Soap template file: " + templateFilename
						+ " could not be found in classpath");
			}
		} catch (IOException e) {
			logger.error("Can not load template for message. Exception: "
					+ e.getMessage());
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return template;
	}

	private String parseInitResult(String xmlResponse) {
		String result = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlResponse));
			Document doc = db.parse(is);
			NodeList nodes = doc
					.getElementsByTagName("ns1:initValidationResponse");
			// iterate the anyType
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				NodeList message = element.getElementsByTagName("return");
				Element lineMessage = (Element) message.item(0);
				if (lineMessage != null) {
					result = getCharacterDataFromElement(lineMessage);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Error in while parsing response SMS: "
					+ ex.getMessage());
		}
		return result;
	}

	private String parseValidationResult(String xmlResponse) {
		String result = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlResponse));
			Document doc = db.parse(is);
			NodeList nodes = doc
					.getElementsByTagName("ns1:doValidateTokenResponse");
			// iterate the anyType
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				NodeList message = element.getElementsByTagName("return");
				Element lineMessage = (Element) message.item(0);
				if (lineMessage != null) {
					result = getCharacterDataFromElement(lineMessage);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Error in while parsing response SMS: "
					+ ex.getMessage());
		}
		return result;
	}

	private static String getCharacterDataFromElement(Element e)
			throws UnsupportedEncodingException, DOMException {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return new String(cd.getData().getBytes(), "UTF-8");
		}
		return "";
	}
}
