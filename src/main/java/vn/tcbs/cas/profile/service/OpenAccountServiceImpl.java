package vn.tcbs.cas.profile.service;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.cas.profile.entity.TcbsApplicationUser;
import vn.tcbs.cas.profile.entity.TcbsIdentification;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserOpenaccountQueue;
import vn.tcbs.cas.profile.object.OpenAccountMessage;
import vn.tcbs.cas.profile.object.OpenAccountValidator;
import vn.tcbs.tool.base.TcbsUtils;

@Service("openAccountService")
public class OpenAccountServiceImpl implements OpenAccountService {

	@Value("${sync.queue.exchangename}")
	private String exchangeName;
	@Value("${active.queue.name.key}")
	private String activeAccountQueueKey;
	@Autowired
	private AmqpTemplate rabbitmqTemplate;
	@Autowired
	private UserService userService;
	@Autowired
	private UserQueueService userQueueService;
	@Autowired
	private FSSService fssService;

	private static Logger logger = LoggerFactory.getLogger(OpenAccountServiceImpl.class);

	public BindingResult validateOpenAccount(TcbsUser user) {
		DataBinder binder = new DataBinder(user);
		binder.setValidator(new OpenAccountValidator());
		binder.validate();
		BindingResult errors = binder.getBindingResult();
		return errors;
	}

	public TcbsUser prepareData(TcbsUser user) {
		for (int i = 0; i < user.getIdentifications().size(); i++) {
			user.getIdentifications().get(i).setUser(user);
		}
		for (int i = 0; i < user.getPhones().size(); i++) {
			user.getPhones().get(i).setUser(user);
		}
		for (int i = 0; i < user.getEmails().size(); i++) {
			user.getEmails().get(i).setUser(user);
		}
		for (int i = 0; i < user.getAddress().size(); i++) {
			user.getAddress().get(i).setUser(user);
		}
		return user;
	}

	public boolean queueToVerify(String phone, String email, String referenceid) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("phone", phone);
		map.put("email", email);
		map.put("referenceid", referenceid);
		ObjectMapper om = JsonService.getInstance().getObjectMapper();
		try {
			String str = om.writeValueAsString(map);
			logger.info("SEND_TO_VERIFY_QUEUE: {}", str);
			send(exchangeName, activeAccountQueueKey, str);
			logger.info("SEND_TO_VERIFY_QUEUE: {} SUCCESS", str);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@ManagedOperation
	private void send(String exchange, String key, String text) {
		rabbitmqTemplate.convertAndSend(exchange, key, text);
	}

	public int openAccount(HttpServletRequest req, OpenAccountMessage msg) throws JsonProcessingException {
		msg.normalizeData();
		HttpSession sess = req.getSession();
		TcbsIdentification ident = (TcbsIdentification) sess.getAttribute("TCB_IDLIST");
		String idNumber = msg.getIdnumber();
		idNumber = ident != null ? ident.getIdNumber() : idNumber;
		int isCasUser = userService.isCasUser(idNumber, msg.getEmail(), msg.getPhone());
		if (isCasUser != 0) {
			if (isCasUser == 1) {
				if (userService.getApplicationUser(idNumber, "FLEX") != null) {
					return isCasUser;
				} else {
					TcbsUser user = userService.getUserByIdNumber(idNumber);
					if (userService.isEmailDuplicated(msg.getEmail(), user.getId())) {
						return 2;
					}
					if (userService.isPhoneDuplicated(msg.getPhone(), user.getId())) {
						return 3;
					}
				}
			} else {
				return isCasUser;
			}
		}
		TcbsUserOpenaccountQueue userQueue = null;
		msg.setIdnumber(idNumber);
		if (req.getSession().getAttribute("referer") != null) {
			msg.setReferer(Long.valueOf(req.getRemoteUser()));
			msg.setPassword(TcbsUtils.randomPassword(6));
		}
		userQueue = userService.buildUserQueueFromRequest(msg, ident);
		if (req.getSession().getAttribute("referenceid") != null) {
			TcbsUserOpenaccountQueue oldQueue = userQueueService.getByReferenceId((String) req.getSession().getAttribute("referenceid"));
			if(oldQueue != null){
				userQueue.setId(oldQueue.getId());
				userQueue.setCreatedDate(new Date());
				userQueue.setReferer(oldQueue.getReferer());
			}
		}
		if (!this.queueToVerify(userQueue.getPhone(), userQueue.getEmail(), userQueue.getReferenceid())) {
			logger.error("OPEN_ACCOUNT_ERROR: {} idnumber={}", "push_to_rabbitmq_error", idNumber);
			return -1;
		}
		userQueueService.addOpenAccountQueue(userQueue);
		logger.info("PROCCESS_OPEN_ACCOUNT_SUCCESS: {}", userQueue.getReferenceid());
		return 0;
	}

	public int openAccountFlexVerify(HttpServletRequest req, OpenAccountMessage msg) throws Exception {
		String idNumber = msg.getIdnumber();
		TcbsIdentification ident = (TcbsIdentification) req.getSession().getAttribute("TCB_IDLIST");
		if (ident != null) {
			idNumber = ident.getIdNumber();
			msg.setIdnumber(idNumber);
		}
		TcbsApplicationUser au = userService.getApplicationUser(idNumber, "FLEX");
		if (au != null) {
			return 1;
		}
		int isCasUser = userService.isCasUser(idNumber, msg.getEmail().toLowerCase(), msg.getPhone());
		logger.info("CAS_USER {}", isCasUser);
		boolean autoConfirm = false;

		TcbsUser user = userService.getUserByIdNumber(idNumber);
		if (user != null) {
			autoConfirm = true;
			if (userService.isEmailDuplicated(msg.getEmail(), user.getId())) {
				return 2;
			}
			if (userService.isPhoneDuplicated(msg.getPhone(), user.getId())) {
				return 3;
			}
		}
		// Account openned by RM
		if (req.getSession().getAttribute("referer") != null) {
			msg.setReferer(Long.valueOf(req.getRemoteUser()));
		}
		TcbsUserOpenaccountQueue userQueue = userService.buildUserQueueFromRequest(msg, ident);
		try {
			Integer res = fssService.openAccount(msg);
			if (res == 0) {
				userQueueService.addOpenAccountQueue(userQueue);
				if (autoConfirm) {
					fssService.sendToConfirmQueue(msg.getReferenceid());
				}
				return 0;
			}
			return res;
		} catch (Exception ex) {
			throw ex;
		}
	}
}