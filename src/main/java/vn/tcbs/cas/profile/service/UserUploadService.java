package vn.tcbs.cas.profile.service;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsUserUpload;

public interface UserUploadService {
	public void addNew(TcbsUserUpload userUpload);
	public List<TcbsUserUpload> getByAlias(String alias);
}