package vn.tcbs.cas.profile.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.validation.BindingResult;

import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.object.OpenAccountMessage;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface OpenAccountService {
	public int openAccount(HttpServletRequest req, OpenAccountMessage msg) throws JsonProcessingException;

	public int openAccountFlexVerify(HttpServletRequest req, OpenAccountMessage msg) throws Exception;

	public BindingResult validateOpenAccount(TcbsUser user);

	public TcbsUser prepareData(TcbsUser user);

	public boolean queueToVerify(String phone, String email, String referenceid);

}