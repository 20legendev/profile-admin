package vn.tcbs.cas.profile.service.account.active;

import java.util.HashMap;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import vn.tcbs.cas.exception.AccountActivatedException;
import vn.tcbs.cas.exception.ActiveAccountException;
import vn.tcbs.cas.exception.TokenNotFoundException;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.service.UserService;
import vn.tcbs.cas.profile.service.authentication.OTPAuthenicationService;
import vn.tcbs.cas.profile.service.notification.NotificationService;
import vn.tcbs.cas.profile.service.token.TokenService;

@Service("activeAccountService")
public class ActiveAccountServiceImpl implements ActiveAccountService {

	@Autowired
	private UserService userService;
	@Autowired
	TokenService tokenService;
	@Autowired
	NotificationService notificationService;
	@Autowired
	OTPAuthenicationService otpAuthenicationService;

	@Autowired
	private VelocityEngine velocityEngine;
	@Value("${sync.queue.exchangename}")
	private String exchangeName;

	@Value("${active.queue.activeaccount.output.key}")
	private String outputKey;

	@Value("${active.queue.activeaccount.fail.key}")
	private String failKey;

	@Autowired
	private AmqpTemplate rabbitmqTemplate;
	
	private static Logger logger = LoggerFactory.getLogger(ActiveAccountServiceImpl.class);

	/*
	@Override
	public void sendRequest(ActiveAccountMessage activeAccountMessage) throws ActiveAccountException, Exception {
		if (activeAccountMessage == null)
			throw new ActiveAccountException("active account message null");
		String[] ignorableFieldNames = { "status" };
		String data = writeValueAsJonString(activeAccountMessage, ignorableFieldNames);
		String token = tokenService.generateToken(data);
		if (token == null)
			throw new ActiveAccountException("Can not generate token");
		String email = activeAccountMessage.getEmail();
		logger.info("SEND_EMAIL_ACTIVE: {}", email);
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("email", email);
		model.put("link", activeAccountUrl + token);
		String emailText = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				"META-INF/velocity/email/active-account.vm", "utf-8", model);
		int sendEmail = notificationService.sendEmail("", email, "", "[TechcomSecurities] Kích hoạt tài khoản",
				emailText, "");
		if (sendEmail == -1) {
			throw new ActiveAccountException("Send email fail");
		}
		logger.info("Send request to " + activeAccountMessage + "successfully.");
	}
	*/

	@Override
	public String activeAccount(String token)
			throws ActiveAccountException, AccountActivatedException, TokenNotFoundException, Exception {
		if (token == null)
			throw new ActiveAccountException("active account token null");
		String transactionID = null;
		ActiveAccountMessage accountMessage = null;
		ObjectMapper om = new ObjectMapper();
		String data = null;
		data = tokenService.verifyToken(token);
		logger.info("Active Account Data: " + data);
		if (data == null)
			throw new TokenNotFoundException("Can not found token");
		accountMessage = om.readValue(data, ActiveAccountMessage.class);
		TcbsUser tcbsUser = userService.getByMainEmail(accountMessage.getEmail());
		if (tcbsUser != null) {
			if (userService.getApplicationUserByEmail(accountMessage.getEmail(), "FLEX") != null) {
				throw new AccountActivatedException("Account " + accountMessage + " activated");
			}
		}
		transactionID = tokenService.generateToken(data);
		int initValidation = otpAuthenicationService.initValidation("{TCBS{SMS{" + accountMessage.getPhone() + "}}}",
				"SMS", transactionID, "Ma xac thuc: {$OTP}");
		if (initValidation == -1) {
			accountMessage.setStatus(-1);
			String[] ignorableFieldNames = { "phone", "email", "status" };
			String value = writeValueAsJonString(accountMessage, ignorableFieldNames);
			rabbitmqTemplate.convertAndSend(exchangeName, failKey, value);
		}
		logger.info("Send otp to " + accountMessage + "successfully.");
		return transactionID;
	}

	@Override
	public int confirmActive(String validationCode, String transactionID) throws ActiveAccountException, Exception {
		int resultCode = -1;
		String data = tokenService.verifyToken(transactionID);
		ObjectMapper om = new ObjectMapper();
		ActiveAccountMessage accountMessage = om.readValue(data, ActiveAccountMessage.class);
		resultCode = otpAuthenicationService.doValidateToken("{TCBS{SMS{" + accountMessage.getPhone() + "}}}", "SMS",
				transactionID, validationCode);
		if (resultCode == 1) {
			logger.info("Authentication " + accountMessage + "sucessfully.");
			accountMessage.setStatus(1);
			String[] ignorableFieldNames = { "phone", "email", "status" };
			String value = writeValueAsJonString(accountMessage, ignorableFieldNames);
			rabbitmqTemplate.convertAndSend(exchangeName, outputKey, value);
			tokenService.deleteToken(transactionID);
		} else {
			throw new ActiveAccountException("Validation " + accountMessage + " fail.");
		}
		return resultCode;
	}

	private String writeValueAsJonString(ActiveAccountMessage accountMessage, String[] ignorableFieldNames)
			throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		FilterProvider filters = new SimpleFilterProvider().addFilter("myFilter",
				SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));
		ObjectWriter writer = mapper.writer(filters);
		String value = writer.writeValueAsString(accountMessage);
		return value;
	}
}