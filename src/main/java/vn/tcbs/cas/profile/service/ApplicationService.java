package vn.tcbs.cas.profile.service;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsApplication;
import vn.tcbs.cas.profile.entity.TcbsApplicationUser;
import vn.tcbs.cas.profile.entity.TcbsUser;

public interface ApplicationService {
	public TcbsApplication getByName(String name);

	public String getCustodyCd(Long userId);

	public List<TcbsApplicationUser> getUserInAppByUserId(Long userId, String appName);

	public void addUserInAppByUserId(TcbsUser user, TcbsApplication app, String inAppId);

	public void addUserInAppByUserId(TcbsUser user, String appName, String inAppId);

	public void updateApplicationUser(TcbsApplicationUser au);

	public Boolean isApplicationUserExist(Long userId, String appName);
}