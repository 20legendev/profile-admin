package vn.tcbs.cas.profile.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcbs.cas.profile.dao.AddressDivisionDAO;
import vn.tcbs.cas.profile.entity.TcbsAddressDivision;

@Service("addressService")
public class AddressServiceImpl implements AddressService {

	@Autowired
	private AddressDivisionDAO addressDivisionDAO;

	public List<TcbsAddressDivision> getByParentName(String parentName) {
		return addressDivisionDAO.getByParentName(parentName);
	}

	public List<TcbsAddressDivision> getByCode(String code) {
		return addressDivisionDAO.getByCode(code);
	}

}