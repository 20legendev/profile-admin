package vn.tcbs.cas.profile.service.token;

public interface TokenService {
	public String generateTokenOneTime(String data);

	public String generateToken(String data);

	public String verifyToken(String token);

	public boolean deleteToken(String token);
}
