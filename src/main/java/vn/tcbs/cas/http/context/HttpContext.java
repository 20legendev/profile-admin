package vn.tcbs.cas.http.context;

import java.io.IOException;
import java.net.Authenticator;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.protocol.Protocol;

import vn.tcbs.cas.algorithm.EasySSLProtocolSocketFactory;
import vn.tcbs.cas.algorithm.ServiceAuthenticator;
import vn.tcbs.cas.algorithm.SimpleParser;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.client.providers.netty.NettyAsyncHttpProvider;
import com.ning.http.client.providers.netty.NettyAsyncHttpProviderConfig;
/**
 * 
 * @author NguyenNam
 *
 */
public class HttpContext {
	private HttpClient client;
	private int connectionTimeout;
	private int socketTimeout;
	private int defaultMaxConnectionPerHost;
	private int maxTotalConnection;
	private Protocol myhttps = null;
	protected AsyncHttpClient asyncHttpClient;
	private HttpUtil httpUtil;

	@SuppressWarnings("deprecation")
	public void init() throws IOException {
		Protocol.registerProtocol("https", new Protocol("https", new EasySSLProtocolSocketFactory(), 443));
		myhttps = new Protocol("https", new EasySSLProtocolSocketFactory(),443);
		// set parameter
		HttpConnectionManagerParams hmcp = new HttpConnectionManagerParams();
		hmcp.setSoTimeout(socketTimeout * 1000);
		hmcp.setConnectionTimeout(connectionTimeout * 1000);
		hmcp.setDefaultMaxConnectionsPerHost(defaultMaxConnectionPerHost);
		hmcp.setMaxTotalConnections(maxTotalConnection);
		// create http client pool
		MultiThreadedHttpConnectionManager multiCm = new MultiThreadedHttpConnectionManager();
		multiCm.setParams(hmcp);
		this.client = new HttpClient(multiCm);
		NettyAsyncHttpProviderConfig nettyConfig = new NettyAsyncHttpProviderConfig(); 
		AsyncHttpClientConfig.Builder builder = new AsyncHttpClientConfig.Builder();
		builder.setMaxRequestRetry(0);
		builder.setAsyncHttpClientProviderConfig(nettyConfig);
		asyncHttpClient = new AsyncHttpClient(new NettyAsyncHttpProvider(builder.build()), builder.build());
		httpUtil = new AsyncHttpClientHttpUtil(asyncHttpClient);
	}

	public void destroy() {
		this.client = null;
	}
	
	public HttpUtil getHttpUtil() {
		return httpUtil;
	}
	
	public void setHttpUtil(HttpUtil httpUtil) {
		this.httpUtil = httpUtil;
	}

	public String httpPost(String uri, String xml) throws Exception {
		String textResponse = null;
		if (uri == null) {
			throw new Exception("Service uri is null .");
		}
		String username = SimpleParser.getUser(uri);
		String password = SimpleParser.getPass(uri);

		if (username != null && password != null) {
			Authenticator cosmoteAuthenticator = (Authenticator) new ServiceAuthenticator(
					username, password);
			Authenticator.setDefault(cosmoteAuthenticator);

			HttpState state = this.client.getState();
			state.setCredentials(new AuthScope(null, -1),
					new UsernamePasswordCredentials(username, password));
			this.client.setState(state);
		}
		if (SimpleParser.getServicePath(uri).contains("https")) {
			client.getHostConfiguration().setHost(SimpleParser.getServerAddress(uri),443,myhttps);
		}
		PostMethod method = new PostMethod(SimpleParser.getServicePath(uri));
		method.setDoAuthentication(true);
		method.getHostAuthState().setAuthAttempted(true);
		method.getHostAuthState().setAuthRequested(true);
		method.getHostAuthState().setPreemptive();

		method.addRequestHeader("Content-Type", "text/xml");
		method.addRequestHeader("SOAPAction", SimpleParser.getSoapAction(uri));
		try {
			StringRequestEntity entity = new StringRequestEntity(xml,
					"application/soap+xml", "utf-8");
			method.setRequestEntity(entity);
			int iRes = this.client.executeMethod(method);
			if (iRes != 200) {
				throw new Exception(
						"Failed to invoke webservice. Http error code: " + iRes);
			}
			byte[] response = method.getResponseBody();
			textResponse = new String(response);
			return textResponse;
		} finally {
			method.releaseConnection();
		}
	}
	
	public String httpGet(String uri, String xml) throws Exception {
		String textResponse = null;
		if (uri == null) {
			throw new Exception("Service uri is null .");
		}
		String username = SimpleParser.getUser(uri);
		String password = SimpleParser.getPass(uri);

		if (username != null && password != null) {
			Authenticator cosmoteAuthenticator = (Authenticator) new ServiceAuthenticator(
					username, password);
			Authenticator.setDefault(cosmoteAuthenticator);

			HttpState state = this.client.getState();
			state.setCredentials(new AuthScope(null, -1),
					new UsernamePasswordCredentials(username, password));
			this.client.setState(state);
		}
		if (SimpleParser.getServicePath(uri).contains("https")) {
			client.getHostConfiguration().setHost(SimpleParser.getServerAddress(uri),443,myhttps);
		}
		GetMethod method = new GetMethod(SimpleParser.getServicePath(uri));
		method.setDoAuthentication(true);
		method.getHostAuthState().setAuthAttempted(true);
		method.getHostAuthState().setAuthRequested(true);
		method.getHostAuthState().setPreemptive();
		method.addRequestHeader("Content-Type", "text/xml");
		method.addRequestHeader("SOAPAction", SimpleParser.getSoapAction(uri));
		try {
			StringRequestEntity entity = new StringRequestEntity(xml,
					"application/soap+xml", "utf-8");
			int iRes = this.client.executeMethod(method);
			if (iRes != 200) {
				throw new Exception(
						"Failed to invoke webservice. Http error code: " + iRes);
			}
			byte[] response = method.getResponseBody();
			textResponse = new String(response);
			return textResponse;
		} finally {
			method.releaseConnection();
		}
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public int getDefaultMaxConnectionPerHost() {
		return defaultMaxConnectionPerHost;
	}

	public void setDefaultMaxConnectionPerHost(int defaultMaxConnectionPerHost) {
		this.defaultMaxConnectionPerHost = defaultMaxConnectionPerHost;
	}

	public int getMaxTotalConnection() {
		return maxTotalConnection;
	}

	public void setMaxTotalConnection(int maxTotalConnection) {
		this.maxTotalConnection = maxTotalConnection;
	}
}