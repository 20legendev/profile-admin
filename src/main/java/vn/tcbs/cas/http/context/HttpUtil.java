package vn.tcbs.cas.http.context;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public interface HttpUtil {

	HttpResponse get(String url, Map<String, String> headers, Map<String, String> queries) throws IOException;

	void get(String url, Map<String, String> headers, Map<String, String> queries,
			HttpOperationListener httpOperationListener);

	HttpResponse post(String url, Map<String, String> headers, Map<String, String> params) throws IOException;

	void post(String url, Map<String, String> headers, Map<String, String> params,
			HttpOperationListener httpOperationListener);

	public UploadHandle upload(String url, File file, Map<String, String> headers, UploadListener uploadListener);

	public static interface HttpOperationListener {
		void onThrowable(Throwable throwable);

		void onResponse(HttpResponse httpResponse);
	}

	public static interface UploadListener {
		void uploaded(long amount, long current, long total);

		void completed(HttpResponse httpResponse);

		void exception(Exception exception);
	}

	public static interface UploadHandle {
		void cancel();
	}
}
