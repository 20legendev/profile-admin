package vn.tcbs.cas.algorithm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

import org.apache.commons.lang.text.StrSubstitutor;

/**
 * 
 * @author NguyenNam
 * @description class parse XML
 * 
 */
public class SimpleParser {
	private String template;

	// protected static Pattern pattern =
	// Pattern.compile("([^\\?]+)(\\?.+=(.*))?");

	public SimpleParser() {

	}

	public SimpleParser(String template) {
		this.template = template;
	}

	public String parse(Map<String, String> valueMap) {
		StrSubstitutor sub = new StrSubstitutor(valueMap);
		String xml = sub.replace(template);
		return xml;
	}
	
	public static String getServerAddress(String serviceUri){
		String result = "";
		int port = 0;
		/*
		 * Matcher matcher = pattern.matcher(uri); if(matcher.find()){
		 * result=matcher.group(1); }
		 */
		try {
			ServiceURI uri = new ServiceURI(serviceUri);
			port = uri.getPort();
			if (port > 0) {
				result =  uri.getHost() + ":" + uri.getPort();
			} else {
				result =  uri.getHost();
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String getServicePath(String serviceUri) {
		String result = "";
		int port = 0;
		/*
		 * Matcher matcher = pattern.matcher(uri); if(matcher.find()){
		 * result=matcher.group(1); }
		 */
		try {
			ServiceURI uri = new ServiceURI(serviceUri);
			port = uri.getPort();
			if (port > 0) {
				result = uri.getScheme() + "://" + uri.getHost() + ":" + uri.getPort()
						+ uri.getPath();
			} else {
				result = uri.getScheme() + "://" + uri.getHost() + uri.getPath();
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static int getPort(String serviceUri){
		ServiceURI uri = null;
		try {
			uri = new ServiceURI(serviceUri);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return uri.getPort();
	}

	public static String getSoapAction(String serviceUri)
			throws URISyntaxException {
		String result = "";
		/*
		 * Matcher matcher = pattern.matcher(uri); if(matcher.find()){
		 * result=matcher.group(3); }
		 */
		ServiceURI uri = new ServiceURI(serviceUri);
		result = uri.getParameter("action");
		return result;
	}

	public static String getTemplateName(String serviceUri)
			throws URISyntaxException {
		String result = "";
		ServiceURI uri = new ServiceURI(serviceUri);
		result = uri.getParameter("template");
		return result;
	}

	public static String getTemplate(String fileName) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new FileReader(fileName));
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line.trim());
			}
			return sb.toString();
		} catch (IOException e) {
			throw e;
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static String getUser(String serviceUri) throws URISyntaxException {
		String result = "";
		ServiceURI uri = new ServiceURI(serviceUri);
		result = uri.getUsername();
		return result;
	}

	public static String getPass(String serviceUri) throws URISyntaxException {
		String result = "";
		ServiceURI uri = new ServiceURI(serviceUri);
		result = uri.getPassword();
		return result;
	}
	
	public static void main(String[] args) throws URISyntaxException, IOException {
		ServiceURI uri = new ServiceURI("http://cloud.wawa.vn/sms/sms.php?gate=dalink?template=Plus-H2T-MOReceiver-template.xml&action=http://cloud.wawa.vn/sms/sms.php?gate=dalink/messageReceiver");
		//System.out.println(getTemplateName("http://cloud.wawa.vn/sms/sms.php?gate=dalink?template=Plus-H2T-MOReceiver-template.xml&action=http://cloud.wawa.vn/sms/sms.php?gate=dalink/messageReceiver"));
		//System.out.println(getTemplateName("soap://card.gate.vn/api/Fo_ws/srvCard.asmx?action=http://psp.gate.vn/CardInput&template=cardInput.xml"));
		System.out.println(getPass("https://nam:nam@10.58.37.195:8443/ScratchCardAPI/ScratchCardAPI?action=http://api.scratchcard.viettel.com/topupCard&amp;template=vtt-card-template.xml"));
		
	}
}
