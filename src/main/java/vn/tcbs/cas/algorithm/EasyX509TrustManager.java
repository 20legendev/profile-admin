package vn.tcbs.cas.algorithm;


import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class EasyX509TrustManager implements X509TrustManager {
	private X509TrustManager standardTrustManager = null;

	private static final Log LOG = LogFactory
			.getLog(EasyX509TrustManager.class);

	public EasyX509TrustManager(KeyStore keystore)
			throws NoSuchAlgorithmException, KeyStoreException {
		TrustManagerFactory factory = TrustManagerFactory
				.getInstance("SunX509");
		factory.init(keystore);
		TrustManager[] trustmanagers = factory.getTrustManagers();
		if (trustmanagers.length == 0) {
			throw new NoSuchAlgorithmException(
					"SunX509 trust manager not supported");
		}
		this.standardTrustManager = ((X509TrustManager) trustmanagers[0]);
	}

	public boolean isClientTrusted(X509Certificate[] certificates) {
		return ((EasyX509TrustManager) this.standardTrustManager).isClientTrusted(certificates);
	}

	public boolean isServerTrusted(X509Certificate[] certificates) {
		if ((certificates != null) && (LOG.isDebugEnabled())) {
			LOG.debug("Server certificate chain:");
			for (int i = 0; i < certificates.length; ++i) {
				LOG.debug("X509Certificate[" + i + "]=" + certificates[i]);
			}
		}
		if ((certificates != null) && (certificates.length == 1)) {
			X509Certificate certificate = certificates[0];
			try {
				certificate.checkValidity();
			} catch (CertificateException e) {
				LOG.error(e.toString());
				return false;
			}
			return true;
		}
		return ((EasyX509TrustManager) this.standardTrustManager).isServerTrusted(certificates);
	}

	public X509Certificate[] getAcceptedIssuers() {
		return this.standardTrustManager.getAcceptedIssuers();
	}

	@Override
	public void checkClientTrusted(X509Certificate[] arg0, String arg1)
			throws CertificateException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void checkServerTrusted(X509Certificate[] arg0, String arg1)
			throws CertificateException {
		// TODO Auto-generated method stub
		
	}
}