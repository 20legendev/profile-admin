/*
 * Version: 1.0.0
 * Date: 2009/10/6
 *
 * (���v�α��v�y�z)
 * Copyright 2006 (C) Hyweb Technology Coperation.
 * All Rights Reserved.
 *
 * 
 * 
 * *********************************************** 
 */
package vn.tcbs.cas.algorithm;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.digest.DigestUtils;



/**
 * The Class DesSecurity.
 */
public class DesSecurity
{
    private static String salt = "hywebpg5";
    
    private static String mode = "ECB";//"CBC";
    
    private static String padding = "NoPadding";//"PKCS5Padding";
    
    /**
     * Encrypt.
     * 
     * @param msg the msg
     * @param k the k
     * @param type the type
     * @return the hex string
     * @throws Exception the exception
     */
    public String Encrypt(String msg, String k, String type) throws Exception
    {
        SecretKeySpec skey = getKey(k, type); 
        
        msg = padding(msg); 
        
        byte[] plainBytes = msg.getBytes(); 
        String encMode = type + "/" + mode + "/" + padding;
        Cipher cipher = Cipher.getInstance(encMode);
        IvParameterSpec ivspec = new IvParameterSpec(salt.getBytes()); 
        if("CBC".equals(mode))
        {
            cipher.init(Cipher.ENCRYPT_MODE, skey, ivspec);
        }
        else
        {
            cipher.init(Cipher.ENCRYPT_MODE, skey);
        }
        byte[] cipherText = cipher.doFinal(plainBytes); 
        return byte2hex(cipherText); 
        
//        IvParameterSpec iv = new IvParameterSpec(salt.getBytes());
//        SecretKeySpec skey = getKey(k, type);
//        
//        String encMode = type + "/" + mode + "/" + padding; //type + "/CBC/PKCS5Padding";
//        Cipher c1 = Cipher.getInstance(encMode);
//        c1.init(Cipher.ENCRYPT_MODE, skey, iv); 
//        byte[] cipherByte = c1.doFinal(msg.getBytes());
//        
//        return byte2hex(cipherByte);
    }
    
    /**
     * DESED e_ decrypt.
     * 
     * @param msg the msg
     * @param k the k
     * @param type the type
     * @return the hex string
     * @throws NoSuchPaddingException 
     * @throws NoSuchAlgorithmException 
     * @throws InvalidAlgorithmParameterException 
     * @throws InvalidKeyException 
     * @throws BadPaddingException 
     * @throws IllegalBlockSizeException 
     * @throws Exception the exception
     */
    public String Decrypt(String msg, String k, String type) throws Exception
    {
        SecretKeySpec skey = getKey(k,type);
        
        byte[] inPut = hex2byte(msg); 
        
        String decMode = type + "/" + mode + "/" + padding;
        Cipher cipher = Cipher.getInstance(decMode);
        IvParameterSpec ivspec = new IvParameterSpec(salt.getBytes()); 
        if("CBC".equals(mode))
        {
            cipher.init(Cipher.DECRYPT_MODE, skey, ivspec);
        }
        else
        {
            cipher.init(Cipher.DECRYPT_MODE, skey);
        }
        
        byte[] output = cipher.doFinal(inPut); 

//        return byte2hex(removePadding(output));  
        return new String(removePadding(output));
        
//        String s = null;
//        byte[] hex = hex2byte(msg);
//
//        IvParameterSpec iv = new IvParameterSpec(salt.getBytes());
//        SecretKeySpec skey = getKey(k,type);
//        
//        String decMode = type + "/" + mode + "/" + padding; //type+"/CBC/PKCS5Padding"
//        Cipher c1 = Cipher.getInstance(decMode);
//        c1.init(Cipher.DECRYPT_MODE, skey, iv); 
//        byte[] cipherByte = c1.doFinal(hex);
//        s = byte2hex(cipherByte);;
//
//        return s;
    }

    /**
     * DESMAC.
     * 
     * @param msg the msg
     * @param k the key
     * @return the string
     * @throws NoSuchAlgorithmException the no such algorithm exception
     * @throws NoSuchPaddingException the no such padding exception
     * @throws InvalidKeyException the invalid key exception
     * @throws InvalidAlgorithmParameterException the invalid algorithm parameter exception
     * @throws IllegalBlockSizeException the illegal block size exception
     * @throws BadPaddingException the bad padding exception
     * @throws InvalidKeySpecException 
     */
    public String DESMAC(String msg, String k, String type) throws Exception
    {  
//        int tmp = msg.length() % 8;
//        if(tmp != 0)
//        {
//            for(int i=0; i < 8-tmp; i++)
//            {       
//                msg = msg + "0";       
//            }
//        }
        
        SecretKeySpec skey = getKey(k,type); 
        
        msg = sha1(msg);
        byte[] mgsByte = macPadding(msg); 
            
        String encMode = type + "/" + mode + "/" + padding;
        Cipher cipher = Cipher.getInstance(encMode);
        IvParameterSpec ivspec = new IvParameterSpec(salt.getBytes()); 
        if("CBC".equals(mode))
        {
            cipher.init(Cipher.ENCRYPT_MODE, skey, ivspec);
        }
        else
        {
            cipher.init(Cipher.ENCRYPT_MODE, skey);
        }
        byte[] cipherText = cipher.doFinal(mgsByte);
        
        
        return byte2hex(cipherText);
        //return byte2hex1(cipherText);
        
//        int tmp = msg.length() % 8;
//        if(tmp != 0)
//        {
//            for(int i=0; i < 8-tmp; i++)
//            {       
//                msg = msg + "0";       
//            }
//        }
//
//        IvParameterSpec iv = new IvParameterSpec(salt.getBytes());       
//        Key skey = getKey(k, type);
//        
//        String macMode = type + "/" + mode + "/" + padding; //type + "/CBC/PKCS5Padding"
//        Cipher c1 = Cipher.getInstance(macMode);
//        c1.init(Cipher.ENCRYPT_MODE, skey, iv); 
//        byte[] cipherByte = c1.doFinal(msg.getBytes());
//
//        return byte2hex1(cipherByte);
    }
    
    public String sha1(String data)throws Exception
    {
//        if(internetloyalty.util.Utility.isStringNonEmpty(data))
        {
//            byte[] dataByteArray = ISOUtil.hex2byte(data);
            try
            {
                return DigestUtils.shaHex(data).toUpperCase();
//
//                MessageDigest algorithm = MessageDigest.getInstance("SHA-1");
//                algorithm.update(dataByteArray);  //set input
//                byte[] digest = algorithm.digest(); //generate digest
//
//                return //ISOUtil.hexString(digest);
            }
            catch (Exception ex)
            {
                throw new Exception("SHA-1: " + ex.toString());
            }      
        }
    }
    
    /**
     * Byte2hex.
     * 
     * @param b the b
     * @return the string
     */
    String byte2hex(byte[] b) 
    {
      String hs = "";
      String stmp = "";
      for (int n = 0; n < b.length; n++)
      {
          stmp = (Integer.toHexString(b[n] & 0XFF));
          
          if (stmp.length() == 1)
          {
              hs = hs + "0" + stmp;
          }
          else 
          {
              hs = hs + stmp;
          }
          
          if (n < (b.length-1))  
          {
              hs = hs + "";             
          }
      }
      return hs.toUpperCase();
    }
    
    /**
     * Byte2hex1.
     * 
     * @param b the b
     * @return the string
     */
    String byte2hex1(byte[] b) 
    {
        String hs = "";
        String stmp = "";
        for (int n = b.length-16; n < b.length-8; n++)
        {
            stmp = (Integer.toHexString(b[n] & 0XFF));
        
            if (stmp.length() == 1)
            {
                hs = hs + "0" + stmp;
            }
            else 
            {
                hs = hs + stmp;
            }
        
            if (n < (b.length-1))
            {  
                hs = hs+"";
            }
        }
        return hs.toUpperCase();
    }
    
    /**
     * Byte2hex_24.
     * 
     * @param b the b
     * @return the string
     */
    String byte2hex_24(byte[] b) 
    {
        String hs = "";
        String stmp = "";
        for (int n=b.length-32; n<b.length-8; n++)
        {
            stmp = (Integer.toHexString(b[n] & 0XFF));
            if (stmp.length()== 1)
            {
              hs = hs + "0" + stmp;
            }
            else 
            {
                hs = hs + stmp;             
            }
            
            if (n<b.length-1)
            {
                hs = hs + "";
            }
        }
        return hs.toUpperCase();
    }
        
    /**
     * Byte2hex_24_en.
     * 
     * @param b the b
     * @return the string
     */
    String byte2hex_24_en(byte[] b) 
    {
        String hs = "";
        String stmp = "";
        for (int n = b.length-24; n < b.length; n++)
        {
            stmp = (Integer.toHexString(b[n] & 0XFF));
            if (stmp.length()==1)
            {
              hs = hs + "0" + stmp;
            }
            else 
            {
                hs = hs + stmp;
            }
            if (n < (b.length-1))
            {
                hs = hs + "";
            }
        }
        return hs.toUpperCase();
    }

    public String byte2hex2(byte[] b)
    {
        String hs = "";
        String stmp = "";
        for (int n = 0; n < b.length; n++)
        {
            stmp = Integer.toHexString(b[n] & 0XFF);

            if (stmp.length() == 1)
            {
                hs = hs + "0" + stmp;
            }
            else
            {
                hs = hs + stmp;
            }

            if (n < (b.length - 1))
            {
                hs = hs + "";
            }
        }
        return hs.toUpperCase();
    }
     
    /**
     * Hex2byte.
     * 
     * @param hex the hex
     * @return the byte[]
     * @throws IllegalArgumentException the illegal argument exception
     */
    byte[] hex2byte(String hex) throws IllegalArgumentException   
    {   
        if((hex.length() % 2) != 0)   
        {   
            throw new IllegalArgumentException();   
        }   
        char[] arr = hex.toCharArray();   
        byte[] b = new   byte[hex.length()/2]; 
            
        for(int i = 0, j = 0, l = hex.length(); i < l; i++, j++)   
        {   
            String swap = "" + arr[i++] + arr[i];   
            int byteint = Integer.parseInt(swap, 16) & 0xFF;   
            b[j] = new Integer(byteint).byteValue();   
        }   
        return b;   
    }   
        
    private String padding(String str) throws Exception
    { 
        byte[] oldByteArray = str.getBytes();
        int numberToPad = 8 - oldByteArray.length % 8; 
        byte[] newByteArray = new byte[oldByteArray.length + numberToPad]; 
        System.arraycopy(oldByteArray, 0, newByteArray, 0, oldByteArray.length); 
        for (int i = oldByteArray.length; i < newByteArray.length; ++i) 
        { 
            newByteArray[i] = 0; 
        } 
        return new String(newByteArray);  
    } 
      
    private byte[] hex2packbyte(String s)
    {int ii = 0;
	int sif = 0;
    	try {
    		
        if(null != s && s.length() > 0)
        {
            int offset = 0;
            int len = s.length();
            byte[] b = s.getBytes();
            byte[] d = new byte[len];
            for (int i = 0; i < len * 2; i++)
            {
            	ii = i;
                int shift = i % 2 == 1 ? 0 : 4;
                sif = shift;
                d[i >> 1] |= Character.digit((char) b[offset + i], 16) << shift;
            }
            return d;
        }
    	} catch (Exception e) {
    		System.out.println(ii);
    		System.out.println(sif);
    	}
        return null;
    }
    
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    
    private byte[] macPadding(String str) 
    { 
//        byte[] oldByteArray = ISOUtil.hex2byte(str);
//    	byte[] oldByteArray = hex2packbyte(str);
    	byte[] oldByteArray = hexStringToByteArray(str);
        int numberToPad = 8 - oldByteArray.length % 8; 
        byte[] newByteArray = new byte[oldByteArray.length + numberToPad]; 
        System.arraycopy(oldByteArray, 0, newByteArray, 0, oldByteArray.length); 
        for (int i = oldByteArray.length; i < newByteArray.length; ++i) 
        { 
            newByteArray[i] = 0; 
        } 
        return newByteArray;  
    } 
    
    private byte[] removePadding(byte[] oldByteArray) 
    { 
        int numberPaded = 0; 
        for (int i = oldByteArray.length; i > 0; --i) 
        { 
            if (oldByteArray[(i - 1)] != 0) 
            { 
                numberPaded = oldByteArray.length - i; 
                break; 
            } 
        } 

        byte[] newByteArray = new byte[oldByteArray.length - numberPaded]; 
        System.arraycopy(oldByteArray, 0, newByteArray, 0, newByteArray.length); 

        return newByteArray; 
    } 
    
    /**
     * Gets the key.
     * 
     * @param keys the keys
     * @param mode the mode
     * @return the key
     */
    private SecretKeySpec getKey(String keys, String mode)
    {   
        SecretKeySpec pass = new SecretKeySpec(keys.getBytes(), mode);
        return pass;
    }
}
