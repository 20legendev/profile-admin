package vn.tcbs.cas.algorithm;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;

import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory;

public class CustomHttpsSocketFactory implements SecureProtocolSocketFactory {

	private SecureProtocolSocketFactory base;

	public CustomHttpsSocketFactory(ProtocolSocketFactory base) {
//		if (base == null || !(base instanceof SecureProtocolSocketFactory))
//			throw new IllegalArgumentException();
		try{
			SSLContext context = SSLContext.getInstance("SSL");
			context.init(null, new TrustManager[] { new EasyX509TrustManager(
					null) }, null);
			this.base = (SecureProtocolSocketFactory) context.getSocketFactory();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private Socket stripSSLv3(Socket socket) {
        if (!(socket instanceof SSLSocket))
            return socket;
        SSLSocket sslSocket = (SSLSocket) socket;
        String[] newProtocols = {"TLSv1"};
		sslSocket.setEnabledProtocols(newProtocols);
        return sslSocket;
    }

    public Socket createSocket(String host, int port) throws IOException {
        return stripSSLv3(base.createSocket(host, port));
    }
    
    public Socket createSocket(
            String host, int port,
            InetAddress localAddress, int localPort) throws IOException {
        return stripSSLv3(base.createSocket(
                host, port,
                localAddress, localPort));
    }
    
    public Socket createSocket(
            String host, int port,
            InetAddress localAddress, int localPort,
            HttpConnectionParams params) throws IOException {
        return stripSSLv3(base.createSocket(
                host, port,
                localAddress, localPort,
                params));
    }
    
    public Socket createSocket(
            Socket socket,
            String host, int port,
            boolean autoClose) throws IOException {
        return stripSSLv3(base.createSocket(
                socket,
                host, port,
                autoClose));
    }
}