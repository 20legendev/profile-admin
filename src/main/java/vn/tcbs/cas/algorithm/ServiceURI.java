/**
 * 
 */
package vn.tcbs.cas.algorithm;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author NguyenNam
 *
 */
public final class ServiceURI {
	private String username;
	private String password;
	private String host;
	private int port;
	private String scheme;
	private String path;
	private Map<String, String> parameterMap;

	public ServiceURI(String uri) throws URISyntaxException {
		URI serviceUri = new URI(uri);

		String userInfo = serviceUri.getUserInfo();
		parseUserInfo(userInfo);
		this.host = serviceUri.getHost();
		this.port = serviceUri.getPort();
		this.scheme = serviceUri.getScheme();
		this.path = serviceUri.getPath();
		parseParameters(serviceUri.getQuery());
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @return the scheme
	 */
	public String getScheme() {
		return scheme;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @return the parameterMap
	 */
	public Map<String, String> getParameterMap() {
		return parameterMap;
	}

	public String getParameter(String name) {
		return parameterMap.get(name);
	}

	private void parseUserInfo(String userInfo) {
		if (userInfo != null) {
			String[] parts = userInfo.split(":");
			if (parts.length > 0) {
				username = parts[0];
			}
			if (parts.length > 1) {
				password = parts[1];
			}
		}
	}

	private void parseParameters(String parameters) {
		parameterMap = new HashMap<String, String>();
		if (parameters == null) {
			return;
		}

		String[] nameValues = parameters.split("&");

		for (String nameValue : nameValues) {
			String[] parts = nameValue.split("=");
			String name = null;
			String value = null;
			if (parts.length > 0) {
				name = parts[0];
			}

			if (parts.length > 1) {
				value = parts[1];
			}

			value = (value != null) ? value : "";

			if (name != null) {
				parameterMap.put(name, value);
			}
		}
	}
}
