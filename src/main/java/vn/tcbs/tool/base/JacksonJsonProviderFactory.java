package vn.tcbs.tool.base;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import vn.tcbs.cas.profile.service.JsonService;

public class JacksonJsonProviderFactory {

	public static JacksonJsonProvider create() {
		ObjectMapper mapper = JsonService.getInstance().getObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		return new JacksonJsonProvider(mapper);
	}
}
