package vn.tcbs.tool.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import vn.tcbs.cas.http.context.HttpContext;

public class BaseESBService {

	@Value("${esb.url}")
	protected String esbUrl;
	@Autowired
	protected HttpContext httpContext;

}