package vn.tcbs.tool.base;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EsbDTO<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	@JsonProperty("Entries")
	private HashMap<String, List<T>> entries;

	public HashMap<String, List<T>> getEntries() {
		return entries;
	}

	public void setEntries(HashMap<String, List<T>> entries) {
		this.entries = entries;
	}

	public List<T> getData(String key) {
		return getEntries().get(key);
	}

}
