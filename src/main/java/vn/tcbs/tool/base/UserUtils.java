package vn.tcbs.tool.base;

import javax.servlet.http.HttpServletRequest;

public class UserUtils {

	public static boolean isLoggedIn(HttpServletRequest req) {
		return req.getRemoteUser() != null;
	}

}