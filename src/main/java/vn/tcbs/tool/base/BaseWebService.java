package vn.tcbs.tool.base;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.service.UserService;

public class BaseWebService {

	@Autowired
	private UserService userService;
	private static Logger logger = LoggerFactory.getLogger(BaseWebService.class);

	@SuppressWarnings("rawtypes")
	public Response generateResponse(ResponseData data) {
		return Response.ok(data).header("Access-Control-Allow-Origin", "*").build();
	}

	protected TcbsUser getUser(HttpServletRequest req) {
		logger.info("user = {}", req.getRemoteUser());
		if (req.getRemoteUser() == null) {
			return null;
		}
		Long userId = Long.valueOf(req.getRemoteUser());
		if (userId == null)
			return null;
		return userService.getById(userId);
	}

	protected boolean isLoggedIn(HttpServletRequest req) {
		return req.getRemoteUser() != null;
	}

	protected void logInfo(String key, Object data) {
		try {
			logger.info("{} = {}", key, new ObjectMapper().writeValueAsString(data));
		} catch (Exception ex) {
		}
	}
}
