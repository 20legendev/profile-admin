package vn.tcbs.tool.base;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.HashMap;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;



public class TcbsSoapUtils {

	private static final Logger logger = LoggerFactory.getLogger(TcbsSoapUtils.class);
	private static HashMap<String, String> soapContent = new HashMap<String, String>();

	public static SOAPConnection getSoapConnection() {
		try {
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();
			return soapConnection;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static SOAPMessage generateSoapMessage(String filePath,
			HashMap<String, Object> data, String action) {
		try {
			String soapText;
			String key = TcbsUtils.MD5(filePath);
			if (soapContent.containsKey(key)) {
				soapText = soapContent.get(key);
			} else {
				soapText = TcbsFileUtils.readResourceFile(filePath);
				soapContent.put(key, soapText);
			}
			if (soapText != null) {
				MessageFactory messageFactory = MessageFactory.newInstance();
				String soapContent = TcbsUtils.replaceHolder(soapText, data);
				
				logger.info("SENT: " + soapContent);
				
				SOAPMessage soapMessage = messageFactory.createMessage(
						new MimeHeaders(),
						new ByteArrayInputStream(soapContent.getBytes(Charset
								.forName("UTF-8"))));
				MimeHeaders headers = soapMessage.getMimeHeaders();
				headers.addHeader("SOAPAction", action);

				soapMessage.saveChanges();
				return soapMessage;
			}
			return null;
		} catch (Exception ex) {
			return null;
		}
	}
}