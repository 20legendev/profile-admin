package vn.tcbs.tool.base;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Binding.DestinationType;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedOperation;

import com.rabbitmq.client.Channel;

import vn.tcbs.cas.profile.admin.ErrorService;
import vn.tcbs.cas.profile.entity.TcbsErrorLog;

public class RetryHandler implements ChannelAwareMessageListener {

	@Autowired
	protected AmqpTemplate rabbitmqTemplate;
	@Autowired
	protected ErrorService errorService;
	@Value("${active.queue.retry.counter}")
	protected Integer maxRetryCount;
	@Autowired
	private AmqpAdmin amqpAdmin;
	protected int retryDelay = 120000; // 2 min

	private boolean retryQueueReady = false;

	protected static Logger logger = LoggerFactory.getLogger(RetryHandler.class);

	public RetryHandler() {
	}

	protected void initRetryQueue(String exchangeName, String requeueKey, String retryQueue) {
		Map<String, Object> config = new HashMap<String, Object>();
		config.put("x-dead-letter-exchange", exchangeName);
		config.put("x-dead-letter-routing-key", requeueKey);
		config.put("x-message-ttl", retryDelay);
		Queue queue = new Queue(retryQueue, true, false, false, config);
		amqpAdmin.declareQueue(queue);
		Binding binding = new Binding(retryQueue, DestinationType.QUEUE, exchangeName, retryQueue, null);
		amqpAdmin.declareBinding(binding);
		retryQueueReady = true;
	}

	protected void doRetry(Message message, String exchangeName, String requeueKey, String errorQueue, Exception ex) {
		if (retryQueueReady == false) {
			initRetryQueue(exchangeName, requeueKey, errorQueue);
		}
		send(exchangeName, errorQueue, message);
	}

	protected void notifyError(String errorKey, String content) {
		errorService.addNew(new TcbsErrorLog(errorKey, content, 0));
		logger.info("NOTIFY_ERROR: {}", errorKey);
	}

	@ManagedOperation
	public void send(String exchange, String key, String text) {
		rabbitmqTemplate.convertAndSend(exchange, key, text);
	}

	@ManagedOperation
	public void send(String exchange, String key, Message message) {
		rabbitmqTemplate.send(exchange, key, message);
	}

	@Override
	public void onMessage(Message arg0, Channel arg1) throws Exception {
	}

}