package vn.tcbs.tool.base;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Errors;

import com.google.common.xml.XmlEscapers;

import vn.tcbs.cas.profile.dao.ContractDAO;
import vn.tcbs.cas.profile.dao.UserDAO;
import vn.tcbs.cas.profile.object.OpenAccountMessage;
import vn.tcbs.cas.profile.object.OpenAccountMessageValidator;

public class TcbsUtils {

	private static Pattern pattern;
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	private static final Pattern phonePattern = Pattern.compile("^(\\+*)\\d{10,12}$");
	private static final Pattern passwordPattern = Pattern.compile("((?=.*\\d)(?=.*[a-zA-Z]).{6,20})");

	public static boolean isMobile(HttpServletRequest req) {
		String userAgent = req.getHeader("User-Agent");
		if (Pattern
				.compile(
						"iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune")
				.matcher(userAgent).find()) {
			return true;
		}
		return false;
	}

	public static String generateContractId(ContractDAO contractDAO) {
		String contractId = new SimpleDateFormat("yyMM").format(new Date());
		String contractNumber = contractId + TcbsUtils.generateRandom(4);
		while (contractDAO.checkContract(contractNumber) != null) {
			contractNumber = contractId + TcbsUtils.generateRandom(4);
		}
		return contractNumber;
	}

	public static String generateUserCode(UserDAO userDAO, String phone, String custodyCd) {
		String sixNumber;
		if (custodyCd != null) {
			sixNumber = custodyCd.substring(4);
		} else if (phone != null) {
			sixNumber = custodyCd.substring(phone.length() - 6);
		} else {
			sixNumber = generateRandom(6);
		}
		while (userDAO.getByUserCode("0001" + sixNumber) != null) {
			sixNumber = nextUserCode(sixNumber);
		}
		return "0001" + sixNumber;
	}

	private static String nextUserCode(String userCode) {
		String data = new StringBuilder(userCode).reverse().toString();
		Integer intData = Integer.valueOf(data);
		intData++;
		String tmp = new StringBuilder(String.valueOf(intData)).reverse().toString();
		return tmp;
	}

	public static boolean hasRole(String role) {
		Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) SecurityContextHolder.getContext()
				.getAuthentication().getAuthorities();
		boolean hasRole = false;
		for (GrantedAuthority authority : authorities) {
			hasRole = authority.getAuthority().equals(role);
			if (hasRole) {
				break;
			}
		}
		return hasRole;
	}

	public static Boolean validateEmail(String email) {
		if (pattern == null) {
			pattern = Pattern.compile(EMAIL_PATTERN);
		}
		if (email == null) {
			return false;
		}
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	public static Boolean validatePassword(String password, Errors errors) {
		Boolean validated = true;
		if (password == null) {
			if (errors != null)
				errors.rejectValue("password", "Mật khẩu trống");
			validated = false;
		}
		if (password.indexOf(" ") >= 0) {
			if (errors != null)
				errors.rejectValue("password", "Mật khẩu chứa ký tự trắng");
			validated = false;
		}
		Matcher matcher = passwordPattern.matcher(password);
		if (!matcher.matches()) {
			if (errors != null)
				errors.rejectValue("password", "Mật khẩu phải chứa ký tự số và chữ");
			validated = false;
		}
		return validated;
	}

	public static String escapeXml(String input) {
		return XmlEscapers.xmlContentEscaper().escape(input);
	}

	public static String escapeJson(String input) {
		return StringEscapeUtils.escapeJson(input);
	}

	public static boolean validatePhone(String s) {
		if (s == null)
			return false;
		Matcher m = phonePattern.matcher(s);
		boolean isFound = m.find();
		return isFound;
	}

	public static boolean validateDate(String dateStr, String dateFormat) {
		if (dateStr == null) {
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);
		try {
			sdf.parse(dateStr);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	public static String escapeString(String input) {
		String[] arr = { "+", "-", "&&", "||", "!", "(", ")", "{", "}", "[", "]", "^", "\"", "~", "*", "?", ":", "\\",
				"AND", "OR" };

		for (int i = 0; i < arr.length; i++) {
			if (input.contains((String) arr[i])) {
				String oldString = (String) arr[i];
				String newString = new String("\\" + arr[i]);
				input = input.replaceAll(oldString, (String) ("\\" + newString));
			}
		}
		System.out.println(input);
		return "OK";
	}

	public static String replaceHolder(Object source, Map<String, Object> valueMap) {
		String staticResolved = new StrSubstitutor(valueMap).replace(source);
		Pattern p = Pattern.compile("(\\$\\{date)(.*?)(\\})");
		Matcher m = p.matcher(staticResolved);
		String dynamicResolved = staticResolved;
		while (m.find()) {
			String result = MessageFormat.format("{0,date" + m.group(2) + "}", new Date());
			dynamicResolved = dynamicResolved.replace(m.group(), result);
		}
		return dynamicResolved;
	}

	public static String generateRandom(int length) {
		Random random = new Random();
		char[] digits = new char[length];
		digits[0] = (char) (random.nextInt(9) + '1');
		for (int i = 1; i < length; i++) {
			digits[i] = (char) (random.nextInt(10) + '0');
		}
		return new String(digits);
	}

	public static String randomPassword(int length) {
		return RandomStringUtils.randomAlphanumeric(length).toLowerCase();
	}

	public static Boolean validateLogin(HttpServletRequest req) {
		String agentId = (String) req.getSession(true).getAttribute("agentid");
		if (agentId == null || agentId.equals("")) {
			return false;
		}
		return true;
	}

	public static String trim(String input) {
		if (input == null) {
			return null;
		}
		input = input.replaceAll("( )+", " ");
		return input;
	}

	public static BindingResult validateOpenAccount(OpenAccountMessage msg) {
		DataBinder binder = new DataBinder(msg);
		binder.setValidator(new OpenAccountMessageValidator());
		binder.validate();
		BindingResult errors = binder.getBindingResult();
		return errors;
	}

	public static String detectEmailProvider(String email) {
		if (email.indexOf("@gmail.com") >= 0) {
			return "GOOGLE";
		}
		if (email.indexOf("@yahoo.com") >= 0) {
			return "YAHOO";
		}
		if (email.indexOf("@live.com") >= 0 || email.indexOf("@outlook.com") >= 0
				|| email.indexOf("@hotmail.com") >= 0) {
			return "MS";
		}
		return "NA";
	}

	public static String MD5(String md5) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		}
		return null;
	}

	public static Boolean preventAttach(HttpSession sess) {
		String lastCall = (String) sess.getAttribute("lastCall");
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date = new Date();
		if (lastCall == null) {
			sess.setAttribute("lastCall", dateFormat.format(date));
			return true;
		}
		sess.setAttribute("lastCall", dateFormat.format(date));
		Date lastDate;
		try {
			lastDate = dateFormat.parse(lastCall);
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		if (lastDate == null) {
			return false;
		}
		if (date.getTime() - lastDate.getTime() < 1500) {
			return false;
		}
		return true;
	}

}