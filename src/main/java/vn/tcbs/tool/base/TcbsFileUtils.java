package vn.tcbs.tool.base;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class TcbsFileUtils {

	public static String readResourceFile(String filePath) throws IOException {
		Resource resource = new ClassPathResource(filePath);
		InputStream is = resource.getInputStream();
		return getStringFromInputStream(is);
	}

	public static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}

	public static String getMimeType(String contentType) {
		if (contentType.indexOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") >= 0
				|| contentType.indexOf("application/vnd.ms-excel") >= 0) {
			return "EXCEL";
		}
		if (contentType.indexOf("image/png") >= 0) {
			return "IMAGETYPE_PNG";
		}
		if (contentType.indexOf("image/jpeg") >= 0) {
			return "IMAGETYPE_JPEG";
		}
		return null;
	}

	public static String generateFilename(String uploadDir, String fileName) {
		String filePath = uploadDir + File.separator + TcbsUtils.generateRandom(6) + "-" + fileName;
		File f = new File(filePath);
		while (f.exists()) {
			filePath = uploadDir + File.separator + TcbsUtils.generateRandom(6) + "-" + fileName;
			f = new File(filePath);
		}
		return filePath;
	}

	public static void setFolderWriteable(String folderPath) {
		File outputF = new File(folderPath);
		if (!outputF.exists()) {
			outputF.getParentFile().mkdirs();
			outputF.getParentFile().setWritable(true);
			outputF.getParentFile().setReadable(true);
		}
	}
}