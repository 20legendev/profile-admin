package vn.tcbs.tool.base;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateRowCount implements Serializable {

	private static final long serialVersionUID = 1L;
	@JsonProperty("UpdatedRowCount")
	private HashMap<String, Integer> entries;

	public Integer getRowCount() {
		return this.entries.get("Value");
	}

}
