TabDictionary = {
	1 : "Danh muc nguoi dung",
	2 : "Cp theo nhom",
	3 : "Hose",
	4 : "GDTT hose",
	5 : "Hnx",
	6 : "GDTT hnx",
	7 : "Upcom"
};
MessageUnmashaller = {
	SEPARATOR : "|",
	map : {
		STOCK : function(b) {
			var a = b.split(MessageUnmashaller.SEPARATOR);
			if (a.length < 40) {
				console.error("StockInfo message structre is change. ")
			}
			itemInfo = {};
			itemInfo.floorCode = a[0];
			itemInfo.tradingDate = a[1];
			itemInfo.time = a[2];
			itemInfo.code = a[3];
			itemInfo.companyName = a[4];
			itemInfo.stockType = a[5];
			itemInfo.totalRoom = a[6];
			itemInfo.currentRoom = a[7];
			itemInfo.basicPrice = a[8];
			itemInfo.openPrice = a[9];
			itemInfo.closePrice = a[10];
			itemInfo.currentPrice = a[11];
			itemInfo.currentQtty = a[12];
			itemInfo.highestPrice = a[13];
			itemInfo.lowestPrice = a[14];
			itemInfo.ceilingPrice = a[15];
			itemInfo.floorPrice = a[16];
			itemInfo.totalOfferQtty = a[17];
			itemInfo.totalBidQtty = a[18];
			itemInfo.matchPrice = a[19];
			itemInfo.matchQtty = a[20];
			itemInfo.matchValue = a[21];
			itemInfo.averagePrice = a[22];
			itemInfo.bidPrice01 = a[23];
			itemInfo.bidQtty01 = a[24];
			itemInfo.bidPrice02 = a[25];
			itemInfo.bidQtty02 = a[26];
			itemInfo.bidPrice03 = a[27];
			itemInfo.bidQtty03 = a[28];
			itemInfo.offerPrice01 = a[29];
			itemInfo.offerQtty01 = a[30];
			itemInfo.offerPrice02 = a[31];
			itemInfo.offerQtty02 = a[32];
			itemInfo.offerPrice03 = a[33];
			itemInfo.offerQtty03 = a[34];
			itemInfo.accumulatedVal = a[35];
			itemInfo.accumulatedVol = a[36];
			itemInfo.buyForeignQtty = a[37];
			itemInfo.sellForeignQtty = a[38];
			itemInfo.projectOpen = a[39];
			itemInfo.sequence = a[40];
			return itemInfo
		},
		MARKETINFO : function(b) {
			var a = b.split(MessageUnmashaller.SEPARATOR);
			itemInfo = {};
			itemInfo.marketID = a[0];
			itemInfo.totalTrade = a[1];
			itemInfo.totalShareTraded = a[2];
			itemInfo.totalValueTraded = a[3];
			itemInfo.advance = a[4];
			itemInfo.decline = a[5];
			itemInfo.noChange = a[6];
			itemInfo.indexValue = a[7];
			itemInfo.changed = a[8];
			itemInfo.tradingTime = a[9];
			itemInfo.tradingDate = a[10];
			itemInfo.floorCode = a[11];
			itemInfo.marketIndex = a[12];
			itemInfo.priorMarketIndex = a[13];
			itemInfo.highestIndex = a[14];
			itemInfo.lowestIndex = a[15];
			itemInfo.shareTraded = a[16];
			itemInfo.status = a[17];
			itemInfo.sequence = a[18];
			return itemInfo
		},
		PT_ORDER : function(b) {
			var a = b.split(MessageUnmashaller.SEPARATOR);
			itemInfo = {};
			itemInfo.floorCode = a[0];
			itemInfo.symbol = a[1];
			itemInfo.price = parseFloat(a[2]);
			itemInfo.volume = parseFloat(a[3]);
			itemInfo.time = a[4];
			itemInfo.sequence = a[5];
			itemInfo.basicPrice = a[6];
			itemInfo.ceilingPrice = a[7];
			itemInfo.floorPrice = a[8];
			return itemInfo
		},
		AD_ORDER : function(b) {
			var a = b.split(MessageUnmashaller.SEPARATOR);
			itemInfo = {};
			itemInfo.floorCode = a[0];
			itemInfo.stockSymbol = a[1];
			itemInfo.price = a[2];
			itemInfo.vol = a[3];
			itemInfo.type = a[4];
			itemInfo.status = a[5];
			itemInfo.time = a[6];
			itemInfo.sequence = a[7];
			itemInfo.basicPrice = a[8];
			itemInfo.ceilingPrice = a[9];
			itemInfo.floorPrice = a[10];
			return itemInfo
		},
		TRANSACTION : function(b) {
			var a = b.split(MessageUnmashaller.SEPARATOR);
			itemInfo = {};
			itemInfo.floorCode = a[0];
			itemInfo.symbol = a[1];
			itemInfo.highest = a[2];
			itemInfo.last = a[3];
			itemInfo.lastVol = a[4];
			itemInfo.lowest = a[5];
			itemInfo.matchType = a[6];
			itemInfo.openPrice = a[7];
			itemInfo.time = a[8];
			itemInfo.sequence = a[9];
			itemInfo.basicPrice = a[10];
			return itemInfo
		}
	},
	regist : function(b, a) {
		if (typeof a == "function") {
			this.map[b] = a
		}
	},
	unmashall : function(a, b) {
		if (typeof this.map[a] == "undefined") {
			return b
		}
		return this.map[a](b)
	}
};
StockBasicInfosManager = {
	stocks : [],
	setInfos : function(a) {
		this.stocks = a
	},
	getHoseList : function() {
		return this.getStockCodes(CONST_HSX_NAME)
	},
	getHnxList : function() {
		return this.getStockCodes(CONST_HNX_NAME)
	},
	getUpcomList : function() {
		return this.getStockCodes(CONST_UPC_NAME)
	},
	getStockCodes : function(b) {
		var a = [];
		for (var c = 0; c < this.stocks.length; c++) {
			if (this.stocks[c].floorCode == b) {
				a.push(this.stocks[c].code)
			}
		}
		return a
	},
	contain : function(b) {
		var a = false;
		for (var c = 0; c < this.stocks.length; c++) {
			if (this.stocks[c].code == b) {
				a = true;
				break
			}
		}
		return a
	}
};
function ConsumerManager() {
	this.init()
}
ConsumerManager.prototype = {
	init : function() {
		this.crrCodesList = {}
	},
	regist : function(d, b) {
		var e = this.crrCodesList[d];
		if (!e) {
			this.crrCodesList[d] = b;
			return b
		}
		var a = ArrayUtil.getChangeArr(b, e);
		for (var c = 0; c < a.length; c++) {
			e.push(a[c])
		}
		return e
	},
	stop : function(e, b) {
		var f = this.crrCodesList[e];
		if (!f) {
			return []
		}
		var a = [];
		for (var d = 0; d < b.length; d++) {
			var c = f.indexOf(b[d]);
			if (c >= 0) {
				f.splice(c, 1);
				a.push(b[d])
			}
		}
		return a
	}
};
function ServerPing() {
}
ServerPing.prototype = {
	getAliveServer : function(a) {
		var b = a.length;
		var c = $.Deferred();
		$.each(a, function(e, d) {
			$.ajax({
				url : d,
				timeout : 10000,
				dataType : "jsonp",
				jsonp : "ping"
			}).done(function() {
				c.resolve(d)
			}).fail(function() {
				b--;
				if (b == 0) {
					c.reject()
				}
			});
		});
		return c
	}
};
function ServerProvider(b) {
	var a = this;
	if (!b) {
		throw Error("Server Provider must be setted server list")
	}
	a.serverList = a.shufferServerList(b);
	a.serverPing = new ServerPing()
}
ServerProvider.prototype = {
	crrServerIndex : 0,
	pingUrls : function() {
		var a = this;
		var c = a.shufferServerList(a.serverList);
		var b = c.splice(0, 3);
		return b
	},
	getRandomServer : function() {
		var a = Math.round(Math.random() * 100) % this.serverList.length;
		this.crrServerIndex = a;
		return this.serverList[a]
	},
	getCrrServer : function() {
		return this.serverList[this.crrServerIndex]
	},
	shufferServerList : function(f) {
		var d = [];
		var e = f.length;
		var a = Math.round(Math.random() * 100) % e;
		for (var c = 0; c < e; c++) {
			var b = (a + c) % e;
			d[c] = f[b]
		}
		return d
	},
	pollNextServer : function() {
		var a = this;
		var c = $.Deferred();
		var b = Config.socket.pingNumberLimit;
		a.serverPing.getAliveServer(a.pingUrls()).done(function(d) {
			c.resolve(d)
		}).fail(function() {
			var d = setInterval(function() {
				a.serverPing.getAliveServer(a.pingUrls()).done(function(e) {
					c.resolve(e);
					clearInterval(d)
				}).fail(function() {
					b--;
					if (b === 0) {
						clearInterval(d);
						c.reject()
					}
				})
			}, 1000)
		});
		return c
	},
	add : function(a) {
		this.serverList.push(a)
	},
	getSockjsRandomServer : function() {
		return this.getRandomServer() + "realtime"
	},
	getSockJSNextServer : function() {
		return this.pollNextServer().then(function(a) {
			return a + "realtime"
		})
	},
	getAjaxRandomServer : function() {
		return this.getRandomServer()
	},
	getAjaxNextServer : function() {
		return this.pollNextServer()
	}
};
SockJS.prototype.handler = {};
SockJS.prototype.onEvent = function(a, b) {
	this.handler[a] = b
};
SockJS.prototype.onmessage = function(a) {
	var b = JSON.parse(a.data);
	if (typeof (this.handler[b.type]) == "function") {
		this.handler[b.type](b.type, b.data)
	}
};
SockJS.prototype.emit = function() {
	var a;
	if (arguments.length == 2) {
		a = JSON.stringify({
			type : arguments[0],
			data : arguments[1]
		})
	} else {
		a = JSON.stringify(arguments[0])
	}
	this.send(a)
};
function SocketManager(b, a) {
	this.serverProvider = b;
	this.CONNECTION_TIME_OUT = 100000;
	this.options = a;
	this.consumerManager = new ConsumerManager()
}
SocketManager.prototype = {
	options : {},
	hasConnect : false,
	state : "",
	eventPool : [],
	eventList : [ "STOCK", "MARKETINFO", "TRANSACTION", "PUT", "PUTEXEC" ],
	crrSequence : 0,
	crrActiveSequence : 0,
	start : function() {
		this.jqueryOb = $(this);
		var a = this;
		a.serverProvider.getSockJSNextServer().done(function(b) {
			a.createConnect(b)
		})
	},
	createConnect : function(b) {
		var a = this;
		console.log(b);
		this.socket = new SockJS(b);
		this.socket.onopen = function() {
			console.log("connected");
			if (this.hasConnect) {
				this.setState("resume")
			} else {
				this.setState("init");
				this.hasConnect = true
			}
			this.listenServerEvent();
			this.consumeAfterConnect()
		}.bind(this);
		this.socket.onclose = function() {
			this.setState("pause")
		}.bind(this);
		this.socket.onheartbeat = function() {
			a.heartbeatTime = new Date().getTime();
			a.socket.emit("heartbeat")
		};
		this.checkConnectionAlive()
	},
	checkConnectionAlive : function() {
		var a = this;
		if (a.checkConnectionAliveInterval) {
			clearInterval(a.checkConnectionAliveInterval)
		}
		a.checkConnectionAliveInterval = setInterval(function() {
			var b = new Date().getTime();
			if ((b - a.heartbeatTime) >= a.CONNECTION_TIME_OUT) {
				a.socket.close()
			}
		}, a.CONNECTION_TIME_OUT)
	},
	listenServerEvent : function() {
		this.socket.onEvent("returnFull", function(c, d) {
			this.emit("receiveFullData", d)
		}.bind(this));
		this.socket.onEvent("returnData", function(c, d) {
			this.emit("returnData", d)
		}.bind(this));
		this.socket.onEvent("finishStop", function(c, d) {
			if (d == DataEventType.STOCK) {
				this.registCrrCodes(DataEventType.STOCK, false)
			}
		}.bind(this));
		for (var b = 0; b < this.eventList.length; b++) {
			var a = this.eventList[b] + "";
			this.socket.onEvent(a, function(d, e) {
				var c = MessageUnmashaller.unmashall(d, e);
				this.handleDataEvent(d, c)
			}.bind(this))
		}
	},
	active : function() {
		this.setState("ready")
	},
	reconnect : function() {
		var a = this;
		setTimeout(function() {
			a.serverProvider.getSockJSNextServer().done(function(b) {
				a.createConnect(b)
			})
		}, Config.reconnectDelayTime)
	},
	handleDataEvent : function(a, b) {
		this.setSequence(b.sequence);
		this.emit(a, b)
	},
	releasePoolEvent : function() {
		for (var a = 0; a < this.eventPool.length; a++) {
			var b = this.eventPool[a];
			this.emit(b.eventName, b.data)
		}
	},
	setSequence : function(a) {
		if (this.crrSequence >= a) {
			return false
		}
		this.crrSequence = a;
		return true
	},
	requestFullData : function(a) {
		this.socket.emit("requestFullData", {
			sequence : this.crrSequence,
			params : a
		})
	},
	postData : function(a) {
		this.socket.emit("post", {
			sequence : this.crrSequence,
			params : a
		})
	},
	setState : function(a) {
		this.state = a;
		switch (a) {
		case "init":
			this.registerCodeInterval();
			break;
		case "active":
			break;
		case "ready":
			this.releasePoolEvent();
			this.setState("active");
			break;
		case "pause":
			this.reconnect();
			break;
		case "resume":
			this.socket.emit("resumne", {
				sequence : this.crrSequence
			});
			this.setState("active");
			break
		}
		this.emit(a, {})
	},
	test : function(a) {
		this.socket.emit("loadTest", {
			sequence : this.crrSequence,
			data : a
		})
	},
	regisEvent : function(a) {
		this.eventList = a
	},
	registConsumer : function(b) {
		if (this.eventList.indexOf(b.type) == -1) {
			this.eventList.push(b.type)
		}
		var a = this.consumerManager.regist(b.type, b.codes);
		try {
			if (this.state != "" && this.state != "pause") {
				this.socket.emit("registConsumer", {
					sequence : this.crrSequence,
					params : {
						name : b.type,
						codes : a
					}
				})
			}
		} catch (c) {
		}
	},
	consumeAfterConnect : function() {
		var c = Object.keys(this.consumerManager.crrCodesList);
		if (!c) {
			return
		}
		for (var b = 0; b < c.length; b++) {
			var a = this.consumerManager.crrCodesList[c[b]];
			this.socket.emit("registConsumer", {
				sequence : this.crrSequence,
				params : {
					name : c[b],
					codes : a
				}
			})
		}
	},
	registerCodeInterval : function() {
		setInterval(function() {
			this.registCrrCodes(DataEventType.STOCK, true)
		}.bind(this), 5 * 60 * 1000)
	},
	registCrrCodes : function(b, c) {
		var a = this.consumerManager.crrCodesList[DataEventType.STOCK];
		this.socket.emit("registConsumer", {
			sequence : this.crrSequence,
			params : {
				name : DataEventType.STOCK,
				codes : a
			},
			isIntervalRegist : c
		})
	},
	stopConsume : function(b) {
		var a = this.consumerManager.stop(b.type, b.codes);
		try {
			if (this.state != "" && this.state != "pause") {
				this.socket.emit("stopConsume", {
					sequence : this.crrSequence,
					params : {
						name : b.type,
						codes : a
					}
				})
			}
		} catch (c) {
		}
	},
	emit : function(b, a) {
		this.jqueryOb.trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	}
};
DataEventType = {
	STOCK : "STOCK",
	AD_ORDER : "AD_ORDER",
	PT_ORDER : "PT_ORDER",
	MARKETINFO : "MARKETINFO",
	TRANSACTION : "TRANSACTION",
	CATEGORY : "CATEGORY",
	MARKET_BEGIN : "MARKET_BEGIN",
	MARKET_END : "MARKET_END",
	MARKET_END_RECOVERY : "MARKET_END_RECOVERY",
	CEILING_FLOOR_COUNT : "CEILING_FLOOR_COUNT",
	MARKET_STATUS : "MARKET_STATUS"
};
function AjaxHelper(a) {
	this.serverProvider = a
}
AjaxHelper.prototype = {
	init : function() {
		return this.resetServer()
	},
	request : function(c, d) {
		var a = this;
		var b = this.crrServer + c;
		$.ajax({
			url : b,
			timeout : 10000,
			dataType : "jsonp",
			jsonp : "jsonp"
		}).done(function(e) {
			if (typeof e == "string") {
				e = JSON.parse(e)
			}
			d(e)
		}).fail(function() {
			a.resetServer().done(function() {
				a.request(c, d)
			})
		}.bind(this, c, d))
	},
	resetServer : function() {
		var a = this;
		return this.serverProvider.getAjaxNextServer().then(function(b) {
			a.crrServer = b
		})
	}
};
TimeBox = function(a) {
	this.init();
	for ( var b in a) {
		if (typeof a[b] != "undefined") {
			this[b] = a[b]
		}
	}
	this.active()
};
TimeBox.prototype = {
	init : function() {
		this.items = [];
		this.lifeTime = 1000;
		this.isNotReleasing = true
	},
	active : function() {
		this.releaseProcess = setTimeout(function() {
			this.release()
		}.bind(this), this.lifeTime)
	},
	push : function(a) {
		this.items.push(a)
	},
	release : function() {
		this.isNotReleasing = false;
		for (var a = 0; a < this.items.length; a++) {
			this.callback(this.items[a])
		}
	},
	reset : function() {
		clearTimeout(this.releaseProcess);
		this.active()
	},
	getLength : function() {
		return this.items.length
	}
};
function StockInfoComponent(a) {
	if (!a || !a.isExtendClass) {
		this.name = "stIf";
		this.isActive = false;
		this.model = new StockInfoModel();
		this.model.cmp = this;
		this.view = new StockInfoView();
		this.view.cmp = this;
		this.listenViewEvent();
		this.listenModelEvent();
		this.listenOutsideEvent();
		this.jqueryObject = $(this)
	}
}
StockInfoComponent.prototype = {
	listenViewEvent : function() {
		this.view.on("rowClick", function(a) {
			a.message.stockInfo = this.model.stocks[a.message.stockCode];
			System.emit("rowClick", a.message)
		}.bind(this));
		this.view.on("headerClick", function(a) {
			System.emit("headerClick")
		});
		this.view.on("renderComplete", function(a) {
			this.emit("renderComplete")
		}.bind(this))
	},
	listenModelEvent : function() {
		this.model.on("needUpdateUI_StockInfo", function(a) {
			this.view.refreshStockInfo(a.message.info, this.name)
		}.bind(this));
		this.model.on("addStock", function(a) {
			this.view.push(a.message, this.name)
		}.bind(this));
		this.model.on("removeStock", function(a) {
			this.view.remove(a.message, this.name)
		}.bind(this));
		this.model.on("onUpdateFullData", function(a) {
			this.view.render(ArrayUtil.getArrFromMap(this.model.stocks),
					this.name)
		}.bind(this));
		this.model.on("onPredictATO", function(a) {
			this.view.predictATO_ATC(a.message, this.name)
		}.bind(this));
		this.model.on("onPredictATC", function(a) {
			this.view.predictATO_ATC(a.message, this.name)
		}.bind(this));
		this.model.on("onClearATCPrice", function(a) {
			this.view.clearATC_ATOPrice(a.message, this.name)
		}.bind(this));
		this.model.on("onClearATOPrice", function(a) {
			this.view.clearATC_ATOPrice(a.message, this.name)
		}.bind(this))
	},
	listenOutsideEvent : function() {
		System.socketManager.on("returnData", function(a) {
			if (!this.isActive) {
				return
			}
			if (a.message.name == DataEventType.STOCK) {
				this.model.updateList(a.message.data)
			}
		}.bind(this));
		System.socketManager.on(DataEventType.STOCK, function(a) {
			if (!this.isActive) {
				return
			}
			this.model.updateStock(a.message)
		}.bind(this))
	},
	requestBaseOnFloorCode : function(a) {
		this.model.requestBaseOnFloorCode(a)
	},
	setWrp : function(a) {
		this.view.setWrp(a)
	},
	active : function() {
		this.isActive = true;
		this.model.active()
	},
	deActive : function() {
		this.isActive = false;
		this.model.stopConsume()
	},
	setCrrRenderFloorCode : function(a) {
		this.view.setCrrRenderFloorCode(a)
	},
	renderHose : function(a) {
		this.view.render(this.model.getHoseList(), a, this.name, true)
	},
	renderHnx : function(a) {
		this.view.render(this.model.getHnxList(), a, this.name, true)
	},
	renderUpcom : function(a) {
		this.view.render(this.model.getUpcomList(), a, this.name, true)
	},
	updateStock : function(a) {
		this.model.updateStock(a)
	},
	updateFullData : function(a) {
		this.model.updateFullData(a)
	},
	getCrrStockList : function() {
		return this.model.getCrrStockList()
	},
	getStockInfo : function(a) {
		return cloneObject(this.model.stocks[a])
	},
	push : function(a) {
		if (a) {
			this.model.push(a)
		}
	},
	scrollTo : function(a) {
		this.view.scrollTo(a, this.name)
	},
	remove : function(a) {
		this.model.remove(a)
	},
	emit : function(b, a) {
		this.jqueryObject.trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	setState : function(a) {
		this.model.setState(a)
	},
	render : function(a) {
		this.view.render(this.model.getStockList(), a, this.name)
	},
	predictATO : function(a) {
		this.model.predictATO(a)
	},
	predictATC : function(a) {
		this.model.predictATC(a)
	},
	clearATCPrice : function(a) {
		this.model.clearATCPrice(a)
	},
	clearATOPrice : function(a) {
		this.model.clearATOPrice(a)
	},
	on : function(b, a) {
		$(this).bind(b, a)
	},
	setTab : function(a) {
		this.model.setTab(a)
	},
	setStockCodes : function(a) {
		if (!this.isActive) {
			return
		}
		this.model.setStockCodes(a)
	}
};
function StockInfoModel() {
	this.crrState = "";
	this.cpm = null;
	this.stocks = {};
	this.crrStockCodes = [];
	this.categozyList = [];
	this.crrSequence = 0;
	this.jqueryObject = $(this)
}
StockInfoModel.prototype = {
	start : function() {
	},
	updateStock : function(b) {
		this.crrSequence = b.sequence;
		this.standardizeData(b);
		if (this.checkIsShow(b.code)) {
			this.emit("needUpdateUI_StockInfo", {
				code : b.code,
				info : b
			})
		}
		if (this.stocks[b.code]) {
			for ( var a in b) {
				if (b[a] && b[a] != "Null" && a != "companyName") {
					this.stocks[b.code][a] = b[a]
				}
			}
		} else {
			this.stocks[b.code] = b
		}
		if (System.marketInfosManager.isATO(b.floorCode)) {
			this.predictATOForOneStock(this.stocks[b.code])
		}
		if (System.marketInfosManager.isATC(b.floorCode)) {
			this.predictATCForOneStock(this.stocks[b.code])
		}
	},
	updateList : function(e) {
		var b = Object.keys(e);
		for (var d = 0; d < b.length; d++) {
			var a = b[d];
			var c = MessageUnmashaller.map.STOCK(e[a]);
			this.updateStock(c)
		}
	},
	getCateList : function(a) {
	},
	getHoseList : function() {
		var a = [];
		for ( var b in this.stocks) {
			if (this.stocks[b].floorCode == CONST_HSX_NAME) {
				a.push(this.stocks[b])
			}
		}
		return a
	},
	getHnxList : function() {
		var a = [];
		for ( var b in this.stocks) {
			if (this.stocks[b].floorCode == CONST_HNX_NAME) {
				a.push(this.stocks[b])
			}
		}
		return a
	},
	getUpcomList : function() {
		var a = [];
		for ( var b in this.stocks) {
			if (this.stocks[b].floorCode == CONST_UPC_NAME) {
				a.push(this.stocks[b])
			}
		}
		return a
	},
	getCrrStockList : function() {
		var a = [];
		switch (this.crrState) {
		case "HOSE":
			for ( var b in this.stocks) {
				if (this.stocks[b].floorCode == CONST_HSX_NAME) {
					a.push(this.stocks[b])
				}
			}
			break
		}
		return a
	},
	getStockList : function() {
		var a = [];
		for ( var b in this.stocks) {
			a.push(this.stocks[b])
		}
		return a
	},
	checkIsShow : function(a) {
		return true
	},
	setState : function(a) {
		if (this.crrState == a) {
			return
		}
		this.crrState = a
	},
	active : function() {
		System.socketManager.registConsumer({
			type : DataEventType.STOCK,
			codes : this.crrStockCodes
		})
	},
	setStockCodes : function(a) {
		var b = ArrayUtil.getChangeArr(this.crrStockCodes, a);
		System.socketManager.stopConsume({
			type : DataEventType.STOCK,
			codes : b
		});
		this.crrStockCodes = a;
		this.getDataFromServer(a);
		System.socketManager.registConsumer({
			type : DataEventType.STOCK,
			codes : a
		})
	},
	getDataFromServer : function(a) {
		System.ajaxHelper.request("priceservice/secinfo/snapshot/q=codes:"
				+ a.join(","), function(b) {
			if (!this.cmp.isActive) {
				return
			}
			this.updateFullData(b)
		}.bind(this))
	},
	updateFullData : function(c) {
		if (!this.cmp.isActive) {
			return
		}
		this.stocks = {};
		this.crrStockCodes = [];
		for (var b = 0; b < c.length; b++) {
			var d = c[b];
			if (d) {
				var a = MessageUnmashaller.map.STOCK(d);
				this.crrStockCodes.push(a.code);
				this.standardizeData(a);
				this.stocks[a.code] = a
			}
		}
		this.emit("onUpdateFullData")
	},
	requestBaseOnFloorCode : function(a) {
		this.crrStockCodes = [];
		System.ajaxHelper.request("priceservice/secinfo/snapshot/q=floorCode:"
				+ a, function(b) {
			if (!this.cmp.isActive) {
				return
			}
			this.updateFullData(b[a]);
			System.socketManager.registConsumer({
				type : DataEventType.STOCK,
				codes : this.crrStockCodes
			})
		}.bind(this))
	},
	add : function(a) {
		this.stocks[a.code] = a;
		this.emit("addStock", a)
	},
	push : function(a) {
		if (!this.cmp.isActive) {
			return
		}
		this.crrStockCodes.push(a);
		System.socketManager.registConsumer({
			type : DataEventType.STOCK,
			codes : [ a ]
		});
		System.ajaxHelper.request("priceservice/secinfo/snapshot/q=codes:" + a,
				function(b) {
					if (!this.cmp.isActive) {
						return
					}
					this.add(b[0])
				}.bind(this))
	},
	remove : function(a) {
		var b = this.crrStockCodes.indexOf(a);
		this.crrStockCodes.splice(b, 1);
		delete this.stocks[a];
		System.socketManager.stopConsume({
			type : DataEventType.STOCK,
			codes : [ a ]
		});
		this.emit("removeStock", a)
	},
	stopConsume : function() {
		System.socketManager.stopConsume({
			type : DataEventType.STOCK,
			codes : this.crrStockCodes
		})
	},
	predictATO : function(a) {
		for ( var b in this.stocks) {
			if (this.stocks[b].floorCode == a) {
				this.predictATOForOneStock(this.stocks[b])
			}
		}
	},
	predictATC : function(a) {
		for ( var b in this.stocks) {
			if (this.stocks[b].floorCode == a) {
				this.predictATCForOneStock(this.stocks[b])
			}
		}
	},
	clearATCPrice : function(a) {
		for ( var b in this.stocks) {
			if (this.stocks[b].floorCode == a) {
				this.clearATCPriceForOneStock(this.stocks[b])
			}
		}
	},
	clearATOPrice : function(a) {
		for ( var b in this.stocks) {
			if (this.stocks[b].floorCode == a) {
				this.clearATOPriceForOneStock(this.stocks[b])
			}
		}
	},
	clearATCPriceForOneStock : function(a) {
		if (a.matchQtty == 0) {
			a.matchPrice = 0;
			this.emit("onClearATCPrice", a)
		}
	},
	clearATOPriceForOneStock : function(a) {
		if (a.matchQtty == 0) {
			a.matchPrice = 0;
			this.emit("onClearATOPrice", a)
		}
	},
	predictATOForOneStock : function(a) {
		this.emit("onPredictATO", a)
	},
	predictATCForOneStock : function(a) {
		this.emit("onPredictATC", a)
	},
	standardizeData : function(a) {
		if (!a.matchPrice || a.matchPrice == "0" || a.matchPrice == "null") {
			a.matchQtty = 0
		}
	},
	emit : function(b, a) {
		this.jqueryObject.trigger({
			type : b,
			message : a,
			time : new Date()
		});
		this.cmp.emit(b, a)
	},
	on : function(b, a) {
		$(this).bind(b, a)
	}
};
function StockInfoView() {
	this.createSortIndexMap();
	this.crrRenderProcess = null;
	this.crrRenderFloorCode = 0
}
StockInfoView.prototype = {
	setWrp : function(a) {
		this.wrp = a
	},
	formatVol : function(a) {
		return NumberFormat.vol(a)
	},
	formatPrice : function(c, a, b) {
		return NumberFormat.price(c, a, b)
	},
	render : function(c, d) {
		var b = $(this.wrp);
		this.crrWrpParentId = $(b.parent()).attr("id");
		this.clearOldRenderProcess();
		b.empty();
		var a = this.getRenderArr(c.length);
		if (a.length == 0) {
			return
		}
		this.recusiveRender(0, a, c, b, d)
	},
	isSatisfyRenderCondition : function(a) {
		if (!this.cmp.isActive) {
			return false
		}
		return true
	},
	isTrueTab : function(a) {
	},
	applySortTable : function(a, b) {
		try {
			$(a.parent())
					.unbind(
							"appendCache applyWidgetId applyWidgets sorton update updateCell")
					.removeClass("tablesorter").find("thead th").unbind(
							"click mousedown").removeClass(
							"header headerSortDown headerSortUp");
			$(a.parent()).tablesorter({
				textExtraction : this.sortTextExtraction
			});
			$(a.parent()).bind("sortStart", function(f, d) {
				this.emit("headerClick")
			}.bind(this))
		} catch (c) {
			console.log(c)
		}
	},
	getRenderArr : function(e) {
		var d = 50;
		if (Browser.isTablet()) {
			d = 50
		}
		var b = parseInt(e / d);
		var a = [];
		for (var c = 0; c < b; c++) {
			a.push({
				start : d * c,
				end : d * (c + 1)
			})
		}
		if (d * b < e) {
			a.push({
				start : d * b,
				end : e
			})
		}
		return a
	},
	recusiveRender : function(c, a, d, b, e) {
		if (!this.isSatisfyRenderCondition(d[0])) {
			return
		}
		this.renderPart(d, b, e, a[c].start, a[c].end);
		if (c < a.length - 1) {
			if (!this.isSatisfyRenderCondition(d[0])) {
				return
			}
			var f = 50;
			if (Browser.isTablet()) {
				f = 10
			}
			this.crrRenderProcess = setTimeout(function() {
				this.recusiveRender(c + 1, a, d, b, e)
			}.bind(this), f)
		} else {
			this.handleRenderComplete(b, d, e)
		}
	},
	clearOldRenderProcess : function() {
		clearTimeout(this.crrRenderProcess)
	},
	handleRenderComplete : function(a, b, c) {
		this.emit("renderComplete");
		if (!Browser.isTablet()) {
			this.applySortTable(a, c)
		}
		this.handleClick(b, c)
	},
	renderPart : function(d, b, e, g, a) {
		var f = "";
		for (var c = g; c < a; c++) {
			f += this.getHtmlTextFromStockInfo(d[c], e)
		}
		if (!this.isSatisfyRenderCondition(d[0])) {
			return
		}
		$(f).appendTo(b)
	},
	handleClick : function(b, c) {
		for (var a = 0; a < b.length; a++) {
			$("#" + c + b[a].code + "sym").click(function(d, e) {
				this.emit("rowClick", {
					stockCode : d,
					element : $("#" + e + d + "row")
				})
			}.bind(this, b[a].code, c));
			$("#" + c + b[a].code + "row").click(function(d, e) {
				RowSelector.select($("#" + e + d + "row"))
			}.bind(this, b[a].code, c))
		}
	},
	remove : function(b, c) {
		$("#" + c + b + "row").remove();
		if (!Browser.isTablet()) {
			var a = $(this.wrp);
			this.applySortTable(a, c)
		}
	},
	push : function(b, c) {
		var a = $(this.wrp);
		a.append(this.getHtmlTextFromStockInfo(b, c));
		if (!Browser.isTablet()) {
			this.applySortTable(a, c)
		}
		this.handleClick([ b ], c)
	},
	getPreviousSortList : function(b) {
		var a = [];
		var d = b.find(">thead th:not([colspan])");
		for (var c = 0; c < d.length; c++) {
			if ($(d[c]).hasClass("headerSortUp")) {
				a.push([ this.getMapIndex(c), 1 ]);
				break
			}
			if ($(d[c]).hasClass("headerSortDown")) {
				a.push([ this.getMapIndex(c), 0 ]);
				break
			}
		}
		return a
	},
	getMapIndex : function(a) {
		return this.sortIndexMap[a]
	},
	createSortIndexMap : function() {
		this.sortIndexMap = {
			0 : 0,
			1 : 1,
			2 : 2,
			3 : 3,
			4 : 4,
			5 : 20,
			6 : 21,
			7 : 22,
			8 : 23,
			9 : 24,
			10 : 5,
			11 : 6,
			12 : 7,
			13 : 8,
			14 : 9,
			15 : 10,
			16 : 11,
			17 : 12,
			18 : 13,
			19 : 14,
			20 : 15,
			21 : 16,
			22 : 17,
			23 : 18,
			24 : 19,
			25 : 25,
			26 : 26,
			27 : 27
		}
	},
	setCrrRenderFloorCode : function(a) {
		this.crrRenderFloorCode = a
	},
	getHtmlTextFromStockInfo : function(b, f) {
		var k = this.getDisplayMatchPrice(b);
		var g = this.getDisplayMatchQtty(b);
		var j = this.getClass(k, b);
		var a = 0;
		var i = 0;
		f += b.code;
		var e = this.getChange(k, g, b);
		if (b.floorCode == CONST_HNX_NAME
				&& typeof b.accumulatedVol != "undefined") {
			if (b.totalBidQtty) {
				a = b.totalBidQtty - b.accumulatedVol;
				if (a < 0) {
					a = 0
				}
			}
			if (b.totalOfferQtty) {
				i = b.totalOfferQtty - b.accumulatedVol;
				if (i < 0) {
					i = 0
				}
			}
		}
		var d = '<tr id="' + f + 'row" >';
		var h = this.formatPrice(b.bidPrice01, b.bidQtty01, b.floorCode);
		var c = this.formatPrice(b.offerPrice01, b.offerQtty01, b.floorCode);
		d += this.getStockCodeHtml(b, j, f);
		d += '<td width="3%" id="' + f + 'ref" class=" txt-gia-tc" >'
				+ this.formatPrice(b.basicPrice) + '</td><td width="3%" id="'
				+ f + 'cel" class=" txt-gia-tran">'
				+ this.formatPrice(b.ceilingPrice) + '</td><td width="3%" id="'
				+ f + 'flo" class=" txt-gia-san">'
				+ this.formatPrice(b.floorPrice) + '</td><td width="6%" id="'
				+ f + 'vol" class="" >' + FormatVolume10(b.accumulatedVol, 0)
				+ '</td><td width="3%" id="' + f + 'bP3" class="'
				+ this.getClass(b.bidPrice03, b) + '" >'
				+ this.formatPrice(b.bidPrice03) + '</td><td width="4%" id="'
				+ f + 'bV3" class="' + this.getClass(b.bidPrice03, b) + '" >'
				+ this.formatVol(b.bidQtty03) + '</td><td width="3%" id="' + f
				+ 'bP2" class="' + this.getClass(b.bidPrice02, b) + '" >'
				+ this.formatPrice(b.bidPrice02) + '</td><td width="4%" id="'
				+ f + 'bV2" class="' + this.getClass(b.bidPrice02, b) + '" >'
				+ this.formatVol(b.bidQtty02) + '</td><td width="3%" id="' + f
				+ 'bP1" class="' + this.getClass(b.bidPrice01, b, h) + '" >'
				+ h + '</td><td width="4%" id="' + f + 'bV1" class="'
				+ this.getClass(b.bidPrice01, b) + '" >'
				+ this.formatVol(b.bidQtty01) + '</td><td width="3%" id="' + f
				+ 'pri" class=" ' + this.getClass(k, b) + ' show-on-mobile">'
				+ this.formatPrice(k) + '</td><td width="4%" id="' + f
				+ 'lat" class=" ' + this.getClass(k, b) + ' show-on-mobile">'
				+ this.formatVol(g) + '</td><td width="2.5%" id="' + f
				+ 'per" class=" ' + this.getClass(k, b) + ' show-on-mobile">'
				+ this.formatPrice(e) + '</td><td width="3%" id="' + f
				+ 'oP1" class="' + this.getClass(b.offerPrice01, b, c) + '">'
				+ c + '</td><td width="4%" id="' + f + 'oV1" class="'
				+ this.getClass(b.offerPrice01, b) + '" >'
				+ this.formatVol(b.offerQtty01) + '</td><td width="3%" id="'
				+ f + 'oP2" class="' + this.getClass(b.offerPrice02, b) + '" >'
				+ this.formatPrice(b.offerPrice02) + '</td><td width="4%" id="'
				+ f + 'oV2" class="' + this.getClass(b.offerPrice02, b) + '" >'
				+ this.formatVol(b.offerQtty02) + '</td><td width="3%" id="'
				+ f + 'oP3" class="' + this.getClass(b.offerPrice03, b) + '" >'
				+ this.formatPrice(b.offerPrice03) + '</td><td width="4%" id="'
				+ f + 'oV3" class="' + this.getClass(b.offerPrice03, b) + '">'
				+ this.formatVol(b.offerQtty03) + '</td><td width="3%" id="'
				+ f + 'higP" class=" ' + this.getClass(b.highestPrice, b)
				+ '">' + this.formatPrice(b.highestPrice)
				+ '</td><td width="3%" id="' + f + 'lowP" class=" '
				+ this.getClass(b.lowestPrice, b) + '">'
				+ this.formatPrice(b.lowestPrice) + '</td><td width="5%" id="'
				+ f + 'bV4" class="">' + this.formatVol(a)
				+ '</td><td width="5%" id="' + f + 'sV4" class="" >'
				+ this.formatVol(i) + '</td><td width="4%" id="' + f
				+ 'forB" class="" >' + this.formatVol(b.buyForeignQtty)
				+ '</td><td width="4%" id="' + f + 'forS" class="" >'
				+ this.formatVol(b.sellForeignQtty)
				+ '</td><td width="5%" id="' + f + 'forR" class="" >'
				+ this.formatVol(b.currentRoom, 0) + "</td></tr>";
		return d
	},
	getStockCodeHtml : function(d, a, b) {
		var c = "";
		c += '<td  width="6%" class="show-on-mobile firstcol" >';
		c += '<a class="' + a + '" id="' + b + 'sym" >' + d.code + "</a>";
		c += "</td>";
		return c
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	},
	clear : function() {
	},
	scrollTo : function(a, b) {
		var c = System.marketInfosManager.isShow ? 315 : 132;
		var d = $("#" + b + a + "row").offset();
		if (d) {
			window.scrollTo(0, d.top - c)
		}
		RowSelector.select($("#" + b + a + "row"))
	},
	refreshStockInfo : function(b, d) {
		var h = this.getDisplayMatchPrice(b);
		var e = this.getDisplayMatchQtty(b);
		var i = "#" + d + b.code;
		var a = 0;
		var f = 0;
		var c = this.getChange(h, e, b);
		if (b.floorCode == CONST_HNX_NAME
				&& typeof b.accumulatedVol != "undefined") {
			if (b.totalBidQtty) {
				a = b.totalBidQtty - b.accumulatedVol;
				if (a < 0) {
					a = 0
				}
			}
			if (b.totalOfferQtty) {
				f = b.totalOfferQtty - b.accumulatedVol;
				if (f < 0) {
					f = 0
				}
			}
		}
		var g = this.getClass(h, b);
		this.setCellColor($(i + "sym"), g);
		setValue(i + "ref", this.formatPrice(b.basicPrice));
		setValue(i + "cel", this.formatPrice(b.ceilingPrice));
		setValue(i + "flo", this.formatPrice(b.floorPrice));
		this.setCell(i + "vol", this.formatVol(b.accumulatedVol), null);
		this.setCell(i + "pri", this.formatPrice(h), [ h, b, "" ]);
		this.setCell(i + "per", this.formatPrice(c), [ h, b, "" ]);
		this.setCell(i + "lat", this.formatVol(e), [ h, b, "" ]);
		this.setCell(i + "higP", this.formatPrice(b.highestPrice), [
				b.highestPrice, b, "" ]);
		this.setCell(i + "lowP", this.formatPrice(b.lowestPrice), [
				b.lowestPrice, b, "" ]);
		this.setCell(i + "bP1", this.formatPrice(b.bidPrice01, b.bidQtty01,
				b.floorCode), [ b.bidPrice01, b, "" ]);
		this.setCell(i + "bV1", this.formatVol(b.bidQtty01), [ b.bidPrice01, b,
				"" ]);
		this.setCell(i + "bP2", this.formatPrice(b.bidPrice02), [ b.bidPrice02,
				b, "" ]);
		this.setCell(i + "bV2", this.formatVol(b.bidQtty02), [ b.bidPrice02, b,
				"" ]);
		this.setCell(i + "bP3", this.formatPrice(b.bidPrice03), [ b.bidPrice03,
				b, "" ]);
		this.setCell(i + "bV3", this.formatVol(b.bidQtty03), [ b.bidPrice03, b,
				"" ]);
		this.setCell(i + "oP1", this.formatPrice(b.offerPrice01, b.offerQtty01,
				b.floorCode), [ b.offerPrice01, b, "" ]);
		this.setCell(i + "oV1", this.formatVol(b.offerQtty01), [
				b.offerPrice01, b, "" ]);
		this.setCell(i + "oP2", this.formatPrice(b.offerPrice02), [
				b.offerPrice02, b, "" ]);
		this.setCell(i + "oV2", this.formatVol(b.offerQtty02), [
				b.offerPrice02, b, "" ]);
		this.setCell(i + "oP3", this.formatPrice(b.offerPrice03), [
				b.offerPrice03, b, "" ]);
		this.setCell(i + "oV3", this.formatVol(b.offerQtty03), [
				b.offerPrice03, b, "" ]);
		this.setCell(i + "sV4", this.formatVol(f), null);
		this.setCell(i + "bV4", this.formatVol(a), null);
		this.setCell(i + "forB", this.formatVol(b.buyForeignQtty), null);
		this.setCell(i + "forS", this.formatVol(b.sellForeignQtty), null);
		this.setCell(i + "forR", this.formatVol(b.currentRoom), null);
		this.emit("finishProcessStock")
	},
	setCell : function(g, f, e) {
		var a = $(g);
		if (!a) {
			return
		}
		if (typeof f == "undefined") {
			return
		}
		var d;
		var c = a.text();
		if ((f == "0") || (f == "0.0") || (f == "0.00")) {
			d = ""
		} else {
			d = f
		}
		if (d != c) {
			a.text(d);
			if (d != "" && d.length > 0) {
				var b;
				if (f == "ATC" || f == "ATO" || e == null) {
					b = ""
				} else {
					b = this.getClass(e[0], e[1], e[2])
				}
				a.attr("class", "hl2 " + b);
				a.orgClass = b;
				HighlightRoom.push(a)
			}
		}
	},
	getClass : function(h, j, b) {
		var i = Math.round(j.ceilingPrice * 10) / 10;
		var g = Math.round(j.floorPrice * 10) / 10;
		var d = Math.round(j.basicPrice * 10) / 10;
		var e = Math.round(h * 10) / 10;
		var a;
		if (!e || e == "0.0" || isNaN(e)) {
			if (b == "ATC" || b == "ATO") {
				a = ""
			} else {
				a = "txt-gia-tc"
			}
		} else {
			if (e == i) {
				a = "txt-magenta"
			} else {
				if (e > d) {
					a = "txt-lime"
				} else {
					if (e == d) {
						a = "txt-gia-tc"
					} else {
						if (e == g) {
							a = "txt-gia-san"
						} else {
							a = "txt-red"
						}
					}
				}
			}
		}
		return a
	},
	removeColorClass : function(b) {
		var a = "txt-gia-tc txt-magenta txt-lime txt-gia-san txt-red txt-gia-tran";
		b.removeClass(a)
	},
	setCellColor : function(b, a) {
		this.removeColorClass(b);
		b.addClass(a)
	},
	getColor : function() {
	},
	highlightCells : function(a) {
		HighlightRoom.push(a)
	},
	highlightCellsInRow : function(a) {
		for (var b = 0; b < a.length; b++) {
			if (a[b] != null) {
				a[b].addClass("hl2");
				HighlightRoom.push(a[b])
			}
		}
	},
	sortTextExtraction : function(a) {
		var c = $(a).text();
		while (c.indexOf(",") >= 0) {
			c = c.replace(",", "")
		}
		return c;
		var b = parseFloat(c)
	},
	predictATO_ATC : function(b, a) {
		this.displayMathPriceAndValue(b, a)
	},
	clearATC_ATOPrice : function(b, a) {
		this.displayMathPriceAndValue(b, a)
	},
	displayMathPriceAndValue : function(g, d) {
		var c = "#" + d;
		var e = this.getDisplayMatchPrice(g);
		var a = this.getDisplayMatchQtty(g);
		this.setCell(c + g.code + "pri", this.formatPrice(e), [ e, g, "" ]);
		this.setCell(c + g.code + "lat", this.formatVol(a), [ e, g, "" ]);
		var f = this.getChange(e, a, g);
		this.setCell("#" + d + g.code + "per", f, null);
		var b = this.getClass(e, g);
		this.setCellColor($(c + "sym"), b)
	},
	getDisplayMatchPrice : function(a) {
		if (a.floorCode == CONST_HNX_NAME || a.floorCode == CONST_UPC_NAME) {
			if (a.currentPrice > 0) {
				if (System.marketInfosManager.isATC(a.floorCode)) {
					return a.currentPrice
				}
				if (a.currentQtty > 0) {
					return a.currentPrice
				}
				return 0
			}
			return a.matchPrice
		}
		if (System.marketInfosManager.isATO(a.floorCode)
				|| System.marketInfosManager.isATC(a.floorCode)) {
			return a.projectOpen
		}
		return a.matchPrice
	},
	getDisplayMatchQtty : function(a) {
		if (a.floorCode == CONST_HNX_NAME || a.floorCode == CONST_UPC_NAME) {
			if (a.currentPrice > 0) {
				return a.currentQtty
			}
			return a.matchQtty
		}
		if (System.marketInfosManager.isATC(a.floorCode)
				|| System.marketInfosManager.isATC(a.floorCode)) {
			return 0
		}
		return a.matchQtty
	},
	getChange : function(b, a, d) {
		var c = 0;
		if (b > 0) {
			c = Math.abs(b - d.basicPrice)
		}
		c = Math.round(c * 10) / 10;
		return c
	}
};
function TabManager(a) {
	this.crrTab = 1;
	this.activeClass = "active";
	this.hiddenClass = "hidden";
	this.specialHide = false;
	this.idPre = {
		tab : "tab",
		box : "box",
		number : 6
	};
	for ( var b in a) {
		if (typeof a[b] != "undefined") {
			this[b] = a[b]
		}
	}
	this.jqueryObject = $(this)
}
TabManager.prototype = {
	setTab : function(a) {
		if (this.crrTab == a) {
			return
		}
		var b = this.crrTab;
		this.crrTab = a;
		this.showTab(a);
		this.hideTab(b);
		this.emit("beforeChangeTab", {
			oldTab : b,
			newTab : a
		});
		this.emit("changeTab", {
			oldTab : b,
			newTab : a
		});
		this.setCookie()
	},
	listenEvent : function() {
	},
	emit : function(b, a) {
		this.jqueryObject.trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	},
	showTab : function(a) {
		$("#" + this.idPre.tab + a).addClass(this.activeClass);
		$("#" + this.idPre.box + a).css({
			display : "block"
		});
		this.setCookie();
		return
	},
	hideTab : function(a) {
		$("#" + this.idPre.tab + a).removeClass(this.activeClass);
		$("#" + this.idPre.box + a).css({
			display : "none"
		});
		return
	},
	setCookie : function() {
		setCookie(this.name + "CK", this.crrTab)
	},
	start : function() {
		this.getCookie()
	},
	getCookie : function() {
		var a = parseInt(getCookie(this.name + "CK"));
		if (a) {
			this.setTab(a)
		}
	}
};
FixHeader = function(a) {
	this.init();
	for ( var b in a) {
		if (typeof a[b] != "undefined") {
			this[b] = a[b]
		}
	}
	this.createElement();
	this.smartDisplay();
	this.listenWindowEvent();
	this.autoSync();
	FixHeader.instants.push(this)
};
FixHeader.instants = [];
FixHeader.all = function(b) {
	for (var a = 0; a < this.instants; a++) {
		b(this.instants[a])
	}
};
FixHeader.prototype = {
	smartDisplay : function() {
		var a = System.marketInfosManager.isShow ? this.cssAtr.top1
				: this.cssAtr.top2;
		if (this.isTrueTab()) {
			if (this.type == "gdtt") {
				this.element.css({
					top : a,
				})
			} else {
				this.element.css({
					top : a,
					left : this.cssAtr.left - $(window).scrollLeft(),
				})
			}
			return
		}
	},
	isTrueTab : function() {
		if (System.tabManager.crrTab == this.tabIndex) {
			return true
		}
		return false
	},
	isTruePosition : function() {
		var a = System.marketInfosManager.isShow ? this.cssAtr.windowScrollTop1
				: this.cssAtr.windowScrollTop2;
		if ($(window).scrollTop() > a) {
			return true
		}
		return false
	},
	autoSync : function() {
		var a = [ 100, 500, 3000 ];
		for (var b = 0; b < a.length; b++) {
			setTimeout(function() {
				this.syncWidth()
			}.bind(this), a[b])
		}
		this.syncInterval()
	},
	syncInterval : function() {
		var a = 12000;
		if (Browser.isTablet()) {
			a = 20000
		}
		setInterval(function() {
			this.syncWidth()
		}.bind(this), a)
	},
	syncWidthWithDelay : function() {
		var a = 0;
		if (this.startedSync) {
			clearTimeout(this.syncProcess);
			a = 50
		}
		this.startedSync = false;
		this.syncProcess = setTimeout(function() {
			this.syncWidth();
			this.startedSync = false
		}.bind(this), a)
	},
	handleScroll : function() {
		if (!this.isActive) {
			return
		}
		return;
		var a = 200;
		clearTimeout(this.handleScrollProcess);
		this.handleScrollProcess = setTimeout(function() {
			this.smartDisplay();
			this.syncWidth()
		}.bind(this), a)
	},
	handleResize : function() {
		if (!this.isActive) {
			return
		}
		clearTimeout(this.handleResizeProcess);
		var a = 100;
		this.handleResizeProcess = setTimeout(function() {
			this.syncWidth()
		}.bind(this), a)
	},
	createElement : function() {
		var c;
		var a;
		var b;
		if (this.type == "gdtt") {
			c = this.wrpElementId;
			a = "inherit";
			b = "banggia"
		} else {
			c = this.originalElement.parent();
			a = "fixed";
			b = "banggia stock-fix-header"
		}
		if (!this.originalElement.isCloned) {
			this.element = $("<table>", {
				id : this.id,
				"class" : b
			});
			if (this.type == "gdtt") {
			}
			this.element.css({
				position : a,
				display : "table",
				top : this.cssAtr.top1,
				"z-index" : 36
			}).appendTo(c);
			this.originalElementId = this.originalElement.attr("id");
			$("#" + this.originalElementId + " colgroup").clone().appendTo(
					this.element);
			$("#" + this.originalElementId + " thead").clone().appendTo(
					this.element);
			this.detectHeaderElement();
			$("#" + this.originalElementId + " thead").css({});
			this.mapElementEvent();
			this.originalElement.isCloned = true;
			$("#" + this.originalElementId + " thead").css({
				opacity : 0,
			})
		}
	},
	mapElementEvent : function() {
		var b = $("#" + this.originalElementId + " thead th");
		var a = $("#" + this.id + " thead th");
		for (var c = 0; c < a.length; c++) {
			if (!$(a[c]).attr("colspan")) {
				$(a[c]).unbind("click");
				$(a[c]).click(function(e, d) {
					e.trigger("click");
					this.clearSortClassOnThTags();
					setTimeout(function(f) {
						d.attr("class", f.attr("class"))
					}.bind(this, e), 100)
				}.bind(this, $(b[c]), $(a[c])))
			}
		}
	},
	clearSortClassOnThTags : function() {
		var a = $("#" + this.id + " thead th");
		for (var b = 0; b < a.length; b++) {
			$(a[b]).removeClass("headerSortDown headerSortUp")
		}
	},
	active : function() {
		this.isActive = true
	},
	deactive : function() {
		this.isActive = true
	},
	listenWindowEvent : function() {
		if (!Browser.isTablet()) {
			$(window).resize(function() {
				this.handleResize()
			}.bind(this))
		}
		System.tabManager.on("changeTab", function(a) {
			this.removeClass();
			this.smartDisplay();
			if (this.isNotAutoHandleTabChange) {
				return
			}
			var b = 100;
			if (Browser.isChrome() || Browser.isSafari()) {
				b = 400
			}
			if (this.isTrueTab()) {
				setTimeout(function() {
					this.syncWidth()
				}.bind(this), b)
			}
		}.bind(this));
		System.marketInfosManager.on("onHide", function(a) {
			this.smartDisplay()
		}.bind(this));
		System.marketInfosManager.on("onShow", function(a) {
			this.smartDisplay()
		}.bind(this))
	},
	init : function() {
		this.startedSync = false;
		this.isActive = false;
		this.startedHandlerScroll = false;
		this.startedHandlerScroll = false;
		this.id = "fixHeader" + (FixHeader.instants.length);
		this.index = FixHeader.instants.length
	},
	replaceThTagByJqueryObject : function(a) {
		for (var b = 0; b < a.length; b++) {
			a[b] = $(a[b])
		}
	},
	removeClass : function() {
		for (var a = 0; a < this.thTags.length; a++) {
			this.thTags[a].attr("class", "")
		}
	},
	syncWidth : function() {
		if (!this.isTrueTab()) {
			return
		}
		for (var a = 0; a < this.thTags.length; a++) {
			this.syncElementWidth(this.thTags[a], this.originalThTags[a])
		}
	},
	detectHeaderElement : function() {
		if (Browser.isChrome() || Browser.isSafari()) {
			this.originalThTags = $("#" + this.originalElementId + " thead th");
			this.thTags = $("#" + this.id + " thead th")
		} else {
			if (Browser.isIE()) {
				this.originalThTags = $("#" + this.originalElementId
						+ " thead th:not([colspan])");
				this.thTags = $("#" + this.id + " thead th:not([colspan])")
			} else {
				this.originalThTags = $("#" + this.originalElementId
						+ " th:not([colspan])");
				this.thTags = $("#" + this.id + " th:not([colspan])")
			}
		}
		this.replaceThTagByJqueryObject(this.thTags);
		this.replaceThTagByJqueryObject(this.originalThTags)
	},
	syncElementWidth : function(d, c) {
		if (Browser.isChrome() || Browser.isSafari()) {
			var a = Math.round(c.width() + 1);
			d.css({
				"min-width" : a
			})
		} else {
			if (Browser.isIE()) {
				var a = c.width();
				d.width(a);
				if ($(window).width < 1000) {
					d.css({
						"min-width" : a + "px"
					})
				}
			} else {
				var b = c.width();
				var a = c.innerWidth();
				d.innerWidth(a);
				d.css({
					"min-width" : b
				})
			}
		}
	}
};
var GSymbolList = [];
var GListStockInfo = new Array();
var GListCode = new Array();
GListCode[0] = "AAA";
GListStockInfo.AAA = "CTCP Nhua va Moi truong xanh An Phat";
GListCode[1] = "AAM";
GListStockInfo.AAM = "CTCP THUY SAN MEKONG";
GListCode[2] = "ABI";
GListStockInfo.ABI = "CTCP Bao hiem Ngan hang Nong Nghiep";
GListCode[3] = "ABT";
GListStockInfo.ABT = "CTCP XNK THUY SAN BEN TRE";
GListCode[4] = "ACB";
GListStockInfo.ACB = "NHTM co phan A Chau";
GListCode[5] = "ACC";
GListStockInfo.ACC = "CTCP BETONG BECAMEX";
GListCode[6] = "ACE";
GListStockInfo.ACE = "CTCP Be tong Ly tam An Giang";
GListCode[7] = "ACL";
GListStockInfo.ACL = "CTCP XNK T.SAN CL-AG";
GListCode[8] = "ADC";
GListStockInfo.ADC = "CTCP Mi thuat va Truyen thong";
GListCode[9] = "ADP";
GListStockInfo.ADP = "CTCP Son A Dong";
GListCode[10] = "AGD";
GListStockInfo.AGD = "CTCP GO DANG";
GListCode[11] = "AGF";
GListStockInfo.AGF = "CTCP XNK THUY SAN ANGIANG";
GListCode[12] = "AGM";
GListStockInfo.AGM = "CTCP XNK AN GIANG";
GListCode[13] = "AGR";
GListStockInfo.AGR = "CTCPCK NHNN & PTNT VN";
GListCode[14] = "ALP";
GListStockInfo.ALP = "CT CP ANPHANAM";
GListCode[15] = "ALT";
GListStockInfo.ALT = "CTCP Van hoa Tan Binh";
GListCode[16] = "ALV";
GListStockInfo.ALV = "CTCP Khoang san Vinas A Luoi";
GListCode[17] = "AMC";
GListStockInfo.AMC = "CTCP Khoang san A Chau";
GListCode[18] = "AME";
GListStockInfo.AME = "CTCP Alphanam Co dien";
GListCode[19] = "AMV";
GListStockInfo.AMV = "CTCP San xuat kinh doanh trang thiet bi y te Viet My";
GListCode[20] = "ANV";
GListStockInfo.ANV = "CT CP NAM VIET";
GListCode[21] = "APC";
GListStockInfo.APC = "CTCP CHIEU XA AN PHU";
GListCode[22] = "APG";
GListStockInfo.APG = "CTCP Chung khoan An Phat";
GListCode[23] = "API";
GListStockInfo.API = "CTCP Dau tu Chau A - Thai Binh Duong";
GListCode[24] = "APP";
GListStockInfo.APP = "CTCP Phat trien phu gia va san pham dau mo";
GListCode[25] = "APS";
GListStockInfo.APS = "CTCP Chung khoan Chau A - Thai Binh Duong";
GListCode[26] = "ARM";
GListStockInfo.ARM = "CTCP Xuat nhap khau hang khong";
GListCode[27] = "ASA";
GListStockInfo.ASA = "CTCP Lien doanh SANA WMT";
GListCode[28] = "ASIAGF";
GListStockInfo.ASIAGF = "QUY DT TANG TRUONG ACB";
GListCode[29] = "ASM";
GListStockInfo.ASM = "CT DTXD SAO MAI AN GIANG";
GListCode[30] = "ASP";
GListStockInfo.ASP = "CTCP DAU KHI AN PHA S.G";
GListCode[31] = "ATA";
GListStockInfo.ATA = "CTCP NTACO";
GListCode[32] = "AVF";
GListStockInfo.AVF = "CTCP VIET AN";
GListCode[33] = "B82";
GListStockInfo.B82 = "CTCP 482";
GListCode[34] = "BBC";
GListStockInfo.BBC = "CTY CP BANH KEO BIEN HOA";
GListCode[35] = "BBS";
GListStockInfo.BBS = "CTCP VICEM Bao bi But Son";
GListCode[36] = "BCC";
GListStockInfo.BCC = "CTCP Xi mang Bim Son";
GListCode[37] = "BCE";
GListStockInfo.BCE = "CTCP XD & GT BINH DUONG";
GListCode[38] = "BCI";
GListStockInfo.BCI = "CTCP DT&XD BINH CHANH";
GListCode[39] = "BDB";
GListStockInfo.BDB = "CTCP Sach va thiet bi Binh Dinh";
GListCode[40] = "BED";
GListStockInfo.BED = "CTCP Sach va Thiet bi truong hoc Da Nang";
GListCode[41] = "BGM";
GListStockInfo.BGM = "CTCP KT&CB KS BAC GIANG";
GListCode[42] = "BHC";
GListStockInfo.BHC = "CTCP Be tong Bien Hoa";
GListCode[43] = "BHP";
GListStockInfo.BHP = "CTCP Bia Ha Noi - Hai Phong";
GListCode[44] = "BHS";
GListStockInfo.BHS = "CTCP DUONG BIEN HOA";
GListCode[45] = "BHT";
GListStockInfo.BHT = "CTCP Dau tu Xay dung Bach Dang TMC";
GListCode[46] = "BHV";
GListStockInfo.BHV = "CTCP Viglacera Ba Hien";
GListCode[47] = "BIC";
GListStockInfo.BIC = "TCTCP BAO HIEM NHDTPTVN";
GListCode[48] = "BKC";
GListStockInfo.BKC = "CTCP Khoang San Bac Kan";
GListCode[49] = "BLF";
GListStockInfo.BLF = "CTCP Thuy san Bac Lieu";
GListCode[50] = "BMC";
GListStockInfo.BMC = "CTCP KHOANG SAN BINH DINH";
GListCode[51] = "BMI";
GListStockInfo.BMI = "TCT CP BAO MINH";
GListCode[52] = "BMJ";
GListStockInfo.BMJ = "CTCP Khoang san Becamex";
GListCode[53] = "BMP";
GListStockInfo.BMP = "CTCP NHUA BINH MINH";
GListCode[54] = "BPC";
GListStockInfo.BPC = "CTCP VICEM Bao bi Bim Son";
GListCode[55] = "BRC";
GListStockInfo.BRC = "CTCP CAO SU BEN THANH";
GListCode[56] = "BSC";
GListStockInfo.BSC = "CTCP Dich vu Ben Thanh";
GListCode[57] = "BSI";
GListStockInfo.BSI = "CTCP CK NHDT & PT VN";
GListCode[58] = "BST";
GListStockInfo.BST = "CTCP Sach va Thiet bi Binh Thuan";
GListCode[59] = "BT6";
GListStockInfo.BT6 = "CTCP BETONG 620-CHAU THOI";
GListCode[60] = "BTC";
GListStockInfo.BTC = "CTCP Co Khi va Xay Dung Binh Trieu";
GListCode[61] = "BTG";
GListStockInfo.BTG = "CTCP Bao bi Tien Giang";
GListCode[62] = "BTH";
GListStockInfo.BTH = "CTCP Che tao bien the va Vat lieu dien Ha Noi";
GListCode[63] = "BTP";
GListStockInfo.BTP = "CTCP NHIET DIEN BA RIA";
GListCode[64] = "BTS";
GListStockInfo.BTS = "CTCP Xi mang VICEM But Son";
GListCode[65] = "BTT";
GListStockInfo.BTT = "CTCP TMDV BEN THANH";
GListCode[66] = "BTW";
GListStockInfo.BTW = "CTCP Cap nuoc Ben Thanh";
GListCode[67] = "BVG";
GListStockInfo.BVG = "CTCP Thep Bac Viet";
GListCode[68] = "BVH";
GListStockInfo.BVH = "TAP DOAN BAO VIET";
GListCode[69] = "BVN";
GListStockInfo.BVN = "CTCP Bong Viet Nam";
GListCode[70] = "BVS";
GListStockInfo.BVS = "CTCP Chung khoan Bao Viet";
GListCode[71] = "BWA";
GListStockInfo.BWA = "CTCP Cap thoat nuoc va Xay dung Bao Loc";
GListCode[72] = "BXD";
GListStockInfo.BXD = "CTCP Van tai va Quan ly ben xe Da Nang";
GListCode[73] = "BXH";
GListStockInfo.BXH = "CTCP VICEM Bao bi Hai Phong";
GListCode[74] = "C21";
GListStockInfo.C21 = "CTCP THE KY 21";
GListCode[75] = "C32";
GListStockInfo.C32 = "CTCP DAU TU XAY DUNG 3-2";
GListCode[76] = "C47";
GListStockInfo.C47 = "CTCP XAY DUNG 47";
GListCode[77] = "C92";
GListStockInfo.C92 = "CTCP Xay dung va Dau tu 492";
GListCode[78] = "CAD";
GListStockInfo.CAD = "CTCP Che bien & XNK Thuy san Cadovimex";
GListCode[79] = "CAN";
GListStockInfo.CAN = "CTCP Do hop Ha Long";
GListCode[80] = "CAP";
GListStockInfo.CAP = "CTCP Lam nong san thuc pham Yen Bai";
GListCode[81] = "CCI";
GListStockInfo.CCI = "CT DTPTCN TM CU CHI";
GListCode[82] = "CCL";
GListStockInfo.CCL = "CTCP DT&PT DK CUU LONG";
GListCode[83] = "CCM";
GListStockInfo.CCM = "CTCP Khoang san va Xi mang Can Tho";
GListCode[84] = "CDC";
GListStockInfo.CDC = "CT CP CHUONG DUONG";
GListCode[85] = "CFC";
GListStockInfo.CFC = "CTCP Cafico Viet Nam";
GListCode[86] = "CHP";
GListStockInfo.CHP = "CTCP Thuy dien mien Trung";
GListCode[87] = "CI5";
GListStockInfo.CI5 = "CTCP Dau tu Xay dung so 5";
GListCode[88] = "CIC";
GListStockInfo.CIC = "CTCP Dau tu va Xay dung Cotec";
GListCode[89] = "CID";
GListStockInfo.CID = "CTCP Xay dung va Phat trien Co so ha tang";
GListCode[90] = "CIG";
GListStockInfo.CIG = "CTCP COMA18";
GListCode[91] = "CII";
GListStockInfo.CII = "CTCP DT HT KY THUAT TPHCM";
GListCode[92] = "CJC";
GListStockInfo.CJC = "CTCP Co dien Mien Trung";
GListCode[93] = "CKV";
GListStockInfo.CKV = "CTCP COKYVINA";
GListCode[94] = "CLC";
GListStockInfo.CLC = "CTCP CAT LOI";
GListCode[95] = "CLG";
GListStockInfo.CLG = "CTCP DT&PT NHA DAT COTEC";
GListCode[96] = "CLP";
GListStockInfo.CLP = "CTCP THUY SAN CUU LONG";
GListCode[97] = "CLW";
GListStockInfo.CLW = "CTCP CAP NUOC CHO LON";
GListCode[98] = "CMC";
GListStockInfo.CMC = "CTCP Dau tu CMC";
GListCode[99] = "CMG";
GListStockInfo.CMG = "CT TAP DOAN CONG NGHE CMC";
GListCode[100] = "CMI";
GListStockInfo.CMI = "CTCP CMISTONE Viet Nam";
GListCode[101] = "CMS";
GListStockInfo.CMS = "CTCP Xay dung va Nhan luc Viet Nam";
GListCode[102] = "CMT";
GListStockInfo.CMT = "CTCP CONG NGHE MANG & TT";
GListCode[103] = "CMV";
GListStockInfo.CMV = "CTCP THUONG NGHIEP CA MAU";
GListCode[104] = "CMX";
GListStockInfo.CMX = "CTCP CBTS & XNK CA MAU";
GListCode[105] = "CNG";
GListStockInfo.CNG = "CTCP CNG VIET NAM";
GListCode[106] = "CNT";
GListStockInfo.CNT = "CTY CP XD&KD VAT TU";
GListCode[107] = "COM";
GListStockInfo.COM = "CTCP VAT TU XANG DAU";
GListCode[108] = "CPC";
GListStockInfo.CPC = "CTCP Thuoc sat trung Can Tho";
GListCode[109] = "CSC";
GListStockInfo.CSC = "CTCP Dau tu va Xay dung Thanh Nam";
GListCode[110] = "CSM";
GListStockInfo.CSM = "CTCP CN CAO SU MIEN NAM";
GListCode[111] = "CT3";
GListStockInfo.CT3 = "CTCP Dau tu va Xay dung Cong trinh 3";
GListCode[112] = "CT6";
GListStockInfo.CT6 = "CTCP Cong trinh 6";
GListCode[113] = "CTA";
GListStockInfo.CTA = "CTCP Vinavico";
GListCode[114] = "CTB";
GListStockInfo.CTB = "CTCP Che tao bom Hai Duong";
GListCode[115] = "CTC";
GListStockInfo.CTC = "CTCP Gia Lai CTC";
GListCode[116] = "CTD";
GListStockInfo.CTD = "CTCP XAY DUNG COTEC";
GListCode[117] = "CTG";
GListStockInfo.CTG = "NH TMCP CONG THUONG VN";
GListCode[118] = "CTI";
GListStockInfo.CTI = "CTCPDTPT CUONGTHUAN IDICO";
GListCode[119] = "CTM";
GListStockInfo.CTM = "CTCP Dau tu xay dung va khai thac mo Vinavico";
GListCode[120] = "CTN";
GListStockInfo.CTN = "CTCP Xay dung cong trinh ngam";
GListCode[121] = "CTS";
GListStockInfo.CTS = "CTCP Chung khoan Ngan hang Cong thuong Viet Nam";
GListCode[122] = "CTV";
GListStockInfo.CTV = "CTCP Dau tu - San xuat va Thuong mai Viet Nam";
GListCode[123] = "CTX";
GListStockInfo.CTX = "Tong CTCP Dau tu va Thuong mai Viet Nam";
GListCode[124] = "CVN";
GListStockInfo.CVN = "CTCP Vinam";
GListCode[125] = "CVT";
GListStockInfo.CVT = "CTCP CMC";
GListCode[126] = "CX8";
GListStockInfo.CX8 = "CTCP Dau tu va Xay lap Constrexim 8";
GListCode[127] = "CYC";
GListStockInfo.CYC = "CTCP GACH MEN CHANG YIH";
GListCode[128] = "CZC";
GListStockInfo.CZC = "CTCP Than Mien Trung - TKV";
GListCode[129] = "D11";
GListStockInfo.D11 = "Cong ty co phan Dia oc 11";
GListCode[130] = "D26";
GListStockInfo.D26 = "CTCP Quan ly va Xay dung Duong bo 26";
GListCode[131] = "D2D";
GListStockInfo.D2D = "CTCP PT DO THI CN SO 2";
GListCode[132] = "DAC";
GListStockInfo.DAC = "CTCP Viglacera Dong Anh";
GListCode[133] = "DAD";
GListStockInfo.DAD = "CTCP Dau tu va Phat trien Giao duc Da Nang";
GListCode[134] = "DAE";
GListStockInfo.DAE = "CTCP Sach giao duc tai Tp. Da Nang";
GListCode[135] = "DAG";
GListStockInfo.DAG = "CTCP TAP DOAN NHUA DONG A";
GListCode[136] = "DAP";
GListStockInfo.DAP = "CTCP Dong A";
GListCode[137] = "DBC";
GListStockInfo.DBC = "CTCP Tap doan DABACO Viet Nam";
GListCode[138] = "DBF";
GListStockInfo.DBF = "CTCP Luong thuc Dong Bac";
GListCode[139] = "DBM";
GListStockInfo.DBM = "CTCP Duoc - Vat tu y te Dak Lak";
GListCode[140] = "DBT";
GListStockInfo.DBT = "CTCP Duoc pham Ben Tre";
GListCode[141] = "DC2";
GListStockInfo.DC2 = "CTCP Dau tu Phat trien Xay dung (DIC) so 2";
GListCode[142] = "DC4";
GListStockInfo.DC4 = "CTCP DIC so 4";
GListCode[143] = "DCL";
GListStockInfo.DCL = "CTY CP DUOC PHAM CUU LONG";
GListCode[144] = "DCS";
GListStockInfo.DCS = "CTCP Dai Chau";
GListCode[145] = "DCT";
GListStockInfo.DCT = "CTCP TAM LOP VLXD DONGNAI";
GListCode[146] = "DDN";
GListStockInfo.DDN = "CTCP Duoc - Thiet bi y te Da Nang";
GListCode[147] = "DGT";
GListStockInfo.DGT = "CTCP Cong trinh Giao thong Dong Nai";
GListCode[148] = "DHA";
GListStockInfo.DHA = "CTY CP HOA AN";
GListCode[149] = "DHC";
GListStockInfo.DHC = "CTCP DONG HAI BEN TRE";
GListCode[150] = "DHG";
GListStockInfo.DHG = "CTCP DUOC PHAM HAU GIANG";
GListCode[151] = "DHI";
GListStockInfo.DHI = "CTCP In Dien Hong";
GListCode[152] = "DHL";
GListStockInfo.DHL = "CTCP Co khi Van tai Thuong mai Dai Hung";
GListCode[153] = "DHM";
GListStockInfo.DHM = "CTCP TM&KT KS DUONG HIEU";
GListCode[154] = "DHP";
GListStockInfo.DHP = "CTCP Dien co Hai Phong";
GListCode[155] = "DHT";
GListStockInfo.DHT = "CTCP Duoc pham Ha Tay";
GListCode[156] = "DIC";
GListStockInfo.DIC = "CTCP DT&TM DIC";
GListCode[157] = "DID";
GListStockInfo.DID = "CTCP DIC Dong Tien";
GListCode[158] = "DIG";
GListStockInfo.DIG = "TCT CP DT PT XAY DUNG";
GListCode[159] = "DIH";
GListStockInfo.DIH = "CTCP Dau tu phat trien Xay dung - Hoi An";
GListCode[160] = "DL1";
GListStockInfo.DL1 = "CTCP Dau tu phat trien dich vu cong trinh cong cong Duc Long - Gia Lai";
GListCode[161] = "DLC";
GListStockInfo.DLC = "CTCP Du lich Can Tho";
GListCode[162] = "DLD";
GListStockInfo.DLD = "CTCP Du lich DakLak";
GListCode[163] = "DLG";
GListStockInfo.DLG = "CTCP TD DUC LONG GIA LAI";
GListCode[164] = "DLR";
GListStockInfo.DLR = "CTCP Dia Oc Da Lat";
GListCode[165] = "DMC";
GListStockInfo.DMC = "CTCP XNK Y TE DOMESCO";
GListCode[166] = "DNC";
GListStockInfo.DNC = "CTCP Dien nuoc lap may Hai Phong";
GListCode[167] = "DNF";
GListStockInfo.DNF = "CTCP Luong thuc Da Nang";
GListCode[168] = "DNL";
GListStockInfo.DNL = "CTCP Logistics Cang Da Nang";
GListCode[169] = "DNM";
GListStockInfo.DNM = "Tong CTCP Y te DANAMECO";
GListCode[170] = "DNP";
GListStockInfo.DNP = "CTCP Nhua Dong Nai";
GListCode[171] = "DNS";
GListStockInfo.DNS = "CTCP Thep Da Nang";
GListCode[172] = "DNT";
GListStockInfo.DNT = "CTCP Du lich Dong Nai";
GListCode[173] = "DNY";
GListStockInfo.DNY = "CTCP Thep Dana-ï¿½";
GListCode[174] = "DPC";
GListStockInfo.DPC = "CTCP Nhua Da Nang";
GListCode[175] = "DPM";
GListStockInfo.DPM = "CTCP PHAN DAM&HOA CHAT DK";
GListCode[176] = "DPP";
GListStockInfo.DPP = "CTCP Duoc Dong Nai";
GListCode[177] = "DPR";
GListStockInfo.DPR = "CTCP CAO SU DONG PHU";
GListCode[178] = "DQC";
GListStockInfo.DQC = "CTCP BONG DEN DIEN QUANG";
GListCode[179] = "DRC";
GListStockInfo.DRC = "CTCP CAO SU DA NANG";
GListCode[180] = "DRH";
GListStockInfo.DRH = "CTCP DT CAN NHA MO UOC";
GListCode[181] = "DRL";
GListStockInfo.DRL = "CTCP THUY DIEN-DIEN LUC 3";
GListCode[182] = "DSN";
GListStockInfo.DSN = "CT CONG VIEN NUOC DAM SEN";
GListCode[183] = "DST";
GListStockInfo.DST = "CTCP Sach va Thiet bi Truong hoc Nam Dinh";
GListCode[184] = "DTA";
GListStockInfo.DTA = "CTCP DE TAM";
GListCode[185] = "DTC";
GListStockInfo.DTC = "CTCP Viglacera Dong Trieu";
GListCode[186] = "DTL";
GListStockInfo.DTL = "CTCP DAI THIEN LOC";
GListCode[187] = "DTT";
GListStockInfo.DTT = "CTCP KY NGHE DO THANH";
GListCode[188] = "DTV";
GListStockInfo.DTV = "CTCP Phat trien dien nong thon Tra Vinh";
GListCode[189] = "DVH";
GListStockInfo.DVH = "CTCP Che tao may dien Viet Nam - Hungari";
GListCode[190] = "DVP";
GListStockInfo.DVP = "CTCP DT&PT CANG DINH VU";
GListCode[191] = "DXG";
GListStockInfo.DXG = "CTCP DV&XD DIA OC DATXANH";
GListCode[192] = "DXL";
GListStockInfo.DXL = "CTCP Du lich va Xuat nhap khau Lang Son";
GListCode[193] = "DXP";
GListStockInfo.DXP = "CTCP Cang Doan Xa";
GListCode[194] = "DXV";
GListStockInfo.DXV = "CTCP XM VLXD XL DA NANG";
GListCode[195] = "DZM";
GListStockInfo.DZM = "CTCP Che tao may Dzi An";
GListCode[196] = "EBS";
GListStockInfo.EBS = "CTCP Sach giao duc tai TP";
GListCode[197] = "ECI";
GListStockInfo.ECI = "CTCP Ban do va tranh anh giao duc";
GListCode[198] = "EFI";
GListStockInfo.EFI = "CTCP Dau tu tai chinh Giao duc";
GListCode[199] = "EIB";
GListStockInfo.EIB = "NH TMCP XNK VIET NAM";
GListCode[200] = "EID";
GListStockInfo.EID = "CTCP Dau tu va Phat trien Giao duc Ha Noi";
GListCode[201] = "ELC";
GListStockInfo.ELC = "CTCP DTPT CNDT VIEN THONG";
GListCode[202] = "EMC";
GListStockInfo.EMC = "CTCP CO DIEN THU DUC";
GListCode[203] = "EVE";
GListStockInfo.EVE = "CTCP EVERPIA VIET NAM";
GListCode[204] = "FBA";
GListStockInfo.FBA = "CTCP Tap doan Quoc Te FBA";
GListCode[205] = "FCM";
GListStockInfo.FCM = "CTCP KHOANG SAN FECON";
GListCode[206] = "FCN";
GListStockInfo.FCN = "CTCP KT NM&CT NGAM FECON";
GListCode[207] = "FDC";
GListStockInfo.FDC = "CTCP NT&PTDT TPHCM";
GListCode[208] = "FDG";
GListStockInfo.FDG = "CTCP DOCIMEXCO";
GListCode[209] = "FDT";
GListStockInfo.FDT = "CTCP Fiditour";
GListCode[210] = "FIT";
GListStockInfo.FIT = "CTCP Dau tu F.I.T";
GListCode[211] = "FLC";
GListStockInfo.FLC = "CTCP TAP DOAN FLC";
GListCode[212] = "FMC";
GListStockInfo.FMC = "CTCP THUC PHAM SAO TA";
GListCode[213] = "FPT";
GListStockInfo.FPT = "CTCP PT DT CONG NGHE FPT";
GListCode[214] = "GAS";
GListStockInfo.GAS = "TCT KHI VIET NAM - CTCP";
GListCode[215] = "GBS";
GListStockInfo.GBS = "CTCP Chung khoan Golden Bridge Viet Nam";
GListCode[216] = "GDT";
GListStockInfo.GDT = "CTCP CHE BIEN GO DUCTHANH";
GListCode[217] = "GDW";
GListStockInfo.GDW = "CTCP Cap nuoc Gia Dinh";
GListCode[218] = "GER";
GListStockInfo.GER = "CTCP The thao Ngoi sao Geru";
GListCode[219] = "GGG";
GListStockInfo.GGG = "CTCP O to Giai Phong";
GListCode[220] = "GHC";
GListStockInfo.GHC = "CTCP Thuy dien Gia Lai";
GListCode[221] = "GIL";
GListStockInfo.GIL = "CTCP SXKD&XNK BINH THANH";
GListCode[222] = "GLT";
GListStockInfo.GLT = "CTCP Ky thuat Dien Toan Cau";
GListCode[223] = "GMC";
GListStockInfo.GMC = "CTCP SX - TM MAY SAI GON";
GListCode[224] = "GMD";
GListStockInfo.GMD = "CTCP DAI LY LH VAN CHUYEN";
GListCode[225] = "GMX";
GListStockInfo.GMX = "CTCP Gach Ngoi Gom Xay dung My Xuan";
GListCode[226] = "GSP";
GListStockInfo.GSP = "CTCP VT SP KHI QUOC TE";
GListCode[227] = "GTA";
GListStockInfo.GTA = "CTCP CHE BIEN GO THUAN AN";
GListCode[228] = "GTH";
GListStockInfo.GTH = "CTCP Xay dung- Giao thong Thua Thien Hue";
GListCode[229] = "GTT";
GListStockInfo.GTT = "CTCP THUAN THAO";
GListCode[230] = "H11";
GListStockInfo.H11 = "CTCP Xay dung HUD 101";
GListCode[231] = "HAD";
GListStockInfo.HAD = "CTCP Bia Ha Noi - Hai Duong";
GListCode[232] = "HAG";
GListStockInfo.HAG = "CTCP HOANG ANH GIA LAI";
GListCode[233] = "HAI";
GListStockInfo.HAI = "CTCP NONG DUOC HAI";
GListCode[234] = "HAP";
GListStockInfo.HAP = "CTY CP GIAY HAI PHONG";
GListCode[235] = "HAR";
GListStockInfo.HAR = "CTCP DTTMBDS AD THAO DIEN";
GListCode[236] = "HAS";
GListStockInfo.HAS = "CT CP XAY LAP BUU DIEN HN";
GListCode[237] = "HAT";
GListStockInfo.HAT = "CTCP Thuong mai Bia Ha Noi";
GListCode[238] = "HAX";
GListStockInfo.HAX = "CTCP DICH VU OTO HANGXANH";
GListCode[239] = "HBC";
GListStockInfo.HBC = "CTCP XD&KD DIA OC HOABINH";
GListCode[240] = "HBE";
GListStockInfo.HBE = "CTCP Sach va thiet bi truong hoc Ha Tinh";
GListCode[241] = "HBS";
GListStockInfo.HBS = "CTCP Chung khoan Hoa Binh";
GListCode[242] = "HCC";
GListStockInfo.HCC = "CTCP Be tong Hoa Cam - Intimex";
GListCode[243] = "HCI";
GListStockInfo.HCI = "CTCP Dau tu - Xay dung Ha Noi";
GListCode[244] = "HCM";
GListStockInfo.HCM = "CTY CPCK TP HO CHI MINH";
GListCode[245] = "HCT";
GListStockInfo.HCT = "CTCP Thuong mai Dich vu Van tai Xi mang Hai Phong";
GListCode[246] = "HDA";
GListStockInfo.HDA = "CTCP Hang son Dong A";
GListCode[247] = "HDC";
GListStockInfo.HDC = "CTCP PT NHA BA RIA-V.TAU";
GListCode[248] = "HDG";
GListStockInfo.HDG = "CTCP HA DO";
GListCode[249] = "HDM";
GListStockInfo.HDM = "CTCP Det May Hue";
GListCode[250] = "HDO";
GListStockInfo.HDO = "CTCP Hung Dao Container";
GListCode[251] = "HEV";
GListStockInfo.HEV = "CTCP Sach Dai hoc va Day nghe";
GListCode[252] = "HFC";
GListStockInfo.HFC = "CTCP Xang dau Chat dot Ha Noi";
GListCode[253] = "HFX";
GListStockInfo.HFX = "CTCP San xuat - Xuat nhap khau Thanh Ha";
GListCode[254] = "HGM";
GListStockInfo.HGM = "CTCP Co khi va Khoang san Ha Giang";
GListCode[255] = "HHC";
GListStockInfo.HHC = "CTCP Banh keo Hai Ha";
GListCode[256] = "HHG";
GListStockInfo.HHG = "CTCP Hoang Ha";
GListCode[257] = "HHL";
GListStockInfo.HHL = "CTCP Hong Ha Long An";
GListCode[258] = "HHS";
GListStockInfo.HHS = "CTCP DT-DV HOANG HUY";
GListCode[259] = "HIG";
GListStockInfo.HIG = "CTCP Tap Doan HIPT";
GListCode[260] = "HJS";
GListStockInfo.HJS = "CTCP Thuy dien Nam Mu";
GListCode[261] = "HLA";
GListStockInfo.HLA = "CTY CP HUU LIEN A CHAU";
GListCode[262] = "HLC";
GListStockInfo.HLC = "CTCP Than Ha Lam - Vinacomin";
GListCode[263] = "HLD";
GListStockInfo.HLD = "CTCP Dau tu va phat trien Bat dong san HUDLAND";
GListCode[264] = "HLG";
GListStockInfo.HLG = "CTCP TAP DOAN HOANG LONG";
GListCode[265] = "HLY";
GListStockInfo.HLY = "CTCP Viglacera Ha Long I";
GListCode[266] = "HMC";
GListStockInfo.HMC = "CTCP KIM KHI TPHCM";
GListCode[267] = "HMH";
GListStockInfo.HMH = "CTCP Hai Minh";
GListCode[268] = "HNM";
GListStockInfo.HNM = "CTCP Sua Ha Noi";
GListCode[269] = "HOM";
GListStockInfo.HOM = "CTCP Xi mang VICEM Hoang Mai";
GListCode[270] = "HOT";
GListStockInfo.HOT = "CTCP DL - DV HOI AN";
GListCode[271] = "HPB";
GListStockInfo.HPB = "CTCP Bao bi PP";
GListCode[272] = "HPC";
GListStockInfo.HPC = "CTCP Chung khoan Hai Phong";
GListCode[273] = "HPG";
GListStockInfo.HPG = "CTCP TAP DOAN HOA PHAT";
GListCode[274] = "HPL";
GListStockInfo.HPL = "CTCP Ben xe Tau pha Can Tho";
GListCode[275] = "HPP";
GListStockInfo.HPP = "CTCP Son Hai Phong";
GListCode[276] = "HPS";
GListStockInfo.HPS = "CTCP Da xay dung Hoa Phat";
GListCode[277] = "HPT";
GListStockInfo.HPT = "CTCP Dich vu Cong nghe Tin hoc HPT";
GListCode[278] = "HQC";
GListStockInfo.HQC = "CTCP TVTMDV DO HOANG QUAN";
GListCode[279] = "HRC";
GListStockInfo.HRC = "CTCP CAO SU HOA BINH";
GListCode[280] = "HSG";
GListStockInfo.HSG = "CTY CP TAP DOAN HOA SEN";
GListCode[281] = "HSI";
GListStockInfo.HSI = "CTCP VTTH&PB HOA SINH";
GListCode[282] = "HST";
GListStockInfo.HST = "CTCP Phat hanh sach va Thiet bi truong hoc Hung Yen";
GListCode[283] = "HT1";
GListStockInfo.HT1 = "CTCP XI MANG HA TIEN 1";
GListCode[284] = "HTB";
GListStockInfo.HTB = "Cong ty Co phan Xay dung Huy Thang";
GListCode[285] = "HTC";
GListStockInfo.HTC = "CTCP Thuong mai Hoc Mon";
GListCode[286] = "HTI";
GListStockInfo.HTI = "CTCP DTPT HA TANG IDICO";
GListCode[287] = "HTL";
GListStockInfo.HTL = "CTCP KT & OTO TRUONG LONG";
GListCode[288] = "HTP";
GListStockInfo.HTP = "CTCP In Sach giao khoa Hoa Phat";
GListCode[289] = "HTV";
GListStockInfo.HTV = "CTCP VAN TAI HA TIEN";
GListCode[290] = "HU1";
GListStockInfo.HU1 = "CTCP DT & XAY DUNG HUD1";
GListCode[291] = "HU3";
GListStockInfo.HU3 = "CTCP DT & XD HUD3";
GListCode[292] = "HUT";
GListStockInfo.HUT = "CTCP Tasco";
GListCode[293] = "HVG";
GListStockInfo.HVG = "CTCP HUNG VUONG";
GListCode[294] = "HVT";
GListStockInfo.HVT = "CTCP Hoa chat Viet Tri";
GListCode[295] = "HVX";
GListStockInfo.HVX = "CTCP XI MANG HAI VAN";
GListCode[296] = "I40";
GListStockInfo.I40 = "CTCP Dau tu va xay dung 40";
GListCode[297] = "ICF";
GListStockInfo.ICF = "CTCP DT TM THUY SAN";
GListCode[298] = "ICG";
GListStockInfo.ICG = "CTCP Xay dung Song Hong";
GListCode[299] = "ICI";
GListStockInfo.ICI = "CTCP Dau tu va Xay dung cong nghiep";
GListCode[300] = "IDI";
GListStockInfo.IDI = "CTCP DT&PT DA QUOC GIA";
GListCode[301] = "IDJ";
GListStockInfo.IDJ = "CTCP Dau tu Tai chinh Quoc te va Phat trien Doanh nghiep IDJ";
GListCode[302] = "IDV";
GListStockInfo.IDV = "CTCP Phat trien ha tang Vinh Phuc";
GListCode[303] = "IHK";
GListStockInfo.IHK = "CTCP In Hang khong";
GListCode[304] = "IJC";
GListStockInfo.IJC = "CTCP PT HA TANG KY THUAT";
GListCode[305] = "ILC";
GListStockInfo.ILC = "CTCP Hop tac lao dong voi nuoc ngoai";
GListCode[306] = "IME";
GListStockInfo.IME = "CTCP Co khi va Xay lap Cong nghiep";
GListCode[307] = "IMP";
GListStockInfo.IMP = "CTCP DUOC PHAM IMEXPHARM";
GListCode[308] = "IN4";
GListStockInfo.IN4 = "CTCP In so 4";
GListCode[309] = "INC";
GListStockInfo.INC = "CTCP Tu van Dau tu IDICO";
GListCode[310] = "INN";
GListStockInfo.INN = "CTCP Bao bi va In Nong nghiep";
GListCode[311] = "ITA";
GListStockInfo.ITA = "CTCP KCN TAN TAO";
GListCode[312] = "ITC";
GListStockInfo.ITC = "CTCP DT KD NHA INTRESCO";
GListCode[313] = "ITD";
GListStockInfo.ITD = "CTCP CONG NGHE TIEN PHONG";
GListCode[314] = "ITQ";
GListStockInfo.ITQ = "CTCP Tap doan Thien Quang";
GListCode[315] = "IVS";
GListStockInfo.IVS = "CTCP Chung khoan Dau tu Viet Nam";
GListCode[316] = "JSC";
GListStockInfo.JSC = "CTCP Dau tu va Xay dung cau duong Ha Noi";
GListCode[317] = "JVC";
GListStockInfo.JVC = "CTCP TB Y TE VIET NHAT";
GListCode[318] = "KAC";
GListStockInfo.KAC = "CT DAU TU DIA OC KHANG AN";
GListCode[319] = "KBC";
GListStockInfo.KBC = "TCT PT DT KINH BAC";
GListCode[320] = "KBE";
GListStockInfo.KBE = "CTCP Sach - Thiet bi truong hoc Kien Giang";
GListCode[321] = "KBT";
GListStockInfo.KBT = "CTCP Gach ngoi Kien Giang";
GListCode[322] = "KCE";
GListStockInfo.KCE = "CTCP Be tong ly tam Dien luc Khanh Hoa";
GListCode[323] = "KDC";
GListStockInfo.KDC = "CONG TY CO PHAN KINH DO";
GListCode[324] = "KDH";
GListStockInfo.KDH = "CTCP DT&KD NHA KHANG DIEN";
GListCode[325] = "KHA";
GListStockInfo.KHA = "CTY CP XNK KHANH HOI";
GListCode[326] = "KHB";
GListStockInfo.KHB = "CTCP Khoang san Hoa Binh";
GListCode[327] = "KHL";
GListStockInfo.KHL = "CTCP Khoang san va Vat lieu Xay dung Hung Long";
GListCode[328] = "KHP";
GListStockInfo.KHP = "CTCP DIEN LUC KHANH HOA";
GListCode[329] = "KKC";
GListStockInfo.KKC = "CTCP San xuat va Kinh doanh Kim khi";
GListCode[330] = "KLS";
GListStockInfo.KLS = "CTCP Chung khoan Kim Long";
GListCode[331] = "KMR";
GListStockInfo.KMR = "CTY CO PHAN MIRAE";
GListCode[332] = "KMT";
GListStockInfo.KMT = "CTCP Kim khi Mien Trung";
GListCode[333] = "KSA";
GListStockInfo.KSA = "CTCP KS BINH THUAN HAMICO";
GListCode[334] = "KSB";
GListStockInfo.KSB = "CTCP KS & XD BINH DUONG";
GListCode[335] = "KSC";
GListStockInfo.KSC = "CTCP Muoi Khanh Hoa";
GListCode[336] = "KSD";
GListStockInfo.KSD = "Tong CTCP Xuat khau Dong Nam A Hamico";
GListCode[337] = "KSH";
GListStockInfo.KSH = "TCT CP KHOANG SAN HA NAM";
GListCode[338] = "KSQ";
GListStockInfo.KSQ = "CTCP Khoang san Quang Anh";
GListCode[339] = "KSS";
GListStockInfo.KSS = "TCTCP KS NA RI HAMICO";
GListCode[340] = "KST";
GListStockInfo.KST = "CTCP KASATI";
GListCode[341] = "KTB";
GListStockInfo.KTB = "CTY DT KHOANG SAN TAY BAC";
GListCode[342] = "KTS";
GListStockInfo.KTS = "CTCP Duong KonTum";
GListCode[343] = "KTT";
GListStockInfo.KTT = "CTCP Dau tu thiet bi va Xay lap dien Thien Truong";
GListCode[344] = "L10";
GListStockInfo.L10 = "CT CP LILAMA 10";
GListCode[345] = "L14";
GListStockInfo.L14 = "CTCP Licogi 14";
GListCode[346] = "L18";
GListStockInfo.L18 = "CTCP Dau tu va Xay dung so 18";
GListCode[347] = "L35";
GListStockInfo.L35 = "CTCP Co lap may Lilama";
GListCode[348] = "L43";
GListStockInfo.L43 = "CTCP Lilama 45.3";
GListCode[349] = "L44";
GListStockInfo.L44 = "CTCP Lilama 45.4";
GListCode[350] = "L61";
GListStockInfo.L61 = "CTCP Lilama 69-1";
GListCode[351] = "L62";
GListStockInfo.L62 = "CTCP Lilama 69.2";
GListCode[352] = "LAF";
GListStockInfo.LAF = "CTCP CB HANG XK LONG AN";
GListCode[353] = "LAS";
GListStockInfo.LAS = "CTCP Supe Phot phat va Hoa chat Lam Thao";
GListCode[354] = "LBE";
GListStockInfo.LBE = "CTCP Sach va Thiet bi Truong hoc Long An";
GListCode[355] = "LBM";
GListStockInfo.LBM = "CTCP VAT LIEU XD LAM DONG";
GListCode[356] = "LCC";
GListStockInfo.LCC = "CTCP Xi mang Lang Son";
GListCode[357] = "LCD";
GListStockInfo.LCD = "CTCP Lilama Thi nghiem Co dien";
GListCode[358] = "LCG";
GListStockInfo.LCG = "CT CP LICOGI 16";
GListCode[359] = "LCM";
GListStockInfo.LCM = "CTCP KT&CB KS LAO CAI";
GListCode[360] = "LCS";
GListStockInfo.LCS = "CTCP Licogi 166";
GListCode[361] = "LDP";
GListStockInfo.LDP = "CTCP Duoc Lam Dong - Ladophar";
GListCode[362] = "LGC";
GListStockInfo.LGC = "CTCP CO KHI-DIEN LU GIA";
GListCode[363] = "LGL";
GListStockInfo.LGL = "CTCP DTPT DT LONG GIANG";
GListCode[364] = "LHC";
GListStockInfo.LHC = "CTCP Dau tu va Xay dung Thuy loi Lam Dong";
GListCode[365] = "LHG";
GListStockInfo.LHG = "CTCP LONG HAU";
GListCode[366] = "LIG";
GListStockInfo.LIG = "CTCP Licogi 13";
GListCode[367] = "LIX";
GListStockInfo.LIX = "CTCP BOT GIAT LIX";
GListCode[368] = "LKW";
GListStockInfo.LKW = "CTCP Cap nuoc Long Khanh";
GListCode[369] = "LM3";
GListStockInfo.LM3 = "CTCP Lilama 3";
GListCode[370] = "LM7";
GListStockInfo.LM7 = "CTCP Lilama 7";
GListCode[371] = "LM8";
GListStockInfo.LM8 = "CTCP LILAMA 18";
GListCode[372] = "LO5";
GListStockInfo.LO5 = "CTCP Lilama 5";
GListCode[373] = "LSS";
GListStockInfo.LSS = "CTCP MIA DUONG LAM SON";
GListCode[374] = "LTC";
GListStockInfo.LTC = "CTCP Dien nhe vien thong";
GListCode[375] = "LUT";
GListStockInfo.LUT = "CTCP Dau tu va Xay dung Luong Tai";
GListCode[376] = "MAC";
GListStockInfo.MAC = "CTCP Cung ung va Dich vu Ky thuat Hang hai";
GListCode[377] = "MAFPF1";
GListStockInfo.MAFPF1 = "QUY DT T.TRUONG MANULIFE";
GListCode[378] = "MAS";
GListStockInfo.MAS = "CTCP Dich vu Hang Khong San Bay Da Nang";
GListCode[379] = "MAX";
GListStockInfo.MAX = "CTCP Khai khoang va Co khi Huu nghi Vinh Sinh";
GListCode[380] = "MBB";
GListStockInfo.MBB = "NGAN HANG TMCP QUAN DOI";
GListCode[381] = "MCC";
GListStockInfo.MCC = "CTCP Gach ngoi cao cap";
GListCode[382] = "MCF";
GListStockInfo.MCF = "CTCP Xay lap Co khi Luong thuc Thuc pham";
GListCode[383] = "MCG";
GListStockInfo.MCG = "CTCP CO DIEN & XD VN";
GListCode[384] = "MCL";
GListStockInfo.MCL = "CTCP Phat trien nha va san xuat vat lieu xay dung Chi Linh";
GListCode[385] = "MCO";
GListStockInfo.MCO = "CTCP MCO Viet Nam";
GListCode[386] = "MCP";
GListStockInfo.MCP = "CTCP IN & BAO BI MY CHAU";
GListCode[387] = "MDC";
GListStockInfo.MDC = "CTCP Than Mong Duong - Vinacomin";
GListCode[388] = "MDF";
GListStockInfo.MDF = "CTCP Go MDF VRG - Quang Tri";
GListCode[389] = "MDG";
GListStockInfo.MDG = "CTCP MIEN DONG";
GListCode[390] = "MEC";
GListStockInfo.MEC = "CTCP Someco Song Da";
GListCode[391] = "MEF";
GListStockInfo.MEF = "CTCP MEINFA";
GListCode[392] = "MHC";
GListStockInfo.MHC = "CTCP HANG HAI HA NOI";
GListCode[393] = "MHL";
GListStockInfo.MHL = "CTCP Minh Huu Lien";
GListCode[394] = "MIC";
GListStockInfo.MIC = "CTCP Ky nghe khoang san Quang Nam";
GListCode[395] = "MIM";
GListStockInfo.MIM = "CTCP Khoang san va Co khi";
GListCode[396] = "MKV";
GListStockInfo.MKV = "CTCP Duoc Thu Y Cai Lay";
GListCode[397] = "MMC";
GListStockInfo.MMC = "CTCP Khoang san Mangan";
GListCode[398] = "MNC";
GListStockInfo.MNC = "CTCP Mai Linh mien Trung";
GListCode[399] = "MPC";
GListStockInfo.MPC = "CTCP THUY HAI SAN MINHPHU";
GListCode[400] = "MSN";
GListStockInfo.MSN = "CTCP TAP DOAN MA SAN";
GListCode[401] = "MTC";
GListStockInfo.MTC = "CTCP Khach san My Tra";
GListCode[402] = "MTG";
GListStockInfo.MTG = "CONG TY CO PHAN MT GAS";
GListCode[403] = "MTH";
GListStockInfo.MTH = "CTCP Moi truong do thi Ha Dong";
GListCode[404] = "MTP";
GListStockInfo.MTP = "CTCP Duoc Trung uong Medipharco - Tenamyd";
GListCode[405] = "NAG";
GListStockInfo.NAG = "CTCP Nagakawa Viet Nam";
GListCode[406] = "NAV";
GListStockInfo.NAV = "CTCP NAM VIET";
GListCode[407] = "NBB";
GListStockInfo.NBB = "CTCP DAU TU NAM BAY BAY";
GListCode[408] = "NBC";
GListStockInfo.NBC = "CTCP Than Nui Beo - Vinacomin";
GListCode[409] = "NBP";
GListStockInfo.NBP = "CTCP Nhiet dien Ninh Binh";
GListCode[410] = "NBS";
GListStockInfo.NBS = "CTCP Ben xe Nghe An";
GListCode[411] = "NBW";
GListStockInfo.NBW = "CTCP Cap nuoc Nha Be";
GListCode[412] = "ND2";
GListStockInfo.ND2 = "CTCP Dau tu va phat trien dien mien Bac 2";
GListCode[413] = "NDC";
GListStockInfo.NDC = "CTCP Nam Duoc";
GListCode[414] = "NDN";
GListStockInfo.NDN = "CTCP Dau tu phat trien Nha Da Nang";
GListCode[415] = "NDX";
GListStockInfo.NDX = "CTCP Xay lap Phat trien Nha Da Nang";
GListCode[416] = "NET";
GListStockInfo.NET = "CTCP Bot giat NET";
GListCode[417] = "NGC";
GListStockInfo.NGC = "CTCP Che bien Thuy san xuat khau Ngo Quyen";
GListCode[418] = "NHA";
GListStockInfo.NHA = "Tong cong ty Dau tu Phat trien Nha va do thi Nam Ha Noi";
GListCode[419] = "NHC";
GListStockInfo.NHC = "CTCP Gach ngoi Nhi Hiep";
GListCode[420] = "NHN";
GListStockInfo.NHN = "CTCP Phat trien Do thi Nam Ha Noi";
GListCode[421] = "NHS";
GListStockInfo.NHS = "CTCP DUONG NINH HOA";
GListCode[422] = "NHW";
GListStockInfo.NHW = "CTCP NGO HAN";
GListCode[423] = "NIS";
GListStockInfo.NIS = "CTCP Dich vu ha tang mang";
GListCode[424] = "NKG";
GListStockInfo.NKG = "CTY CP THEP NAM KIM";
GListCode[425] = "NLC";
GListStockInfo.NLC = "CTCP Thuy dien Na Loi";
GListCode[426] = "NLG";
GListStockInfo.NLG = "CTCP DAU TU NAM LONG";
GListCode[427] = "NNC";
GListStockInfo.NNC = "CTCP DA NUI NHO";
GListCode[428] = "NNT";
GListStockInfo.NNT = "CTCP Cap nuoc Ninh Thuan";
GListCode[429] = "NOS";
GListStockInfo.NOS = "CTCP Van tai Bien Bac";
GListCode[430] = "NPS";
GListStockInfo.NPS = "CTCP May Phu Thinh Nha Be";
GListCode[431] = "NSC";
GListStockInfo.NSC = "CTCP GIONG CAY TRONG TW";
GListCode[432] = "NSN";
GListStockInfo.NSN = "CTCP Xay dung 565";
GListCode[433] = "NSP";
GListStockInfo.NSP = "CTCP Nhua Sam Phu";
GListCode[434] = "NST";
GListStockInfo.NST = "CTCP Ngan Son";
GListCode[435] = "NT2";
GListStockInfo.NT2 = "CTCP Dien luc Dau khi Nhon Trach 2";
GListCode[436] = "NTL";
GListStockInfo.NTL = "CTCP PT DO THI TU LIEM";
GListCode[437] = "NTP";
GListStockInfo.NTP = "CTCP Nhua Thieu nien - Tien Phong";
GListCode[438] = "NTW";
GListStockInfo.NTW = "CTCP Cap nuoc Nhon Trach";
GListCode[439] = "NVB";
GListStockInfo.NVB = "NHTM co phan Nam Viet";
GListCode[440] = "NVC";
GListStockInfo.NVC = "CTCP Nam Vang";
GListCode[441] = "NVN";
GListStockInfo.NVN = "CTCP NHA VIET NAM";
GListCode[442] = "NVT";
GListStockInfo.NVT = "CTCP BDS DL NINH VAN BAY";
GListCode[443] = "OCH";
GListStockInfo.OCH = "CTCP Khach san va dich vu Dai Duong";
GListCode[444] = "OGC";
GListStockInfo.OGC = "CTCP TAP DOAN DAI DUONG";
GListCode[445] = "ONE";
GListStockInfo.ONE = "CTCP Truyen thong so 1";
GListCode[446] = "OPC";
GListStockInfo.OPC = "CTY CP DUOC PHAM OPC";
GListCode[447] = "ORS";
GListStockInfo.ORS = "CTCP Chung khoan Phuong Dong";
GListCode[448] = "PAC";
GListStockInfo.PAC = "CTCP PIN AC QUY MIEN NAM";
GListCode[449] = "PAN";
GListStockInfo.PAN = "CTCP XUYEN THAI BINH";
GListCode[450] = "PCG";
GListStockInfo.PCG = "CTCP Dau tu Phat trien Gas do thi";
GListCode[451] = "PCT";
GListStockInfo.PCT = "CTCP Dich vu - Van tai Dau khi Cuu Long";
GListCode[452] = "PDC";
GListStockInfo.PDC = "CTCP Du lich Dau khi Phuong Dong";
GListCode[453] = "PDN";
GListStockInfo.PDN = "CTCP CANG DONG NAI";
GListCode[454] = "PDR";
GListStockInfo.PDR = "CTCP PT BDS PHAT DAT";
GListCode[455] = "PEC";
GListStockInfo.PEC = "CTCP Co khi Dien luc";
GListCode[456] = "PET";
GListStockInfo.PET = "CTCP DV-DL DAU KHI";
GListCode[457] = "PFL";
GListStockInfo.PFL = "CTCP Dau khi Dong Do";
GListCode[458] = "PFV";
GListStockInfo.PFV = "CTCP Dau tu va Thuong mai PFV";
GListCode[459] = "PGC";
GListStockInfo.PGC = "CTCP GAS PETROLIMEX";
GListCode[460] = "PGD";
GListStockInfo.PGD = "CTCP PP KHI THAP AP DAU";
GListCode[461] = "PGI";
GListStockInfo.PGI = "CTCP BAO HIEM PJICO";
GListCode[462] = "PGS";
GListStockInfo.PGS = "CTCP Kinh doanh Khi hoa long mien Nam";
GListCode[463] = "PGT";
GListStockInfo.PGT = "CTCP Taxi Gas Sai Gon Petrolimex";
GListCode[464] = "PHC";
GListStockInfo.PHC = "CTCP Xay dung Phuc Hung Holdings";
GListCode[465] = "PHH";
GListStockInfo.PHH = "CTCP Hong Ha Viet Nam";
GListCode[466] = "PHR";
GListStockInfo.PHR = "CTCP CAO SU PHUOC HOA";
GListCode[467] = "PHS";
GListStockInfo.PHS = "CTCP Chung khoan Phu Hung";
GListCode[468] = "PID";
GListStockInfo.PID = "CTCP Trang tri noi that Dau Khi";
GListCode[469] = "PIT";
GListStockInfo.PIT = "CTCP XNK PETROLIMEX";
GListCode[470] = "PIV";
GListStockInfo.PIV = "CTCP PIV";
GListCode[471] = "PJC";
GListStockInfo.PJC = "CTCP Thuong mai va Van tai Petrolimex Ha Noi";
GListCode[472] = "PJS";
GListStockInfo.PJS = "CTCP Cap nuoc Phu Hoa Tan";
GListCode[473] = "PJT";
GListStockInfo.PJT = "CTCP VTXD DT PETROLIMEX";
GListCode[474] = "PLC";
GListStockInfo.PLC = "Tong Cong ty Hoa dau Petrolimex- CTCP";
GListCode[475] = "PMC";
GListStockInfo.PMC = "CTCP Duoc pham duoc lieu Pharmedic";
GListCode[476] = "PMS";
GListStockInfo.PMS = "CTCP Co khi Xang dau";
GListCode[477] = "PMT";
GListStockInfo.PMT = "CTCP Vat lieu Buu dien";
GListCode[478] = "PNC";
GListStockInfo.PNC = "CTCP VAN HOA PHUONG NAM";
GListCode[479] = "PNJ";
GListStockInfo.PNJ = "CTCP VB DQ PHU NHUAN";
GListCode[480] = "POM";
GListStockInfo.POM = "CTCP THEP POMINA";
GListCode[481] = "POT";
GListStockInfo.POT = "CTCP Thiet bi Buu dien";
GListCode[482] = "POV";
GListStockInfo.POV = "CTCP Xang dau Dau khi Vung Ang";
GListCode[483] = "PPC";
GListStockInfo.PPC = "CTCP NHIET DIEN PHA LAI";
GListCode[484] = "PPE";
GListStockInfo.PPE = "CTCP Tu van Dien luc Dau khi Viet Nam";
GListCode[485] = "PPG";
GListStockInfo.PPG = "CTCP San xuat - Thuong Mai - Dich vu Phu Phong";
GListCode[486] = "PPI";
GListStockInfo.PPI = "CTCP PT HT & BDS TBD";
GListCode[487] = "PPP";
GListStockInfo.PPP = "CTCP Duoc pham Phong Phu";
GListCode[488] = "PPS";
GListStockInfo.PPS = "CTCP Dich vu Ky thuat Dien luc Dau khi Viet Nam";
GListCode[489] = "PRC";
GListStockInfo.PRC = "CTCP Portserco";
GListCode[490] = "PRUBF1";
GListStockInfo.PRUBF1 = "QDT CAN BANG PRUDENTIAL";
GListCode[491] = "PSB";
GListStockInfo.PSB = "CTCP Dau tu Dau khi Sao Mai ï¿½ Ben Dinh";
GListCode[492] = "PSC";
GListStockInfo.PSC = "CTCP Van tai va Dich vu Petrolimex Sai Gon";
GListCode[493] = "PSD";
GListStockInfo.PSD = "CTCP Dich vu Phan phoi Tong hop Dau khi";
GListCode[494] = "PSG";
GListStockInfo.PSG = "CTCP Dau tu va Xay lap Dau khi Sai Gon";
GListCode[495] = "PSI";
GListStockInfo.PSI = "CTCP Chung khoan Dau khi";
GListCode[496] = "PSL";
GListStockInfo.PSL = "CTCP Chan nuoi Phu Son";
GListCode[497] = "PSP";
GListStockInfo.PSP = "CTCP Cang dich vu Dau khi Dinh Vu";
GListCode[498] = "PTB";
GListStockInfo.PTB = "CTCP PHU TAI";
GListCode[499] = "PTC";
GListStockInfo.PTC = "CTCP DT&XD BUU DIEN";
GListCode[500] = "PTD";
GListStockInfo.PTD = "CTCP Thiet ke - Xay dung - Thuong mai Phuc Thinh";
GListCode[501] = "PTG";
GListStockInfo.PTG = "CTCP May Xuat Khau Phan Thiet";
GListCode[502] = "PTH";
GListStockInfo.PTH = "CTCP Van tai va Dich vu Petrolimex Ha Tay";
GListCode[503] = "PTI";
GListStockInfo.PTI = "Tong CTCP Bao hiem Buu dien";
GListCode[504] = "PTK";
GListStockInfo.PTK = "CTCP LUYEN KIM PHU THINH";
GListCode[505] = "PTL";
GListStockInfo.PTL = "CTCP DT HT & DT DAU KHI";
GListCode[506] = "PTM";
GListStockInfo.PTM = "CTCP San xuat";
GListCode[507] = "PTP";
GListStockInfo.PTP = "CTCP Dich vu vien thong va In buu dien";
GListCode[508] = "PTS";
GListStockInfo.PTS = "CTCP Van tai va Dich vu Petrolimex Hai Phong";
GListCode[509] = "PTT";
GListStockInfo.PTT = "CTCP Van tai dau khi Dong Duong";
GListCode[510] = "PV2";
GListStockInfo.PV2 = "CTCP Dau tu PV2";
GListCode[511] = "PVA";
GListStockInfo.PVA = "CTCP Tong Cong ty Xay lap Dau khi Nghe An";
GListCode[512] = "PVC";
GListStockInfo.PVC = "Tong cong ty Dung dich khoan va Hoa pham Dau khi - CTCP";
GListCode[513] = "PVD";
GListStockInfo.PVD = "CTCP KHOAN VA DV DAU KHI";
GListCode[514] = "PVE";
GListStockInfo.PVE = "Tong Cong ty Tu van Thiet ke Dau khi - CTCP";
GListCode[515] = "PVF";
GListStockInfo.PVF = "TCT TC CP DAU KHI VN";
GListCode[516] = "PVG";
GListStockInfo.PVG = "CTCP Kinh doanh Khi hoa long mien Bac";
GListCode[517] = "PVI";
GListStockInfo.PVI = "CTCP PVI";
GListCode[518] = "PVL";
GListStockInfo.PVL = "CTCP Dia oc Dau khi";
GListCode[519] = "PVR";
GListStockInfo.PVR = "CTCP Kinh doanh dich vu cao cap Dau khi Viet Nam";
GListCode[520] = "PVS";
GListStockInfo.PVS = "Tong CTCP Dich vu Ky Thuat Dau khi Viet Nam";
GListCode[521] = "PVT";
GListStockInfo.PVT = "TCT CP VAN TAI DAU KHI";
GListCode[522] = "PVV";
GListStockInfo.PVV = "CTCP Dau tu Xay dung Vinaconex - PVC";
GListCode[523] = "PVX";
GListStockInfo.PVX = "TCTCP Xay lap Dau khi Viet Nam";
GListCode[524] = "PX1";
GListStockInfo.PX1 = "CTCP Xi mang Dau khi Nghe An";
GListCode[525] = "PXA";
GListStockInfo.PXA = "CTCP Dau tu va Thuong mai Dau khi Nghe An";
GListCode[526] = "PXI";
GListStockInfo.PXI = "CTCP XD CN & DD DAU KHI";
GListCode[527] = "PXL";
GListStockInfo.PXL = "CT DTKCNDK IDICO LONG SON";
GListCode[528] = "PXM";
GListStockInfo.PXM = "CTCP XL DAU KHI MIENTRUNG";
GListCode[529] = "PXS";
GListStockInfo.PXS = "CTCP KCKL & LM DAU KHI";
GListCode[530] = "PXT";
GListStockInfo.PXT = "CTCP XAY LAP DOBC DAU KHI";
GListCode[531] = "QCC";
GListStockInfo.QCC = "CTCP Xay lap va phat trien Dich vu Buu dien Quang Nam";
GListCode[532] = "QCG";
GListStockInfo.QCG = "CTCP QUOC CUONG GIA LAI";
GListCode[533] = "QHD";
GListStockInfo.QHD = "CTCP Que han dien Viet Duc";
GListCode[534] = "QNC";
GListStockInfo.QNC = "CTCP Xi mang va Xay dung Quang Ninh";
GListCode[535] = "QST";
GListStockInfo.QST = "CTCP Sach va Thiet bi Truong hoc Quang Ninh";
GListCode[536] = "QTC";
GListStockInfo.QTC = "CTCP Cong trinh Giao thong van tai Quang Nam";
GListCode[537] = "RAL";
GListStockInfo.RAL = "CTCP B.DEN PH.NUOC R.DONG";
GListCode[538] = "RCL";
GListStockInfo.RCL = "CTCP Dia oc Cho Lon";
GListCode[539] = "RDP";
GListStockInfo.RDP = "CTCP NHUA RANG DONG";
GListCode[540] = "REE";
GListStockInfo.REE = "CTY CP CO DIEN LANH";
GListCode[541] = "REM";
GListStockInfo.REM = "CTCP Tu bo Di tich Trung Uong - Vinaremon";
GListCode[542] = "RHC";
GListStockInfo.RHC = "CTCP Thuy dien Ry Ninh II";
GListCode[543] = "RIC";
GListStockInfo.RIC = "CTCP QUOC TE HOANG GIA";
GListCode[544] = "S12";
GListStockInfo.S12 = "CTCP Song Da 12";
GListCode[545] = "S33";
GListStockInfo.S33 = "CTCP Mia duong 333";
GListCode[546] = "S55";
GListStockInfo.S55 = "CTCP Song Da 505";
GListCode[547] = "S74";
GListStockInfo.S74 = "CTCP Song Da 7.04";
GListCode[548] = "S91";
GListStockInfo.S91 = "CTCP Song Da 9.01";
GListCode[549] = "S96";
GListStockInfo.S96 = "CTCP Song Da 9.06";
GListCode[550] = "S99";
GListStockInfo.S99 = "CTCP Song Da 9.09";
GListCode[551] = "SAF";
GListStockInfo.SAF = "CTCP Luong thuc Thuc pham SAFOCO";
GListCode[552] = "SAM";
GListStockInfo.SAM = "CTCP CAP & VAT LIEU VT";
GListCode[553] = "SAP";
GListStockInfo.SAP = "CTCP In Sach Giao khoa tai Tp. HCM";
GListCode[554] = "SAV";
GListStockInfo.SAV = "CTCP HOP TAC KT & XNK";
GListCode[555] = "SBA";
GListStockInfo.SBA = "CTCP SONG BA";
GListCode[556] = "SBC";
GListStockInfo.SBC = "CTCP VT & GN BIA SAI GON";
GListCode[557] = "SBT";
GListStockInfo.SBT = "CTCP MIA DUONG BOURBON TN";
GListCode[558] = "SC5";
GListStockInfo.SC5 = "CTCP XAY DUNG SO 5";
GListCode[559] = "SCD";
GListStockInfo.SCD = "CTCP NUOC GIAI KHAT C.D";
GListCode[560] = "SCJ";
GListStockInfo.SCJ = "CTCP Xi mang Sai Son";
GListCode[561] = "SCL";
GListStockInfo.SCL = "CTCP Song Da Cao Cuong";
GListCode[562] = "SCO";
GListStockInfo.SCO = "CTCP Cong nghiep Thuy san";
GListCode[563] = "SCR";
GListStockInfo.SCR = "CTCP Dia oc Sai Gon Thuong tin";
GListCode[564] = "SD1";
GListStockInfo.SD1 = "CTCP Song Da 1";
GListCode[565] = "SD2";
GListStockInfo.SD2 = "CTCP Song Da 2";
GListCode[566] = "SD4";
GListStockInfo.SD4 = "CTCP Song Da 4";
GListCode[567] = "SD5";
GListStockInfo.SD5 = "CTCP Song Da 5";
GListCode[568] = "SD6";
GListStockInfo.SD6 = "CTCP Song Da 6";
GListCode[569] = "SD7";
GListStockInfo.SD7 = "CTCP Song Da 7";
GListCode[570] = "SD9";
GListStockInfo.SD9 = "CTCP Song Da 9";
GListCode[571] = "SDA";
GListStockInfo.SDA = "CTCP SIMCO Song Da";
GListCode[572] = "SDB";
GListStockInfo.SDB = "CTCP Song Da 207";
GListCode[573] = "SDC";
GListStockInfo.SDC = "CTCP Tu van Song Da";
GListCode[574] = "SDD";
GListStockInfo.SDD = "CTCP Dau tu va Xay lap Song da";
GListCode[575] = "SDE";
GListStockInfo.SDE = "CTCP Ky thuat dien Song Da";
GListCode[576] = "SDG";
GListStockInfo.SDG = "CTCP Sadico Can Tho";
GListCode[577] = "SDH";
GListStockInfo.SDH = "CTCP Xay dung ha tang Song Da";
GListCode[578] = "SDI";
GListStockInfo.SDI = "CTCP Dau tu va Phat trien Do thi Sai Dong";
GListCode[579] = "SDK";
GListStockInfo.SDK = "CTCP Co khi luyen kim";
GListCode[580] = "SDN";
GListStockInfo.SDN = "CTCP Son Dong Nai";
GListCode[581] = "SDP";
GListStockInfo.SDP = "CTCP Dau tu va thuong mai Dau khi Song Da";
GListCode[582] = "SDT";
GListStockInfo.SDT = "CTCP Song Da 10";
GListCode[583] = "SDU";
GListStockInfo.SDU = "CTCP Dau tu xay dung va Phat trien do thi Song Da";
GListCode[584] = "SDV";
GListStockInfo.SDV = "CTCP Dich vu Sonadezi";
GListCode[585] = "SDY";
GListStockInfo.SDY = "CTCP Xi mang Song Da Yaly";
GListCode[586] = "SEB";
GListStockInfo.SEB = "CTCP Dau tu va phat trien Dien Mien trung";
GListCode[587] = "SEC";
GListStockInfo.SEC = "CTCP MIA DUONG ND GIA LAI";
GListCode[588] = "SED";
GListStockInfo.SED = "CTCP Dau tu va Phat trien Giao duc Phuong Nam";
GListCode[589] = "SEL";
GListStockInfo.SEL = "CTCP Song Da 11 - Thang Long";
GListCode[590] = "SFC";
GListStockInfo.SFC = "CTCP NHIEN LIEU SAI GON";
GListCode[591] = "SFI";
GListStockInfo.SFI = "CTCP DAI LY VAN TAI SAFI";
GListCode[592] = "SFN";
GListStockInfo.SFN = "CTCP Det luoi Sai Gon";
GListCode[593] = "SGC";
GListStockInfo.SGC = "CTCP XNK Sa Giang";
GListCode[594] = "SGD";
GListStockInfo.SGD = "CTCP Sach giao duc tai Tp. HCM";
GListCode[595] = "SGH";
GListStockInfo.SGH = "CTCP Khach san Sai Gon";
GListCode[596] = "SGS";
GListStockInfo.SGS = "CTCP Van tai bien Sai Gon";
GListCode[597] = "SGT";
GListStockInfo.SGT = "CTCP CN VIEN THONG SAIGON";
GListCode[598] = "SHA";
GListStockInfo.SHA = "CTCP Son Ha Sai Gon";
GListCode[599] = "SHB";
GListStockInfo.SHB = "NHTM Co phan Sai Gon - Ha Noi";
GListCode[600] = "SHI";
GListStockInfo.SHI = "CTCP QUOC TE SON HA";
GListCode[601] = "SHN";
GListStockInfo.SHN = "CTCP Dau tu Tong hop Ha Noi";
GListCode[602] = "SHP";
GListStockInfo.SHP = "CTCP Thuy dien mien Nam";
GListCode[603] = "SHS";
GListStockInfo.SHS = "CTCP Chung khoan Sai Gon Ha Noi";
GListCode[604] = "SHV";
GListStockInfo.SHV = "CTCP Hai Viet";
GListCode[605] = "SIC";
GListStockInfo.SIC = "CTCP Dau tu - Phat trien Song Da";
GListCode[606] = "SII";
GListStockInfo.SII = "CTCP DT HA TANG BDS SG";
GListCode[607] = "SJ1";
GListStockInfo.SJ1 = "CTCP Thuy san so 1";
GListCode[608] = "SJC";
GListStockInfo.SJC = "CTCP Song Da 1.01";
GListCode[609] = "SJD";
GListStockInfo.SJD = "CTCP THUY DIEN CAN DON";
GListCode[610] = "SJE";
GListStockInfo.SJE = "CTCP Song Da 11";
GListCode[611] = "SJM";
GListStockInfo.SJM = "CTCP Song Da 19";
GListCode[612] = "SJS";
GListStockInfo.SJS = "CTCP DTPT DT&KCN SONG DA";
GListCode[613] = "SKS";
GListStockInfo.SKS = "CTCP Cong trinh giao thong song Da";
GListCode[614] = "SLS";
GListStockInfo.SLS = "CTCP Mia duong Son La";
GListCode[615] = "SMA";
GListStockInfo.SMA = "CTCP TB-PT SAI GON";
GListCode[616] = "SMB";
GListStockInfo.SMB = "CTCP Bia Sai Gon - Mien Trung";
GListCode[617] = "SMC";
GListStockInfo.SMC = "CTCP DAU TU THUONGMAI SMC";
GListCode[618] = "SMT";
GListStockInfo.SMT = "CTCP Vat lieu dien va Vien thong Sam Cuong";
GListCode[619] = "SNG";
GListStockInfo.SNG = "CTCP Song Da 10.1";
GListCode[620] = "SPC";
GListStockInfo.SPC = "CTCP Bao ve Thuc vat Sai Gon";
GListCode[621] = "SPD";
GListStockInfo.SPD = "CTCP Xuat nhap khau Thuy san mien Trung";
GListCode[622] = "SPI";
GListStockInfo.SPI = "CTCP Da Spilit";
GListCode[623] = "SPM";
GListStockInfo.SPM = "CTCP S.P.M";
GListCode[624] = "SPP";
GListStockInfo.SPP = "CTCP Bao bi Nhua Sai Gon";
GListCode[625] = "SQC";
GListStockInfo.SQC = "CTCP Khoang san Sai Gon - Quy Nhon";
GListCode[626] = "SRA";
GListStockInfo.SRA = "CTCP SARA Viet Nam";
GListCode[627] = "SRB";
GListStockInfo.SRB = "CTCP Tap doan Sara";
GListCode[628] = "SRC";
GListStockInfo.SRC = "CTCP CAO SU SAO VANG";
GListCode[629] = "SRF";
GListStockInfo.SRF = "CTCP KY NGHE LANH";
GListCode[630] = "SSC";
GListStockInfo.SSC = "CTY CP GIONG CAY TRONG MN";
GListCode[631] = "SSF";
GListStockInfo.SSF = "CTCP Giay Sai Gon";
GListCode[632] = "SSG";
GListStockInfo.SSG = "CTCP Van tai bien Hai Au";
GListCode[633] = "SSI";
GListStockInfo.SSI = "CT CP CHUNG KHOAN SAI GON";
GListCode[634] = "SSM";
GListStockInfo.SSM = "CTCP Che tao Ket cau thep VNECO.SSM";
GListCode[635] = "ST8";
GListStockInfo.ST8 = "CT CP SIEU THANH";
GListCode[636] = "STB";
GListStockInfo.STB = "NH TMCP SG THUONG TIN";
GListCode[637] = "STC";
GListStockInfo.STC = "CTCP Sach va Thiet bi Truong hoc Tp";
GListCode[638] = "STG";
GListStockInfo.STG = "CTCP KHO VAN MIEN NAM";
GListCode[639] = "STP";
GListStockInfo.STP = "CTCP Cong nghiep Thuong mai Song Da";
GListCode[640] = "STS";
GListStockInfo.STS = "CTCP Dich Vu Van Tai Sai Gon";
GListCode[641] = "STT";
GListStockInfo.STT = "CTCP VC SAI GON TOURIST";
GListCode[642] = "STU";
GListStockInfo.STU = "CTCP Moi truong Do thi Son Tay";
GListCode[643] = "STV";
GListStockInfo.STV = "CTCP Che tac da Viet Nam";
GListCode[644] = "SVC";
GListStockInfo.SVC = "CTCP DV TONG HOP SAI SON";
GListCode[645] = "SVI";
GListStockInfo.SVI = "CTCP BAO BI BIEN HOA";
GListCode[646] = "SVN";
GListStockInfo.SVN = "CTCP SOLAVINA";
GListCode[647] = "SVT";
GListStockInfo.SVT = "CTCP CN SG VIEN DONG";
GListCode[648] = "SWC";
GListStockInfo.SWC = "CTCP Duong song Mien Nam";
GListCode[649] = "SZL";
GListStockInfo.SZL = "CTCP SONADEZI LONG THANH";
GListCode[650] = "TAC";
GListStockInfo.TAC = "CTCP DAU THUC VAT TUONGAN";
GListCode[651] = "TAG";
GListStockInfo.TAG = "CTCP The gioi so Tran Anh";
GListCode[652] = "TAS";
GListStockInfo.TAS = "CTCP Chung khoan Trang An";
GListCode[653] = "TBC";
GListStockInfo.TBC = "CTCP THUY DIEN THAC BA";
GListCode[654] = "TBT";
GListStockInfo.TBT = "CTCP Xay Dung Cong Trinh Giao Thong Ben Tre";
GListCode[655] = "TBX";
GListStockInfo.TBX = "CTCP Xi mang Thai Binh";
GListCode[656] = "TC6";
GListStockInfo.TC6 = "CTCP Than Coc Sau - Vinacomin";
GListCode[657] = "TCL";
GListStockInfo.TCL = "CTCP DLGNVT XEPDO TANCANG";
GListCode[658] = "TCM";
GListStockInfo.TCM = "CTCP DET MAY THANH CONG";
GListCode[659] = "TCO";
GListStockInfo.TCO = "CTCP VT DPT DUYEN HAI";
GListCode[660] = "TCR";
GListStockInfo.TCR = "CTCP CN GOM SU TAICERA";
GListCode[661] = "TCS";
GListStockInfo.TCS = "CTCP Than Cao Son - Vinacomin";
GListCode[662] = "TCT";
GListStockInfo.TCT = "CTCP Cap treo Nui Ba Tay Ninh";
GListCode[663] = "TDC";
GListStockInfo.TDC = "CTCP KD&PT BINH DUONG";
GListCode[664] = "TDH";
GListStockInfo.TDH = "CTCP PT NHA THU DUC";
GListCode[665] = "TDN";
GListStockInfo.TDN = "CTCP Than Deo Nai - Vinacomin";
GListCode[666] = "TDS";
GListStockInfo.TDS = "CTCP Thep Thu Duc";
GListCode[667] = "TDW";
GListStockInfo.TDW = "CTCP CAP NUOC THU DUC";
GListCode[668] = "TET";
GListStockInfo.TET = "CTCP Vai soi may mac mien Bac";
GListCode[669] = "TGP";
GListStockInfo.TGP = "CTCP Truong Phu";
GListCode[670] = "TH1";
GListStockInfo.TH1 = "CTCP Xuat nhap khau Tong hop I Viet Nam";
GListCode[671] = "THB";
GListStockInfo.THB = "CTCP Bia Thanh Hoa";
GListCode[672] = "THG";
GListStockInfo.THG = "CTCP DT & XD TIEN GIANG";
GListCode[673] = "THT";
GListStockInfo.THT = "CTCP Than Ha Tu - Vinacomin";
GListCode[674] = "TIC";
GListStockInfo.TIC = "CTCP DT DIEN TAY NGUYEN";
GListCode[675] = "TIE";
GListStockInfo.TIE = "CTCP TIE";
GListCode[676] = "TIG";
GListStockInfo.TIG = "CTCP Tap doan Dau tu Thang Long";
GListCode[677] = "TIS";
GListStockInfo.TIS = "CTCP Gang thep Thai Nguyen";
GListCode[678] = "TIX";
GListStockInfo.TIX = "CTCP SXKD XNKDVDT TANBINH";
GListCode[679] = "TJC";
GListStockInfo.TJC = "CTCP Dich vu Van tai va Thuong mai";
GListCode[680] = "TKC";
GListStockInfo.TKC = "CTCP Xay Dung va Kinh doanh Dia Oc Tan Ky";
GListCode[681] = "TKU";
GListStockInfo.TKU = "CTCP Cong nghiep Tungkuang";
GListCode[682] = "TLG";
GListStockInfo.TLG = "CTCP TAP DOAN THIEN LONG";
GListCode[683] = "TLH";
GListStockInfo.TLH = "CTCP TD THEP TIEN LEN";
GListCode[684] = "TLT";
GListStockInfo.TLT = "CTCP Viglacera Thang Long";
GListCode[685] = "TMC";
GListStockInfo.TMC = "CTCP Thuong mai Xuat nhap khau Thu Duc";
GListCode[686] = "TMP";
GListStockInfo.TMP = "CTCP THUY DIEN THAC MO";
GListCode[687] = "TMS";
GListStockInfo.TMS = "CTY CP KHO VAN GIAO NHAN";
GListCode[688] = "TMT";
GListStockInfo.TMT = "CTCP O TO TMT";
GListCode[689] = "TMW";
GListStockInfo.TMW = "CTCP Tong hop Go Tan Mai";
GListCode[690] = "TMX";
GListStockInfo.TMX = "CTCP VICEM Thuong mai Xi mang";
GListCode[691] = "TNA";
GListStockInfo.TNA = "CTCP TM XNK THIEN NAM";
GListCode[692] = "TNB";
GListStockInfo.TNB = "CTCP Thep Nha Be";
GListCode[693] = "TNC";
GListStockInfo.TNC = "CTCP CAO SU THONG NHAT";
GListCode[694] = "TNG";
GListStockInfo.TNG = "CTCP Dau tu va Thuong mai TNG";
GListCode[695] = "TNM";
GListStockInfo.TNM = "CTCP Xuat nhap khau va Xay dung cong trinh";
GListCode[696] = "TNT";
GListStockInfo.TNT = "CTCP TAI NGUYEN";
GListCode[697] = "TNY";
GListStockInfo.TNY = "CTCP Dau tu Xay dung Thanh nien";
GListCode[698] = "TPC";
GListStockInfo.TPC = "CTCP NHUA TAN DAI HUNG";
GListCode[699] = "TPH";
GListStockInfo.TPH = "CTCP In Sach giao khoa tai Tp. Ha Noi";
GListCode[700] = "TPP";
GListStockInfo.TPP = "CTCP Nhua Tan Phu";
GListCode[701] = "TRA";
GListStockInfo.TRA = "CONG TY CP TRAPHACO";
GListCode[702] = "TRC";
GListStockInfo.TRC = "CTCP CAO SU TAY NINH";
GListCode[703] = "TS4";
GListStockInfo.TS4 = "CTY CP THUY SAN SO 4";
GListCode[704] = "TSB";
GListStockInfo.TSB = "CTCP Ac quy Tia Sang";
GListCode[705] = "TSC";
GListStockInfo.TSC = "CTCP VT KT NN CAN THO";
GListCode[706] = "TSM";
GListStockInfo.TSM = "CTCP Xi mang Tien Son Ha Tay";
GListCode[707] = "TST";
GListStockInfo.TST = "CTCP Dich vu Ky thuat Vien thong";
GListCode[708] = "TTC";
GListStockInfo.TTC = "CTCP Gach men Thanh Thanh";
GListCode[709] = "TTF";
GListStockInfo.TTF = "CTCP TDKN GO TRUONG THANH";
GListCode[710] = "TTG";
GListStockInfo.TTG = "CTCP May Thanh Tri";
GListCode[711] = "TTP";
GListStockInfo.TTP = "CTCP BAO BI NHUA TAN TIEN";
GListCode[712] = "TTR";
GListStockInfo.TTR = "CTCP Du lich thuong mai va dau tu";
GListCode[713] = "TTZ";
GListStockInfo.TTZ = "CTCP Dau tu Xay dung va Cong nghe Tien Trung";
GListCode[714] = "TV1";
GListStockInfo.TV1 = "CTCP TV & XD DIEN 1";
GListCode[715] = "TV2";
GListStockInfo.TV2 = "CTCP Tu van Xay dung dien 2";
GListCode[716] = "TV3";
GListStockInfo.TV3 = "CTCP Tu van Xay dung dien 3";
GListCode[717] = "TV4";
GListStockInfo.TV4 = "CTCP Tu van Xay dung dien 4";
GListCode[718] = "TVD";
GListStockInfo.TVD = "CTCP Than Vang Danh - Vinacomin";
GListCode[719] = "TVG";
GListStockInfo.TVG = "CTCP Tu van Dau tu va Xay dung Giao thong van tai";
GListCode[720] = "TXM";
GListStockInfo.TXM = "CTCP VICEM Thach cao Xi mang";
GListCode[721] = "TYA";
GListStockInfo.TYA = "CTCP DAY&CAP DIEN TAYA VN";
GListCode[722] = "UDC";
GListStockInfo.UDC = "CTCP XD&PT DT TINH BRVT";
GListCode[723] = "UDJ";
GListStockInfo.UDJ = "CTCP Phat trien Do Thi";
GListCode[724] = "UIC";
GListStockInfo.UIC = "CTCP DTPT NHA&DT IDICO";
GListCode[725] = "UNI";
GListStockInfo.UNI = "CTCP Vien Lien";
GListCode[726] = "V11";
GListStockInfo.V11 = "CTCP Xay dung so 11";
GListCode[727] = "V12";
GListStockInfo.V12 = "CTCP Xay dung so 12";
GListCode[728] = "V15";
GListStockInfo.V15 = "CTCP Xay dung so 15";
GListCode[729] = "V21";
GListStockInfo.V21 = "CTCP VINACONEX 21";
GListCode[730] = "VAT";
GListStockInfo.VAT = "CTCP Vien thong Van Xuan";
GListCode[731] = "VBC";
GListStockInfo.VBC = "CTCP Nhua Bao bi Vinh";
GListCode[732] = "VBH";
GListStockInfo.VBH = "CTCP Dien tu Binh Hoa";
GListCode[733] = "VC1";
GListStockInfo.VC1 = "CTCP Xay dung so 1";
GListCode[734] = "VC2";
GListStockInfo.VC2 = "CTCP Xay dung so 2";
GListCode[735] = "VC3";
GListStockInfo.VC3 = "CTCP Xay dung so 3";
GListCode[736] = "VC5";
GListStockInfo.VC5 = "CTCP Xay dung so 5";
GListCode[737] = "VC6";
GListStockInfo.VC6 = "CTCP Vinaconex 6";
GListCode[738] = "VC7";
GListStockInfo.VC7 = "CTCP Xay dung so 7";
GListCode[739] = "VC9";
GListStockInfo.VC9 = "CTCP Xay dung so 9";
GListCode[740] = "VCA";
GListStockInfo.VCA = "CTCP Thep Bien Hoa";
GListCode[741] = "VCB";
GListStockInfo.VCB = "NH TMCP NGOAI THUONG VN";
GListCode[742] = "VCC";
GListStockInfo.VCC = "CTCP Vinaconex 25";
GListCode[743] = "VCF";
GListStockInfo.VCF = "CTCP VINACAFE BIEN HOA";
GListCode[744] = "VCG";
GListStockInfo.VCG = "Tong CTCP XNK va Xay dung Viet Nam";
GListCode[745] = "VCM";
GListStockInfo.VCM = "CTCP Nhan luc va Thuong mai Vinaconex";
GListCode[746] = "VCR";
GListStockInfo.VCR = "CTCP Dau tu & Phat trien Du lich Vinaconex";
GListCode[747] = "VCS";
GListStockInfo.VCS = "CTCP Da op lat cao cap Vinaconex";
GListCode[748] = "VCT";
GListStockInfo.VCT = "CTCP Tu van xay dung Vinaconex";
GListCode[749] = "VCV";
GListStockInfo.VCV = "CTCP Van tai Vinaconex";
GListCode[750] = "VDL";
GListStockInfo.VDL = "CTCP Thuc pham Lam Dong";
GListCode[751] = "VDN";
GListStockInfo.VDN = "CTCP Vinatex Da Nang";
GListCode[752] = "VDS";
GListStockInfo.VDS = "CTCP Chung khoan Rong Viet";
GListCode[753] = "VDT";
GListStockInfo.VDT = "CTCP Luoi thep Binh Tay";
GListCode[754] = "VE1";
GListStockInfo.VE1 = "CTCP Xay dung dien VNECO 1";
GListCode[755] = "VE2";
GListStockInfo.VE2 = "CTCP Xay dung dien VNECO 2";
GListCode[756] = "VE3";
GListStockInfo.VE3 = "CTCP Xay dung dien VNECO3";
GListCode[757] = "VE4";
GListStockInfo.VE4 = "CTCP Xay dung Dien Vneco 4";
GListCode[758] = "VE8";
GListStockInfo.VE8 = "CTCP Xay dung Dien Vneco 8";
GListCode[759] = "VE9";
GListStockInfo.VE9 = "CTCP Dau tu va Xay dung VNECO 9";
GListCode[760] = "VFC";
GListStockInfo.VFC = "CTCP Vinafco";
GListCode[761] = "VFG";
GListStockInfo.VFG = "CTCP KHU TRUNG VIET NAM";
GListCode[762] = "VFMVF1";
GListStockInfo.VFMVF1 = "QUY DAU TU CK VN VF1";
GListCode[763] = "VFMVF4";
GListStockInfo.VFMVF4 = "QUY DT DN HANG DAU VN";
GListCode[764] = "VFR";
GListStockInfo.VFR = "CTCP Van tai va Thue tau";
GListCode[765] = "VGP";
GListStockInfo.VGP = "CTCP Cang Rau Qua";
GListCode[766] = "VGS";
GListStockInfo.VGS = "CTCP Ong thep Viet Duc VGPIPE";
GListCode[767] = "VHC";
GListStockInfo.VHC = "CTCP VINH HOAN";
GListCode[768] = "VHF";
GListStockInfo.VHF = "CTCP Xay dung va che bien luong thuc Vinh Ha";
GListCode[769] = "VHG";
GListStockInfo.VHG = "CTCP DAU TU&SX VIET HAN";
GListCode[770] = "VHH";
GListStockInfo.VHH = "CTCP Dau tu Xay dung Viwaseen - Hue";
GListCode[771] = "VHL";
GListStockInfo.VHL = "CTCP Viglacera Ha Long";
GListCode[772] = "VIC";
GListStockInfo.VIC = "CTCP VINCOM";
GListCode[773] = "VID";
GListStockInfo.VID = "CTCP GIAY VIEN DONG";
GListCode[774] = "VIE";
GListStockInfo.VIE = "CTCP Cong nghe Vien thong Viteco";
GListCode[775] = "VIG";
GListStockInfo.VIG = "CTCP Chung khoan Thuong mai va Cong nghiep Viet Nam";
GListCode[776] = "VIP";
GListStockInfo.VIP = "CTCP V.TAI X.DAU VIPCO";
GListCode[777] = "VIR";
GListStockInfo.VIR = "CTCP Du lich quoc te Vung Tau";
GListCode[778] = "VIS";
GListStockInfo.VIS = "CTCP THEP VIET Y";
GListCode[779] = "VIT";
GListStockInfo.VIT = "CTCP Viglacera Tien Son";
GListCode[780] = "VIX";
GListStockInfo.VIX = "CTCP Chung khoan Xuan Thanh";
GListCode[781] = "VKC";
GListStockInfo.VKC = "CTCP Cap nhua Vinh Khanh";
GListCode[782] = "VKD";
GListStockInfo.VKD = "CTCP Nuoc khoang Khanh Hoa";
GListCode[783] = "VLA";
GListStockInfo.VLA = "CTCP Dau tu va phat trien cong nghe Van Lang";
GListCode[784] = "VLF";
GListStockInfo.VLF = "CTCP LT-TP VINH LONG";
GListCode[785] = "VMC";
GListStockInfo.VMC = "CTCP Vimeco";
GListCode[786] = "VMD";
GListStockInfo.VMD = "CTCP Y DP VIMEDIMEX";
GListCode[787] = "VNA";
GListStockInfo.VNA = "CTCP VT BIEN VINASHIP";
GListCode[788] = "VNC";
GListStockInfo.VNC = "CTCP Tap doan Vinacontrol";
GListCode[789] = "VND";
GListStockInfo.VND = "CTCP Chung khoan VNDIRECT";
GListCode[790] = "VNE";
GListStockInfo.VNE = "CT CP XD DIEN VIET NAM";
GListCode[791] = "VNF";
GListStockInfo.VNF = "CTCP Van tai Ngoai thuong";
GListCode[792] = "VNG";
GListStockInfo.VNG = "CTCP DU LICH GOLF VN";
GListCode[793] = "VNH";
GListStockInfo.VNH = "CT THUY HAI SAN VIET NHAT";
GListCode[794] = "VNI";
GListStockInfo.VNI = "CTCP DT BDS VIEN NAM";
GListCode[795] = "VNL";
GListStockInfo.VNL = "CTCP GN VT TM VINALINK";
GListCode[796] = "VNM";
GListStockInfo.VNM = "CTCP SUA VIET NAM";
GListCode[797] = "VNN";
GListStockInfo.VNN = "CTCP Dau tu Vietnamnet";
GListCode[798] = "VNR";
GListStockInfo.VNR = "Tong CTCP Tai Bao hiem Quoc gia Viet Nam";
GListCode[799] = "VNS";
GListStockInfo.VNS = "CTY CP ANH DUONG VIET NAM";
GListCode[800] = "VNT";
GListStockInfo.VNT = "CTCP Giao nhan Van tai Ngoai thuong";
GListCode[801] = "VNX";
GListStockInfo.VNX = "CTCP Quang cao va Hoi cho Thuong mai - Vinexad";
GListCode[802] = "VOS";
GListStockInfo.VOS = "CTCP VAN TAI BIEN VN";
GListCode[803] = "VPC";
GListStockInfo.VPC = "CTCP Dau tu va Phat trien Nang luong Viet Nam";
GListCode[804] = "VPH";
GListStockInfo.VPH = "CTCP VAN PHAT HUNG";
GListCode[805] = "VPK";
GListStockInfo.VPK = "CTCP BAO BI DAU THUC VAT";
GListCode[806] = "VQC";
GListStockInfo.VQC = "CTCP Giam dinh Vinacomin";
GListCode[807] = "VRC";
GListStockInfo.VRC = "CTCP XL & DIA OC VUNG TAU";
GListCode[808] = "VSC";
GListStockInfo.VSC = "CTCP CONTAINER VIET NAM";
GListCode[809] = "VSG";
GListStockInfo.VSG = "CTCP Container Phia Nam";
GListCode[810] = "VSH";
GListStockInfo.VSH = "CTCP THUY DIEN VS-SH";
GListCode[811] = "VSI";
GListStockInfo.VSI = "CTCP DT&XD CAP THOAT NUOC";
GListCode[812] = "VSP";
GListStockInfo.VSP = "CTCP Van tai Bien va Bat dong san Viet Hai";
GListCode[813] = "VST";
GListStockInfo.VST = "CTCP VT& THUE TAU BIEN VN";
GListCode[814] = "VT1";
GListStockInfo.VT1 = "CTCP Vat tu Ben Thanh";
GListCode[815] = "VTA";
GListStockInfo.VTA = "CTCP Vitaly";
GListCode[816] = "VTB";
GListStockInfo.VTB = "CTCP DIEN TU TAN BINH";
GListCode[817] = "VTC";
GListStockInfo.VTC = "CTCP Vien thong VTC";
GListCode[818] = "VTF";
GListStockInfo.VTF = "CTCP THUY SAN VIET THANG";
GListCode[819] = "VTI";
GListStockInfo.VTI = "Cong ty Co phan SX - XNK Det May";
GListCode[820] = "VTL";
GListStockInfo.VTL = "CTCP Vang Thang Long";
GListCode[821] = "VTO";
GListStockInfo.VTO = "CTCP VT XANG DAU VITACO";
GListCode[822] = "VTS";
GListStockInfo.VTS = "CTCP Viglacera Tu Son";
GListCode[823] = "VTV";
GListStockInfo.VTV = "CTCP VICEM Vat tu Van tai Xi mang";
GListCode[824] = "VXB";
GListStockInfo.VXB = "CTCP Vat lieu xay dung Ben tre";
GListCode[825] = "WCS";
GListStockInfo.WCS = "CTCP Ben xe Mien Tay";
GListCode[826] = "WSB";
GListStockInfo.WSB = "CTCP Bia Sai Gon - Mien Tay";
GListCode[827] = "WSS";
GListStockInfo.WSS = "CTCP Chung khoan Pho Wall";
GListCode[828] = "WTC";
GListStockInfo.WTC = "CTCP Van tai thuy - Vinacomin";
GListCode[829] = "XMC";
GListStockInfo.XMC = "CTCP Be tong va xay dung Vinaconex Xuan Mai";
GListCode[830] = "YBC";
GListStockInfo.YBC = "CTCP Xi mang va Khoang san Yen Bai";
function getURLParameters(h) {
	var g = $(location).attr("href");
	if (g.indexOf("?") > 0) {
		var c = g.split("?");
		var f = c[1].split("&");
		var a = new Array(f.length);
		var d = new Array(f.length);
		var e = 0;
		for (e = 0; e < f.length; e++) {
			var b = f[e].split("=");
			a[e] = b[0];
			if (b[1] != "") {
				d[e] = unescape(b[1])
			} else {
				d[e] = "No Value"
			}
		}
		for (e = 0; e < f.length; e++) {
			if (a[e] == h) {
				return d[e]
			}
		}
		return ""
	}
}
function checkDevice() {
	var a = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i
			.test(navigator.userAgent.toLowerCase()));
	if (a) {
		checkSmartPhone = 1
	}
}
function setCookie(b, c, a) {
	var d = new Date();
	if (typeof a == "undefined") {
		a = 120
	}
	d.setDate(d.getDate() + a);
	var e = escape(c) + ";expires=" + d.toUTCString() + ";path=/";
	document.cookie = b + "=" + e
}
function getCookie(b) {
	var c, a, e, d = document.cookie.split(";");
	for (c = 0; c < d.length; c++) {
		a = d[c].substr(0, d[c].indexOf("="));
		e = d[c].substr(d[c].indexOf("=") + 1);
		a = a.replace(/^\s+|\s+$/g, "");
		if (a == b) {
			return unescape(e)
		}
	}
	return ""
}
function FormatVolume10(b) {
	var a = StringToInt(b) * 10;
	var c = FormatCurrency(a.toString(), ",", ".");
	return c.substring(0, c.length - 1)
}
function FormatPrice(a) {
	if (typeof a == "undefined") {
		return ""
	}
	if (a in [ "0", "0.0", "0.00", "", null ]) {
		return ""
	}
	return Highcharts.numberFormat(a, "1", ".")
}
function FormatCurrency(d, a, f) {
	var b, c;
	d = d.toString().replace(/\$|\,/g, "");
	if (isNaN(d)) {
		d = "0"
	}
	b = (d == (d = Math.abs(d)));
	var h = d.toString();
	var g = h.split(f);
	if (g.length > 1) {
		var c = new String(g[1]);
		if (c.length < 2) {
			c = c + "0"
		}
	} else {
		c = ""
	}
	d = g[0];
	for (var e = 0; e < Math.floor((d.length - (1 + e)) / 3); e++) {
		d = d.substring(0, d.length - (4 * e + 3)) + a
				+ d.substring(d.length - (4 * e + 3))
	}
	if (c == "") {
		ret_value = (((b) ? "" : "-") + d)
	} else {
		ret_value = (((b) ? "" : "-") + d + f + c)
	}
	return ret_value
}
function FormatTime(a) {
	if (typeof a == "undefined" || a.length == 0) {
		return ""
	}
	return [ a.slice(0, 2), ":", a.slice(2, 4), ":", a.slice(4) ].join("")
}
function getStatus(b, a) {
	var c = "";
	if (b == "null") {
		c = "ÄÃ³ng cá»­a"
	} else {
		if (b == "O") {
			if ((a == CONST_HSX_NAME) || (a == CONST_VN30_NAME)
					|| (a == CONST_HNX_NAME) || (a == CONST_UPC_NAME)) {
				c = "PhiÃªn LiÃªn tá»¥c"
			} else {
				c = "Má»Ÿ cá»­a"
			}
		} else {
			if (b == "P") {
				if ((a == CONST_HSX_NAME) || (a == CONST_VN30_NAME)) {
					c = "PhiÃªn ATO"
				} else {
					c = "ÄÃ³ng cá»­a"
				}
			} else {
				if (b == "A") {
					c = "PhiÃªn ATC"
				} else {
					if (b == "C") {
						c = "PhiÃªn GDTT"
					} else {
						if (b == "I" || b == "BSU") {
							c = "Nghá»‰ trÆ°a"
						} else {
							c = "ÄÃ³ng cá»­a"
						}
					}
				}
			}
		}
	}
	return c
}
function StringToInt(b) {
	var a = parseInt(b, 10);
	if (isNaN(a)) {
		return 0
	} else {
		return a
	}
}
function getClass(g, e, d, b) {
	var a;
	if (b == "0.0") {
		a = "txt-gia-tc"
	} else {
		if (b == g) {
			a = "txt-magenta"
		} else {
			if (b > d) {
				a = "txt-lime"
			} else {
				if (b == d) {
					a = "txt-gia-tc"
				} else {
					if (b == e) {
						a = "txt-gia-san"
					} else {
						a = "txt-red"
					}
				}
			}
		}
	}
	return a
}
function getClassForStock(b, a) {
	var c = System.stockInfoComponent.getStockInfo(b);
	if (!c) {
		return "txt-gia-tc"
	}
	return getClass(c.ceilingPrice, c.floorPrice, c.basicPrice, a)
}
function setValue(d, c) {
	var a = $(d);
	if (a) {
		var b = a.html();
		if (b != c) {
			a.html(c)
		}
	}
}
function setCell(g, e, b) {
	var a = $(g);
	var d = 0;
	if (a) {
		var c = a.html();
		if (c != e) {
			if ((e == "0") || (e == "0.0") || (e == "0.00")) {
				if ((c == "") || (c == "&nbsp;")) {
					d = 0
				} else {
					if (a.html.length > 0) {
						a.html("");
						d = 1
					}
				}
			} else {
				a.html(e);
				d = 1
			}
			removeAllClass(a);
			a.attr("class", b);
			if (d == 1) {
				var f = getColor(b);
				a.css({
					color : f
				});
				return a
			}
		} else {
			var h = g.indexOf("lat");
			if (h > 0) {
				var f = getColor(b);
				a.css({
					color : f
				});
				return a
			}
		}
	}
	return null
}
function processPrice(a, b, c) {
	if (b == "ATO" || b == "ATC") {
		return setCell(a, b, "w")
	} else {
		return setCell(a, Highcharts.numberFormat(b, "1"), c)
	}
}
function processVolume(a, b, c) {
	return setCell(a, FormatVolume10(b, 0), c)
}
function processPriceVolume(c, b) {
	if (c != null) {
		var a = c.split("|");
		processPrice("#" + b + "bP1", a[0], a[2]);
		processVolume("#" + b + "bV1", a[1], a[2])
	}
}
function getValueFix(b, c) {
	var a = "";
	if (c == "0") {
		a = "&nbsp;"
	} else {
		if (c == "0.0") {
			a = "&nbsp;"
		} else {
			if (b == 1) {
				a = c
			} else {
				a = FormatVolume10(c, 0)
			}
		}
	}
	return a
}
function removeAllClass(b) {
	try {
		var a = b;
		if (a != null) {
			a.removeClass("i");
			a.removeClass("d");
			a.removeClass("c ");
			a.removeClass("f");
			a.removeClass("e")
		}
	} catch (c) {
		console.log("error: removeAllClass: ");
		console.log(c)
	}
}
function setColor(c, b) {
	var a = $(c);
	removeAllClass(a);
	a.addClass(b)
}
function getColor(a) {
	if (a == "i") {
		return "#03ff04"
	} else {
		if (a == "d") {
			return "#ff0000"
		} else {
			if (a == "c") {
				return "#ff00fe"
			} else {
				if (a == "f") {
					return "#00FFFF"
				} else {
					if (a == "e") {
						return "#ffff00"
					} else {
						if (a == "w") {
							return "#ffffff"
						} else {
							return ""
						}
					}
				}
			}
		}
	}
}
function highlightCells(a) {
	for (var b = 0; b < a.length; b++) {
		if (a[b] != null) {
			a[b].addClass("hl2")
		}
	}
	setTimeout(function() {
		for (var c = 0; c < a.length; c++) {
			if (a[c] != null) {
				a[c].removeClass("hl2")
			}
		}
	}, 1000)
}
function highlightCell(a) {
	if (a != null) {
		a.addClass("hl2")
	}
	setTimeout(function() {
		if (a != null) {
			a.removeClass("hl2")
		}
	}, 1000)
}
Schedule = {
	schedule : function(a, c) {
		var b = new Date();
		var d = a.getTime() - b.getTime();
		if (d < 0) {
			return false
		}
		setTimeout(callback, d);
		return true
	},
	createJob : function(f, h) {
		var a = typeof f.hour == "undefined" ? 0 : f.hour;
		var g = typeof f.minute == "undefined" ? 0 : f.minute;
		var b = typeof f.second == "undefined" ? 0 : f.second;
		var c = new Date();
		var e = new Date();
		e.setHours(a, g, b, 0);
		var d = e.getTime() - c.getTime();
		if (d < 0) {
			d += 24 * 3600 * 1000
		}
		setTimeout(function(i) {
			h();
			Schedule.createJob(i, h)
		}.bind(this, f), d)
	}
};
NumberFormat = {
	price : function(c, a, b) {
		if (!c || c == "0" || c == "0.0") {
			if (typeof b != "undefined" && a && a != 0) {
				if (System.marketInfosManager.isATO(b)) {
					return "ATO"
				}
				if (System.marketInfosManager.isATC(b)) {
					return "ATC"
				}
			}
			return ""
		}
		return Highcharts.numberFormat(c, "1")
	},
	vol : function(b, a) {
		a = a ? a : true;
		if (!b) {
			return ""
		}
		var c = Highcharts.numberFormat(10 * b, 0);
		if (!a) {
			return c
		}
		return c.substring(0, c.length - 1)
	},
	baseVol : function(a) {
		return Highcharts.numberFormat(a, 0)
	},
	currency : function(a) {
		return Highcharts.numberFormat(a, 3)
	},
	cp : function(a) {
		return Highcharts.numberFormat(10 * a, 0)
	},
	index : function(a) {
		return Highcharts.numberFormat(a, 2)
	},
	milionVol : function(b, a) {
		if (!b) {
			return ""
		}
		var c = 10 * b;
		if (c < 1000000) {
			return this.vol(b, a)
		} else {
			c = c / 1000000;
			return Highcharts.numberFormat(c, 2) + "tr"
		}
	}
};
function cloneObject(a) {
	var b = JSON.stringify(a);
	if (b != "" && typeof b != "undefined") {
		return JSON.parse(b)
	}
}
function syncWidthOfFixHeader(g, f) {
	var a;
	var d;
	if (Browser.isChrome()) {
		a = $("#" + f + " thead th");
		d = $("#" + g + " thead th");
		if (a.length != d.length) {
			console.log("ERR: Headers has different structure");
			return
		}
		for (var c = 0; c < a.length; c++) {
			var b = Math.round($(a[c]).width() + 1);
			$(d[c]).width(b);
			$(d[c]).css({
				"min-width" : b
			})
		}
	} else {
		if (Browser.isIE()) {
			a = $("#" + f + " th:not([colspan])");
			d = $("#" + g + " th:not([colspan])");
			if (a.length != d.length) {
				console.log("ERR: Headers has different structure");
				return
			}
			for (var c = 0; c < a.length; c++) {
				var b = Math.round($(a[c]).width() + 1);
				$(a[c]).width(b);
				$(d[c]).width(b);
				$(d[c]).css({
					"min-width" : b
				})
			}
		} else {
			a = $("#" + f + " col");
			d = $("#" + g + " col");
			if (a.length != d.length) {
				console.log("ERR: Headers has different structure");
				return
			}
			for (var c = 0; c < a.length; c++) {
				var e = Math.round($(a[c]).innerWidth()) + "px";
				var b = Math.round($(a[c]).innerWidth()) + "px";
				$(a[c]).innerWidth(b);
				$(d[c]).innerWidth(b);
				$(d[c]).css({
					"min-width" : e
				})
			}
		}
	}
}
function syncWidth(d, c) {
	if (Browser.isChrome()) {
		var a = Math.round(c.width() + 1);
		d.width(a);
		d.css({
			"min-width" : a
		})
	} else {
		if (Browser.isIE()) {
			var a = Math.round(c.width() + 1);
			c.width(a);
			d.width(a);
			d.css({
				"min-width" : a
			})
		} else {
			var b = Math.round(c.innerWidth());
			var a = Math.round(c.innerWidth());
			c.innerWidth(a);
			d.innerWidth(a);
			d.css({
				"min-width" : b
			})
		}
	}
}
Browser = {
	detect : function() {
		this.detecter = {};
		var a = navigator.userAgent;
		this.detecter.isChrome = /Chrome/.test(a)
				&& /Google Inc/.test(navigator.vendor);
		this.detecter.isSafari = /Safari/.test(a)
				&& /Apple Computer/.test(navigator.vendor);
		this.detecter.isIpad = /iPad/i.test(a) || /iPhone OS 3_1_2/i.test(a)
				|| /iPhone OS 3_2_2/i.test(a);
		this.detecter.isTablet = /Android|webOS|iPhone|iPad|iPod|silk|BlackBerry|IEMobile|Opera Mini/i
				.test(a);
		this.detecter.isIE = navigator.appName.indexOf("Internet Explorer") >= 0
	},
	isChrome : function() {
		return this.detecter.isChrome
	},
	isIE : function() {
		return this.detecter.isIE
	},
	isSafari : function() {
		return this.detecter.isSafari
	},
	isIpad : function() {
		return this.detecter.isIpad
	},
	isTablet : function() {
		return this.detecter.isTablet
	}
};
Browser.detect();
haftClearTable = function(c) {
	var a = $("#" + c + " > tbody").children();
	for (var b = 0; b < a.length; b++) {
		$(a[b]).remove()
	}
};
HighlightRoom = {
	list : [],
	activeTimePerBox : 50,
	active : function() {
		this.lifeTime = 1000;
		if (Browser.isTablet()) {
			if (Browser.isChrome()) {
				this.lifeTime = 200
			} else {
				this.lifeTime = 700
			}
			this.activeTimePerBox = 20
		}
		this.callback = function(a) {
			a.attr("class", a.orgClass)
		};
		this.createBox()
	},
	push : function(a) {
		this.crrBox.push(a)
	},
	createBox : function() {
		var a = new TimeBox({
			callback : this.callback,
			lifeTime : this.lifeTime
		});
		this.crrBox = a;
		setTimeout(function() {
			this.createBox()
		}.bind(this), this.activeTimePerBox);
		this.list.push(this.crrBox)
	}
};
RowSelector = {
	selectedClass : "selected",
	crrRow : null,
	select : function(a) {
		this.clear();
		a.addClass(this.selectedClass);
		this.crrRow = a
	},
	clear : function() {
		if (this.crrRow) {
			this.crrRow.removeClass(this.selectedClass)
		}
	}
};
required = function() {
};
OOPHelper = {
	extend : function(a, d, b) {
		a.prototype = new d;
		for ( var c in b) {
			a.prototype[c] = b[c]
		}
	}
};
DateTimeUtil = {
	getSecondFromTimeString : function(b) {
		var a = b.split(":");
		return a[0] * 3600 + a[1] * 60 + a[2]
	}
};
ArrayUtil = {
	getChangeArr : function(d, c) {
		var b = [];
		for (var a = 0; a < d.length; a++) {
			if (c.indexOf(d[a]) < 0) {
				b.push(d[a])
			}
		}
		return b
	},
	orArr : function(c, b) {
		var a = [];
		var e = this.getChangeArr(c, b);
		for (var d = 0; d < e.length; d++) {
			a.push(e[d])
		}
		for (var d = 0; d < b.length; d++) {
			a.push(b[d])
		}
		return a
	},
	andArr : function(b, a) {
		var d = [];
		for (var c = 0; c < b.length; c++) {
			if (a.indexOf(b[c]) >= 0) {
				d.push(b[c])
			}
		}
		return d
	},
	getArrFromMap : function(d) {
		var a = [];
		var c = Object.keys(d);
		for (var b = 0; b < c.length; b++) {
			a.push(d[c[b]])
		}
		return a
	}
};
Loading = {
	show : function() {
		$("#loading").css({
			top : 0,
			left : 0,
			width : "100%",
			height : "100%",
			display : "block"
		})
	},
	hide : function() {
		$("#loading").css({
			display : "none"
		})
	}
};
function AdOrderCmp(a) {
	for ( var b in a) {
		if (typeof a[b] != "undefined") {
			this[b] = a[b]
		}
	}
	this.model = new AdOrderModel();
	this.view = new AdOrderView(this.wrp);
	this.listenModelEvent()
}
AdOrderCmp.prototype = {
	render : function() {
		this.view.render()
	},
	add : function(a) {
		this.model.add(a)
	},
	setAdOrders : function(a) {
		this.model.setAdOrders(a)
	},
	listenModelEvent : function() {
		this.model.on("changeAdOrderList", function(a) {
			this.view.render(this.model.adOrders)
		}.bind(this));
		this.model.on("addItem", function(a) {
			this.view.addRow(a.message)
		}.bind(this));
		this.model.on("removeItem", function(a) {
			this.view.remove(a.message)
		}.bind(this));
		this.model.on("refreshAdOders", function(a) {
			this.view.render(this.model.adOrders, a.message)
		}.bind(this))
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	}
};
function AdOrderModel() {
	this.reset()
}
AdOrderModel.prototype = {
	add : function(a) {
		if (this.sequence.indexOf(a.sequence) >= 0) {
			return
		}
		this.sequence.push(a.sequence);
		if (a.status == 0) {
			this.remove(a);
			return
		}
		this.adOrders.push(a);
		if (this.crrTime <= a.time) {
			this.crrTime = a.time;
			this.emit("addItem", a)
		} else {
			this.sortAdOrders();
			this.emit("refreshAdOders", a.type)
		}
	},
	setAdOrders : function(a) {
		this.reset();
		this.adOrders = a;
		this.sortAdOrders();
		this.emit("changeAdOrderList")
	},
	remove : function(b) {
		var c = cloneObject(b);
		c.status = 1;
		var a = this.adOrders.indexOf(c);
		this.adOrders.splice(a, 1);
		this.emit("removeItem", c)
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	},
	sortAdOrders : function() {
		this.adOrders.sort(function(b, a) {
			return DateTimeUtil.getSecondFromTimeString(b.time)
					- DateTimeUtil.getSecondFromTimeString(a.time)
		})
	},
	reset : function() {
		this.crrTime = "";
		this.adOrders = [];
		this.sequence = []
	}
};
function AdOrderView(a) {
	this.wrp = a
}
AdOrderView.prototype = {
	render : function(a, c) {
		if (typeof c == "undefined") {
			this.wrp.buy.html("");
			this.wrp.sell.html("");
			for (var b = 0; b < a.length; b++) {
				this.addRow(a[b])
			}
		} else {
			if (c == "B") {
				this.wrp.buy.html("")
			} else {
				if (c == "S") {
					this.wrp.sell.html("")
				}
			}
			for (var b = 0; b < a.length; b++) {
				if (a[b].type == c) {
					this.addRow(a[b])
				}
			}
		}
	},
	addRow : function(a) {
		var b = this.getHtml(a);
		if (a.type == "B") {
			this.wrp.buy.append(b)
		} else {
			if (a.type == "S") {
				this.wrp.sell.append(b)
			}
		}
	},
	remove : function(a) {
		$("#" + this.getItemId(a)).remove()
	},
	getHtml : function(b) {
		htmlText = '<tr id="' + this.getItemId(b) + '">';
		var a = getClass(b.ceilingPrice, b.floorPrice, b.basicPrice, b.price);
		htmlText += '<td class="texttimelignleft ptimedding4px ' + a
				+ ' " width="%">' + b.stockSymbol + "</td>";
		htmlText += '<td class="' + a + '">' + FormatPrice(b.price) + "</td>";
		htmlText += '<td class="clryellow">'
				+ NumberFormat.baseVol(b.vol * 10, ",", ".") + "</td>";
		htmlText += "<td >" + b.time + "</td>";
		htmlText += "</tr>";
		return htmlText
	},
	getItemId : function(a) {
		return a.stockSymbol + a.type + a.time + a.price + a.vol
	}
};
function PtOrderCmp(a) {
	for ( var b in a) {
		if (typeof a[b] != "undefined") {
			this[b] = a[b]
		}
	}
	this.model = new PtOrderModel();
	this.view = new PtOrderView(this.wrp);
	this.listenModelEvent()
}
PtOrderCmp.prototype = {
	render : function() {
		this.view.render()
	},
	add : function(a) {
		this.model.add(a)
	},
	setPtOrders : function(a) {
		this.model.setPtOrders(a)
	},
	listenModelEvent : function() {
		this.model.on("changePtOrders", function() {
			this.view.render(this.model.ptOrders)
		}.bind(this));
		this.model.on("addItem", function(a) {
			this.view.addRow(a.message)
		}.bind(this));
		this.model.on("changeTotal", function(a) {
			this.view.updateTotal(this.model.totalVal, this.model.totalVol)
		}.bind(this))
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	}
};
function PtOrderModel() {
	this.reset()
}
PtOrderModel.prototype = {
	add : function(a) {
		this.ptOrders.push(a);
		this.totalVol += a.volume;
		this.totalVal += a.price * a.volume;
		this.emit("changeTotal");
		if (this.crrTime <= a.time) {
			this.crrTime = a.time;
			this.emit("addItem", a)
		} else {
			this.sortPtOrders();
			this.emit("changePtOrders")
		}
	},
	setPtOrders : function(a) {
		this.reset();
		this.ptOrders = a;
		this.sortPtOrders();
		for ( var b in this.ptOrders) {
			this.totalVol += this.ptOrders[b].volume;
			this.totalVal += this.ptOrders[b].price * this.ptOrders[b].volume
		}
		this.emit("changeTotal");
		this.emit("changePtOrders")
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	},
	sortPtOrders : function() {
		this.ptOrders.sort(function(b, a) {
			return DateTimeUtil.getSecondFromTimeString(b.time)
					- DateTimeUtil.getSecondFromTimeString(a.time)
		})
	},
	reset : function() {
		this.crrTime = "";
		this.ptOrders = [];
		this.sequence = [];
		this.totalVol = 0;
		this.totalVal = 0
	}
};
function PtOrderView(a) {
	this.wrp = a
}
PtOrderView.prototype = {
	render : function(b) {
		this.wrp.el.html("");
		for (var a = 0; a < b.length; a++) {
			this.addRow(b[a])
		}
	},
	addRow : function(a) {
		var b = this.getHtml(a);
		this.wrp.el.append(b)
	},
	updateTotal : function(b, a) {
		this.wrp.totalVal.html(NumberFormat.price(b));
		this.wrp.totalVol.html(NumberFormat.baseVol(a))
	},
	getHtml : function(b) {
		var c = "";
		var a = getClass(b.ceilingPrice, b.floorPrice, b.basicPrice, b.price);
		c = '<tr id="' + this.getItemId(b) + '">';
		c += '<td class="texttimelignleft ptimedding4px ' + a + ' " width="%">'
				+ b.symbol + "</td>";
		c += '<td class="' + a + '">' + NumberFormat.price(b.price) + "</td>";
		c += '<td class="txt-yellow">'
				+ NumberFormat.baseVol(b.volume, ",", ".") + "</td>";
		c += "<td >" + NumberFormat.index(b.volume * b.price) + "</td>";
		c += "<td>" + b.time + "</td>";
		c += "</tr>";
		return c
	},
	getItemId : function(a) {
		return a.symbol + a.time
	}
};
function MarketInfoCmp(a) {
	this.jqueryObject = $(this);
	for ( var b in a) {
		if (typeof a[b] != "undefined") {
			this[b] = a[b]
		}
	}
	this.cookieKey = this.floorCode + "ck";
	this.view = new MarketInfoView(this.wrp, this.model);
	this.mode = "show";
	this.listenViewEvent();
	this.listenModelEvent();
	this.loadConfigFromCookie()
}
MarketInfoCmp.prototype = {
	render : function() {
		this.view.render()
	},
	add : function(a) {
		this.model.add(a)
	},
	setSnapshot : function(a) {
		if (this.isATC() && !this.isATCStatus(a)) {
			this.emit("onEndATC")
		}
		if (this.isATO() && !this.isATOStatus(a)) {
			this.emit("onEndATO")
		}
		this.model.setSnapshot(a)
	},
	setHistory : function(a) {
		this.model.setHistory(a)
	},
	isATO : function() {
		return this.isATOStatus(this.model.snapShot)
	},
	isATC : function() {
		return this.isATCStatus(this.model.snapShot)
	},
	isATOStatus : function(a) {
		if (a.floorCode == CONST_HSX_NAME || a.floorCode == CONST_VN30_NAME) {
			if (a.status == "P" || a.status == "2") {
				return true
			}
		}
		return false
	},
	isATCStatus : function(a) {
		if (a.status == "A" || a.status == "9"
				|| (a.floorCode == CONST_HNX_NAME && a.status == "30")
				|| (a.floorCode == CONST_HX30_NAME && a.status == "30")) {
			return true
		}
		return false
	},
	listenModelEvent : function() {
		this.model.on("updateSnapshot", function(a) {
			this.emit("updateSnapshot", a.message);
			if (this.mode == "hide") {
				return
			}
			this.view.renderInfoBlock(this.floorCode, this.model.snapShot)
		}.bind(this));
		this.model.on("updateHistory", function(a) {
			if (this.mode == "hide") {
				return
			}
			this.view.renderChart(this.model.volData,
					this.model.advanceIndexData, this.model.declineIndexData,
					this.model.refIndex)
		}.bind(this));
		this.model.on("addItem", function(a) {
			if (this.mode == "hide") {
				return
			}
			this.view.renderChart(this.model.volData,
					this.model.advanceIndexData, this.model.declineIndexData,
					this.model.refIndex)
		}.bind(this));
		this.model.on("OnModelChange", function(a) {
			if (this.mode == "hide") {
				return
			}
			switch (a.message.name) {
			case "ceilingCount":
				this.view.refeshCeilingCount(a.message);
				break;
			case "floorCount":
				this.view.refreshFloorCount(a.message);
			default:
				break
			}
		}.bind(this))
	},
	listenViewEvent : function() {
		this.btn.onclick = function() {
			if (this.mode == "show") {
				this.hide()
			} else {
				if (this.mode == "hide") {
					this.show()
				}
			}
		}.bind(this);
		this.wrp.el.children()[0].onclick = function() {
			this.hide()
		}.bind(this)
	},
	show : function() {
		this.btn.className = "active";
		this.view.show();
		this.view.renderInfoBlock(this.floorCode, this.model.snapShot);
		this.view.renderChart(this.model.volData, this.model.advanceIndexData,
				this.model.declineIndexData, this.model.refIndex);
		this.mode = "show";
		setCookie(this.cookieKey, "show")
	},
	hide : function(a) {
		this.btn.className = "";
		this.view.hide();
		this.mode = "hide";
		if (!a) {
			setCookie(this.cookieKey, "hide")
		}
	},
	loadConfigFromCookie : function() {
		var a = getCookie(this.cookieKey);
		if (a == "hide") {
			this.hide(true)
		} else {
			this.show()
		}
	},
	emit : function(b, a) {
		this.jqueryObject.trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		this.jqueryObject.bind(b, a)
	}
};
function MarketInfoModel(a) {
	this.jqueryObject = $(this);
	this.floorCode = a;
	this.reset();
	this.snapShot = {};
	this.ceilingCount = 0;
	this.floorCount = 0
}
MarketInfoModel.prototype = {
	add : function(a) {
		if (a.marketIndex) {
			this.setSnapshot(a);
			this.crrTime = a.tradingTime
		}
		this.pushToModel(a, true)
	},
	setSnapshot : function(b) {
		if (typeof b == "undefined") {
			return
		}
		this.crrSequence = b.sequence;
		var a = this.snapShot.status;
		this.snapShot = b;
		this.emit("updateSnapshot", b);
		if (a != MarketStatus.ATO && b.status == MarketStatus.ATO) {
			this.emit("ATO");
			return
		}
		if (a == MarketStatus.ATO && b.status != MarketStatus.ATO) {
			this.emit("onEndATC");
			return
		}
		if (a != MarketStatus.ATC && b.status == MarketStatus.ATC) {
			this.emit("ATC");
			return
		}
		if (a == MarketStatus.ATC && b.status != MarketStatus.ATC) {
			this.emit("onEndATC");
			return
		}
	},
	updateCeilingFloorCount : function(a) {
		if (a.type == "CEILING") {
			this.setCeilingCount(a.count);
			return
		}
		if (a.type == "FLOOR") {
			this.setFloorCount(a.count);
			return
		}
	},
	setCeilingCount : function(a) {
		if (typeof a == "undefined") {
			return
		}
		this.set("ceilingCount", a)
	},
	setFloorCount : function(a) {
		if (typeof a == "undefined") {
			return
		}
		this.set("floorCount", a)
	},
	getSnapshot : function() {
		return this.snapShot
	},
	getStatus : function() {
		return this.snapShot.status
	},
	getCeilingCount : function() {
		return this.ceilingCount
	},
	getFloorCount : function() {
		return this.floorCount
	},
	getFloorCode : function() {
		return this.floorCode
	},
	set : function(b, a) {
		this[b] = a;
		this.emit("onModelChange", {
			name : b,
			value : a
		})
	},
	setHistory : function(b) {
		if (!b || b.length == 0) {
			return
		}
		this.reset();
		this.status = 1;
		if (b.marketIndex && b.marketIndex[0] != null) {
			this.lastTradingTime = this.convertDate(b.tradingTime[0]);
			this.lastVol = parseFloat(b.totalShareTraded[0])
		}
		for (var a = 1; a < b.marketIndex.length; a++) {
			this.pushToModel({
				tradingTime : b.tradingTime[a],
				sequence : b.sequence[a],
				totalShareTraded : b.totalShareTraded[a],
				priorMarketIndex : b.priorMarketIndex[a],
				marketIndex : b.marketIndex[a]
			}, false)
		}
		this.emit("updateHistory")
	},
	pushToModel : function(c, d) {
		if (c.marketIndex == 0 || c.tradingTime == null) {
			return
		}
		if (this.isATOStatus(c)) {
			return
		}
		if (this.isATCStatus(c) && (c.floorCode == "10" || c.floorCode == "11")) {
			return
		}
		this.sequences.push(c.sequence);
		this.setRefValue(c);
		var e = this.convertDate(c.tradingTime);
		if (this.lastTradingTime == e) {
			return
		}
		var a = parseFloat(c.totalShareTraded) - this.lastVol;
		if (a >= 0) {
			this.volData.push([ e, a ])
		}
		this.lastVol = parseFloat(c.totalShareTraded);
		if (c.marketIndex >= this.refIndex) {
			if (this.status == 0) {
				this.status = 1;
				var b = (e + this.lastTradingTime) / 2;
				if (this.declineIndexData.length > 0) {
					this.declineIndexData.push([ b, this.refIndex ]);
					this.declineIndexData.push(null)
				}
				this.advanceIndexData.push([ b, this.refIndex ])
			}
			this.advanceIndexData.push([ this.convertDate(c.tradingTime),
					parseFloat(c.marketIndex) ])
		} else {
			if (this.status == 1) {
				this.status = 0;
				var b = (e + this.lastTradingTime) / 2;
				if (this.advanceIndexData.length > 0) {
					this.advanceIndexData.push([ b, this.refIndex ]);
					this.advanceIndexData.push(null)
				}
				this.declineIndexData.push([ b, this.refIndex ])
			}
			this.declineIndexData.push([ this.convertDate(c.tradingTime),
					parseFloat(c.marketIndex) ])
		}
		this.lastTradingTime = e;
		if (d) {
			this.emit("addItem")
		}
	},
	isATOStatus : function(a) {
		if (a.floorCode == CONST_HSX_NAME || a.floorCode == CONST_VN30_NAME) {
			if (a.status == "P" || a.status == "2") {
				return true
			}
		}
		return false
	},
	isATO : function() {
		var a = this.snapShot;
		return this.isATOStatus(a)
	},
	isATCStatus : function(a) {
		if (a.status == "A" || a.status == "9"
				|| (a.floorCode == CONST_HNX_NAME && a.status == "30")
				|| (a.floorCode == CONST_HX30_NAME && a.status == "30")) {
			return true
		}
		return false
	},
	isATC : function() {
		var a = this.snapShot;
		return this.isATCStatus(a)
	},
	setRefValue : function(a) {
		if (this.isSetRefValue) {
			return
		}
		if (a.priorMarketIndex) {
			this.refIndex = parseFloat(a.priorMarketIndex);
			this.isSetRefValue = true
		}
	},
	emit : function(b, a) {
		this.jqueryObject.trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		this.jqueryObject.bind(b, a)
	},
	convertDate : function(b) {
		var a = b.split(":");
		return Date.UTC(0, 0, 0, a[0], a[1])
	},
	reset : function() {
		this.isShow = true;
		this.history = [];
		this.volData = [];
		this.declineIndexData = [];
		this.advanceIndexData = [];
		this.crrTime = "00:00:00";
		this.crrSequence = 0;
		this.sequences = [];
		this.status = 1;
		this.lastTradingTime = 0;
		this.lastVol = 0
	}
};
function MarketInfoView(a, b) {
	this.wrp = a;
	this.model = b
}
MarketInfoView.prototype = {
	nameMap : {
		"10" : "VN-INDEX",
		"02" : "HNX-INDEX",
		"11" : "VN30-INDEX",
		"12" : "HNX30-INDEX",
		"03" : "UPCOM"
	},
	render : function() {
	},
	renderChart : function(g, a, e, f) {
		var d = Date.UTC(0, 0, 0, 9, 0);
		var h = Date.UTC(0, 0, 0, 15, 0);
		var c = [];
		var b = [];
		c.push({
			data : g,
			color : "#60A0FF",
			bars : {
				show : true,
				lineWidth : 1.5
			},
			yaxis : 2
		});
		c.push({
			data : a,
			color : "#00FF00"
		});
		c.push({
			data : e,
			color : "#FF0000"
		});
		c.push({
			data : [ [ d, f ], [ h, f ] ],
			color : "#F8F093",
			lines : {
				show : true,
				lineWidth : 1.2
			}
		});
		this.chart = $.plot(this.wrp.chart, c, {
			xaxis : {
				mode : "time",
				min : (d),
				max : (h),
				tickFormatter : function(j, i) {
					var k = new Date();
					k.setTime(j);
					return k.getUTCHours() + "h"
				}
			},
			yaxis : {
				ticks : 2,
				show : false
			},
			grid : {
				backgroundColor : {
					colors : [ "#000000", "#3E3300" ]
				},
				borderWidth : 0.5
			},
			lines : {
				steps : true,
				lineWidth : 1.2
			},
			legend : {
				show : false
			}
		})
	},
	hide : function() {
		this.wrp.el.css("display", "none")
	},
	show : function() {
		this.wrp.el.css("display", "block")
	},
	renderInfoBlock : function(a, b) {
		var c = "";
		c += "<p>";
		c += '<span class="txt-orange">' + this.getName(a) + "&nbsp;</span>";
		c += '<span id ="' + a
				+ 'IndexArrow" class="icon-arrowup">+</span>&nbsp;';
		c += '<span id ="' + a + 'Index" class="txt-gia-tc">0</span>&nbsp;';
		c += '<span id ="' + a
				+ 'SpChange" class="txt-gia-tc">(0.00 0.00%)</span>';
		c += "</p>";
		c += "<p>";
		c += '<span id="' + a + 'SpVol" class="">0</span>';
		c += '<span class="txt-orange">&nbsp;CP&nbsp;</span>';
		c += '<span id ="' + a + 'SpVal" class="">0.000</span>';
		c += '<span class="txt-orange">&nbsp;Tá»·&nbsp;</span>';
		c += "</p>";
		c += "<p>";
		c += '<span class="icon-arrowup">+</span>&nbsp;';
		c += '<span id ="' + a + 'SpUp" class="txt-lime">0</span>&nbsp;';
		c += '<span id ="' + a + 'SpCel" class="txt-magenta ">('
				+ this.model.getCeilingCount() + ")</span>&nbsp;";
		c += '<span class="icon-square">Â·</span>&nbsp;';
		c += '<span id ="' + a
				+ 'spRef" class="txt-yellow">0</span>&nbsp;&nbsp;';
		c += '<span class="icon-arrowdown">-</span>&nbsp;';
		c += '<span id ="' + a + 'spDown" class="txt-red">0</span>&nbsp;';
		c += '<span id ="' + a + 'spFlo" class="txt-aqua ">('
				+ this.model.getFloorCount() + ")</span>&nbsp;";
		c += '<span id ="' + a + 'spStatus" class=""></span>';
		c += "</p>";
		this.wrp.infoBlock.html(c);
		this.update(b)
	},
	update : function(e) {
		if (typeof e.floorCode == "undefined") {
			return
		}
		var d = "#" + e.floorCode;
		var b = this.getIndexChangeInfo(e);
		var a = this.getClass(e);
		var c = this.getIconClass(e);
		$(d + "Index").html(NumberFormat.index(e.marketIndex));
		$(d + "Index").attr("class", a);
		$(d + "IndexArrow").attr("class", c);
		$(d + "SpChange").html(b);
		$(d + "SpChange").attr("class", a);
		$(d + "SpVol").html(NumberFormat.cp(e.totalShareTraded));
		$(d + "SpVal")
				.html(NumberFormat.currency(e.totalValueTraded, ",", "."));
		$(d + "SpUp").html(e.advance);
		$(d + "spRef").html(e.noChange);
		$(d + "spDown").html(e.decline);
		$(d + "spStatus").html(this.getStatus(e))
	},
	refeshCeilingCount : function() {
		$("#" + this.model.getFloorCode() + "SpCel").text(
				"(" + this.model.getCeilingCount() + ")")
	},
	refreshFloorCount : function(a) {
		$("#" + this.model.getFloorCode() + "spFlo").text(
				"(" + this.model.getFloorCount() + ")")
	},
	getName : function(a) {
		return this.nameMap[a]
	},
	getIndexChangeInfo : function(d) {
		var c = "0.00";
		var b = "0.00";
		if (d.priorMarketIndex && d.marketIndex && d.priorMarketIndex != "0") {
			c = d.marketIndex - d.priorMarketIndex;
			b = c * 100 / d.priorMarketIndex;
			c = Highcharts.numberFormat(Math.abs(c));
			b = Highcharts.numberFormat(Math.abs(b))
		}
		var a = "(" + c + " " + b + "%)";
		return a
	},
	getStatus : function(d) {
		var b = d.status;
		var a = d.floorCode;
		var c = "ÄÃ³ng cá»­a";
		if (b == "null") {
			c = "ÄÃ³ng cá»­a"
		} else {
			if (b == "O" || b == "5") {
				c = "LiÃªn tá»¥c"
			} else {
				if (b == "P" || b == "2") {
					if ((a == CONST_HSX_NAME) || (a == CONST_VN30_NAME)) {
						c = "PhiÃªn ATO"
					} else {
						c = "ÄÃ³ng cá»­a"
					}
				} else {
					if (b == "A" || b == "9" || b == "30") {
						c = "PhiÃªn ATC"
					} else {
						if (b == "C" || b == "0" || b == "35") {
							c = "PhiÃªn GDTT"
						} else {
							if (b == "I" || b == "BSU" || b == "3" || b == "10") {
								c = "Nghá»‰ trÆ°a"
							} else {
								if (b == "11") {
									c = "ÄÃ³ng cá»­a"
								}
							}
						}
					}
				}
			}
		}
		return c
	},
	getClass : function(b) {
		var a;
		if (b.priorMarketIndex < b.marketIndex) {
			a = "txt-lime"
		} else {
			if (b.priorMarketIndex == b.marketIndex) {
				a = "txt-gia-tc"
			} else {
				a = "txt-red"
			}
		}
		return a
	},
	getIconClass : function(b) {
		var a;
		if (b.priorMarketIndex < b.marketIndex) {
			a = "icon-arrowup"
		} else {
			if (b.priorMarketIndex == b.marketIndex) {
				a = "icon-square"
			} else {
				a = "icon-arrowdown"
			}
		}
		return a
	},
};
function MarketInfosManager(a) {
	this.isShow = true;
	this.marketInfoGroup = a;
	this.wrp = $("#chartsWrp");
	this.cmps = {};
	var b = $("#chartBtns>li>a");
	this.cmps[FloorCodes.HOSE] = new MarketInfoCmp({
		wrp : {
			chart : $("#HoseChartWrp"),
			infoBlock : $("#HoseInfoWrp"),
			el : $("#HoseElWrp")
		},
		btn : b[1],
		floorCode : FloorCodes.HOSE,
		model : a.getMarketInfoModel(FloorCodes.HOSE)
	});
	this.cmps[FloorCodes.HNX] = new MarketInfoCmp({
		wrp : {
			chart : $("#HnxChartWrp"),
			infoBlock : $("#HnxInfoWrp"),
			el : $("#HnxElWrp")
		},
		btn : b[3],
		floorCode : FloorCodes.HNX,
		model : a.getMarketInfoModel(FloorCodes.HNX)
	});
	this.cmps[FloorCodes.HNX30] = new MarketInfoCmp({
		wrp : {
			chart : $("#Hnx30ChartWrp"),
			infoBlock : $("#Hnx30InfoWrp"),
			el : $("#Hnx30ElWrp")
		},
		btn : b[4],
		floorCode : FloorCodes.HNX30,
		model : a.getMarketInfoModel(FloorCodes.HNX30)
	});
	this.cmps[FloorCodes.VN30] = new MarketInfoCmp({
		wrp : {
			chart : $("#Vn30ChartWrp"),
			infoBlock : $("#Vn30InfoWrp"),
			el : $("#Vn30ElWrp")
		},
		btn : b[2],
		floorCode : FloorCodes.VN30,
		model : a.getMarketInfoModel(FloorCodes.VN30)
	});
	this.cmps[FloorCodes.UPCOME] = new MarketInfoCmp({
		wrp : {
			chart : $("#UpcomChartWrp"),
			infoBlock : $("#UpcomInfoWrp"),
			el : $("#UpcomElWrp")
		},
		btn : b[5],
		floorCode : FloorCodes.UPCOME,
		model : a.getMarketInfoModel(FloorCodes.UPCOME)
	});
	this.listenEvent()
}
MarketInfosManager.prototype = {
	add : function(a) {
		if (a.floorCode == CONST_HX30_NAME) {
			a.status = System.marketInfosManager.cmps[CONST_HNX_NAME].model.snapShot.status
		}
		this.cmps[a.floorCode].add(a)
	},
	setFull : function(a) {
		this.cmps[a.floorCode].setSnapshot(a.snapshot.data);
		setTimeout(function(b) {
			this.cmps[b.floorCode].setHistory(b.history)
		}.bind(this, a), 20)
	},
	hide : function(a) {
		this.cmps[a].hide()
	},
	show : function(a) {
		this.cmps[a].show()
	},
	isATO : function(a) {
		var b = this.convertFloorCode(a);
		if (typeof this.cmps[b] == "undefined") {
			console.log("khong ton tai floor code: " + b);
			return false
		}
		return this.cmps[b].isATO()
	},
	isATC : function(a) {
		var b = this.convertFloorCode(a);
		if (typeof this.cmps[b] == "undefined") {
			console.log("khong ton tai floor code: " + b);
			return false
		}
		return this.cmps[b].isATC()
	},
	listenEvent : function() {
		$("#chartDisplayBtn").click(function() {
			if (this.isShow) {
				$("#chartDisplayBtn").addClass("down");
				this.wrp.css({
					display : "none"
				});
				this.isShow = false;
				this.emit("onHide")
			} else {
				$("#chartDisplayBtn").removeClass("down");
				this.wrp.css({
					display : "block"
				});
				this.isShow = true;
				this.emit("onShow")
			}
		}.bind(this));
		this.cmps[CONST_HSX_NAME].on("updateSnapshot", function(b) {
			var a = b.message;
			if (a.status == "P" || a.status == "2") {
				this.emit("ATO", CONST_HSX_NAME);
				return
			}
			if (status == "A" || status == "9") {
				this.emit("ATC", CONST_HSX_NAME)
			}
		}.bind(this));
		this.cmps[CONST_HSX_NAME].on("onEndATC", function(a) {
			this.emit("onEndATC", CONST_HSX_NAME)
		}.bind(this));
		this.cmps[CONST_HSX_NAME].on("onEndATO", function(a) {
			this.emit("onEndATO", CONST_HSX_NAME)
		}.bind(this));
		this.cmps[CONST_HNX_NAME].on("updateSnapshot", function(b) {
			var a = b.message;
			if (a.status == "A" || a.status == "30") {
				this.emit("ATC", CONST_HNX_NAME)
			}
		}.bind(this));
		this.cmps[CONST_HNX_NAME].on("onEndATC", function(a) {
			this.emit("onEndATC", CONST_HNX_NAME)
		}.bind(this))
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	},
	convertFloorCode : function(a) {
		if (a == CONST_HSX_NAME) {
			return CONST_HSX_NAME
		}
		if (a == CONST_HNX_NAME || a == "2") {
			return CONST_HNX_NAME
		}
		if (a == CONST_HX30_NAME) {
			return CONST_HX30_NAME
		}
		if (a == CONST_VN30_NAME) {
			return CONST_VN30_NAME
		}
		if (a == CONST_UPC_NAME || a == "3") {
			return CONST_UPC_NAME
		}
		return a
	}
};
function TransactionModel() {
	this.reset()
}
TransactionModel.prototype = {
	setStockInfo : function(b) {
		if (!b) {
			this.reset();
			return
		}
		var a = b.code;
		if (this.crrStock === a || a.length === 0) {
			this.reset()
		} else {
			this.reset();
			this.crrStock = a;
			this.stockInfo = b
		}
		this.emit("changeStockCode", this.crrStock)
	},
	setTransactionHistory : function(b) {
		this.clear();
		var c = 0;
		if (b && b.length > 0) {
			this.sort(b);
			this.crrRefVal = this.stockInfo.basicPrice;
			this.lastTime = this.convertTime(b[c].time);
			this.lastIsBigerFlag = b[c].last >= this.crrRefVal;
			for (var a = c; a < b.length; a++) {
				this.addTransaction(b[a], true)
			}
		}
		this.emit("updateHistory")
	},
	addTransaction : function(e, d) {
		if (e.symbol != this.crrStock) {
			return
		}
		this.crrRefVal = this.stockInfo.basicPrice;
		var c = this.convertTime(e.time);
		this.history.push(e);
		this.vols.push([ c, e.lastVol ]);
		var a = (this.lastTime + c) / 2;
		var b = (e.last >= this.crrRefVal);
		if (b) {
			if (!this.lastIsBigerFlag) {
				this.lowerVals.push([ a, this.crrRefVal ]);
				this.lowerVals.push(null);
				this.upperVals.push([ a, this.crrRefVal ])
			}
			this.upperVals.push([ c, e.last ])
		} else {
			if (this.lastIsBigerFlag) {
				this.upperVals.push([ a, this.crrRefVal ]);
				this.upperVals.push(null);
				this.lowerVals.push([ a, this.crrRefVal ])
			}
			this.lowerVals.push([ c, e.last ])
		}
		this.lastIsBigerFlag = b;
		this.lastTime = c;
		if (!d) {
			this.emit("add_transaction", e)
		}
	},
	reset : function() {
		this.crrSequence = 0;
		this.crrStock = "";
		this.crrRefVal = 0;
		this.clear()
	},
	clear : function() {
		this.history = [];
		this.vols = [];
		this.lowerVals = [];
		this.upperVals = []
	},
	convertTime : function(b) {
		var a = b.split(":");
		return Date.UTC(0, 0, 0, a[0], a[1])
	},
	sort : function(a) {
		a.sort(function(g, e) {
			var c = g.time.split(":");
			var f = Date.UTC(0, 0, 0, c[0], c[1], c[2]);
			var b = e.time.split(":");
			var d = Date.UTC(0, 0, 0, b[0], b[1], b[2]);
			return f - d
		})
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		});
		this.cmp.emit(b, a)
	},
	on : function(b, a) {
		$(this).bind(b, a)
	}
};
function TransactionView() {
}
TransactionView.prototype = {
	setTargetElement : function(a) {
		this.targetRow = a
	},
	clear : function() {
		if (this.element) {
			this.element.remove()
		}
	},
	render : function() {
		this.clear();
		if (this.model.crrStock === "") {
			return
		}
		this.renderWrp();
		setTimeout(function() {
			this.renderChart()
		}.bind(this), 200);
		this.renderList(this.listWrp)
	},
	renderWrp : function() {
		var b = this.cmp.name + "ElementWrp";
		var a = this.cmp.name + "ChartWrp";
		var c = this.cmp.name + "ListWrp";
		var d = '<tr class="detail" style="height: 145px;" id="'
				+ b
				+ '"><td colspan="28"><div class="row detail clrfx"><h5 class="txtc"><span class="txt-orange">'
				+ this.model.crrStock
				+ "</span> - "
				+ this.model.stockInfo.companyName
				+ '</h5><div class="col col22" style="height: 150px;width: 400px" id="'
				+ a
				+ '"></div><div class="col col8 last" style="height: 150px; overflow:auto"><table ><thead><tr><th colspan="4">Lá»‹ch sá»­ khá»›p lá»‡nh</th></tr><tr class=""><th class="txtc" width="90">Thá»i gian</td><th class="txtc" width="90">KL</td><th class="txtc" width="90">GiÃ¡</td><th class="txtc" width="90">+/-</td></tr></thead><tbody id="'
				+ c + '" ></tbody></table></div></div></td></tr>';
		$(d).insertAfter(this.targetRow);
		this.element = $("#" + b);
		this.chartWrp = $("#" + a);
		this.listWrp = $("#" + c)
	},
	renderChart : function(a) {
		var a = this.chartWrp;
		var e = this.model.vols;
		var b = this.model.upperVals;
		var d = this.model.lowerVals;
		var c = this.model.crrRefVal;
		var g = Date.UTC(0, 0, 0, 9, 0);
		var h = Date.UTC(0, 0, 0, 15, 0);
		var f = [];
		f.push({
			data : e,
			color : "#60A0FF",
			bars : {
				lineWidth : 1.5,
				show : true
			},
			yaxis : 2
		});
		f.push({
			data : b,
			color : "#00FF00"
		});
		f.push({
			data : d,
			color : "#FF0000"
		});
		f.push({
			data : [ [ g, c ], [ h, c ] ],
			color : "#F8F093",
			lines : {
				show : true,
				lineWidth : 1.2
			}
		});
		this.chart = $.plot(a, f, {
			xaxis : {
				mode : "time",
				min : (g),
				max : (h)
			},
			yaxis : {
				ticks : 2
			},
			grid : {
				backgroundColor : {
					colors : [ "#000000", "#3E3300" ]
				},
				borderWidth : 0.5
			},
			lines : {
				steps : true,
				lineWidth : 1
			},
			bars : {
				barWidth : 0.5
			},
			legend : {
				show : false
			},
			xaxes : [ {
				mode : "time"
			} ],
			yaxes : [ {
				position : "right"
			} ]
		})
	},
	renderList : function() {
		var b = "";
		if (this.model.history.length > 0) {
			for (var a = this.model.history.length - 1; a >= 0; a--) {
				b += this.getHtmlOfRow(this.model.history[a])
			}
		}
		this.listWrp.html(b)
	},
	addRow : function(a) {
		var b = this.getHtmlOfRow(a);
		this.listWrp.prepend(b)
	},
	getHtmlOfRow : function(b) {
		var a = NumberFormat.price(b.last - b.basicPrice);
		var c = '<tr class=""><td class="txtc">' + b.time
				+ '</td><td class="txtr">' + b.lastVol
				+ '</td><td class="txtr">' + b.last + '</td><td class="txtr">'
				+ a + "</td></tr>";
		return c
	}
};
function TransactionCmp(a) {
	for ( var b in a) {
		if (typeof a[b] != "undefined") {
			this[b] = a[b]
		}
	}
	this.model = new TransactionModel();
	this.model.cmp = this;
	this.view = new TransactionView();
	this.view.cmp = this;
	this.view.model = this.model;
	this.model.view = this.view;
	this.listenModelEvent()
}
TransactionCmp.prototype = {
	render : function() {
		this.view.render()
	},
	setStockInfo : function(a) {
		this.model.setStockInfo(a.stockInfo);
		this.view.setTargetElement(a.element)
	},
	setTransactionHistory : function(a) {
		this.model.setTransactionHistory(a)
	},
	addTransaction : function(a) {
		this.model.addTransaction(a)
	},
	listenModelEvent : function() {
		this.model.on("changeStockCode", function(a) {
			if (a.message.length == 0) {
				this.view.clear();
				return
			}
		}.bind(this));
		this.model.on("updateHistory", function(a) {
			this.view.render()
		}.bind(this));
		this.model.on("add_transaction", function(a) {
			this.view.renderChart();
			this.view.addRow(a.message)
		}.bind(this))
	},
	close : function() {
		this.setStockInfo({
			info : null,
			element : ""
		})
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	},
	getCrrStock : function() {
		return this.model.crrStock
	}
};
SingleCategoryCmp = function() {
};
SingleCategoryCmp.prototype = {
	render : function(a) {
		this.view.createElement(a);
		this.listenEvent()
	},
	setModel : function(a) {
		this.model = a
	},
	setView : function(a) {
		this.view = a;
		this.view.model = this.model
	},
	listenEvent : function() {
		var a = this;
		a.view.on("onDeleted", function() {
			a.model.set("isDeleted", true)
		});
		a.view.on("onSelected", function() {
			a.model.set("isSelected", true)
		});
		a.view.on("onEditCateNameSubmit", function(b) {
			a.model.changeState(b.message)
		});
		a.model.on("onModelChange", function(b) {
			switch (b.message.name) {
			case "isDeleted":
				a.view.remove();
				break;
			case "state":
				a.view.changeState();
				break;
			case "displayValue":
				a.view.changeDisplayValue();
				break;
			case "isSelected":
				a.view.changeBackgroundColor();
				break;
			default:
				a.view.update(b.message);
				break
			}
		})
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	}
};
SingleCategoryModel = function(a) {
	if (a) {
		this.isSelected = a.isSelected;
		this.isDeleted = a.isDeleted;
		this.state = a.state;
		this.value = a.value;
		this.displayValue = a.displayValue;
		this.codes = a.codes;
		this.id = a.id
	} else {
		this.isSelected = false;
		this.isDeleted = false;
		this.state = "normal";
		this.value = "";
		this.displayValue = "";
		this.selectedCode = "";
		this.codes = []
	}
};
SingleCategoryModel.prototype = {
	set : function(a, b) {
		this[a] = b;
		this.emit("onModelChange", {
			name : a,
			value : b
		})
	},
	addCode : function(c) {
		var b = this.codes.indexOf(c);
		var a = this;
		a.set("selectedCode", c);
		if (b > -1) {
			return
		}
		this.codes.push(c);
		this.emit("onModelChange", {
			name : "addCode",
			value : c
		})
	},
	blur : function() {
		this.set("state", "normal");
		this.set("isSelected", false)
	},
	changeState : function(a) {
		switch (this.state) {
		case "normal":
			this.set("state", "editing");
			break;
		case "editing":
			this.set("state", "normal");
			this.set("displayValue", a);
			break;
		default:
			break
		}
	},
	selected : function() {
		this.set("isSelected", true)
	},
	removeCode : function(b) {
		var a = this.codes.indexOf(b);
		if (a > -1) {
			this.codes.splice(a, 1)
		}
		this.emit("onModelChange", {
			name : "removeCode",
			value : b
		})
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	}
};
SingleCategoryView = function() {
};
SingleCategoryView.prototype = {
	createElement : function(c) {
		var a = this.model;
		var b = '<li id ="'
				+ a.id
				+ 'Wrp"  ><a id="'
				+ a.id
				+ 'Link">'
				+ a.displayValue
				+ '</a><input id="'
				+ a.id
				+ 'TextBox" type="text" class="txtbox_dmnd02 hidden" value = "'
				+ a.displayValue
				+ '"/><input id="'
				+ a.id
				+ 'EditBtn" type="image" src="css/img/icoedit.png" class="icoEdit_dmnd" title="Sá»­a tÃªn danh má»¥c"><input id="'
				+ a.id
				+ 'DeleteBtn"  type="image" src="css/img/icoclose.png" class="icoDel_dmnd" title="XÃ³a danh má»¥c"></li>';
		this.element = $(b);
		this.element.appendTo(c);
		this.linkEl = $("#" + a.id + "Link");
		this.textBox = $("#" + a.id + "TextBox");
		this.editBtn = $("#" + a.id + "EditBtn");
		this.deleteBtn = $("#" + a.id + "DeleteBtn");
		if (this.model.isSelected) {
			this.element.addClass("selected")
		}
		this.listenEvent();
		return this.element
	},
	listenEvent : function() {
		var a = this;
		a.deleteBtn.click(function() {
			a.emit("onDeleted")
		});
		a.element.click(function() {
			a.emit("onSelected")
		});
		a.editBtn.click(function() {
			var b = a.textBox.val();
			a.emit("onEditCateNameSubmit", b)
		});
		a.textBox.keypress(function(c) {
			if (c.keyCode == 13) {
				var b = a.textBox.val();
				a.emit("onEditCateNameSubmit", b)
			}
		})
	},
	changeDisplayValue : function() {
		this.linkEl.text(this.model.displayValue)
	},
	changeBackgroundColor : function() {
		if (this.model.isSelected) {
			this.element.addClass("selected")
		} else {
			this.element.removeClass("selected")
		}
	},
	changeState : function() {
		var a = this;
		if (a.model.state == "normal") {
			a.linkEl.css("display", "block");
			a.textBox.css("display", "none");
			a.element.removeClass("editing")
		} else {
			a.textBox.val(this.model.displayValue);
			setTimeout(function() {
				a.textBox.select();
				a.textBox.focus()
			}, 100);
			a.linkEl.css("display", "none");
			a.textBox.css("display", "block");
			a.element.addClass("editing")
		}
	},
	empty : function() {
		this.element.empty()
	},
	appendTo : function(a) {
		this.element.appendTo(a)
	},
	insertBefore : function(a) {
		this.element.insertBefore(a)
	},
	update : function(b) {
		var c = b.name;
		var a = b.value
	},
	remove : function() {
		this.element.remove()
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	}
};
AddCateBoxCmp = function(c, a, b) {
	this.model = c;
	this.view = a;
	this.view.model = this.model;
	this.listenEvent()
};
AddCateBoxCmp.prototype = {
	listenEvent : function() {
		var a = this;
		a.view.on("onFocus", function() {
			a.model.set("isEditing", true)
		});
		a.view.on("onAddCateSubmit", function(b) {
			if (a.model.isEditing) {
				a.model.set("displayAddedCate", b.message);
				a.model.set("isEditing", false)
			} else {
				a.model.set("isEditing", true)
			}
		});
		a.model.on("onModelChange", function(b) {
			a.view.update(b.message)
		})
	},
	getElement : function() {
		return this.view.element
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	}
};
AddCateBoxModel = function() {
	this.isEditing = false;
	this.displayAddedCate = ""
};
AddCateBoxModel.prototype = {
	placeHolderText : "ThÃªm danh muc",
	set : function(a, b) {
		this[a] = b;
		this.emit("onModelChange", {
			name : a,
			value : b
		})
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	}
};
AddCateBoxView = function(a) {
	this.render(a)
};
AddCateBoxView.prototype = {
	render : function(a) {
		var b = '<li><span class="bgtxtbox_dmnd"><input id = "addCateTextBox" class="txtbox_dmnd01" type="text" value="ThÃªm danh má»¥c"></span><input id = "addCateBtn" class="icoAdd_dmnd" type="image" title="Äiá»n tÃªn danh má»¥c vÃ  báº¥m phÃ­m Enter Ä‘á»ƒ hoÃ n thÃ nh" src="css/img/icoeadd.png"></li>';
		this.element = $(b);
		this.element.appendTo(a);
		this.btn = this.element.find("#addCateBtn");
		this.input = this.element.find("#addCateTextBox");
		this.listenEvent()
	},
	appendTo : function(a) {
		this.element.appendTo(a)
	},
	listenEvent : function() {
		var a = this;
		a.btn.click(function() {
			a.emit("onAddCateSubmit", a.input.val())
		});
		a.input.click(function() {
			a.emit("onFocus")
		});
		a.input.keypress(function(b) {
			if (b.keyCode == 13) {
				a.emit("onAddCateSubmit", a.input.val())
			}
		})
	},
	update : function(b) {
		switch (b.name) {
		case "isEditing":
			var a = b.value;
			if (a) {
				this.input.focus();
				this.input.val("")
			} else {
				this.input.val(this.model.placeHolderText)
			}
			break;
		default:
			break
		}
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	}
};
function UserCategoryCmp(a) {
	this.wrp = a.wrp;
	this.btn = a.btn;
	this.btnDefaultDisplay = "Danh má»¥c ngÆ°á»i dÃ¹ng"
}
UserCategoryCmp.prototype = {
	setModel : function(b) {
		var a = this;
		a.model = b;
		a.createElementFromModel();
		a.listenModel()
	},
	setView : function(a) {
		this.view = a
	},
	getCateCmps : function() {
		return this.cateCmps
	},
	listenModel : function() {
		var a = this;
		a.model.on("onAddCategory", function(c) {
			var b = c.message;
			a.createSingleCateCmpFromSingleCateModel(b)
		});
		a.model.on("onChangeCrrCate", function(c) {
			var b = c.message;
			var d = a.btn.children("a");
			if (b.displayValue.length > 0) {
				d.text(b.displayValue)
			} else {
				d.text(a.btnDefaultDisplay)
			}
		})
	},
	createElementFromModel : function() {
		var b = this;
		var c = this.model.addCateBoxModel;
		var d = new AddCateBoxView(this.wrp);
		this.addCateCmp = new AddCateBoxCmp(c, d);
		var a = b.model.getCates();
		$.each(a, function(f, e) {
			b.createSingleCateCmpFromSingleCateModel(e)
		})
	},
	createSingleCateCmpFromSingleCateModel : function(a) {
		var c = new SingleCategoryCmp();
		var b = new SingleCategoryView();
		c.setModel(a);
		c.setView(b);
		c.render(this.wrp);
		c.view.insertBefore(this.addCateCmp.getElement())
	},
	removeCmp : function(b) {
		var a = this.cateCmps.indexOf(cate);
		if (a > -1) {
			this.cateCmps.splice(a, 1)
		}
	}
};
function UserCategoryModel() {
	this.cates = {};
	this.crrCate = this.getDefaultCate();
	this.createAddCateBoxModel()
}
UserCategoryModel.prototype = {
	cateCookieName : "cateCookieName",
	crrCateCookieName : "crrCateCookieName",
	defaultCateNumber : 2,
	oldCookieName : "fullStockLists",
	createAddCateBoxModel : function() {
		var a = this;
		a.addCateBoxModel = new AddCateBoxModel();
		a.addCateBoxModel.on("onModelChange", function(b) {
			if (b.message.name == "displayAddedCate") {
				a.addCategory(b.message.value)
			}
		})
	},
	getCateFromCookie : function() {
		var a = this;
		a.cates = null;
		var b = getCookie(this.cateCookieName).trim();
		try {
			a.cates = JSON.parse(b)
		} catch (c) {
		}
		if (!a.cates || Object.keys(a.cates).length == 0) {
			a.cates = a.retrieveCategoryFromOldCookie()
		} else {
			for ( var d in a.cates) {
				a.cates[d] = new SingleCategoryModel(a.cates[d]);
				a.listenCateEvents(a.cates[d])
			}
		}
		a.saveCateToCookie();
		return a.cates
	},
	retrieveCategoryFromOldCookie : function() {
		var b = this;
		var a = {};
		for (var c = 1; c <= b.defaultCateNumber; c++) {
			var d = b.createCategoryFromOldCookie(c);
			a[d.id] = d
		}
		return a
	},
	saveCateToCookie : function() {
		var a = JSON.stringify(this.cates);
		setCookie(this.cateCookieName, a)
	},
	createCategoryFromOldCookie : function(b) {
		var a = getCookie(this.oldCookieName + b).split([ "," ]);
		var c = new SingleCategoryModel();
		c.set("id", this.generateCategoryId(b));
		c.set("displayValue", "Danh má»¥c " + b);
		c.set("codes", a);
		this.listenCateEvents(c);
		return c
	},
	getCrrCate : function() {
		return this.crrCate
	},
	getCates : function() {
		return this.cates
	},
	generateCategoryId : function(a) {
		var b = new Date();
		var c = "DM" + b.getTime() + a + Math.random();
		c = c.replace(".", "");
		return c
	},
	addCategory : function(a) {
		if (!a || a.length == 0) {
			return
		}
		var b = this.createCategory(a);
		this.cates[b.id] = b;
		this.setCrrCate(b);
		this.emit("onAddCategory", b);
		this.saveCateToCookie();
		return b.id
	},
	removeCategory : function(b) {
		delete this.cates[b];
		var a = this.getFirstCate();
		this.setCrrCate(a);
		this.emit("onDeleteCategory", b);
		this.saveCateToCookie()
	},
	createCategory : function(b) {
		var a = this;
		var c = new SingleCategoryModel();
		c.set("id", a.generateCategoryId(0));
		c.set("displayValue", b);
		c.set("codes", []);
		a.listenCateEvents(c);
		return c
	},
	listenCateEvents : function(b) {
		var a = this;
		b.on("onModelChange", function(d) {
			switch (d.message.name) {
			case "isSelected":
				if (d.message.value) {
					a.setCrrCate(b);
					return
				}
				break;
			case "isDeleted":
				a.removeCategory(b.id);
				break;
			case "addCode":
			case "removeCode":
				a.emit("onChangeCrrCate", a.crrCate);
				break;
			case "selectedCode":
				var c = d.message.value;
				a.emit("onChangeSelectedCode", c);
			default:
				break
			}
			a.saveCateToCookie()
		})
	},
	getCrrCategory : function() {
		return this.crrCate
	},
	setCrrCate : function(a) {
		if (this.crrCate == a) {
			return
		}
		if (this.crrCate && this.crrCate.set) {
			this.crrCate.blur()
		}
		this.crrCate = a;
		a.selected();
		this.saveCrrCateToCookie();
		this.emit("onChangeCrrCate", this.crrCate)
	},
	getDefaultCate : function() {
		var a = new SingleCategoryModel();
		a.set("id", null);
		a.set("displayValue", "");
		a.set("codes", []);
		return a
	},
	getFirstCate : function() {
		var a = Object.keys(this.cates);
		if (a.length > 0) {
			var b = a[0];
			return this.cates[b]
		}
		return this.getDefaultCate()
	},
	retrieveCrrCateByCookie : function() {
		var a = getCookie(this.crrCateCookieName);
		if (!this.cates[a]) {
			a = Object.keys(this.cates)[0]
		}
		this.setCrrCate(this.cates[a])
	},
	saveCrrCateToCookie : function() {
		setCookie(this.crrCateCookieName, this.crrCate.id)
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	on : function(b, a) {
		$(this).bind(b, a)
	}
};
UserStockInfoView = function() {
	this.createSortIndexMap();
	this.crrRenderProcess = null;
	this.crrRenderFloorCode = 0
};
UserStockInfoView.prototype = new StockInfoView();
UserStockInfoView.prototype.getStockCodeHtml = function(d, a, b) {
	var c = "";
	c += '<td class="show-on-mobile" title="" width = 70>';
	c += '<a class="' + a + '" id="' + b + 'sym" title="' + d.code + '">'
			+ d.code + "</a>";
	c += '<span class="show-on-hover">';
	c += "<a class=\"icon icon-delete\" onclick=\"System.emit('onRemoveUserStock','"
			+ d.code + "');\"></a>";
	c += "</span>";
	c += "</td>";
	return c
};
UserStockInfoCmp = function(a) {
	this.name = "userStIf";
	this.isActive = false;
	this.model = new StockInfoModel();
	this.model.cmp = this;
	this.view = new UserStockInfoView();
	this.view.cmp = this;
	this.listenViewEvent();
	this.listenModelEvent();
	this.listenOutsideEvent();
	this.jqueryObject = $(this)
};
UserStockInfoCmp.prototype = new StockInfoComponent({
	isExtendClass : true
});
function StockCategozyManager(b, a, c) {
	this.crrIndex = -1;
	this.preIndex = -1;
	this.el = b;
	this.wrp = a;
	this.stockInfoWrp = c;
	this.stockInfoComponent = new StockInfoComponent();
	this.stockInfoComponent.setWrp($("#mcsTable tbody"));
	this.stockInfoComponent.name = "StockCate";
	this.forceHidden = false;
	this.createFixHeader();
	this.listenEvent();
	this.getDataFromServer()
}
StockCategozyManager.prototype = {
	getDataFromServer : function() {
		System.ajaxHelper.request("priceservice/category/snapshot/",
				function(a) {
					this.addCates(a)
				}.bind(this))
	},
	addCates : function(a) {
		this.cates = a;
		this.renderCates(a)
	},
	renderCates : function(a) {
		var c = "";
		for ( var b in a) {
			c += this.getCateHtml(a[b], b)
		}
		this.wrp.html(c);
		this.listenItemClick(a)
	},
	getCateHtml : function(b, a) {
		var c = "";
		c += '<li id="cate' + a + '">';
		c += "<a>" + b.cateName + "</a>";
		c += "</li>";
		return c
	},
	listenItemClick : function(a) {
		for ( var b in a) {
			$("#cate" + b).on("click touchstart", function(c) {
				this.openList(c)
			}.bind(this, b))
		}
	},
	openList : function(a) {
		this.hide(true);
		System.tabManager.setTab(2);
		this.removeOldStocks();
		this.preIndex = this.crrIndex;
		this.crrIndex = a;
		setTimeout(function() {
			this.renderNewStocks()
		}.bind(this), 100)
	},
	removeOldStocks : function() {
		this.stockInfoWrp.html("")
	},
	renderNewStocks : function() {
		this.stockInfoComponent
				.setStockCodes(this.cates[this.crrIndex].stockCodes);
		this.fixHeader.syncWidth()
	},
	updateStock : function(b) {
		if (this.crrIndex === -1) {
			return
		}
		var a = this.cates[this.crrIndex].stockCodes;
		if (a.indexOf(b.code) == -1) {
			return
		}
		this.stockInfoComponent.updateStock(b)
	},
	add : function(a) {
		this.stockInfoComponent.push(System.stockInfoComponent.getStockInfo(a),
				this.stockInfoWrp)
	},
	remove : function(a) {
		this.stockInfoComponent.remove(a)
	},
	active : function() {
		this.stockInfoComponent.active()
	},
	deActive : function() {
		this.hide(true);
		this.stockInfoComponent.deActive()
	},
	listenEvent : function() {
		System.tabManager.on("changeTab", function(a) {
			var b = a.message.newTab;
			if (b == 2) {
				this.active()
			}
		}.bind(this));
		System.tabManager.on("beforeChangeTab", function(a) {
			var b = a.message.newTab;
			if (b != 2) {
				this.deActive()
			}
		}.bind(this));
		this.el.click(function() {
			this.show()
		}.bind(this));
		this.el.hover(function() {
			if (Browser.isTablet()) {
				return
			}
			this.show()
		}.bind(this));
		this.el.mouseout(function() {
			this.hide()
		}.bind(this));
		this.wrp.mouseout(function() {
			if (Browser.isTablet()) {
				return
			}
			this.hide()
		}.bind(this));
		this.wrp.mouseover(function() {
			this.show()
		}.bind(this))
	},
	show : function() {
		if (this.forceHidden) {
			return
		}
		clearTimeout(this.hideEvent);
		this.wrp.css({
			display : "block"
		})
	},
	hide : function(a) {
		clearTimeout(this.hideEvent);
		if (a) {
			this.forceHidden = true;
			this.wrp.css({
				display : "none"
			});
			setTimeout(function() {
				this.forceHidden = false
			}.bind(this), 500);
			return
		}
		if (this.forceHidden) {
			return
		}
		var b = 100;
		if (Browser.isTablet()) {
			b = 1000
		}
		this.hideEvent = setTimeout(function() {
			this.wrp.css({
				display : "none"
			})
		}.bind(this), b)
	},
	predictATO : function(a) {
		this.stockInfoComponent.predictATO(a)
	},
	predictATC : function(a) {
		this.stockInfoComponent.predictATC(a)
	},
	clearATCPrice : function(a) {
		this.stockInfoComponent.clearATCPrice(a)
	},
	clearATOPrice : function(a) {
		this.stockInfoComponent.clearATOPrice(floorCode)
	},
	createFixHeader : function() {
		this.fixHeader = new FixHeader({
			originalElement : $("#mcsTable"),
			tabIndex : 2,
			cssAtr : {
				top1 : "264px",
				top2 : "84px",
				left : 0,
				windowScrollTop1 : 52,
				windowScrollTop2 : 80
			}
		});
		this.fixHeader.active()
	}
};
function StockAutoComplete(b, a) {
	this.el = b;
	this.addBtn = a;
	this.stocks = [];
	this.handleBodyKeydown();
	this.isSet = false;
	this.getDataFromServer()
}
StockAutoComplete.prototype = {
	setSource : function(a) {
		if (this.isSet) {
			return
		}
		for (var b = 0; b < a.length; b++) {
			this.stocks.push({
				value : a[b].code,
				label : a[b].code + " - " + a[b].companyName
			})
		}
		this.isSet = true;
		this.el.autocomplete({
			source : function(e, c) {
				var d = $.ui.autocomplete.escapeRegex(e.term.toUpperCase());
				var f = new RegExp("^[A-z0-9]*" + d, "i");
				c($.grep(this.stocks, function(g) {
					return f.test(g.value)
				}))
			}.bind(this),
			select : function(d, e) {
				var c = $.trim(e.item.value.toUpperCase());
				this.crrStock = this.emit("select", c);
				setTimeout(function() {
					this.el.val("")
				}.bind(this), 100)
			}.bind(this)
		});
		this.el.keyup(function(d) {
			if (d.keyCode == 13) {
				var c = $.trim(this.el.val().toUpperCase());
				this.crrStock = this.emit("select", c);
				this.el.val("");
				$("#ui-id-1").hide()
			}
		}.bind(this));
		this.addBtn.click(function() {
			var c = $.trim(this.el.val().toUpperCase());
			this.crrStock = this.emit("select", c);
			this.el.val("");
			$("#ui-id-1").hide()
		}.bind(this))
	},
	getDataFromServer : function() {
		System.ajaxHelper.request("priceservice/company/snapshot/",
				function(a) {
					this.setSource(a);
					StockBasicInfosManager.setInfos(a)
				}.bind(this))
	},
	emit : function(b, a) {
		$(this).trigger({
			type : b,
			message : a,
			time : new Date()
		})
	},
	handleBodyKeydown : function() {
		$("body").keydown(function() {
		}.bind(this))
	},
	on : function(b, a) {
		$(this).bind(b, a)
	}
};
var curr = 0;
$(document).ready(function() {
	var a = "ajax/news/getHotNews.shtml";
	getHotNews(a);
	setInterval(function() {
		getHotNews(a)
	}, 600000)
});
function getHotNews(a) {
	$.ajax({
		url : a,
		type : "POST",
		dataType : "json",
		success : function(e) {
			var c = e.model.jsonObjectString;
			var d = JSON.parse(c);
			var b = d.listNews;
			slideHotNews(b)
		}
	})
}
function slideHotNews(c) {
	if (c == null) {
		return
	}
	var b = '<span class="news-ticker hide-on-tablet"><strong>Tin má»›i nháº¥t: </strong><span id="newsLink">&nbsp;&nbsp;</span></span>';
	for (var a = 0; a < c.length; a++) {
		var e = c[a];
		var g = e.newsHeader.replace(/[\:\%\:\&\^\#\@\!\~\;\.\,\"'\?\/]/g, "");
		g = g.replace(/\s/g, "-");
		var d = e.newsHeader.replace(/['\"]/g, "");
		if (d.split(" ").length > 12) {
			d = d.split(" ").slice(0, 12).join(" ") + " ..."
		}
		var f = "";
		if (e.newsType == "MacVN") {
			f = "/tin-trong-nuoc/"
		} else {
			if (e.newsType == "MacWorld") {
				f = "/tin-quoc-te/"
			} else {
				if (e.newsType == "DailyReport") {
					f = "/nhan-dinh-thi-truong/"
				} else {
					if (e.newsType == "VNDIRECT") {
						f = "/vndirect/tin-vndirect/"
					}
				}
			}
		}
		b += '<input type="hidden" id="linkDetail'
				+ a
				+ '" value="<a href=\''
				+ PORTAL_URL
				+ f
				+ g
				+ "-"
				+ e.newsId
				+ ".shtml' target='_blank' >"
				+ d
				+ "</a><span style='margin-left: 5px;' class='icon-new'></span>\"/>"
	}
	b += "</span>";
	$("#newWrp").html(b);
	slideVNDSNews()
}
function slideVNDSNews() {
	var a = [];
	a.push($("#linkDetail0").val());
	a.push($("#linkDetail1").val());
	a.push($("#linkDetail2").val());
	a.push($("#linkDetail3").val());
	a.push($("#linkDetail4").val());
	a.push($("#linkDetail5").val());
	a.push($("#linkDetail6").val());
	a.push($("#linkDetail7").val());
	a.push($("#linkDetail8").val());
	a.push($("#linkDetail9").val());
	slideNews(a);
	intervalID = setInterval(function() {
		slideNews(a)
	}, 120000);
	$("#newsLink").hover(function() {
		if (intervalID != "") {
			clearInterval(intervalID);
			intervalID = ""
		}
	}, function() {
		if (intervalID == "") {
			slideNews(a);
			intervalID = setInterval(function() {
				slideNews(a)
			}, 60000)
		}
	})
}
function slideNews(a) {
	$("#newsLink").fadeOut(1000, function() {
		$("#newsLink").html(a[curr % a.length])
	});
	$("#newsLink").fadeIn("fast");
	curr += 1
}
function slideToPrevNew() {
};
$(document).ready(function() {
	var a = $(".btn-linkv3");
	a.click(function() {
		ga("send", {
			hitType : "pageview",
			page : "invite-to-v3/click-button",
			title : "NgÆ°á»i dÃ¹ng click chuyá»ƒn sang v3"
		})
	})
});