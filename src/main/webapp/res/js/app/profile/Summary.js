Summary = {
	init : function() {
		this.config = {};
		this.initObject();
		if (undefined != window.mode) {
			switch (mode) {
			case "active-flex":
				this.showActiveFlex();
				break;
			case "data-miss":
				this.showMissingData();
				break;
			}
		}
		this.renderChart();
	},
	
	updateChart: function(data){
		this.config.chart.series[0].setData([data.sumStock, data.sumBond, 0, 0]);
		$('#assetInvested').html(data.sum.format());
	},
	
	renderChart: function(){
		Highcharts.setOptions({
		    colors: ['#F50101', '#242424', '#595959', '#A6A6A6', '#64E572', '#FF9655', '#CB2326', '#6AF9C4']
	    });
	    this.config.chart = new Highcharts.Chart({
	        chart: {
	            renderTo: 'container',
	            type: 'pie',
	            width: 460,
	            height: 320
	        },
	        credits: {
	            enabled: false
	        },
	        title:{
	            text:''
	        },
	        plotOptions: {
	            pie: {
	                innerSize: '50%'
	            },
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    formatter: function() {
	                        return Math.round(this.percentage*100)/100 + ' %';
	                    },
	                    distance: -40,
	                    color:'white',
	                    style: {"color": "contrast", "fontSize": "11px", "textShadow": "none", "fontWeight":"normal"}
	                }
	            }
	        },
	        series: [{
	            data: [
	                ['Cổ phiếu', 0],
	                ['Trái phiếu', 0],
	                ['Quỹ', 0],
	                ['SP Cấu trúc', 0]
	                ]}]
	    },
	                                     
	    function(chart) { // on complete
	        var textX = chart.plotLeft + (chart.plotWidth  * 0.5);
	        var textY = chart.plotTop  + (chart.plotHeight * 0.5);

	        var span = '<span id="pieChartInfoText" class="personal-pie-title">';
	        span += '<span class="up-title">Tài sản đã phân bổ</span><br>';
	        span += '<span id="assetInvested" class="lower-title">Lower</span>';
	        span += '</span>';

	        $("#addText").append(span);
	        span = $('#pieChartInfoText');
	        span.css('left', textX + (span.width() * -0.5));
	        span.css('top', textY + (span.height() * -0.5));
	    });
	},

	showMissingData : function() {
		$("#missingData").fadeTo(2000, 500).slideUp(500, function() {
		});
	},

	showActiveFlex : function() {
		var html = tmpl("activeFlexTmpl");
		this.settingContent.html(html);
	},

	activeFlex : function() {
		$.ajax({
			url : "/api/app/active-flex",
			method : "POST",
			success : function(data) {
				console.log(data);
			}
		});
	},

	initObject : function() {
		this.settingContent = $('#setting-content');
	}
};

$(document).ready(function() {
	Summary.init();
});