Account = {

	init : function() {
		this.config = {
			cache : {}
		};
		this.validator = $('#changePasswordForm')
				.validate(
						{
							rules : {
								password : "required",
								newpassword : {
									required : true,
									passwordrule : true
								},
								newpasswordcf : {
									required : true,
									equalTo : "#newpassword"
								}
							},
							messages : {
								password : "Vui lòng nhập mật khẩu hiện tại",
								newpassword : {
									required : "Vui lòng nhập mật khẩu mới",
									passwordrule : "Mật khẩu phải chứa ít nhất một chữ cái, số và dài hơn 6 ký tự",
								},
								newpasswordcf : {
									required : "Vui lòng nhập mật lại khẩu mới",
									equalTo : "Mật khẩu không khớp"
								}
							}
						});
	},

	change : function(field) {
		Loading.show();
		var obj = $('#' + field);
		var val = obj.val();
		var data = {};
		data[field] = val;
		$.ajax({
			url : "api/user/update",
			method : "POST",
			data : data,
			success : function(data) {
				Loading.hide();
			},
			failure : function() {
				Loading.hide();
			}
		});
	},

	changePassword : function(e) {
		e.preventDefault();
		e.stopPropagation();
		if (!this.validator.form()) {
			return;
		}
		var password = $('#password').val();
		var newpassword = $('#newpassword').val();
		var newpasswordcf = $('#newpasswordcf').val();
		this.sendPasswordChange(password, newpassword);
	},

	showError : function(obj, msg) {
		obj.addClass("state-error");
		obj.find("input").addClass("invalid");
		obj.parent().append('<em class="invalid">' + msg + '<em>');
	},

	showSuccess : function(obj, msg) {
		obj.addClass("state-success");
		obj.find("input").addClass("valid");
	},

	hideError : function(obj) {
		obj.removeClass("state-error");
		obj.find("input").removeClass("invalid");
		obj.parent().find("em").remove();
	},
	getMe : function(callback, error) {
		$.ajax({
			url : "api/user/me",
			method : "GET",
			success : function(data) {
				if (data.status == 0) {
					if (callback)
						callback(data.data);
				} else {
					if (error)
						error(data);
				}
			},
			failure : function() {
				if (error)
					error();
			}
		});
	},

	sendPasswordChange : function(password, newPassword) {
		var _self = this;
		Loading.show();
		$
				.ajax({
					url : "api/account/change-pwd",
					method : "POST",
					data : {
						password : password,
						newpassword : newPassword
					},
					success : function(data) {
						switch (data.status) {
						case 4:
							_self.validator
									.showErrors({
										"password" : "Mật khẩu phải chứa ít nhất một chữ cái, số và dài hơn 6 ký tự"
									});
						case 3:
							_self.validator.showErrors({
								"password" : "Mật khẩu hiện tại không đúng"
							});
							break;
						case 1:
							_self.validator.showErrors({
								"password" : "Bạn thực hiện thao tác quá nhanh"
							});
							break;
						case 0:
							$(".alert-success").fadeTo(2000, 500).slideUp(500,
									function() {
									});
							break;
						}
						Loading.hide();
					},
					failure : function() {
						alert("Error");
						Loading.hide();
					}
				});
	},

	edit : function() {
		this.setNewTcbsUser(tcbsUser);
		if (tcbsUser.address.length > 0) {
			tcbsUser.haveAddress = true;
		}
		if (tcbsUser.identifications.length > 0) {
			tcbsUser.noIdentifications = false;
			for (var i = 0; i < tcbsUser.identifications.length; i++) {
				tcbsUser.identifications[i].idDateFormated = moment(
						tcbsUser.identifications[i].idDate,
						"YYYY/MM/DD hh:mm:ss").format('DD/MM/YYYY');
			}
		}
		var html = HandleBarService.template("account-template", tcbsUser);
		$('#account-information').html(html);
		$('.profile-edit .control').removeClass('mode-2').addClass('mode-1');
	},

	cancelEdit : function() {
		console.log(tcbsUser);
		if (tcbsUser.address.length > 0) {
			tcbsUser.haveAddress = true;
		}
		if (tcbsUser.identifications.length > 0) {
			tcbsUser.noIdentifications = false;
		}
		var html = HandleBarService.template("account-view-template", tcbsUser);
		$('#account-information').html(html);
		$('.profile-edit .control').removeClass('mode-1').addClass('mode-2');
	},

	setNewTcbsUser : function(data) {
		tcbsUser = data;
		if (tcbsUser.birthday) {
			tcbsUser.birthdayFormated = moment(tcbsUser.birthday,
					"YYYY/MM/DD hh:mm:ss").format("DD/MM/YYYY");
		}else{
			tcbsUser.birthdayFormated = '';
		}
	},

	update : function() {
		var data = {
			fullname : $('#edit-fullname').val(),
			birthday : $('#edit-birthday').val(),
			gender : $('#edit-gender').val(),
			email : $('#edit-email').val(),
			phone : $('#edit-phone').val(),
			address : $('#edit-address').val()
		};
		if($('#edit-idnumber')){
			data.iddate = $('#edit-iddate').val();
			data.idnumber = $('#edit-idnumber').val();
			data.idplace = $('#edit-idplace').val(); 
		}
		Loading.show();
		$('.error').addClass('hide');
		var _self = this;
		$
				.ajax({
					url : "api/user/update",
					method : "POST",
					data : data,
					success : function(data) {
						Loading.hide();
						switch (data.msg) {
						case "SUCCESS":
							_self.getMe(function(data) {
								_self.setNewTcbsUser(data);
								_self.cancelEdit();
							}, function() {
								window.location.href = window.location.href;
							});
							break;
						case "DATE_FORMAT_ERROR":
							$('#birthday-error').html('Ngày sinh chưa đúng định dạng').removeClass("hide");
							break;
						case "PHONE_DUPLICATED":
							$('#phone-error').html('Số điện thoại đã được sử dụng').removeClass("hide");
							break;
						case "IDNUMBER_DUPLICATED":
							$('#id-error')
									.html(
											"Chứng minh nhân dân đã tồn tại hoặc thiếu thông tin")
									.removeClass('hide');
							break;
						case "IDDATE_ERROR":
							$('#id-error').html("Ngày cấp CMND chưa đúng")
									.removeClass('hide');
							break;
						case "EMAIL_DUPLICATED":
							$('#email-error').html("Email này đã được sử dụng")
									.removeClass('hide');
							break;
						}
					},
					failure : function() {
						Loading.hide();
					}
				});
	}
};

$(document).ready(function() {
	Account.init();
});