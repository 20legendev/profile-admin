Profile = {
	init : function() {
		var _self = this;
		var main = $('#profile-main');
		main.delegate("a[data-pmb-action=edit]", "click", function() {
			_self.toggleDisplay(true, $(this));
		});
		main.delegate("[data-pmb-action=reset]", "click", function() {
			_self.toggleDisplay(false, $(this));
		});
		main.delegate(".form-control[type=text]", "focus", function() {
			$(this).closest(".fg-line").addClass("fg-toggled");
		});
		main.delegate(".form-control[type=text]", "focusout", function() {
			$(this).closest(".fg-line").removeClass("fg-toggled");
		});
		main.delegate("[data-pmb-action=save]", "click", function() {
			_self.save($(this));
		});
		main.delegate(".pmbb-edit input", "keyup", function(e) {
			if (e && e.keyCode == 13) {
				_self.save($(this));
			}
		});
	},

	toggleDisplay : function(isDisplay, obj) {
		if (isDisplay) {
			obj.closest(".pmb-block").addClass('toggled');
		} else {
			obj.closest(".pmb-block").removeClass('toggled');
		}
	},

	save : function(obj) {
		var parent = obj.closest(".pmbb-edit");
		var child = parent.find("[name]");
		var data = {};
		var type, tmp;
		for (var i = 0; i < child.length; i++) {
			tmp = $(child[i]);
			type = tmp.attr("tcbs-type");
			if (!data[tmp.attr("name")]) {
				switch (type) {
				case 'keyval':
					data[tmp.attr("name")] = (tmp.attr("id") == undefined ? -1
							: tmp.attr("id"))
							+ "/" + tmp.val();
					break;
				default:
					data[tmp.attr("name")] = tmp.val();
					break;
				}
			} else {
				switch (type) {
				case 'keyval':
					data[tmp.attr("name")] += ":"
							+ (tmp.attr("id") == undefined ? -1 : tmp
									.attr("id")) + "/" + tmp.val();
					break;
				default:
					data[tmp.attr("name")] = tmp.val();
					break;
				}
			}
		}
		var action = parent.attr('action');
		var _self = this;
		Loading.show();
		$.ajax({
			url : action,
			method : "POST",
			data : data,
			success : function() {
				_self.update();
				_self.toggleDisplay(false, obj);
				Loading.hide();
			},
			failure : function() {
				alert("Error");
				Loading.hide();
			}
		})
	},

	update : function() {
		var _self = this;
		Loading.show();
		$.ajax({
			url : 'api/account/me',
			method : 'POST',
			success : function(data) {
				if (data.status == 0) {
					_self.showUpdate(data.data);
				} else {
					window.location.reload();
				}
				Loading.hide();

			},
			failure : function() {
				window.location.reload();
			}
		})
	},

	showUpdate : function(data) {
		if (data.firstname) {
			$('[tcbs-data-field=fullname]').html(
					data.lastname + ' ' + data.firstname);
		}
		if (data.gender != null) {
			$('[tcbs-data-field=gender]')
					.html(
							data.gender == 0 ? "Nữ" : data.gender == 1 ? "Nam"
									: "Khác");
		}
		if (data.birthday != null) {
			var now = moment(data.birthday, 'YYYY-MM-DD').format('DD/MM/YYYY');
			$('[tcbs-data-field=birthday]').html(now);
		}
		if (data.relationship != null) {
			var name = data.relationship == 0 ? "Độc thân"
					: data.relationship == 1 ? "Đã có gia đình"
							: data.relationship == 2 ? "Ly hôn" : "Khác ";
			$('[tcbs-data-field=relationship]').html(name);
		}
	},

	addMore : function(obj) {
		var data = obj.parent().children('.add-more-content');

		console.log(data);
	}
};

$(document).ready(function() {
	Profile.init();
})
