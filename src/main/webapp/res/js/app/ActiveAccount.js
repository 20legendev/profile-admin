ActiveAccount = {
	startTimer : function(duration, display) {
		var timer = duration, minutes, seconds;
		tt = setInterval(function() {
			minutes = parseInt(timer / 60, 10);
			seconds = parseInt(timer % 60, 10);

			minutes = minutes < 10 ? "0" + minutes : minutes;
			seconds = seconds < 10 ? "0" + seconds : seconds;

			display.textContent = minutes + ":" + seconds;

			if (--timer < 0) {
				timer = duration;
			}
		}, 1000);
	},
	
	
	validateEmail : function(tag, e) {
		var email = $(tag).val();
		var isValidate = Utils.validateEmail(email);
		if (isValidate) {
			$("#passwordForm").addClass('status-1').removeClass('status-0');
			Password.hideError();
		} else {
			$("#passwordForm").removeClass('status-1').removeClass('status-0');
		}
		if (e && e.keyCode == 13) {
			if (isValidate) {
				this.requestChange();
			} else {
				Password.showError("Email chưa đúng định dạng");
			}
		}
	},

	requestActive : function() {
		var email = $("#emailInput").val();
		$("#passwordForm").removeClass('status-1').removeClass('status-0');
		Loading.show('.loading-small');
		Password.hideError();
		$.ajax({
			url : "email-kich-hoat.html",
			method : "POST",
			data : {
				email : email
			},
			success : function(e) {
				Loading.hide('.loading-small');
				if (e.status != 0) {
					ActiveAccount.showError();
					return;
				}
				ActiveAccount.showSuccessCard(email);
			},
			failure : function() {
				Loading.hide('.loading-small');
				ActiveAccount.hideError();
			}
		})
	},
	
	requestOTP : function() {
		var trans = $("#trans").val();
		$.ajax({
			url : "requestOTP.html",
			method : "POST",
			data : {
				trans : trans
			},
			success : function(response) {
				$("#trans").val(response);
				clearInterval(tt);
				ActiveAccount.runTimer(3);
			},
			failure : function() {
				Loading.hide('.loading-small');
				ActiveAccount.hideError();
			}
		})
	},

	showSuccessCard : function(email) {
		$('#forgotContent').hide();
		$('.success-card').show();
		setTimeout(function() {
			$('.success-card').addClass('show');
			$('#resend-email').attr('href',
					$('#resend-email').attr('href') + email);
		}, 10);
		var provider = Utils.detectEmailProvider(email);
		switch (provider) {
		case "GOOGLE":
			$("#email-link").attr('href', "http://mail.google.com");
			break;
		case "YAHOO":
			$("#email-link").attr('href', "http://mail.yahoo.com");
			break;
		case "MS":
			$("#email-link").attr('href', "http://outlook.com");
			break;
		default:
			$("#email-link").hide();
			break;
		}
	},

	showError : function(content) {
		$('#forgotContent .error-message').html(
				content ? content : "Không tìm thấy email của bạn")
		$('#forgotContent .error-message').addClass('show');
	},

	hideError : function() {
		$('.error-message').removeClass('show');
	},
	
	runTimer : function(time){
		 var fiveMinutes = 60 * time,
		 display = document.querySelector('#time');
		 ActiveAccount.startTimer(fiveMinutes, display);
	},
};
window.onload = function () {
	ActiveAccount.runTimer(3);
};