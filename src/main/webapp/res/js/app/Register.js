Register = {

	init : function() {
		this.config = {
			check : {}
		};
		this.bindEvent();
	},

	showLoading : function(isShow) {
		if (isShow) {
			$('.loading').show();
		} else {
			$('.loading').hide();
		}
	},

	bindEvent : function() {
		var _self = this;
		$('input').on('keyup', function(e) {
			if (e.keyCode == 13) {
				_self.addAccount();
			}
		});
		if ($('#bankplace').val() != -1) {
			Register.changeProvince($('#bankplace'));
		}
	},

	changeCheckbox : function(obj) {
		if (obj.prop('checked')) {
			var group = "input:checkbox[name='" + obj.attr("name") + "']";
			$(group).prop('checked', false);
			obj.prop('checked', true);
		} else {
			obj.prop('checked', false);
		}
	},

	changeGender : function(obj) {
		Register.changeCheckbox(obj);
		var gender = this.getGender();
		if (gender == undefined) {
			this.showError("gender", true, "Giới tính bị để trống");
		} else {
			this.showError("gender", false);
		}
	},

	addAccount : function() {
		var obj = $('#confirm');
		if (!obj.prop('checked')) {
			obj.parent().addClass('error-text');
		} else {
			obj.parent().removeClass('error-text');
		}
		this.showLoading(true);
		var val = {}, error = {};
		$('.error-msg').removeClass('show');
		error = this.getValue(['lastname', 'firstname', 'email', 'phone', 'birthday', 'phone', 'address', 'idnumber', 'iddate', 'idplace', 'province'], error, val);
		if (!error['birthday']) {
			if (!Utils.validateSmallerDate(val['birthday'])) {
				error['birthday'] = "Ngày lớn hơn ngày hiện tại";
			}
		}
		val['gender'] = this.getGender();
		if (val['gender'] == null) {
			error['gender'] = 1;
		}
		val['captcha'] = $('#g-recaptcha-response').val();
		if (this.config.isbankaccount == undefined)
			this.config.isbankaccount = 1;
		val['isbank'] = this.config.isbankaccount;
		if (val['isbank']) {
			error = this.getValue(['bankaccountnumber', 'bankaccountname', 'bankplace', 'bankname', 'bankcode'], error, val);
		}
		if (!Utils.validateEmail(val["emailInput"])) {
			$('#emailInput').addClass('error');
		}
		val['mode'] = "SELF_OPEN";
		if (havePassword == 1) {
			if (!this.validatePassword($('#password'))) {
				error['password'] = 'Mật khẩu có ký tự trắng hoặc ít hơn 6 ký tự';
			} else if (!this.validatePassword($('#password-retype'))) {
				error['password-retype'] = 'Mật khẩu không trùng khớp';
			} else {
				val['password'] = $('#password').val();
			}
		} else {
			val['mode'] = "RM_OPEN";
		}
		if (Object.keys(error).length > 0) {
			this.renderError(error);
			this.showLoading(false);
			return;
		}
		val['bankbranch'] = $("#bankcode option[value=" + val['bankcode'] + "]").html();
		val['bankname'] = $("#bankname option[value=" + val['bankname'] + "]").html();
		val['birthday'] = moment(val['birthday'], 'DD/MM/YYYY').format('YYYY/MM/DD');
		val['iddate'] = moment(val['iddate'], 'DD/MM/YYYY').format('YYYY/MM/DD');
		console.log(val);
		this.sendRegister(val);
	},

	getValue : function(arr, error, val) {
		for (var i = 0; i < arr.length; i++) {
			obj = $('#' + arr[i]);
			val[arr[i]] = Utils.trim(obj.val());
			if (!val[arr[i]] || val[arr[i]].length == 0 || val[arr[i]] == -1) {
				error[arr[i]] = "001";
			}
		}
		return error;
	},

	sendRegister : function(obj) {
		var _self = this;
		$('#captcha').removeClass("show");
		$('.form-group input').removeClass('error');
		$('.form-group select').removeClass('error');
		this.showLoading(true);
		$.ajax({
			url : 'api/account/open',
			data : obj,
			method : "POST",
			success : function(data) {
				_self.showLoading(false);
				_self.checkRegisterError(data);
			},
			failure : function() {
				grecaptcha.reset();
				_self.showLoading(false);
			}
		});
	},

	showLoading : function(isShow) {
		if (isShow) {
			Loading.show();
		} else {
			Loading.hide();
		}
	},

	checkRegisterError : function(data) {
		switch (data.status) {
		case 0:
			if (window.redirecturl != undefined) {
				window.location.href = redirecturl;
			} else {
				window.location.href = "mo-tai-khoan-thanh-cong.html";
			}
			break;
		default:
			switch (data.msg) {
			case 'CAPTCHA_ERROR':
				$('#captcha').addClass("show");
				break;
			case 'ID_DUPLICATED':
				this.showError('idnumber', true, 'Chứng minh thư đã được sử dụng');
				break;
			case 'NOT_AUTHORIZED':
				top.location.href = 'logout.html';
				break;
			case 'SYSTEM_ERROR':
				$('#myModalLabel-default').html("Lỗi hệ thống");
				$('#errorModalContent-default').html('Hệ thống đang tiến hành bảo trì. Vui lòng thử lại sau ít phút. Cảm ơn bạn!');
				$('#basicModal').modal();
				break;
			default:
				this.showErrorToUser(data.data);
				break;
			}
			grecaptcha.reset();
			break;
		}
	},

	chooseConfirm : function(idNumber) {
		$('#idnumber').val(idNumber);
	},

	renderError : function(err) {
		var obj;
		for (var key in err) {
			obj = $('.error-msg[for=' + key + ']');
			obj.addClass('show');
			var errMsg = err[key] != "001" ? err[key] : "Trống hoặc không đúng định dạng";
			obj.html(errMsg);
		}
		this.showLoading(false);
	},

	getGender : function() {
		var gender0 = $('#gender0').prop('checked');
		var gender1 = $('#gender1').prop('checked');
		var gender2 = $('#gender2').prop('checked');
		var gender = gender0 ? 0 : ( gender1 ? 1 : gender2 ? 2 : undefined);
		return gender;
	},

	changeProvince : function(obj, holder) {
		this.validateProvince(obj);
		$('#branch').html('<option value=-1>Chọn chi nhánh</option>');
		this.showLoading(true);
		var _self = this;

		ProfileService.getBankByProvince(obj.val(), function(data) {
			var bank = _self.sortBank(data);
			var bankWrapper = $('#bankname');
			bankWrapper.html('');
			bankWrapper.append("<option value=-1>Chọn ngân hàng</option>");
			for (var i = 0; i < bank.length; i++) {
				bankWrapper.append("<option value='" + bank[i].banksys + "'>" + bank[i].bankname + "</option>");
			}
			if (restorebank.mybank) {
				$('#bankname').val(restorebank.mybank);
				_self.changeBank($('#bankname'));
			}
			Loading.hide();
		}, function() {
			Loading.hide();
		});
	},

	sortBank : function(data) {
		var arr = ['310', '201', '202', '203', '204'];
		var result = data.sort(function(a, b) {
			if (arr.indexOf(a.banksys) >= 0)
				return -1;
			return 1;
		});
		return result;
	},

	changeBank : function(obj) {
		var _self = this;
		ProfileService.getBankBranch($('#bankplace').val(), obj.val(), function(data) {
			var bankWrapper = $('#bankcode');
			bankWrapper.html('<option value=-1>Chọn chi nhánh</option>');
			for (var i = 0; i < data.data.length; i++) {
				bankWrapper.append("<option value='" + data[i].bankcode + "'>" + data[i].branchname + "</option>");
			}
			if (restorebank.bankcode) {
				$('#bankcode').val(restorebank.bankcode);
			}
		});
		this.showError('bankname', false);
	},
	validateName : function(obj) {
		var val = obj.val();
		val = Utils.trim(val);
		if (val.length == 0 || Utils.validateSpecialCharacter(val)) {
			obj.addClass("error");
		} else {
			this.showError(obj.attr('id'), false);
		}
	},

	validatePassword : function(obj) {
		var password = obj.val();
		var id = obj.attr('id');
		this.showError(id, false);
		if (password.indexOf(" ") >= 0) {
			this.showError(id, true, "Mật khẩu có chứa ký tự trắng");
			return false;
		}
		if (password.length < 6) {
			this.showError(id, true, "Mật khẩu ít nhất 6 ký tự");
			return false;
		}
		if (id == 'password-retype') {
			var p = $('#password').val();
			if (p != password) {
				this.showError(id, true, "Mật khẩu không trùng khớp");
				return false;
			}
		}
		return true;
	},

	validateEmail : function(obj) {
		var val = obj.val();
		if (!Utils.validateEmail(val)) {
			this.showError(obj.attr('id'), true, "Email không đúng định dạng ");
		} else {
			this.showError(obj.attr('id'), false);
		}
	},

	validatePhone : function(obj) {
		var val = obj.val();
		if (!Utils.validatePhone(val)) {
			obj.addClass("error");
		} else {
			this.showError(obj.attr('id'), false);
		}
	},

	validateDate : function(obj, year, text) {
		if (!Utils.validateSmallerDate(obj.val(), year)) {
			text = text ? text : "Ngày sai định dạng";
			this.showError(obj.attr('id'), true, text);
		} else {
			this.showError(obj.attr('id'), false);
		}
	},

	validateProvince : function(obj) {
		var val = obj.val();
		if (val == -1) {
			this.showError(obj.attr('id'), true);
		} else {
			this.showError(obj.attr('id'), false);
		}
	},

	changeMoneyInput : function(obj) {
		this.changeCheckbox(obj);
		this.config.isbankaccount = $('#isMoneyInput-1').prop('checked') ? 1 : 0;
		if (this.config.isbankaccount) {
			$('.showbank').show();
		} else {
			$('.showbank').hide();
		}
	},

	confirm : function(obj) {
		if (obj.prop('checked')) {
			obj.parent().removeClass('error-text');
		}
	},

	showErrorToUser : function(data) {
		var tmp;
		for (var i = 0; i < data.length; i++) {
			tmp = data[i];
			if (tmp.field) {
				this.showError(tmp.field, true, tmp.code);
			} else {
				if ($('#' + tmp.objectName)) {
					this.showError(tmp.objectName, true, tmp.defaultMessage);
				}
				if (tmp.objectName == "error") {
					switch (tmp.defaultMessage) {
					case "001":
						$('#AccountExistModal').modal();
						break;
					case "002":
						$('#AccountExistEmailModal').modal();
						break;
					case "003":
						$('#AccountExistPhoneModal').modal();
						break;
					}
				}
			}
		}
		var keys = Object.keys(data);
		for (var i = 0; i < keys.length; i++) {
			this.showError(keys[i], true, data[keys[i]]);
			if ($('#' + keys[i])) {
				$('#' + keys[i]).addClass('error');
			}
		}
	},

	showError : function(id, isShow, text) {
		if (isShow) {
			$('#' + id).addClass('error');
			$('div[for=' + id + ']').addClass('show').html( text ? text : 'Không đúng định dạng');
		} else {
			$('#' + id).removeClass('error');
			$('div[for=' + id + ']').removeClass('show');
		}
	},

	findCustomer : function(e) {
		if (e) {
			if (e.keyCode != 13)
				return;
		}
		var idNumber = $('#idnumber').val();
		var fullName = $('#fullname').val();
		this.showLoading(true);
		var _self = this;
		$.ajax({
			url : 'api/account/check-id',
			method : "POST",
			data : {
				idnumber : idNumber,
				fullname : fullName
			},
			success : function(data) {
				if (data.status == 0) {
					if (data.data != null) {
						var html;
						var obj = $('#customerFilterBody');
						$('#totalCount').html(data.data.length);
						obj.html('');
						for (var i = 0; i < data.data.length; i++) {
							data.data[i].loop = i + 1;
							html = new EJS({
								element : "customerLine",
								type : "["
							}).render(data.data[i]);
							obj.append(html);
						}
					}
				}
				_self.showLoading(false);
			},
			failure : function() {
				_self.showLoading(false);
			}
		})
	},

	showOption : function(obj) {
		$('#actionBar').remove();
		var html = $('#actionBarWrapper').html();
		obj.after(html);
		var id = obj.find('td:nth-child(3)').html();
		$('a[name=idopen]').attr("href", "tai-khoan/xac-nhan-mo-tai-khoan.html?idopen=" + id);
		$('#actionBar-cancel').attr('idnumber', id);
	},

	showCancelConfirm : function(idNumber, fullName) {
		this.config.cancelId = idNumber;
		this.config.cancelName = fullName;
		$('#cancelName').html(fullName);
		$('#cancelConfirm').modal();
	},

	showConfirmContract : function(idNumber, fullName) {
		this.config.confirmContract = idNumber;
		this.config.confirmContractName = fullName;
		$('#okName').html(fullName);
		$('#okConfirm').modal();
	},

	doConfirmContract : function(idNumber) {
		if (!idNumber) {
			idNumber = this.config.confirmContract;
		}
		var _self = this;
		this.showLoading(true);
		$.ajax({
			url : "api/account/confirm-contract",
			method : "post",
			data : {
				idnumber : idNumber
			},
			success : function(data) {
				_self.showLoading(false);
				var obj = $('#error-message');
				if (data.status == 0) {
					obj.html("Đã ký hợp đồng thành công với khách hàng <b>" + _self.config.confirmContractName + "<b>");
					obj.addClass('alert-success show');
					_self.findCustomer();
				} else {
					obj.html("Có lỗi xảy ra, vui lòng liên hệ IT support để được hỗ trợ");
					obj.addClass('alert-success show');
				}
				setTimeout(function() {
					obj.removeClass('show');
				}, 2000);
			},
			failure : function() {
				_self.showLoading(false);
			}
		})
	},

	doCancel : function(idNumber) {
		if (!idNumber) {
			idNumber = this.config.cancelId;
		}
		var _self = this;
		this.showLoading(true);
		$.ajax({
			url : "api/account/cancel",
			method : "post",
			data : {
				idnumber : idNumber
			},
			success : function(data) {
				_self.showLoading(false);
				var obj = $('#error-message');
				if (data.status == 0) {
					obj.html("Đã huỷ thành công yêu cầu mở tài khoản của khách hàng <b>" + _self.config.cancelName + "<b>");
					obj.addClass('alert-success show');
					_self.findCustomer();
				} else {
					obj.html("Có lỗi xảy ra khi huỷ yêu cầu mở tài khoản khách hàng <b>" + _self.config.cancelName + "<b>");
					obj.addClass('alert-danger show');
				}
				setTimeout(function() {
					obj.removeClass('show');
				}, 2000);
			},
			failure : function() {
				_self.showLoading(false);
			}
		})
	},

	reOpen : function(idNumber) {
		var _self = this;
		this.showLoading(true);
		$.ajax({
			url : "api/account/re-open",
			method : "post",
			data : {
				idnumber : idNumber
			},
			success : function(data) {
				_self.showLoading(false);
				var obj = $('#error-message');
				if (data.status == 0) {
					obj.html("Đã yêu cầu mở tài khoản của khách hàng <b>" + _self.config.cancelName + "<b> thành công");
					obj.addClass('alert-success show');
					_self.findCustomer();
				} else {
					obj.html("Yêu cầu mở tài khoản của khách hàng <b>" + _self.config.cancelName + "<b> không thành công");
					obj.addClass('alert-danger show');
				}
				setTimeout(function() {
					obj.removeClass('show');
				}, 2000);
			},
			failure : function() {
				_self.showLoading(false);
			}
		})
	},

	confirmOpen : function(idNumber) {
		var _self = this;
		this.showLoading(true);
		$.ajax({
			url : "api/account/confirm-open",
			method : "post",
			data : {
				idnumber : idNumber
			},
			success : function(data) {
				_self.showLoading(false);
				var obj = $('#error-message');
				if (data.status == 0) {
					obj.html("Đã xác nhận tài khoản của khách hàng <b>" + _self.config.cancelName + "<b> thành công");
					obj.addClass('alert-success show');
					_self.findCustomer();
				} else {
					obj.html("Xác nhận tài khoản của khách hàng <b>" + _self.config.cancelName + "<b> không thành công");
					obj.addClass('alert-danger show');
				}
				setTimeout(function() {
					obj.removeClass('show');
				}, 2000);
			},
			failure : function() {
				_self.showLoading(false);
			}
		});
	},

	printIt : function(idNumber) {
		var _self = this;
		if (!idNumber) {
			idNumber = $("#actionBar-cancel").attr('idnumber');
		}
		this.showLoading(true);
		$.ajax({
			url : "api/account/convert",
			method : "POST",
			data : {
				idnumber : idNumber
			},
			success : function(data) {
				_self.showLoading(false);
				if (data.status == 0) {
					top.location.href = "file/download/" + data.data + ".html";
					$('#tcDownload')[0].click();
				}
			},
			failure : function(err) {
				_self.showLoading(false);
			}
		})
	},

	getExcelData : function() {
		$.ajax({
			url : "api/account/get-excel-content",
			method : "POST",
			success : function(data) {
				if (data.status == 0) {
					console.log(data.data);
				}
			},
			failure : function() {

			}
		})
	}
};
$(document).ready(function() {
	Register.init();
}); 