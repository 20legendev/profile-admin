Realtime = {
	init : function() {
		this.config = {
			ready: 0,
			sum: 0,
			sumStock: 0,
			sumBond: 0,
			stock : {},
			mystock: tickers
		};
		this.getData();
		this.checkContent();
		
		
		var bondList = $('#bondList');
		bondList.append(HandleBarService.template("dashboard-no-data", {
			message: "Đang cập nhật tài sản trái phiếu của bạn.",
			button: "Đầu tư ngay",
			location: "https://invest.tcbs.com.vn/bond/Home/Login/"
		}));
	},
	
	checkContent: function(){
		if(tickers.length == 0){
			var stockListWrapper = $('#stockList');
			var html = HandleBarService.template("dashboard-no-data", {
				message: "Bạn chưa đầu tư vào Chứng khoán nào",
				button: "Đầu tư ngay",
				location: "https://invest.tcbs.com.vn/profile/invest/sso.html"
			});
			stockListWrapper.append(html);
		}
	},
	
	getData : function() {
		var data = ['10', '03', '02'];
		for(var i=0; i < data.length; i++){
			$.ajax({
				dataType : "jsonp",
				type : "GET",
				url : "https://103.27.63.246/priceservice/secinfo/snapshot/q=floorCode:"+data[i]+"?jsonp=Realtime.priceList",
				beforeSend : function(request) {
					request.withCredentials = true;
					request.setRequestHeader("Authorization", "Basic "
							+ btoa('admin' + ":" + 'password'));
				}
			});
		}
	},

	priceList : function(data) {
		var keys = Object.keys(data);
		var tmp;
		var tmpData;
		for (var i = 0; i < keys.length; i++) {
			tmp = data[keys[i]];
			for (var j = 0; j < tmp.length; j++) {
				tmpData = tmp[j].split("|");
				this.config.stock[tmpData[3]] = tmpData[19];
			}
		}
		var ticker;
		var mystock = this.config.mystock;
		for (var i = 0; i < mystock.length; i++) {
			ticker = mystock[i];
			if (this.config.stock[ticker.symbol] && !ticker.ready) {
				ticker.price = parseFloat(this.config.stock[ticker.symbol]) * 1000;
				ticker.priceFormated = ticker.price.format();
				ticker.value = ticker.seBalance * ticker.price;
				ticker.valueFormated = ticker.value.format();
				ticker.ready = 1;
				this.config.ready++;
				this.config.sumStock += ticker.value;
			}
		}
		if(this.config.ready == mystock.length){
			this.config.sum = this.config.sumStock + this.config.sumBond;
			this.render();
		}
	},
	
	render: function(){
		Summary.updateChart(this.config);
		var stockListWrapper = $('#stockList');
		var mystock = this.config.mystock;
		mystock = mystock.sort(function(a, b){
			if(a.symbol > b.symbol) return 1;
			return -1;
		});
		for (var i = 0; i < mystock.length; i++) {
			var html = HandleBarService.template("dashboard-stockitem", mystock[i]);
			stockListWrapper.append(html);
		}
		$('#stockSum').html(this.config.sumStock.format());
		$('#total-stock').html(this.config.sumStock.format());
		$('#totalasset').html(this.config.sum.format());
	}
};
window.onload = function() {
	Realtime.init();
};