ProfileService = {

	getBankByProvince : function(province, cb, err) {
		var _self = this;
		$.ajax({
			url : 'api/flex/bank/province/' + province,
			success : function(data) {
				if (data.status == 0) {
					var bank = _self.sortBank(data.data);
					if (cb)
						cb(bank);
				} else {
					if (err)
						err(data);
				}
			},
			
			failure : function() {
				if (err)
					err();
			}
		});
	},

	getBankBranch : function(province, bankSys, cb) {
		Loading.show();
		$.ajax({
			url : 'api/flex/bank/branch/' + province + '/' + bankSys,
			success : function(data) {
				if (data.status == 0) {
					if (cb)
						cb(data.data);
				}
				Loading.hide();
			},
			failure : function() {
				Loading.hide();
			}
		});
	},

	sortBank : function(data) {
		var arr = ['310', '201', '202', '203', '204'];
		var result = data.sort(function(a, b) {
			if (arr.indexOf(a.banksys) >= 0)
				return -1;
			return 1;
		});
		return result;
	}
};
