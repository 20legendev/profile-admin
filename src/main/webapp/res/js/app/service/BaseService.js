BaseService = {
	
	setCache : function(key, data, ttl) {
		localStorage.setItem(key, data);
		localStorage.setItem(key + '.ttl', new Date().getTime() + ttl);
	},

	getCache : function(key) {
		var data = localStorage.getItem(key);
		if (!data)
			return null;

		var time = localStorage.getItem(key + '.ttl');
		if (new Date(parseFloat(time)) >= new Date()) {
			return data;
		}
		localStorage.removeItem(key);
		localStorage.removeItem(key + '.ttl');
		return null;
	},
};
