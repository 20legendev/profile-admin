AdminService = {
	getProvince : function(cb) {
		var cache = BaseService.getCache("admin.province");
		if(cache){
			if(cb) cb(JSON.parse(cache));
			return;
		}
		Loading.show();
		$.ajax({
			url : "api/flex/province",
			success : function(data) {
				if (data.status == 0) {
					if (cb)
						cb(data.data);
					BaseService.setCache("admin.province", JSON.stringify(data.data), 60 * 60 * 1000);
				}
				Loading.hide();
			},
			failure: function(){
				Loading.hide();
			}
		});
	},
	
	getUserStatisticByMonth: function(cb){
		Loading.show();
		$.ajax({
			url: 'api/admin/analytic/user/by-month',
			success: function(ret){
				if(ret.status == 0){
					if(cb) cb(ret.data);
				}
				Loading.hide();
			}, 
			failure: function(){
				Loading.hide();
			}
		});
	},
	
	getBankAccount: function(userId, cb) {
		Loading.show();
		$.ajax({
			url : "api/admin/user/get-bank-account/" + userId,
			success : function(data) {
				if (data.status == 0) {
					if (cb)
						cb(data.data);
				}
				Loading.hide();
			},
			failure: function(){
				Loading.hide();
			}
		});
	},

	updatePassword : function(data, cb, err) {
		$.ajax({
			url : "api/admin/update-pwd",
			method : "POST",
			data : data,
			success : function(res) {
				if (res.status == 0) {
					if (cb)
						cb();
				} else {
					if (err)
						err(res.msg);
				}
			}
		});
	},
	
	deleteBank: function(bankId, cb){
		$.ajax({
			url : "api/admin/delete-bank",
			data: {
				bankId: bankId
			},
			method: 'POST',
			success : function(data) {
				if(cb) cb();
				Loading.hide();
			},
			failure: function(){
				if(err) err();
				Loading.hide();
			}
		});
	},
	
	saveBanks : function(userId, data, cb, err) {
		Loading.show();
		$.ajax({
			url : "api/admin/update-bank",
			data: {
				userId: userId,
				data: data
			},
			method: 'POST',
			success : function(data) {
				if(cb) cb();
				Loading.hide();
			},
			failure: function(){
				if(err) err();
				Loading.hide();
			}
		});
	},
};
