HandleBarService = {
	init : function() {
		this.config = {
			template : {}
		};
	},
	template : function(id, data) {
		if (!this.config.template[id]) {
			var source = $("#" + id).html();
			this.config.template[id] = Handlebars.compile(source);
		}
		return this.config.template[id](data);
	}
};
$(document).ready(function() {
	HandleBarService.init();
});