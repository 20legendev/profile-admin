Loading = {
	show : function(tag) {
		tag = tag ? tag : ".loading";
		$(tag).addClass("show");
		setTimeout(function() {
			$(tag).css("opacity", 1);
		}, 10)

	},
	hide : function(tag) {
		tag = tag ? tag : ".loading";
		$(tag).css("opacity", 0);
		setTimeout(function() {
			$(tag).removeClass("show");
		}, 500);
	}
}