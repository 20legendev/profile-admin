MyCustomer = {
	init : function() {
		this.config = {
			customers : {}
		};
	},

	renderUploaded : function(data) {
		for (var i = 0; i < data.length; i++) {
			this.config.uploaded.push(data[i].fileAlias);
			data[i].type = this.detectFile(data[i].fileAlias);
			var html = HandleBarService.template("file-uploaded-template", data[i]);
			$('#uploaded-files').append(html);
		}
	},

	detectFile : function(link) {
		link = link.toLowerCase();
		if (link.indexOf(".jpg") >= 0)
			return 'image';
		if (link.indexOf(".png") >= 0)
			return 'image';
		if (link.indexOf(".pdf") >= 0)
			return 'pdf';
		if (link.indexOf(".xlsx") >= 0 || link.indexOf(".xls") >= 0)
			return 'excel';
		if (link.indexOf(".doc") >= 0 || link.indexOf(".docx") >= 0)
			return 'doc';
		return 'other';
	},

	editOAR : function(referenceid) {
		console.log(this.config.customers[referenceid]);
		$.ajax({
			url : 'api/rm/edit-oar',
			method : 'POST',
			data : {
				referenceid : referenceid
			},
			success : function(e) {
				if (e.status == 0) {
					window.location.href = e.data;
				} else {
					alert(e.msg);
				}
			},
			failure : function() {
				alert("Có lỗi xảy ra, vui lòng báo IT support để được hỗ trợ");
			}
		});
	},

	initFileUpload : function() {
		var _self = this;
		$('#progress .bar').css('width', '0%');
		$('#fileupload').fileupload({
			dataType : 'json',
			done : function(e, data) {
				Loading.hide();
				if (data.result.data.length > 0) {
					_self.renderUploaded(data.result.data);
				}
				_self.updateOpenRequest(_self.config.editreferenceid, true);
			},
			progressall : function(e, data) {
				Loading.show();
				var progress = parseInt(data.loaded / data.total * 100, 10);
				$('#progress .bar').css('width', progress + '%');
			},
			start : function(e, data) {
				console.log(e, data);
			},
			dropZone : $('#dropzone')
		});
	},

	showCancelConfirm : function(referenceid) {
		this.config.cancelRefererenceId = referenceid;
		$('#cancelName').html(
				this.config.customer[referenceid].lastname + " "
						+ this.config.customer[referenceid].firstname);
		$('#cancelConfirm').modal();
	},

	updateOpenRequest : function(referenceid, refresh) {
		var _self = this;
		Loading.show();
		var customerData = this.config.customers[referenceid];
		$.ajax({
			url : "api/rm/update-open",
			method : "POST",
			data : {
				referenceid : referenceid,
				uploaded : JSON.stringify(_self.config.uploaded)
			},
			success : function(data) {
				Loading.hide();
				var obj = $('#error-message');
				if (data.status == 0) {
					obj.html("Đã cập nhật tài liệu cho khách hàng <b>"
							+ customerData.lastname + " "
							+ customerData.firstname + "</b>");
					obj.addClass('alert-success show').removeClass(
							"alert-danger");
				} else {
					obj.html("Có lỗi khi cập nhật tài liệu về khách hàng <b>"
							+ customerData.lastname + " "
							+ customerData.firstname + "</b>");
					obj.addClass('alert-danger').removeClass("alert-success");
				}
				setTimeout(function() {
					obj.removeClass('show');
				}, 2000);
				if (!refresh) {
					$('#admin-user-detail-action').remove();
					_self.findCustomer();
				}
			},
			failure : function() {
				Loading.hide();
				$('#admin-user-detail-action').remove();
			}
		});
	},

	confirmOpen : function(referenceid) {
		var _self = this;
		var customer = this.config.customers[referenceid];
		var c = confirm("Bạn có muốn xác nhận thông tin và mở tài khoản cho khách hàng "
				+ customer.lastname + " " + customer.firstname + " không?");
		if (!c)
			return;
		Loading.show();
		$
				.ajax({
					url : "api/rm/confirm-open",
					method : "post",
					data : {
						referenceid : referenceid
					},
					success : function(data) {
						Loading.hide();
						var obj = $('#error-message');
						if (data.status == 0) {
							obj
									.html("Đã xác nhận tài khoản của khách hàng thành công");
							obj.addClass('alert-success show').removeClass(
									"alert-danger");
							_self.findCustomer();
						} else {
							obj
									.html("Xác nhận tài khoản của khách hàng không thành công: "
											+ data.msg);
							obj.addClass('alert-danger show');
						}
						setTimeout(function() {
							obj.removeClass('show');
						}, 2000);
					},
					failure : function() {
						Loading.hide();
					}
				});
	},

	confirmContract : function(referenceid) {
		var _self = this;
		Loading.show();
		var obj = $('#error-message');
		$
				.ajax({
					url : "api/rm/confirm-contract",
					method : "post",
					data : {
						referenceid : referenceid
					},
					success : function(data) {
						Loading.hide();
						if (data.status == 0) {
							obj
									.html("Đã ký hợp đồng thành công với khách hàng");
							obj.addClass('alert-success show').removeClass(
									"alert-danger");
							_self.findCustomer();
						} else {
							obj
									.html("Có lỗi xảy ra, vui lòng liên hệ IT support để được hỗ trợ");
							obj.addClass('alert-success show');
						}
						setTimeout(function() {
							obj.removeClass('show');
						}, 2000);
					},
					failure : function() {
						Loading.hide();
					}
				});
	},

	printIt : function(referenceid, obj) {
		var _self = this;
		Loading.show();
		$('#admin-user-detail-action').remove();
		$.ajax({
			url : "api/rm/print-contract",
			method : "POST",
			data : {
				referenceid : referenceid
			},
			success : function(data) {
				$('#download-contract').remove();
				Loading.hide();
				if (data.status == 0) {
					var customer = _self.config.customers[referenceid];
					var html = HandleBarService.template(
							"adminuser-download-contract", {
								contract : "file/download/" + data.data
										+ ".html",
								tc : 'file/download/tc.html',
							});
					obj.parent().parent().after(html);
				}
			},
			failure : function(err) {
				Loading.hide();
			}
		});
	},

	findCustomer : function(e) {
		if (!this.config)
			this.init();
		if (e) {
			if (e.keyCode != 13) {
				return;
			}
		}
		var idNumber = $('#idnumber').val();
		this.config.idnumber = idNumber;
		Loading.show();
		var _self = this;
		$
				.ajax({
					url : 'api/rm/check-id',
					method : "POST",
					data : {
						idnumber : idNumber
					},
					success : function(data) {
						if (data.status == 0) {
							if (data.data != null) {
								var content = JSON.parse(Base64
										.decode(data.data));
								var html;
								var obj = $('#customerFilterBody');
								$('#totalCount').html(content.length);
								obj.html('');
								for (var i = 0; i < content.length; i++) {
									content[i].loop = i + 1;
									content[i].createdDateFormated = moment(
											new Date(content[i].createdDate))
											.format("HH:mm:ss DD-MM-YYYY");
									if (window.mode
											&& window.mode == 'SALE_OFFICE') {
										html = HandleBarService.template(
												"rm-customerline-less",
												content[i]);
									} else {
										html = HandleBarService.template(
												"rm-customerline", content[i]);
									}
									obj.append(html);
									_self.config.customers[content[i]['referenceid']] = content[i];
								}
							}
						}
						Loading.hide();
					},
					failure : function() {
						Loading.hide();
					}
				});
	},

	showAction : function(referenceid, obj) {
		$('#admin-user-detail-action').remove();
		var data = this.config.customers[referenceid];
		var html = HandleBarService.template("rm-customerline-action-template",
				data);
		obj.after(html);
	},
	
	removeUpload: function(e, obj, fileAlias){
		var c = confirm("Bạn có muốn xoá file này không?");
		if(!c) return;
		e.preventDefault();
		e.stopPropagation();
		var idx = this.config.uploaded.indexOf(fileAlias);
		if(idx >= 0){
			this.config.uploaded.splice(idx, 1);
		}
		obj.parent().parent().remove();
		this.updateOpenRequest(this.config.editreferenceid, true);
	},

	uploadFile : function(referenceid, obj) {
		this.config.uploaded = [];
		var uploaded = [];
		if(this.config.customers[referenceid].uploaded){
			uploaded = JSON.parse(this.config.customers[referenceid].uploaded);
		}
		var data = [];
		for(var i=0; i < uploaded.length; i++){
			data.push({
				fileAlias: uploaded[i]
			});
		}
		this.config.editreferenceid = referenceid;
		$('#admin-user-detail-action').remove();
		var html = HandleBarService.template("upload-template", {
			referenceid : referenceid
		});
		obj.parent().parent().after(html);
		this.initFileUpload();
		this.renderUploaded(data);
	}
};

$(document).ready(function() {
	MyCustomer.init();
});