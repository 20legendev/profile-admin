Admin = {

	init : function() {
		this.config = {
			cache : {},
			user : {},
			queue : {},
			errorsearch : {
				status : 0,
				page : 1
			},
			banks : {}
		};
	},

	detectFile : function(link) {
		link = link.toLowerCase();
		if (link.indexOf(".jpg") >= 0)
			return 'image';
		if (link.indexOf(".png") >= 0)
			return 'image';
		if (link.indexOf(".pdf") >= 0)
			return 'pdf';
		if (link.indexOf(".xlsx") >= 0 || link.indexOf(".xls") >= 0)
			return 'excel';
		if (link.indexOf(".doc") >= 0 || link.indexOf(".docx") >= 0)
			return 'doc';
		return 'other';
	},

	viewQueueDetail : function(referenceid, obj) {
		var data = this.config.queue[referenceid];
		var render = function() {
			$('#admin-user-detail-action').remove();
			var fileData = [];
			if (data.uploaded) {
				var uploadedImage = JSON.parse(data.uploaded);
				for (var i = 0; i < uploadedImage.length; i++) {
					var tmp = {};
					tmp.link = uploadedImage[i];
					tmp.type = Admin.detectFile(tmp.link);
					fileData.push(tmp);
				}
			};
			data.uploadedData = fileData;
			var html = HandleBarService.template('admin-queue-detail', data);
			obj.after(html);
		};
		if (data.referer) {
			$.ajax({
				url : 'api/admin/user-detail/' + data.referer,
				success : function(e) {
					data.refererData = e.data;
					render();
				}
			});
		} else {
			render();
		}
	},

	searchUserQueue : function() {
		var data = {
			phone : $('#phone').val(),
			fullname : $('#fullname').val(),
			email : $('#email').val(),
			idnumber : $('#idnumber').val()
		};
		var _self = this;
		Loading.show();
		$.ajax({
			url : "api/admin/search-queue",
			method : "POST",
			data : data,
			success : function(res) {
				if (res.status == 0) {
					_self.renderQueueData(res.data);
				} else {
					alert("Có lỗi: " + res.msg);
				}
				Loading.hide();
			},
			failure : function() {
				Loading.hide();
				alert("Có lỗi hệ thống");
			}
		});
	},
	search : function(page) {
		if (page == undefined)
			page = 0;
		this.config.userSearchPage = page;
		console.log(page);
		var data = {
			phone : $('#phone').val(),
			fullname : $('#fullname').val(),
			email : $('#email').val(),
			idnumber : $('#idnumber').val(),
			username : $('#username').val(),
			fromdate : $('#fromdate').val(),
			todate : $('#todate').val(),
			vsdstatus : $('#vsdstatus').val(),
			page : page
		};
		var _self = this;
		Loading.show();
		$.ajax({
			url : "api/admin/search",
			method : "POST",
			data : data,
			success : function(res) {
				if (res.status == 0) {
					if (res.data.length == 0 && this.config.userSearchPage != 0)
						this.config.userSearchPage--;
					_self.renderData(res.data);
				} else {
					alert("Có lỗi: " + res.msg);
				}
				Loading.hide();
			},
			failure : function() {
				Loading.hide();
				alert("Có lỗi hệ thống");
			}
		});
	},

	updatePassword : function() {
		var data = {
			pwd : $('#edit-pwd').val()
		};
		if (!confirm("Bạn có chắc chắn muốn thay đổi mật khẩu thành " + data.pwd + " không?"))
			return;
		Loading.show();
		AdminService.updatePassword({
			pwd : data.pwd,
			userid : this.config.edit,
			sendemail : $('#send-email-checkbox').is(":checked") ? 1 : 0,
			sendsms : $('#send-sms-checkbox').is(":checked") ? 1 : 0
		}, function() {
			alert("Đã cập nhật thành công mật khẩu");
			Admin.search();
			Loading.hide();
		}, function(res) {
			alert(res);
			Loading.hide();
		});
	},

	getEditIdCode : function() {
		var idobject = {};
		var tmp = $('.edit-idnumber');
		for (var i = 0; i < tmp.length; i++) {
			var obj = {};
			obj.idNumber = $(tmp[i]).val();
			obj.id = $(tmp[i]).attr('ident-id');
			obj.idDate = moment($('.edit-iddate[ident-id=' + obj.id + "]").val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
			obj.idPlace = $('.edit-idplace[ident-id=' + obj.id + "]").val();
			idobject[obj.id] = obj;
		}
		var objtmp = [];
		var keys = Object.keys(idobject);
		for (var i = 0; i < keys.length; i++) {
			objtmp.push(idobject[keys[i]]);
		}
		return objtmp;
	},

	getEditAddress : function() {
		var idobject = {};
		var tmp = $('.edit-address');
		for (var i = 0; i < tmp.length; i++) {
			var obj = {};
			obj.address = $(tmp[i]).val();
			obj.id = $(tmp[i]).attr('address-id');
			idobject[obj.id] = obj;
		}
		var objtmp = [];
		var keys = Object.keys(idobject);
		for (var i = 0; i < keys.length; i++) {
			objtmp.push(idobject[keys[i]]);
		}
		return objtmp;
	},

	save : function() {
		var c = confirm("Bạn có chắc chắn muốn lưu thông tin không?");
		if (!c)
			return;

		var data = {
			fullname : $('#edit-fullname').val(),
			phone : $('#edit-phone').val(),
			email : $('#edit-email').val().trim(),
			username : $('#edit-username').val(),
			userid : this.config.edit,
			birthday : moment($('#edit-birthday').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
			idcode : JSON.stringify(this.getEditIdCode()),
			address : JSON.stringify(this.getEditAddress())
		};
		Loading.show();
		$.ajax({
			url : "api/admin/update-user",
			method : "POST",
			data : data,
			success : function(res) {
				if (res.status == 0) {
					alert("Đã cập nhật thành công");
					Admin.search();
				} else {
					alert(res.msg);
				}
				Loading.hide();
			}
		});
	},

	syncBond : function() {
		var data = {
			userid : this.config.edit,
			appname : "BOND"
		};
		Loading.show();
		$.ajax({
			url : "api/admin/open-app",
			method : "POST",
			data : data,
			success : function(res) {
				if (res.status == 0) {
					alert("Đã sync sang BOND thành công");
					Admin.search();
				} else {
					alert(res.msg);
				}
				Loading.hide();
			}
		});
	},

	delete : function() {
		var c = confirm("Xoá tài khoản đấy! Bạn chắc không?");
		if (!c)
			return;
		var data = {
			userid : this.config.edit
		};
		Loading.show();
		$.ajax({
			url : "api/admin/rmv-user",
			method : "POST",
			data : data,
			success : function(res) {
				if (res.status == 0) {
					alert("Đã cập nhật thành công");
					Admin.search();
				} else {
					alert(res.msg);
				}
				Loading.hide();
			}
		});
	},

	viewDetail : function(id, obj) {
		$('#admin-user-detail-item').remove();
		this.config.edit = id;
		var _self = this;
		var user = this.config.user[id];
		if (user.identifications.length > 0) {
			for (var i = 0; i < user.identifications.length; i++) {
				user.identifications[i].idDateFormated = moment(user.identifications[i].idDate, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
			}
		} else {
			user.idnumber = "Chưa có";
			user.iddateformated = "";
		}
		user.havebank = user.banks.length > 0 ? true : false;
		user.birthdayformated = moment(user.birthday, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
		AdminService.getProvince(function(data) {
			user.province = data;
			var html = HandleBarService.template("adminuser-detail", user);
			obj.after(html);
			_self.loadOldBank();
		});

	},

	loadOldBank : function() {
		$('#admin-add-bank-wrapper').html('');
		var wrapper = $('#admin-bank-wrapper');
		wrapper.html('');

		var user = this.config.user[this.config.edit];
		var _self = this;
		AdminService.getBankAccount(user.id, function(banks) {
			AdminService.getProvince(function(data) {
				for (var i = 0; i < banks.length; i++) {
					banks[i].province = data;
					_self.config.banks[banks[i].id] = banks[i];
					var html = HandleBarService.template('admin-bank-account-template', banks[i]);
					console.log(html);
					wrapper.append(html);
					_self.changeProvince(banks[i].bankProvince, banks[i].id);
				}
			});
			/*
			 _self.config.banks = {};
			 for (var i = 0; i < banks.length; i++) {
			 $('.bankplace[bank-id=' + banks[i].id + ']').val(banks[i].bankProvince);
			 _self.config.banks[banks[i].id] = banks[i];
			 _self.changeProvince(banks[i].bankProvince, banks[i].id);
			 }
			 */
		});
	},

	changeProvince : function(province, bankid, nobank) {
		var _self = this;
		Loading.show();
		if (!this.config.cacheBanks)
			this.config.cacheBanks = {};
		var bank = this.config.banks[bankid];
		console.log("bank", bank);
		ProfileService.getBankByProvince(province, function(data) {
			var wrapper = $('.bankname[bank-id=' + bankid + ']');
			wrapper.html('');
			wrapper.append('<option value="-1">Chọn ngân hàng</option>');
			for (var i = 0; i < data.length; i++) {
				wrapper.append("<option value='" + data[i].banksys + "'>" + data[i].bankname + "</option>");
				_self.config.cacheBanks[data[i].banksys] = data[i];
			}
			if (bank) {
				if (nobank) {
					$('.bankcode[bank-id=' + bank.id + ']').val(-1);
					wrapper.val(-1);
				} else {
					wrapper.val(bank.bankSys);
					_self.changeBank(bank.bankProvince, bank.bankSys, bankid);
				}
			}
			Loading.hide();
		}, function() {
			Loading.hide();
		});
	},

	changeBank : function(bankProvince, bankSys, bankId, nobank) {
		if (!this.config.bankBranch)
			this.config.bankBranch = {};
		if (!bankProvince) {
			bankProvince = $('.bankplace[bank-id=' + bankId + ']').val();
		}
		var _self = this;
		ProfileService.getBankBranch(bankProvince, bankSys, function(data) {
			var bankWrapper = $('.bankcode[bank-id=' + bankId + ']');
			bankWrapper.html('<option value=-1>Chọn chi nhánh</option>');
			for (var i = 0; i < data.length; i++) {
				bankWrapper.append("<option value='" + data[i].bankcode + "'>" + data[i].branchname + "</option>");
				_self.config.bankBranch[data[i].bankcode] = data[i];
			}
			if (!nobank) {
				var bank = _self.config.banks[bankId];
				bankWrapper.val(bank.bankCode);
			}
		});
	},

	addBank : function(obj) {
		$('#admin-add-bank').addClass('adding');
		AdminService.getProvince(function(province) {
			var data = {
				province : province
			};
			var html = HandleBarService.template("admin-add-bank-account-template", data);
			$('#admin-bank-wrapper').append(html);
		});
	},

	deleteBank : function(bankid) {
		var _self = this;
		if (bankid == -1) {
			$('#bank-item-' + bankid).remove();
		} else {
			if (confirm("Bạn chắc chắn muốn xoá tài khoản ngân hàng này không?")) {
				AdminService.deleteBank(bankid, function() {
					_self.loadOldBank();
				});
			}
		}
	},

	addBankCancel : function(obj) {
		obj.parent().remove();
		$('#admin-add-bank').removeClass('adding');
	},

	saveBank : function() {
		if (!confirm("Bạn có chắc chắn muốn lưu thông tin không?"))
			return;
		var banks = Admin.getBankEditResult();
		var _self = this;
		AdminService.saveBanks(this.config.edit, JSON.stringify(banks), function() {
			_self.loadOldBank();
			alert("Đã lưu thông tin thành công");
		});
	},

	getBankEditResult : function() {
		var arr = [], _self = this;
		var tmp = $('.bankplace');
		for (var i = 0; i < tmp.length; i++) {
			var obj = {};
			obj.bankProvince = $(tmp[i]).val();
			obj.id = $(tmp[i]).attr('bank-id');
			obj.bankName = _self.config.cacheBanks[$('.bankname[bank-id=' + obj.id + "]").val()].bankname;
			obj.bankCode = $('.bankcode[bank-id=' + obj.id + "]").val();
			obj.bankBranch = _self.config.bankBranch[obj.bankCode].branchname;
			obj.bankAccountName = $('.ba-name[bank-id=' + obj.id + "]").val();
			obj.bankAccountNo = $('.ba-no[bank-id=' + obj.id + "]").val();
			arr.push(obj);
		}
		return arr;
	},

	renderData : function(data) {
		var obj = $("#admin-user-body");
		if (this.config.userSearchPage == 0) {
			obj.html('');
		}
		var html = '';
		for (var i = 0; i < data.length; i++) {
			this.config.user[data[i].id] = data[i];
			data[i].fullname = data[i].lastname + " " + data[i].firstname;
			data[i].index = i + 1;
			html = HandleBarService.template('adminuser-template', data[i]);
			obj.append(html);
		}
	},

	renderQueueData : function(data) {
		var obj = $("#admin-queue-body");
		obj.html('');
		var html = '';
		for (var i = 0; i < data.length; i++) {
			this.config.user[data[i].id] = data[i];
			data[i].fullname = data[i].lastname + " " + data[i].firstname;
			data[i].index = i + 1;
			data[i].createdDateFormated = moment(new Date(data[i].createdDate)).format("HH:mm:ss DD-MM-YYYY");
			html = HandleBarService.template('adminuser-queue-template', data[i]);
			obj.append(html);
			this.config.queue[data[i].referenceid] = data[i];
		}
	},

	searchError : function() {
		this.config.errorsearch.errorType = $("#errorType").val();
		this.config.errorsearch.status = $("#status").val();
		this.config.errorsearch.data = $('#data').val();
		this.config.errorsearch.page = 0;
		this.loadErrorPage(undefined, 1);
	},

	countError : function() {
		$.ajax({
			url : 'api/admin/count-error',
			method : "POST",
			data : this.config.errorsearch,
			success : function(data) {
				Loading.hide();
				if (data.status == 0) {
					$('#errorCount').html(data.data);
				}
			},
			failure : function() {
				Loading.hide();
			}
		});
	},
	loadErrorPage : function(obj, page) {
		if (obj) {
			$('#error-pagination li').removeClass('active');
			obj.addClass('active');
		}
		Loading.show();
		page = page - 1;
		this.config.errorsearch.page = page;
		$.ajax({
			url : 'api/admin/get-error',
			method : "POST",
			data : this.config.errorsearch,
			success : function(data) {
				Loading.hide();
				if (data.status == 0) {
					Admin.renderErrorData(data.data);
				}
			},
			failure : function() {
				Loading.hide();
			}
		});
		this.countError();
	},

	renderErrorData : function(data) {
		scrolltotop.scrollup();
		var wrapper = $("#result-body");
		wrapper.html('');
		this.config.errorList = this.config.errorList ? this.config.errorList : {};
		for (var i = 0; i < data.length; i++) {
			data[i].index = this.config.errorsearch.page * 25 + i + 1;
			data[i].shortDesc = data[i].data ? data[i].data.substr(0, 75) + '...' : '';
			data[i].sinceDate = moment(data[i].since).format("YYYY-MM-DD hh:mm:ss");
			var html = HandleBarService.template("admin-error-item", data[i]);
			wrapper.append(html);
			this.config.errorList[data[i].id] = data[i];
		}
	},

	viewErrorDetail : function(obj, id) {
		$('#admin-error-detail-item').remove();
		var data = this.config.errorList[id];
		var html = HandleBarService.template("admin-error-detail", data);
		obj.after(html);
	},

	resolveError : function(id) {
		var c = confirm("Bạn có chắc là đã giải quyết vấn đề này không?");
		if (!c)
			return;
		$.ajax({
			url : 'api/admin/resolve-error',
			method : "POST",
			data : {
				id : id
			},
			success : function(data) {
				Loading.hide();
				Admin.loadErrorPage(undefined, Admin.config.errorsearch.page + 1);
			},
			failure : function() {
				Loading.hide();
			}
		});
	}
};

AdminUserHome = {
	init : function() {
		var _self = this;
		$(window).on('scroll', function(e) {
			if ($(window).scrollTop() >= $(document).height() - $(window).height()) {
				_self.nextPage();
			}
		});
	},

	nextPage : function() {
		if (Admin.config.userSearchPage != undefined) {
			Admin.search(Admin.config.userSearchPage + 1);
		}
	},

	exportExcel : function() {
		var data = {
			phone : $('#phone').val(),
			fullname : $('#fullname').val(),
			email : $('#email').val(),
			idnumber : $('#idnumber').val(),
			username : $('#username').val(),
			fromdate : $('#fromdate').val(),
			todate : $('#todate').val(),
			vsdstatus : $('#vsdstatus').val(),
			page: -1
		};
		var _self = this;
		Loading.show();
		$.ajax({
			url : "api/export/user/search",
			method : "POST",
			data : data,
			success : function(res) {
				if (res.status == 0) {
					window.location.href = "./file/download-generated-file.html";
				} else {
					alert("Có lỗi: " + res.msg);
				}
				Loading.hide();
			},
			failure : function() {
				Loading.hide();
				alert("Có lỗi hệ thống");
			}
		});
	}
};
