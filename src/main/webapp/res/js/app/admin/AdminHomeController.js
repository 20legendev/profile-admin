AdminHomeController = {
	init: function(){
		this.initStatistic(statistic);
		this.initMonthlyChart(monthdata);
	},
	
	changeChartView: function(obj){
		var data = obj.val();
		var _self = this;
		if(data == 2){
			AdminService.getUserStatisticByMonth(function(data){
				_self.initMonthlyChart(data);
			});
		}
	},
	
	initStatistic: function(data){
		var obj = [{
			name: "Đã kích hoạt",
			y: data.activated,
			color: '#FF0000'
		}, {
			name: "Chưa kích hoạt",
			y: data.total - data.activated,
			color: "#999"
		}];
		$('#statistic-chart').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            credits: {
            	enabled: false
            },
            title: {
                text: 'Tỉ lệ kích hoạt toàn hệ thống'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: obj
            }]
        });
	},
	
	initMonthlyChart: function(data){
		var total = [];
		var activated = [];
		var category = [];
		var queue = [];
		for(var i=0; i < data.length; i++){
			total.unshift(data[i].total);
			activated.unshift(data[i].activated);
			category.unshift(data[i].reportDate);
			queue.unshift(data[i].queue);
		}
	    $('#monthly-chart').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Thống kê mở tài khoản'
	        },
	        subtitle: {
	        	text: ' '
	        },
	        credits: {
	        	enabled: false
	        },
	        xAxis: {
	            categories: category,
	            crosshair: true,
	            labels: {
                	enabled: false
				}
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'users'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0">{point.y}</td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
	            name: 'Tổng',
	            data: total,
	            color: '#FF0000',
	            pointWidth: 4
	
	        }, {
	            name: 'Đã kích hoạt',
	            lineWidth: 1,
	            data: activated,
	            color: '#2ecc71',
				yAxis: 0,
				pointWidth: 4,
				marker: {
                	enabled: false
                }
	        },{
	            name: 'Chưa kích hoạt',
	            lineWidth: 1,
	            data: queue,
	            color: '#555',
				yAxis: 0,
				pointWidth: 4,
				marker: {
                	enabled: false
                }
	        }]
	    });

	}
};
