Summary = {
	init : function() {
		this.initObject();
		switch (mode) {
		case "active-flex":
			this.showActiveFlex();
			break;
		case "data-miss":
			this.showMissingData();
			break;
		}
	},

	showMissingData : function() {
		$("#missingData").fadeTo(2000, 500).slideUp(500, function() {
		});
	},

	showActiveFlex : function() {
		var html = tmpl("activeFlexTmpl");
		this.settingContent.html(html);
	},

	activeFlex : function() {
		$.ajax({
			url : "api/app/active-flex",
			method : "POST",
			success : function(data) {
				console.log(data);
			}
		});
	},

	initObject : function() {
		this.settingContent = $('#setting-content');
	}
};

$(document).ready(function() {
	Summary.init();
});