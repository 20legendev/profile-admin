ChangePassword = {
	verifyPassword : function(obj, isRetype) {
		var password = obj.val();
		var isOk = Utils.validatePassword(password);
		if (isOk == 0) {
			if(isRetype){
				var lastPass = $("#password").val();
				if(password == lastPass){
					obj.addClass('success');
					$('#errorMessage').html("");
				}else{
					obj.removeClass('success');
					$('#errorMessage').html("Mật khẩu không trùng nhau");
				}
			}else{
				obj.addClass('success').removeClass('error');
			}
		}else{
			obj.removeClass('success').removeClass('error');
		}
	}
}

Password = {
	validateEmail : function(tag, e) {
		var email = $(tag).val();
		var isValidate = Utils.validateEmail(email);
		if (isValidate) {
			$("#passwordForm").addClass('status-1').removeClass('status-0');
			Password.hideError();
		} else {
			$("#passwordForm").removeClass('status-1').removeClass('status-0');
		}
		if (e && e.keyCode == 13) {
			if (isValidate) {
				this.requestChange();
			} else {
				Password.showError("Email chưa đúng định dạng");
			}
		}
	},

	requestChange : function() {
		var email = $("#emailInput").val();
		$("#passwordForm").removeClass('status-1').removeClass('status-0');
		Loading.show('.loading-small');
		Password.hideError();
		$.ajax({
			url : "api/account/forgot-pwd",
			method : "POST",
			data : {
				email : email
			},
			success : function(e) {
				Loading.hide('.loading-small');
				if (e.status != 0) {
					Password.showError();
					return;
				}
				Password.showSuccessCard(email);
			},
			failure : function() {
				Loading.hide('.loading-small');
				Password.hideError();
			}
		})
	},

	showSuccessCard : function(email) {
		$('#forgotContent').hide();
		$('.success-card').show();
		setTimeout(function() {
			$('.success-card').addClass('show');
			$('#resend-email').attr('href',
					$('#resend-email').attr('href') + email);
		}, 10);
		var provider = Utils.detectEmailProvider(email);
		switch (provider) {
		case "GOOGLE":
			$("#email-link").attr('href', "http://mail.google.com");
			break;
		case "YAHOO":
			$("#email-link").attr('href', "http://mail.yahoo.com");
			break;
		case "MS":
			$("#email-link").attr('href', "http://outlook.com");
			break;
		default:
			$("#email-link").hide();
			break;
		}
	},

	showError : function(content) {
		$('#forgotContent .error-message').html(
				content ? content : "Không tìm thấy email của bạn")
		$('#forgotContent .error-message').addClass('show');
	},

	hideError : function() {
		$('.error-message').removeClass('show');
	},

// changePassword : function(event) {
// var password = $("#password").val();
// var rePassword = $("#retype-password").val();
// if (password != rePassword) {
// alert("Nhập trùng cái mày");
// return;
// }
// $.ajax({
// url : "api/account/update-password",
// method : "POST",
// data : {
// password : password
// },
// success : function(e) {
// console.log(e);
// alert(JSON.stringify(e));
// }
// })
// }
};