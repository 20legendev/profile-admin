Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};
Utils = {

	validatePassword : function(pw) {
		if (pw.indexOf(" ") >= 0) {
			return 1;
		}
		if (pw.length < 6) {
			return 2;
		}
		return 0;
	},

	validateEmail : function(email) {
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		return re.test(email);
	},

	validateSpecialCharacter : function(input) {
		var regex = new RegExp('[$&+:;=?@#|]', 'g');
		return regex.test(input);
	},

	validatePhone : function(input) {
		var regex = new RegExp('^(\\+*)\\d{10,12}$', 'g');
		return regex.test(input);
	},

	validateSmallerDate : function(date, year) {
		var d = date.split("/");
		date = d[1] + "/" + d[0] + "/" + d[2];
		dateObj = new Date(date);
		if (!year) {
			return dateObj < new Date();
		} else {
			var obj = new Date();
			var newDate = new Date(obj.getFullYear() - year, obj.getMonth(),
					obj.getDate());
			return dateObj <= newDate;
		}
	},

	trim : function(str) {
		if (!str)
			return str;
		str = str.replace(/^\s+/, '');
		for (var i = str.length - 1; i >= 0; i--) {
			if (/\S/.test(str.charAt(i))) {
				str = str.substring(0, i + 1);
				break;
			}
		}
		return str;
	},

	detectEmailProvider : function(email) {
		if (email.indexOf('@gmail.com') >= 0) {
			return "GOOGLE";
		}
		if (email.indexOf('@yahoo.com') >= 0) {
			return "YAHOO";
		}
		if (email.indexOf('@live.com') >= 0
				|| email.indexOf('@outlook.com') >= 0
				|| email.indexOf('@hotmail.com') >= 0) {
			return "MS";
		}
	}

};