/**
 * Load modules for application
 */

angular.module('tcbs', ['ngAnimate', 'ngTouch', 'toastr', 'RecursionHelper', 'infinite-scroll','ui.tree', 'ui.router', 'anim-in-out', 'tcbs.services', 'tcbs.directives', 'tcbs.filters', 'timer', 'angucomplete-alt', 'ngSanitize', 'ui.gravatar','angular-loading-bar'])
.config(function(toastrConfig) {
	angular.extend(toastrConfig, {
		autoDismiss : false,
		containerId : 'toast-container',
		maxOpened : 0,
		newestOnTop : true,
		positionClass : 'toast-top-right',
		preventDuplicates : false,
		preventOpenDuplicates : false,
		target : 'body'
	});
}).config(['gravatarServiceProvider',
function(gravatarServiceProvider) {
	gravatarServiceProvider.defaults = {
		size : 80,
		"default" : 'mm'
	};
}]).config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
}])
.controller('mainController', function($scope, $state, cfpLoadingBar) {

	$scope.$state = $state;

	$scope.isEmail = function(e) {
		var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/gi;
		return !e || re.test(e);
	};

	$scope.setBusy = function(value) {
		$scope.busy = value;
		if (value) {
			cfpLoadingBar.start();
		} else {
			cfpLoadingBar.complete();
		}
	};

	$scope.isTouchDevice = function() {
		return (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
	};

	$scope.bondUrl = "https://invest.tcbs.com.vn/bond/Home/Login";
	$scope.flexSSOUrl = "invest/sso";
});
