angular.module('tcbs.filters')
.filter('timeAgo', function() {
    return function(input) {
        if (input === false) {
            return __('not yet taken');
        }
        if (input) {
            return moment(input,"DD-MM-YYYY hh:mm:ss").fromNow();
        }
        return input;
    };
})
.filter('nameCase', function() {
    return function(str) {
        return str.toLowerCase().split(' ').map(s => s.charAt(0).toUpperCase() + s.slice(1)).join(' ');
    };
})
.filter('formatDateTime', function() {
    return function(input) {
        if (input) {
            return moment(input, 'YYYY-MM-DD').format('DD/MM/YYYY LT');
        }
        return input;
    };
})
.filter('dateTimeFormat', function() {
    return function(input) {
        if (input) {
            return moment(input, 'YYYY-MM-DD hh:mm:ss').format('DD/MM/YYYY');
        }
        return input;
    };
})
.filter('formatDateTimeVietnamese', function() {
    return function(input) {
        if (input) {
            return moment(input, 'YYYY-MM-DD hh:mm:ss').format('hh:mm:ss DD/MM/YYYY');
        }
        return input;
    };
})
.filter('formatDate', function() {
    return function(input) {
        if (input) {
            return moment(input, 'YYYY-MM-DD').format('DD/MM/YYYY');
        }
        return input;
    };
})
.filter('formatMonth', function() {
    return function(input) {
        if (input) {
        	var monthShortNames = ["T1", "T2", "T3", "T4", "T5", "T6",
        	                       "T7", "T8", "T9", "T10", "T11", "T12"];
        	var t = new Date(moment(input, 'DD/MM/YYYY').format('MM/DD/YYYY'));
        	return (t.getMonth() + 1)  + "/" + t.getFullYear();
        }
        return input;
    };
})
.filter('currency', function() {
    return function(input) {
        if (input) {
            var isNegative = input < 0;
            input = input > 0 ? input: -input;
            var sx = (''+input).split('.'), s = '', i, j;
            var sep = ','; // default seperator
            var grp = 3; // default grouping
            i = sx[0].length;
            while (i > grp) {
                j = i - grp;
                s = sep + sx[0].slice(j, i) + s;
                i = j;
            }
            s = sx[0].slice(0, i) + s;
            sx[0] = s;
            return isNegative ? '-' + sx.join('.'): sx.join('.');
        }
        return input;
    };
})
.filter('trim', function () {
    return function(value) {
        if(!angular.isString(value)) {
            return value;
        }  
        return value.replace(/^\s+|\s+$/g, ''); // you could use .trim, but it's not going to work in IE<9
    };
})
.filter('stockprice', function() {
    return function(input) {
        if (input) {
            input = input / 1000;
            return input;
        }
        return input;
    };
})
.filter('bondprice', function() {
    return function(input) {
        if (input) {
            input = input / 1000000000;
            return input;
        }
        return input;
    };
})
.filter('prorata', function() {
    return function(price,q1,q2) {
        return Math.round(price * q1/ q2);
    };
})
.filter('totalValueBond', function() {
    return function(bondList) {
        var total = 0;
        for (var i=0; i < bondList.length; i++) {
            total += Math.round(bondList[i].assetData.price * bondList[i].assetData.quantity / bondList[i].assetData.originQuantity);
        };
        return total;
    };
})
.filter('totalValueFund', function() {
    return function(fundList) {
        var total = 0;
        for (var i=0; i < fundList.length; i++) {
            total = total +  Math.round(fundList[i].BALANCE);
        };
        return total;
    };
})
.filter('stockprofit', function() {
    return function(netQtty, costPrice0, costPrice1, realtimePrice) {
        return Math.round((realtimePrice - (costPrice1 == 0 ? costPrice0 : costPrice1)) * netQtty);
    };
})
.filter('stockprofitpercent', function() {
    return function(netQtty, costPrice0, costPrice1, realtimePrice) {
        var costPrice = costPrice1 == 0 ? costPrice0 : costPrice1;
        var profit = Math.round((realtimePrice - costPrice) * netQtty);
        var profitPercent = Math.round((profit/(costPrice * netQtty)) * 1000)/10;
        if(profitPercent > 0){
            return "+" + profitPercent;
        }
        return profitPercent;
    };
})
.filter('stockprofitcolor', function() {
    return function(costPrice0, costPrice1, realtimePrice) {
        var costPrice = costPrice1 == 0 ? costPrice0 : costPrice1;
        return realtimePrice >= costPrice ? 'green' : 'red';
    };
})
.filter('profitcolor', function() {
    return function(value) {
        return (value == null || value >= 0) ? 'green' : 'red';
    };
})
.filter('sum', function() {
    return function() {
        var total = 0;
        for (var i=0; i < arguments.length; i++) {
        	var item = 0;
        	if (!isNaN(arguments[i])){
        		item = arguments[i];
        	}
            total += parseFloat(item);
        };
        return Math.round(total * 100) / 100;
    };
})
.filter('totalValueStock', function() {
    return function(stockList) {
        var total = 0;
        for (var i=0; i < stockList.length; i++) {
            total += stockList[i].seBalance * stockList[i].realtimePrice;
        };
        return total;
    };
})
.filter('ratioStock', function() {
    return function(value,stockList) {
        var total = 0;
        for (var i=0; i < stockList.length; i++) {
            total += stockList[i].seBalance * stockList[i].realtimePrice;
        };
        return Math.round(value / total * 100);
    };
})
.filter('percent', function() {
    return function(value , offset) {
    	if(value != undefined){
    		value = Math.round(value * Math.pow(10, offset))/(Math.pow(10, offset))
    		return value + "%";
    	}
    	return "";
    };
});