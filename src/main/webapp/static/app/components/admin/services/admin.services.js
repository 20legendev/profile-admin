(function() {
	'use strict';

	angular.module('tcbs.services').factory('AdminService', function(jsonModel, $http) {
		var factory = {
			searchUser : searchUser,
			getProvince : getProvince,
			getUserBank : getUserBank,
			getBankByProvince : getBankByProvince,
			getBankBranch : getBankBranch,
			sortBank : sortBank
		};

		function searchUser(data, cb, err) {
			$http.post('api/admin/search', data).then(function(res) {
				if (res.status == 200 && res.data.status == 0) {
					if (cb)
						cb(res.data.data);
				} else {
					if (err)
						err();
				}
			}, function(errData) {
				if (err)
					err();
			});
		}

		function getProvince(cb) {
			var cache = BaseService.getCache("admin.province");
			if (cache) {
				if (cb)
					cb(JSON.parse(cache));
				return;
			}
			$http.get("api/flex/province").then(function(res) {
				if (res.status == 200 && res.data.status == 0) {
					if (cb)
						cb(res.data.data);
					BaseService.setCache("admin.province", JSON.stringify(res.data.data), 60 * 60 * 1000);
				};
			});
		};

		function getUserBank(userId, cb, err) {
			$http.get("api/admin/user/get-bank-account/" + userId).then(function(res) {
				if (res.status == 200 && res.data.status == 0) {
					if (cb)
						cb(res.data.data);
				};
			});
		};

		function getBankByProvince(province, cb, err) {
			$http.get("api/flex/bank/province/" + province).then(function(res) {
				if (res.status == 200 && res.data.status == 0) {
					if (cb)
						cb(res.data.data);
				};
			});
		};

		function getBankBranch(province, bankSys, cb) {
			$http.get('api/flex/bank/branch/' + province + '/' + bankSys).then(function(res) {
				if (res.status == 200 && res.data.status == 0) {
					if (cb)
						cb(res.data.data);
				};
			});
		};

		function sortBank(data) {
			var arr = ['310', '201', '202', '203', '204'];
			var result = data.sort(function(a, b) {
				if (arr.indexOf(a.banksys) >= 0)
					return -1;
				return 1;
			});
			return result;
		};

		return factory;
	});
})();
