(function () {
    'use strict';

    angular.module('tcbs')
        .controller('detailStockController', detailStockController);

    function detailStockController(Stock, TcbsService, Performance, utils, $scope, $filter, $state, $stateParams, $httpParamSerializer) {
        $scope.stockData = {};
        $scope.orderHistory = {};
        var stockList = [];
        var currentSymbol = $stateParams.symbol;
        $scope.hChart = [];
        $scope.hChart.chartConfig = {
    		title: currentSymbol
        };
        
        $scope.loadStockHistory = function(symbol){
        	if(!symbol){
        		symbol = $stateParams.symbol;
        	}
        	TcbsService.getStockHistory(symbol, "1M", function(data){
        		$scope.renderChart(symbol, data);
        	});
        }
        
        $scope.getCompanyRatio = function(symbol){
        	if(!symbol){
        		symbol = $stateParams.symbol;
        	}
        	$scope.setBusy(true);
        	Stock.getRatioByTicker();
        	Stock.data.get(symbol).then(function(data){
        		var result = data.data;
        		$scope.roe = Math.round(result.roe * 10000)/100;
        		$scope.roa = Math.round(result.roa * 10000)/100;
        		$scope.pb = Math.round(result.pb * 100)/100;
        		$scope.pe = Math.round(result.pe * 100)/100;
        	}).finally(function(){
        		$scope.setBusy(false);
        	});
        }
        
        $scope.renderChart = function(symbol, data){
        	var category = [];
        	var tradingData = [];
        	var priceData = [];
        	for(var i= data.length - 1; i >= 0; i--){
        		category.push(moment(data[i]['dateReport']).format('DD/MM'));
        		tradingData.push(parseFloat(data[i]['totalShare']));
        		priceData.push(parseFloat(data[i]['closePrice'])/100);
        	}
        	$scope.hChart = [];
            $scope.hChart.chartConfig = {}
            $scope.hChart.loading = true;
        	$scope.hChart.chartConfig = {
    			options: {
                    chart: {
                        backgroundColor: '#fff',
                        height: 300
                    },
                    legend: {
                    	enabled: false
                    },
                    plotOptions: {
                    	series: {
                    		borderWidth: 0
                    	}
                    }
                },
                credits:{
                	enabled: false
                },
                title: {
                    text: symbol
                },
                xAxis: {
                    categories: category,
                    labels: {
                    	enabled: false
                    },
                    tickLength: 0
                },
                yAxis: [{
                	title: {
                		enabled: true,
                		text: "GIÁ (VND)",
                		style: {
                			'font-size': '9px'
                		}
                	}
                },{
                	opposite: true,
            		title: {
                		enabled: true,
                		text: "KHỐI LƯỢNG GIAO DỊCH",
                		style: {
                			'font-size': '9px'
                		}
                	}
                }],
                labels: {
                    items: [{
                        html: 'Total fruit consumption',
                        style: {
                            left: '50px',
                            top: '18px',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                        }
                    }]
                },
                series: [{
                    type: 'column',
                    name: 'Khối lượng giao dịch',
                    data: tradingData,
                    color: '#FF0000',
                    pointWidth: 4,
                    yAxis: 1
                }, {
                    type: 'spline',
                    name: 'Giá',
                    data: priceData,
                    color: '#333',
                	lineWidth: 1,
                    marker: {
                    	enabled: false
                    },
                    yAxis: 0
                }]
        	}
        };
        
        $scope.loadMyStock = function(){
            if (stockList.length === 0) {
                $scope.setBusy(true);
                Stock.getMyStock();
                Stock.data.post().then(function (res) {
                    if (angular.isUndefined(res.data)) {
                        return;
                    }
                    stockList = res.data;
                    $scope.stockData =  $filter('filter')(stockList, { symbol: currentSymbol})[0];
                    if ($scope.stockData == null){
                        $state.go('stock.portfolio');
                    };
                }).catch(function(response) {
                    if (response.status == 500) {
                        $scope.$emit('error', {
                          title: 'Service Unavailable',
                          content: 'This service is currently not available. Please try later. Totally sorry about that'});
                      } else {
                        throw response;
                      }
                }).finally(function() {
                    $scope.setBusy(false);
                });
            }
        };

        $scope.loadOrderHistory = function(symbol){
            $scope.setBusy(true);
            var options = {
                fromdate: '01/01/2000',
                todate: moment().format('DD/MM/YYYY'),
                symbol: symbol
            };
            Stock.getOrderHistory();
            Stock.data.post($httpParamSerializer(options)).then(function (res) {
                if (angular.isUndefined(res.data)) {
                    $scope.orderHistory  = [];
                    return;
                }
                $scope.orderHistory = res.data;
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.setBusy(false);
            });
        };


        $scope.nextSymbol = function(){
            $scope.stockData = utils.nextElement(stockList, currentSymbol, "symbol");
            currentSymbol = $scope.stockData.symbol;
            $scope.loadOrderHistory(currentSymbol);
            $scope.loadStockHistory(currentSymbol);
            $scope.getCompanyRatio(currentSymbol);
        };

        $scope.prevSymbol = function(){
            $scope.stockData = utils.prevElement(stockList, currentSymbol, "symbol");
            currentSymbol = $scope.stockData.symbol;
            $scope.loadOrderHistory(currentSymbol);
            $scope.loadStockHistory(currentSymbol);
            $scope.getCompanyRatio(currentSymbol);
        };

        $scope.loadMyStock();
        $scope.loadOrderHistory(currentSymbol);
        $scope.loadStockHistory();
        $scope.getCompanyRatio();
    }
})();