(function () {
    'use strict';

    angular.module('tcbs')
        .controller('portfolioStockController', portfolioStockController);

    function portfolioStockController(Stock, $scope) {
        $scope.stockList = [];
        $scope.chartData = [{ id: 0, label: 'Giá 1 ngày'},
                	 { id: 1, label: 'Giá 1 tuần' },
        	         { id: 2, label: 'Giá 1 tháng' }
        	         ];
        $scope.selected = $scope.chartData[2];

        $scope.loadMyStock = function(){
            $scope.setBusy(true);
            Stock.getMyStock();
            Stock.data.post().then(function (res) {
                if (angular.isUndefined(res.data)) {
                    return;
                }
                $scope.stockList = res.data;
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.setBusy(false);
            });
        };
        
        $scope.changeChartWindow = function(){
        	$scope.$broadcast("change-chart-window", {
        		dataWindow: $scope.selected
        	});
        }
        
        $scope.loadMyStock();
    }
})();