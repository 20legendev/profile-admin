(function () {
    'use strict';

    angular.module('tcbs')
        .controller('effectiveStockController', effectiveStockController);

    function effectiveStockController(Performance, $scope, $filter) {

        $scope.loadDailyChart = function(){
        	Highcharts.setOptions({
        	    lang: {
        	        decimalPoint: '.',
        	        thousandsSep: ','
        	    }
        	});
            $scope.daily = [];
            $scope.daily.loading = true;
            Performance.getDailyData();
            Performance.data.query().then(function (res) {
                if (angular.isUndefined(res)) {
                    return;
                };
                var min = 100;
                var max = 100;
                angular.forEach(res.cusData, function (val, key) {
                	if(val.data > max)
                		max = val.data;
                	if(val.data < min)
                		min = val.data;
                });
                
                angular.forEach(res.vnIndexData, function (val, key) {
                	if(val.data > max)
                		max = val.data;
                	if(val.data < min)
                		min = val.data;
                });
                console.log(max + "@" + min);
                
                $scope.daily.chartConfig = {
                    options: {
                        colors: ['#c4161c', '#5f5f5f'],
                        chart: {
                            type: 'line',
                            backgroundColor: '#fff',
                            height: 400
                        }
                    },
                    title: {
                        text: 'HIỆU QUẢ ĐẦU TƯ TÍCH LUỸ',
                        style: {
                            color: '#5f5f5f',
                            fontWEight: 'bold',
                            fontSize: '15px'
                        },                        
                        verticvalAlign: 'bottom',
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                    	gridLineColor: 'transparent',
                    	type: "datetime",
                        minTickInterval: 24,
                        categories: $.map(res.cusData, function(obj, key){ return [$filter('formatMonth')(obj.date)] }),
                    },
                    yAxis: {min: min - 20, max: max + 5,minTickInterval: 20,maxTickInterval: 20,
                    	title: {
                            enabled: false,
                            text: 'Custom with <b>simple</b> <i>markup</i>',
                            style: {
                                fontWeight: 'normal'
                            }
                        },
                        gridLineColor: 'transparent',
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: true
                            },
                            enableMouseTracking: false
                        }
                    },
                    series: [{
                        name: 'DANH MỤC',
                        data: $.map(res.cusData, function(obj, key){ return [(Math.round(obj.data * 100) / 100)] })
                    }, {
                        name: 'VNINDEX',
                        data: $.map(res.vnIndexData, function(obj, key){ return [(Math.round(obj.data * 100) / 100)] })
                    }]
                };
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.daily.loading = false;
            });
        };

        $scope.loadMonthlyChart = function(){
            $scope.monthly = [];
            $scope.monthly.loading = true;
            Performance.getMonthlyData();
            Performance.data.query().then(function (res) {
                if (angular.isUndefined(res)) {
                    return;
                };
                $scope.monthly.chartConfig = {
                    options: {
                        colors: ['#c4161c', '#5f5f5f'],
                        chart: {
                            type: 'column',
                            backgroundColor: '#fff',
                            height: 400
                        }
                    },
                    title: {
                        text: 'HIỆU QUẢ ĐẦU TƯ THEO THÁNG',
                        style: {
                            color: '#5f5f5f',
                            fontWEight: 'bold',
                            fontSize: '15px'
                        },
                        verticvalAlign: 'middle',
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                    	gridLineColor: 'transparent',
                        categories: $.map(res.userData, function(obj, key){ return [obj.time] })
                    },
                    yAxis: {
                    	gridLineColor: 'transparent',
	                    title: {
	                        enabled: false,
	                        text: 'Custom with <b>simple</b> <i>markup</i>',
	                        style: {
	                            fontWeight: 'normal'
	                        }
	                    }	
                    },
                    series: [{
                        name: 'DANH MỤC CỔ PHIẾU',
                        data: $.map(res.userData, function(obj, key){ return [obj.value * 1] }),
                        pointWidth: 26
                    }, {
                        name: 'VNINDEX',
                        data: $.map(res.vnIndexData, function(obj, key){ return [obj.value * 1] }),
                        pointWidth: 26
                    }]
                };
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.monthly.loading = false;
            });
        };

        $scope.loadClassify = function(){
            $scope.classify = [];
            $scope.classify.loading = true;
            Performance.getClassify();
            Performance.data.query().then(function (res) {
                if (angular.isUndefined(res)) {
                    return;
                };
                $scope.classify.data = res;
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.classify.loading = false;
            });
        };

        $scope.loadBenchmark = function(){
            $scope.benchmark = [];
            $scope.benchmark.loading = true;
            Performance.getBenchmark();
            Performance.data.query().then(function (res) {
                if (angular.isUndefined(res)) {
                    return;
                };
                $scope.benchmark.userData = res.userData;
                $scope.benchmark.vnIndexData = res.vnIndexData;
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.benchmark.loading = false;
            });
        };

        $scope.loadIVChart = function(){
            $scope.ivChart = [];
            $scope.ivChart.chartConfig = {}
            $scope.ivChart.loading = true;
            var datas = [];
            Performance.getStock();
            Performance.data.query().then(function (res) {
                if (angular.isUndefined(res.data)) {
                    return;
                }
                var total = $filter('currency')(res.data.total);
                angular.forEach(res.data.number, function (val, key) {
                    var data = [];
                    data.push(key);
                    data.push(parseInt(val));
                    data.push(parseFloat(val));
                    datas.push(data);
                });
                var number = res.data.number;
                datas.sort();
                $scope.ivChart.chartConfig = {
                    options: {
                        colors: ['#FF0000', '#D00000', '#7F7F7F', '#595959', '#404040', '#8493B4', '#D5DBE5', '#6AF9C4'],
                        chart: {
                            type: 'pie',
                            backgroundColor: '#fff',
                            height: 400
                        }
                    },

                    series: [{
                        showInLegend: true,
                        type: 'pie',
                        colorByPoint: true,
                        tooltip: {
                        	pointFormat: '{series.name}: {point.y:,.1f} vnd'
                        },
                        name: 'Giá trị',
                        innerSize: '65%',
                        data: datas,
                        dataLabels: {
                            formatter: function () {
                                return this.percentage > 5 ? Highcharts.numberFormat(this.percentage, 2) + '%' : null;
                            },
                            distance: -25,
                            style:{
                            	'textShadow': 'none'
                            }
                        }
                    }],
                    credits: {
                        enabled: false
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false
                    },
                    title: {
                        text: 'GIÁ TRỊ ĐẦU TƯ <br>' + total,
                        style: {
                            color: '#5f5f5f',
                            fontWEight: 'bold',
                            fontSize: '15px'
                        },
                        verticvalAlign: 'middle',
                        y: 200,
                        x: 0
                    },
                    tooltip: {
                        valueSuffix: '%'
                    },
                    plotOptions: {
                        pie: {
                            borderColor: '#000000',
                            size: 5,
                            allowPointSelect: false,
                            dataLabels: {
                                enabled: false,
                                distance: -150,
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white',
                                    textShadow: '0px 1px 2px black',
                                }
                            },
                            startAngle: -90,
                            endAngle: 90,
                            center: ['30%', '75%']
                        }
                    },
                    loading: false
                }
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.ivChart.loading = false;
            });
        };

        $scope.loadMonthlyChart();
        $scope.loadIVChart();
        $scope.loadBenchmark();
        $scope.loadClassify();
        $scope.loadDailyChart();
    }
})();