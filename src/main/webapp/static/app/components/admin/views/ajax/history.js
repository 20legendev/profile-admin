(function () {
    'use strict';

    angular.module('tcbs')
        .controller('historyStockController', historyStockController);

    function historyStockController(Stock, $scope, $httpParamSerializer) {
        $scope.histories = [];
        $scope.filter = {};
        $scope.sortBy = 'TXDATE';
        $scope.reverseSort = true;
        $scope.busy = false;

        $scope.sort = function (sortBy) {
            if (sortBy === $scope.sortBy) {
              $scope.reverseSort = !$scope.reverseSort;
            } else {
              $scope.sortBy = sortBy;
              $scope.reverseSort = false;
            }
            $scope.offset = 0;
            $scope.loadHistories();
        };

        $scope.setDefaultLoader = function() {
            $scope.loader = [];
            $scope.loader.offset = 0;
            $scope.loader.limit = 15;
            $scope.loader.lastPortionEmpty = false;
        };

        if ($scope.histories.length === 0) {
            $scope.setDefaultLoader();
        };

        $scope.search = function() {
            $scope.setDefaultLoader();
            $scope.loadHistories();
        };

        $scope.loadHistories = function() {
            if ($scope.loader.lastPortionEmpty || $scope.loader.loading || $scope.busy) {
              return;
            };
            $scope.setBusy(true);
            var typeOrder = $scope.reverseSort ? '' : '-';
            var options = {
              size: $scope.loader.limit,
              page: $scope.loader.offset,
              sort : typeOrder + $scope.sortBy
            };

            if (angular.isDefined($scope.filter.symbol) && $scope.filter.symbol.length > 0) {
                options.symbol = angular.uppercase($scope.filter.symbol);
            };

            if (angular.isDefined($scope.filter.from) && $scope.filter.from.length > 0) {
                options.fromdate = $scope.filter.from;
            };

            if (angular.isDefined($scope.filter.to) && $scope.filter.to.length > 0) {
                options.todate = $scope.filter.to;
            };

            Stock.getOrderHistory();
            Stock.data.post($httpParamSerializer(options)).then(function (res) {
                if (angular.isUndefined(res.data) && $scope.loader.offset === 0) {
                    $scope.histories  = [];
                    return;
                }
                if (res.data.length < $scope.loader.limit || res.data.length === 0) {
                    $scope.loader.lastPortionEmpty = true;
                };
                if ($scope.loader.offset === 0) {
                    $scope.histories = res.data;
                } else {
                    $scope.histories = $.merge($scope.histories, res.data);
                }
                $scope.loader.offset ++;
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.setBusy(false);
            });
        };
        
        $scope.exportExcel = function(){
        	var options = {};
        	if (angular.isDefined($scope.filter.symbol) && $scope.filter.symbol.length > 0) {
                options.symbol = angular.uppercase($scope.filter.symbol);
            };

            if (angular.isDefined($scope.filter.from) && $scope.filter.from.length > 0) {
                options.fromdate = $scope.filter.from;
            };

            if (angular.isDefined($scope.filter.to) && $scope.filter.to.length > 0) {
                options.todate = $scope.filter.to;
            };
            Stock.exportExcel();
            Stock.data.post($httpParamSerializer(options)).then(function (res) {
            	if(res.status == 0){
            		window.location.href = "file/order-history";
            	}
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.setBusy(false);
            });
        }
    };
})();