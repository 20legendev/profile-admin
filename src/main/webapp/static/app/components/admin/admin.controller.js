(function() {
	'use strict';

	angular.module('tcbs').controller('userManagerController', userManagerController);
	function userManagerController(AdminService,$scope, $compile) {
		$scope.search = {
			phone : null,
			fullname : null,
			email : null,
			idnumber : null,
			username : null,
			fromdate : null,
			todate : null,
			vsdstatus : null,
			page : 0
		};
		$scope.list = [];

		$scope.search = function() {
			AdminService.searchUser($scope.search, function(data) {
				$scope.list = data;
			});
		};

		$scope.viewDetail = function(index) {
			angular.element('#admin-user-line').remove();
			var user = $scope.list[index];
			var tmplData = angular.element(document.createElement('adminuser'));
			tmplData.attr('user', user);
			tmplData.attr('height', angular.element('.content-wrapper').height());
			var el = $compile( tmplData )($scope);
			angular.element('.content-wrapper').append(tmplData);
		};
	}

})();
