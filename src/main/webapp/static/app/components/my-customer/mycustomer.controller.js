(function() {
	'use strict';

	angular.module('tcbs').controller('myCustomerController', myCustomerController);

	function myCustomerController(PGDService, TcbsService, $compile, $templateCache, $scope, $http, $timeout, $location, $stateParams, $httpParamSerializer) {
		$scope.data = {
			idnumber : '',
			list : [],
			blank : false
		};
		$scope.status = 'hidden';

		$scope.files = [];

		$scope.search = function() {
			angular.element('#admin-user-detail-action').remove();
			PGDService.getById();
			PGDService.data.post({
				idnumber : $scope.data.idnumber,
				mode: 'MY_CUSTOMER'
			}).then(function(res) {
				if (res.status == 0) {
					$scope.list = JSON.parse(Base64.decode(res.data));
					console.log($scope.list);
				} else {
					$scope.blank = true;
				}
			}).catch(function(response) {
				console.log("ERROR");
			}).finally(function() {
				console.log("DONE");
			});
		};

		$scope.printContract = function(index) {
			var _self = this;
			angular.element('#admin-user-detail-action').remove();
			PGDService.printContract();
			PGDService.data.post({
				referenceid : $scope.list[index].referenceid
			}).then(function(res) {
				if (res.status == 0) {
					var tmplData = angular.element(document.createElement('downloadcontract'));
					var el = $compile( tmplData )($scope);
					angular.element('#user-line-' + index).after(tmplData);
				}
			}).catch(function(response) {
				console.log("ERROR");
			}).finally(function() {
				console.log("DONE");
			});
		};

		$scope.uploadDocument = function(index) {
			var _self = this;
			angular.element('#admin-user-detail-action').remove();
			$scope.uploadEl = angular.element(document.createElement('uploaddocument'));
			$scope.uploadEl.attr('data', JSON.stringify($scope.list[index]));
			var el = $compile( $scope.uploadEl )($scope);
			angular.element('#user-line-' + index).after($scope.uploadEl);
		};

		$scope.confirmOpen = function(index) {
			var customer = $scope.list[index];
			var c = confirm("Bạn có muốn xác nhận thông tin và mở tài khoản cho khách hàng " + customer.lastname + " " + customer.firstname + " không?");
			if (!c)
				return;
			TcbsService.confirmOpen(customer.referenceid, function(data) {
				$scope.search();
				$scope.showSuccess(customer, 'Bạn đã xác nhận mở tài khoản cho khách hàng');
			}, function() {
				$scope.showError(customer, 'Xác nhận mở tài khoản cho khách hàng');
			});
		};

		$scope.confirmContract = function(index) {
			var customer = $scope.list[index];
			var c = confirm("Bạn có muốn xác nhận bạn đã ký hợp đồng và lấy chữ ký khách hàng " + customer.lastname + " " + customer.firstname + " không?");
			if (!c)
				return;
			TcbsService.confirmContract(customer.referenceid, function(data) {
				$scope.search();
				$scope.showSuccess(customer, 'Xác nhận ký hợp đồng và kiểm tra CMND khách hàng');
			}, function() {
				$scope.showError(customer, 'Xác nhận ký hợp đồng và kiểm tra CMND khách hàng');
			});
		};

		$scope.showSuccess = function(customer, text) {
			$scope.status = 'text-green';
			$scope.statusText = text + customer.lastname + ' ' + customer.firstname + " thành công!";
			$timeout(function() {
				$scope.hideError();
			}, 2000);
		};
		$scope.showError = function(customer, text) {
			$scope.status = 'text-red';
			$scope.statusText = text + ' ' + customer.lastname + ' ' + customer.firstname + " không thành công!";
			$timeout(function() {
				$scope.hideError();
			}, 2000);
		};
		$scope.hideError = function() {
			$scope.status = 'hidden';
			$scope.statusText = '';
		};
		
		$scope.search();
	}

})();
