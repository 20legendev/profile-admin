(function() {
    'use strict';

    angular.module('tcbs')
        .factory('Company', function(jsonModel) {
            return jsonModel('/info', 'company');
        })
        .controller('wlStockController', wlStockController);
        
    function wlStockController($scope, $state, $q, Company) {
        var stocks = ['VNM','GAS','VIC','MSN','VCB','BMP','CTG','DRC','FPT', 'CII'];
        var request = [];
        $scope.stockInfos = [];
        angular.forEach(stocks, function(value, key){
            request.push(Company.get(value));
        });

        $q.all(request).then(function(values) {
            $scope.stockInfos = values;
        });
    };
})();