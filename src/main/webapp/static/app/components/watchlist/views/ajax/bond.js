(function() {
    'use strict';

    angular.module('tcbs')
        .controller('wlBondController', wlBondController);
    function wlBondController($scope, $state, Bond) {
		$scope.bonds = [];

		Bond.getProduct();
    	Bond.data.query().then(function (res) {
            if (angular.isUndefined(res.data)) {
                return;
            }
            $scope.bonds = res.data;
       }).catch(function(response) {
             throw response;
       });
    }
})();