angular.module('tcbs.directives')
.directive('groupItem', function(RecursionHelper) {
	return {
		restrict : 'E',
		scope : {
			data: '='
		},
		// templateUrl : 'static/app/components/permission/views/directive/group-item.htm',
		template: '<p>{{ data.groupname }}</p>'+
	        '<ul>' + 
	        '<li ng-repeat="(key, child) in data.children">' + 
	            '<group-item data="child"></group-item>' +
	        '</li>' +
	    '</ul>',
		// link : function(scope, element, attrs) {
		//	console.log("A");
		// }
	    compile: function(element) {
            return RecursionHelper.compile(element, function(scope, iElement, iAttrs, controller, transcludeFn){
            });
        }
	};
});
