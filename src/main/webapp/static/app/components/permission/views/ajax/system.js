(function() {
	'use strict';

	angular.module('tcbs').controller('systemController', systemController);
	function systemController($scope, $timeout, $state, SystemService) {
		$scope.init = function() {
			SystemService.listAll(function(data) {
				$scope.data = data;
			});
		};

		$scope.init();
	}

})();
