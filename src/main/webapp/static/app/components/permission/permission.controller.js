(function() {
	'use strict';

	angular.module('tcbs').controller('permissionController', permissionController);
	function permissionController($scope, $rootScope, PermissionService, $templateCache) {
		$scope.init = function() {
			$scope.initGroupId = -1;
			$scope.reload();

		};

		$scope.reload = function() {
			PermissionService.getGroupTree(function(data) {
				if (data.length > 0) {
					$scope.groups = $scope.prepareData(data);
					console.log($scope.groups);
				}
			});
		};

		$scope.newSubItem = function(scope) {
			var nodeData = scope.$modelValue;
			if (!nodeData.children)
				nodeData.children = [];
			$scope.initGroupId -= 1;
			$scope.group = {
				groupid : $scope.initGroupId,
				grouplevel : nodeData.grouplevel + 1,
				groupname : "Tên group",
				groupstatus : 1,
				grouptype : nodeData.grouptype,
				parentid : nodeData.groupid,
				children : []
			};
			nodeData.children.push($scope.group);
			$scope.groupData[$scope.group.groupid] = $scope.group;
		};

		$scope.saveGroup = function() {
			PermissionService.updateGroup($scope.group, function(data) {
				$scope.reload();
			}, function(err) {
				console.log(err);
			});
		};

		$scope.drop = function(e) {
			console.log(e);
		};

		$scope.delete = function(scope) {
			if(scope.$modelValue.groupid <= 0){
				scope.remove(scope);
			}else if (confirm("Bạn có chắc chắn muốn xoá group " + scope.$modelValue.groupname + " không?")) {
				PermissionService.removeGroup(scope.$modelValue.groupid, function(data) {
					scope.remove(scope);
				}, function(err) {
					console.log(err);
				});
			}
		};

		$scope.editGroup = function(groupid) {
			$scope.group = $scope.groupData[groupid];
			PermissionService.countGroupMember(groupid, function(data) {
				$scope.groupmembercount = data;
			});
		};

		$scope.prepareData = function(groups) {
			var groupsData = {};
			groups.forEach(function(item) {
				item.children = [];
				item.id = item.groupid;
				groupsData[item.groupid] = item;
			});
			for (var i = 0; i < groups.length; i++) {
				if (groups[i].parentid) {
					if (!groupsData[groups[i].parentid].children)
						groupsData[groups[i].parentid].children = [];
					groupsData[groups[i].parentid].children.push(groups[i]);
				}
			}
			$scope.groupData = JSON.parse(JSON.stringify(groupsData));
			var data = [];
			for (var i = 0; i < groups.length; i++) {
				if (groups[i].parentid) {
					delete groupsData[groups[i].groupid];
				} else {
					data.push(groupsData[groups[i].groupid]);
				}
			}
			$scope.group = data[0];
			return data;
		};

		$scope.treeOptions = {
			dropped : function(e) {
				var child = e.source.nodeScope.$modelValue.groupid;
				var parent = e.dest.nodesScope.$nodeScope.$modelValue.groupid;
				if($scope.groupData[child].parentid != parent){
					$scope.groupData[child].parentid = parent;
					$scope.group = $scope.groupData[child];
					$scope.saveGroup();
				}
			}
		};

		$scope.group = {};
		$scope.groups = {};
		$scope.init();
	}

})();
