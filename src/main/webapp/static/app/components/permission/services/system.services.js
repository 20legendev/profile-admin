(function() {
	'use strict';

	angular.module('tcbs.services').factory('SystemService', function(jsonModel, $http) {
		var factory = {
			listAll : listAll,
		};

		function listAll(cb, err) {
			$http.get('api/system/list').then(function(res) {
				if (res.status == 200 && res.data.status == 0) {
					if (cb)
						cb(res.data.data);
				} else {
					if (err)
						err();
				}
			});
		}

		return factory;
	});
})();
