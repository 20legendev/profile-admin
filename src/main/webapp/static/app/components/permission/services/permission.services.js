(function() {
	'use strict';

	angular.module('tcbs.services').factory('PermissionService', function(jsonModel, $http) {
		var factory = {
			getGroupTree : getGroupTree,
			buildTree : buildTree,
			countGroupMember : countGroupMember,
			updateGroup : updateGroup,
			removeGroup: removeGroup,
			updateParent: updateParent
		};

		function removeGroup(groupid, cb, err) {
			$http.post('api/group/remove', {
				groupid : groupid
			}).then(function(res) {
				if (res.status == 200 && res.data.status == 0) {
					if (cb)
						cb(res.data.data);
				} else {
					if (err)
						err();
				}
			});
		}

		function getGroupTree(cb, err) {
			$http.get('api/permission/group/tree').then(function(res) {
				if (res.status == 200 && res.data.status == 0) {
					if (cb)
						cb(res.data.data);
				} else {
					if (err)
						err();
				}
			});
		}

		function updateGroup(data, cb, err) {
			$http.post('api/group/update', data).then(function(res) {
				if (res.status == 200 && res.data.status == 0) {
					if (cb)
						cb();
				} else {
					if (err)
						err();
				}
			});
		};
		
		function updateParent(child, parent, cb, err) {
			$http.post('api/group/update-parent', {
				groupid: child,
				parentid: parent
			}).then(function(res) {
				if (res.status == 200 && res.data.status == 0) {
					if (cb)
						cb();
				} else {
					if (err)
						err();
				}
			});
		};

		function countGroupMember(groupid, cb, err) {
			$http.get('api/group/count-member/' + groupid).then(function(res) {
				if (res.status == 200 && res.data.status == 0) {
					if (cb)
						cb(res.data.data);
				} else {
					if (err)
						err();
				}
			});
		}

		function buildTree(groups) {

		}

		return factory;
	});
})(); 