(function() {
	'use strict';
	angular.module('tcbs.services').factory('PGDService', function(jsonModel) {

		var factory = {
			data : [],
			getById : getById,
			printContract: printContract
		};

		function getById() {
			factory.data = jsonModel('/check-id', 'rm');
		};

		function printContract() {
			factory.data = jsonModel("/print-contract", "rm");
		}

		return factory;

	});
})();
