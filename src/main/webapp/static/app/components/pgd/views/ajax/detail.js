(function () {
    'use strict';

    angular.module('tcbs')
        .controller('detailNotificationController', detailNotificationController);

    function detailNotificationController(Notification, $scope, $stateParams) {
        var type = $stateParams.type;
        var id = $stateParams.id;
        $scope.notification = [];

        $scope.loadNotification = function(){
            $scope.busy = true;
            Notification.get(id,{"type": type}).then(function (res) {
                if (angular.isUndefined(res.data)) {
                    return;
                }
                $scope.notification = res.data;
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.setBusy(false);
            });
        };
        $scope.loadNotification();
    }
})();