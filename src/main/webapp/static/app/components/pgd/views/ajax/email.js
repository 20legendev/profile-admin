(function () {
    'use strict';

    angular.module('tcbs')
        .controller('emailNotificationController', emailNotificationController);

    function emailNotificationController(Notification, $scope) {
        $scope.notifications = [];

        if ($scope.notifications.length === 0) {
            $scope.setDefaultLoader();
        };

        $scope.getNotifications = function(){
            if ($scope.loader.lastPortionEmpty || $scope.loader.loading || $scope.busy) {
              return;
            };
            $scope.setBusy(true);
            var options = {
              number: $scope.loader.limit,
              page: $scope.loader.offset,
              type: 'mail'
            };
            Notification.query(options).then(function (res) {
                if (angular.isUndefined(res.data) && $scope.loader.offset === 0) {
                    $scope.notifications  = [];
                    return;
                }
                if (res.data.length < $scope.loader.limit || res.data.length === 0) {
                    $scope.loader.lastPortionEmpty = true;
                };
                if ($scope.loader.offset === 0) {
                    $scope.notifications = res.data;
                } else {
                    $scope.notifications = $.merge($scope.notifications, res.data);
                }
                $scope.loader.offset ++;
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.setBusy(false);
            });
        };
    }
})();