(function() {
	'use strict';

	angular.module('tcbs').controller('profileDetailController',
			profileDetailController);
	function profileDetailController(Profile, TcbsService, $scope) {
		$scope.edit = {
			email: undefined,
			emailError: undefined,
			phone: undefined,
			otp: ''
		};
		
		$scope.getFullProfile = function(){
    		$scope.setBusy(true);
    		Profile.getFull();
    		Profile.data.query().then(function (res) {
                if (angular.isUndefined(res)) {
                    return;
                }
                $scope.user = res.data;
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.setBusy(false);
            });
    	}
		
		$scope.editData = function(type, enable){
			if(type){
				$scope[type + 'edit']  =$scope[type + 'edit'] ? false: true;
				$scope.edit[type] = $scope.user[type];
			}else{
				var arr = ['email','phone'];
				for(var i=0; i < arr.length; i++){
					$scope[arr[i] + 'edit']  = enable != undefined ? enable : $scope[arr[i] + 'edit'] ? false: true;
					$scope.edit[arr[i]] = $scope.user[arr[i]];
				}
			}
    	}
		
		$scope.clearError = function(form){
			$scope.edit.emailSystemError = false;
		}
		
		$scope.saveData = function(form){
			$scope.setBusy(true);
			Profile.update();
			var data = {};
			if($scope.edit.email && $scope.edit.email != ''){
				data.email = $scope.edit.email;
			}
			if($scope.edit.phone && $scope.edit.phone != ''){
				data.phone = $scope.edit.phone;
			}
			Profile.data.post(data).then(function(res){
				$scope.setBusy(false);
				switch(res.status){
				case 1:
					form.input.$error.email = true;
					break;
				case 0:
					$scope.showotp = true;
					$scope.edit.otp = '';
					$('timer')[0].reset();
					$('timer')[0].start();
					break;
				case -1:
					$scope.showEditEmailError("Email đã được sử dụng. Vui lòng sử dụng email khác!");
					break;
				default:
					$scope.showEditEmailError("Hệ thống đang có lỗi, vui lòng thử lại sau");
					setTimeout(function(){
						$scope.cancelSaveEmail();
					}, 3000);
					break;
				}
			});
		}
		
		$scope.showEditEmailError = function(text){
			$scope.edit.emailSystemError = true;
			$scope.edit.errorText = text;
		}
		
		$scope.submitOtp = function(){
			if($scope.edit.otp != ''){
				$scope.setBusy(true);
				TcbsService.validateOtp();
				TcbsService.data.post({
					code: $scope.edit.otp
				}).then(function(res){
					if(res.status == 0){
						$scope.cancelSaveEmail();
						$scope.getFullProfile();
						$scope.editData(undefined, false);
					}else{
						$scope.cancelSaveEmail();
						$scope.showEditEmailError("Mã xác nhận không đúng. Xin vui lòng thử lại");
					}
				}).catch(function(response) {
                    if (response.status == 500) {
                        $scope.$emit('error', {
                          title: 'Service Unavailable',
                          content: 'This service is currently not available. Please try later. Totally sorry about that'});
                      } else {
                        throw response;
                      }
                }).finally(function() {
                    $scope.setBusy(false);
                });
			}
		}
		
		$scope.cancelOtp = function(){
			$scope.cancelSaveEmail();
			$scope.editData(undefined, false);
			$scope.clearError();
		}
		
		$scope.cancelSaveEmail = function(){
			$scope.showotp = false;
			$('timer')[0].stop();
		}
    	$scope.getFullProfile();
	}
})();