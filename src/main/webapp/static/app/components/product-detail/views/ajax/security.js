(function() {
	'use strict';

	angular.module('tcbs').controller('profileSecurityController',
			profileSecurityController);
	function profileSecurityController(Profile, $scope, $timeout, $state) {
		$scope.model = {
			oldpass: '',
			newpass: '',
			newpassretype: '',
			otp: ''
		}
		
		$scope.clearError = function(){
			
		}
		
		$scope.typePassword = function(e){
			if(e.keyCode != 13){
				$scope.model.wrongPassword = false;
			}
		}
		
		$scope.passwordEnter = function(form){
			if($scope.model.newpass != $scope.model.newpassretype){
				$scope.model.passwordMatch = true;
			}else{
				$scope.model.passwordMatch = false;
			}
		}
		
		$scope.changePasswordRequest = function(form){
			if($scope.model.newpassretype != $scope.model.newpass){
				return;
			}
			$scope.setBusy(true);
			Profile.changePasswordRequest();
			Profile.data.post({
				password: $scope.model.oldpass,
				newpassword: $scope.model.newpassretype
			}).then(function(res){
				console.log(res);
				switch(res.status){
				case 3:
					$scope.model.wrongPassword = true;
					break;
				case 0:
					$scope.showOtp();
					break;
				}
			}).finally(function(){
				$scope.setBusy(false);
			});
		}
		
		$scope.submitOtp = function(){
			if($scope.model.otp != ''){
				$scope.setBusy(true);
				Profile.changePasswordValidate();
				Profile.data.post({
					otp: $scope.model.otp
				}).then(function(res){
					switch(res.status){
					case 0:
						$scope.changePasswordSuccess();
						break;
					case 2:
						$scope.changeError = true;
						$scope.errorMessage = "Việc cập nhật mật khẩu có lỗi. Xin vui lòng thử lại sau. Cảm ơn bạn!";
						break;
					case 5:
						$scope.changeError = true;
						$scope.errorMessage = "Phiên làm việc của bạn đã hết hạn. Vui lòng thử lại";
					default:
						$scope.changeError = true;
						$scope.errorMessage = "Mã OTP bạn nhập chưa chính xác. Xin vui lòng thử lại!";
						break;
					}
				}).catch(function(response) {
                    if (response.status == 500) {
                        $scope.$emit('error', {
                          title: 'Service Unavailable',
                          content: 'This service is currently not available. Please try later. Totally sorry about that'});
                      } else {
                        throw response;
                      }
                }).finally(function() {
                    $scope.setBusy(false);
                });
			}
		}
		
		$scope.changePasswordSuccess = function(){
			$scope.changeSuccess = true;
			$scope.showotp = true;
			$scope.model = {
				oldpass: '',
				newpass: '',
				newpassretype: '',
				otp: ''
			}
			$timeout(function(){
				$scope.showotp = false;
				$state.go("profile.detail");
			}, 1500);
		}
		
		$scope.showOtp = function(){
			$scope.showotp = true;
			$scope.model.otp = '';
			$('timer')[0].reset();
			$('timer')[0].start();
		}
		
		$scope.cancelOtp = function(){
			$scope.showotp = false;
		}
	}
})();