(function () {
    'use strict';

    angular.module('tcbs')
        .controller('productDetailController', productDetailController);

    function productDetailController(TcbsService, $scope, $stateParams, $rootScope, $filter) {
    	var symbol = $stateParams.id;
    	
    	
    	$scope.loadStockHistory = function(symbol){
        	if(!symbol){
        		symbol = $stateParams.symbol;
        	}
        	$scope.setBusy(true);
        	TcbsService.getStockHistory(symbol, "1M", function(data){
        		$scope.renderChart(symbol, data);
        		$scope.setBusy(false);
        	});
        }
    	
    	$scope.renderChart = function(symbol, data){
        	var category = [];
        	var tradingData = [];
        	var priceData = [];
        	for(var i= data.length - 1; i >= 0; i--){
        		category.push(moment(data[i]['dateReport']).format('DD/MM'));
        		tradingData.push(parseFloat(data[i]['totalShare']));
        		priceData.push(parseFloat(data[i]['closePrice'])/100);
        	}
        	$scope.hChart = [];
            $scope.hChart.chartConfig = {}
            $scope.hChart.loading = true;
        	$scope.hChart.chartConfig = {
    			options: {
                    chart: {
                        backgroundColor: '#fff',
                        height: 400
                    },
                    legend: {
                    	enabled: false
                    },
                    plotOptions: {
                    	series: {
                    		borderWidth: 0
                    	}
                    }
                },
                credits:{
                	enabled: false
                },
                title: {
                    text: symbol
                },
                xAxis: {
                    categories: category,
                    labels: {
                    	enabled: false
                    },
                    tickLength: 0
                },
                yAxis: [{
                	title: {
                		enabled: true,
                		text: "GIÁ (VND)",
                		style: {
                			'font-size': '9px'
                		}
                	}
                },{
                	opposite: true,
            		title: {
                		enabled: true,
                		text: "KHỐI LƯỢNG GIAO DỊCH",
                		style: {
                			'font-size': '9px'
                		}
                	}
                }],
                labels: {
                    items: [{
                        html: 'Total fruit consumption',
                        style: {
                            left: '50px',
                            top: '18px',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                        }
                    }]
                },
                series: [{
                    type: 'column',
                    name: 'Khối lượng giao dịch',
                    data: tradingData,
                    color: '#FF0000',
                    pointWidth: 4,
                    yAxis: 1
                }, {
                    type: 'spline',
                    name: 'Giá',
                    data: priceData,
                    color: '#333',
                	lineWidth: 1,
                    marker: {
                    	enabled: false
                    },
                    yAxis: 0
                }]
        	}
        };
        $scope.loadStockHistory(symbol);
	}
})();

