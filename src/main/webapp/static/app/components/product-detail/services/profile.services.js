(function() {
	'use strict';

	angular.module('tcbs.services').factory('Profile', function(jsonModel) {
		var factory = {
			data : [],
			getFull : getFull,
			update : update,
			changePasswordRequest: changePasswordRequest,
			changePasswordValidate: changePasswordValidate
		};

		function getFull() {
			factory.data = jsonModel('/detail', 'me');
		}

		function update() {
			factory.data = jsonModel('/update', 'me');
		}
		
		function changePasswordRequest() {
			factory.data = jsonModel('/change-pwd-request', 'me');
		}
		
		function changePasswordValidate() {
			factory.data = jsonModel('/change-pwd-validate', 'me');
		}
		
		return factory;
	})
})();