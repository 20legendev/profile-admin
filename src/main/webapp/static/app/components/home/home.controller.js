(function () {
    'use strict';

    angular.module('tcbs')
        .controller('homeController', homeController);

    function homeController($scope, $rootScope, $filter, $timeout) {
        $scope.config = {};
        $scope.stockList = [];
        $scope.bondList = [];
        $scope.fundList = [];
        $scope.totalStock = 0;
        $scope.originTotalStock = 0;
        $scope.count = 0;
        // socket.connect(Config.socketUrl);
        /*
        socket.on('pricedata', function (data) {
            if ( $scope.stockList.length > 0 ) {
                angular.forEach($scope.stockList, function(stock, index){
                    if (stock.symbol == data.symbol && stock.realtimePrice != data.formattedMatchPrice){
                        $scope.stockList[index].realtimePrice = data.formattedMatchPrice;
                        $scope.stockList[index].shake=true;
                        $timeout(function(){
                            $scope.stockList[index].shake=false;
                        },2000,true);
                        angular.extend($scope.stockList[index], {isGrowth: data.formattedChangeValue > 0});
                        $scope.totalStock = $filter('totalValueStock')($scope.stockList);
                        FooterManager.asset.setAvailable($scope.totalStock  - $scope.originTotalStock);
                        $scope.originTotalStock = $scope.totalStock;
                        FooterManager.asset.setEquity();
                        $scope.$apply();
                    }
                });
            };
        });

        $scope.loadMyFund = function(){
            $scope.setBusy(true);
            Fund.getBalance();
            Fund.data.query().then(function (res) {
                if (angular.isUndefined(res)) {
                    return;
                }
                if(res.length == 0){
                	$scope.fundListBlank = true;
                }else{
	                $scope.fundList = res;
	                $scope.refindData(undefined, res);
                }
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.setBusy(false);
            });
        };

        $scope.loadMyBond = function(){
            $scope.setBusy(true);
            Bond.getAssets();
            Bond.data.query().then(function (res) {
                if (angular.isUndefined(res.data)) {
                    return;
                }
                if(res.data.length == 0){
                	$scope.bondListBlank = true
                }else{
	                $scope.bondList = res.data;
	                $scope.refindData(undefined, undefined, res.data);
                }
            }).catch(function(response) {
            	console.log("BOND ", response);
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.setBusy(false);
            });
        };

        $scope.loadMyStock = function(){
            $scope.setBusy(true);
            Stock.getMyStock();
            Stock.data.post().then(function (res) {
                if (angular.isUndefined(res.data)) {
                    return;
                }
                if(res.data.length == 0){
                	$scope.stockListBlank = true;
                }else{
	                $scope.stockList = res.data;
                    angular.forEach($scope.stockList, function(stock, index){
                        angular.extend($scope.stockList[index], {isGrowth: $scope.stockList[index].realtimePriceChange >= 0});
                    });
	                $scope.totalStock = $filter('totalValueStock')(res.data);
                    $scope.originTotalStock = $filter('totalValueStock')(res.data);
	                $scope.refindData(res.data);
                }
            }).catch(function(response) {
                if (response.status == 500) {
                    $scope.$emit('error', {
                      title: 'Service Unavailable',
                      content: 'This service is currently not available. Please try later. Totally sorry about that'});
                  } else {
                    throw response;
                  }
            }).finally(function() {
                $scope.setBusy(false);
            });
        };
        
        $scope.refindData = function(stockData, fundData, bondData){
        	var formatData = [];
        	stockData = stockData != undefined ? stockData : $scope.config.stockData == undefined ? [] : $scope.config.stockData;
        	$scope.config.stockData = stockData;
            for(var i=0; i < stockData.length; i++){
            	formatData.push([stockData[i].symbol, stockData[i].seBalance * stockData[i].realtimePrice]);
            }
            
            fundData = fundData != undefined ? fundData : $scope.config.fundData == undefined ? [] : $scope.config.fundData;
            $scope.config.fundData = fundData;
            for(var i=0; i < fundData.length; i++){
            	formatData.push([fundData[i].FUND, parseFloat(fundData[i].BALANCE)]);
            }
            
            bondData = bondData != undefined ? bondData : $scope.config.bondData == undefined ? [] : $scope.config.bondData;
        	$scope.config.bondData = bondData;
        	var bond;
        	for(var i=0; i < bondData.length; i++){
        		bond = bondData[i].assetData;
        		formatData.push([bond.bondProductCode, $filter('prorata')(bond.price, bond.quantity, bond.originQuantity)]);
        	}
            $scope.setPieData(formatData);
        }
        
        $scope.setPieData = function(data){
        	if(data.length == 0){
        		data.push(["Giá trị đầu tư", 0.000001]);
        	}
        	if($scope.piechart && $scope.piechart.chartConfig){
        		var seriesArray = $scope.piechart.chartConfig.series[0];
        		seriesArray.data = data;
        		$scope.piechart.chartConfig.subtitle.text = 'VND ' + $filter('currency')($scope.assetValue ? $scope.assetValue.available : '');
        		return;
        	}
        	
        	$scope.piechart = [];
            $scope.piechart.chartConfig = {}
            $scope.piechart.chartConfig = true;
        	$scope.piechart.chartConfig = {
                    options: {
                        colors: ['#FF0000', '#D00000', '#7F7F7F', '#595959', '#404040', '#8493B4', '#D5DBE5', '#6AF9C4'],
                        chart: {
            	            renderTo: 'home-piechart-container',
            	            type: 'pie',
            	            height: 400
            	        },
                        legend: {
                        	enabled: false
                        }
                    },
                    credits: {
        	            enabled: false
        	        },
                    series: [{
                        showInLegend: true,
                        type: 'pie',
                        colorByPoint: true,
                        tooltip: {
                    		pointFormatter: function(){
                    			if(this.y > 0.000001){
                    				return this.series.name + ": " + $filter('currency')(this.y) + ' VND'
                    			}
                    			return this.series.name + ": 0";
                    		}
                        },
                        name: 'Giá trị',
                        innerSize: '70%',
                        data: data,
                        dataLabels: {
                        	formatter: function() {
    	                    	var val = Math.round(this.percentage*10) / 10;
    	                    	if(val > 5){
    	                    		return val + "%";
    	                    	}
    	                    	return "";
    	                    },
    	                    distance: -25,
    	                    color:'white',
    	                    style: {"color": "contrast", "fontSize": "11px", "textShadow": "none", "fontWeight":"bold"}
                        }
                    }],
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false
                    },
                    title: {
                        text: 'GIÁ TRỊ ĐẦU TƯ',
                        style: {'color': '#929292', 'font-size': '12px', 'margin-bottom': '5px', 'font-weight': 'normal'},
                        y: 200,
                        x: 0
                    },
                    subtitle: {
                        text: 'VND ' + $filter('currency')($scope.assetValue ? $scope.assetValue.available : ''),
                		y: 230,
                        x: 0,
                        style: {
                        	'color': '#131313',
                        	'font-size': '19px'
                        }
                    },
                    tooltip: {
                        valueSuffix: '%'
                    },
                    plotOptions: {
        	            pie: {
        	            	size: 5,
        	                innerSize: '70%',
        	                showInLegend: true,
        	                center: ['30%', '75%'],
        	                startAngle: -90,
                            endAngle: 90,
        	            },
        	            series: {
        	                dataLabels: {
        	                    enabled: true,
        	                    formatter: function() {
        	                    	var val = Math.round(this.percentage*10) / 10;
        	                    	if(val > 5){
        	                    		return val + "%";
        	                    	}
        	                    	return "";
        	                    },
        	                    distance: -20,
        	                    color:'white',
        	                    style: {"color": "contrast", "fontSize": "13px", "textShadow": "none", "fontWeight":"bold"}
        	                }
        	            }
        	        },
                    loading: false
                }
        };

        $scope.showWaterfallChart = function(value) {
        	if($scope.hChart && $scope.hChart.chartConfig){
        		var seriesArray = $scope.hChart.chartConfig.series[0];
        		seriesArray.data = [value.available, value.allocated, -value.debt, value.available + value.allocated - value.debt];
        		return;
        	}
            $scope.hChart = [];
            $scope.hChart.chartConfig = {}
            $scope.hChart.loading = true;
            $scope.hChart.chartConfig = {
        		options: {
                    chart: {
                        type: 'waterfall',
                        backgroundColor: '#fff',
                        height: 300
                    },
                    
                    plotOptions: {
                    	series: {
                    		borderWidth: 0
                    	}
                    }
                },
                title: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    type: 'category',
                    tickLength: 0,
                    labels: {
                        style: {'font-size': '16px', 'padding-top': '20px', 'color': '#646464', 'font-family': 'MyriadPro-Regular'}
                    }
                },

                yAxis: {
                    title: {
                        text: ''
                    },
                    gridLineColor: '#FFFFFF',
                    labels: {
                        enabled: false
                    }
                },
                
                series: [{
                	tooltip: {
                 		pointFormatter: function(){
             				return $filter('currency')(this.y) + ' VND'
                 		}
                    },
                    showInLegend: false,
                    name: 'Giá trị',
                    data: [{
                        name: 'Giá trị đầu tư',
                        y: value.available,
                        color:'#595959'
                    }, {
                        name: 'Tiền',
                        y: value.allocated,
                        color:'#B0ABAA'
                    }, {
                        name: 'Tổng nợ',
                        y: -value.debt,
                        color:'#FF0000'
                    }, {
                        name: 'Tài sản ròng',
                        isIntermediateSum: true,
                        color:'#D5DAE7'
                    }],
                    dataLabels: {
                        enabled: false,
                        style: {
                            color: '#FFFFFF',
                            fontWeight: 'bold',
                            textShadow: 'none'
                        }
                    },
                    pointPadding: 0
                }]
            };
        };
        
        $scope.changedAsset = function(value){
            $scope.assetValue = value;
            $scope.showWaterfallChart(value);
            $scope.refindData();
        };

        $rootScope.$on('asset:changed', function assetChanged(event, value) {
        	$scope.changedAsset(value);
        });
        
        if (FooterManager.asset.available > 0) {
            $scope.changedAsset(FooterManager.asset);
        };
        $scope.setPieData([]);
        
        $scope.loadMyStock();
        $scope.loadMyBond();
        $scope.loadMyFund();
        */
   }
})();

