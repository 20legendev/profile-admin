angular.module('tcbs').directive('stockBlock', function() {
	return {
		templateUrl : 'static/app/components/home/views/stockblock.view.htm'
	};
}).directive('fundBlock', function() {
	return {
		templateUrl : 'static/app/components/home/views/fundblock.view.htm'
	};
}).directive('bondBlock', function() {
	return {
		templateUrl : 'static/app/components/home/views/bondblock.view.htm'
	};
});
