angular.module('tcbs.directives').directive('header', function(LayoutManager, utils, toastr, $state, $httpParamSerializer, NavManager) {
	return {
		restrict : 'E',
		templateUrl : 'static/app/elements/header.htm',
		scope : false,
		link : function($scope, $elm) {
			$scope.user = NavManager.user;

			// $scope.header = LayoutManager.header;
			// $scope.notifications = [];
			// $scope.unread = 0;
			/*
			 Notification.request("count").then(function (res) {
			 if (angular.isUndefined(res.data)) {
			 return;
			 }
			 $scope.unread = res.data.data;
			 }).catch(function(response) {
			 throw response;
			 });

			 $scope.toggleNotification = function(event){
			 event.preventDefault();
			 event.stopPropagation();
			 $scope.isOpen = !$scope.isOpen;
			 if ($scope.isOpen) {
			 Notification.query({"number": 10, "page": 0}).then(function (res) {
			 if (angular.isUndefined(res.data)) {
			 return;
			 }
			 $scope.notifications = res.data;
			 }).catch(function(response) {
			 throw response;
			 });
			 }
			 };

			 $scope.searchSelected = function(data){
			 $state.go('product-detail', {id: data.originalObject.name});
			 }

			 $scope.actionLink = function(notification){
			 var data = {read: 1};
			 Notification.edit(notification.notiId, $httpParamSerializer(data)).then(function (res) {
			 }).catch(function(response) {
			 throw response;
			 });
			 $state.go('notification.' + notification.type);
			 };

			 $(window).click(function(){
			 $scope.isOpen = false;
			 $scope.$apply();
			 });

			 $scope.toggleMenu = function(){
			 if (!$(".leftcol").hasClass("left300")) {
			 $(".leftcol").addClass("left300");
			 $(".rightcolin").addClass("left0");
			 if ($( window ).width() >= 1200) {
			 $(".rhead-toolbox").addClass("right0");
			 }

			 } else {
			 $(".leftcol").removeClass("left300");
			 $(".rightcolin").removeClass("left0");
			 if ($( window ).width() >= 1200) {
			 $(".rhead-toolbox").removeClass("right0");
			 }
			 }
			 };

			 Notification.request('login').then(function(res){
			 token = res.data.data;
			 var connectionOptions = {
			 "force new connection": true,
			 "reconnectionAttempts": "Infinity",
			 "timeout": 10000,
			 "transports": ["websocket"],
			 'query': "token=" + token
			 };
			 socket.connect(Config.notiUrl, connectionOptions);
			 socket.on('noti', function (msg) {
			 $scope.unread ++;
			 $scope.$apply();
			 toastr.success(msg.title);
			 });
			 });
			 */
		}
	};
}).directive('footer', function(FooterManager) {
	return {
		restrict : 'E',
		templateUrl : 'static/app/elements/footer.htm',
		scope : false,
		link : function($scope) {
			// $scope.asset = FooterManager.asset;
		}
	};
}).directive('downloadcontract', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'static/app/elements/download-contract.htm'
	};
}).directive('uploaddocument', function(TcbsService) {
	return {
		restrict : 'E',
		scope : false,
		replace : true,
		templateUrl : 'static/app/elements/upload-document.htm',
		link : function(scope, element, attrs) {
			scope.data = JSON.parse(attrs.data);
			scope.data.uploaded = JSON.parse(scope.data.uploaded);
			scope.files = [];
			for (var i = 0; i < scope.data.uploaded.length; i++) {
				scope.files.push({
					fileAlias : scope.data.uploaded[i],
					type : Utils.detectFile(scope.data.uploaded[i])
				});
			}

			scope.initFileUpload = function() {
				var _self = this;
				$('#fileupload').fileupload({
					dataType : 'json',
					done : function(e, data) {
						if (data.result.data.length > 0) {
							var tmp = data.result.data[0];
							tmp.type = Utils.detectFile(tmp.fileAlias);
							scope.files.push(tmp);
						}
						scope.$apply();
						scope.update();
					},
					progressall : function(e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
					},
					start : function(e, data) {
						console.log(e, data);
					},
					dropZone : $('#dropzone')
				});
			};

			scope.removeUpload = function(index) {
				var c = confirm("Bạn có muốn xoá file này không?");
				if (!c)
					return;
				var data = scope.files.splice(index, 1);
				scope.update();

			};

			scope.update = function() {
				var tmp = [];
				for (var i = 0; i < scope.files.length; i++) {
					tmp.push(scope.files[i].fileAlias);
				}
				TcbsService.updateOpenRequest({
					referenceid : scope.data.referenceid,
					uploaded : JSON.stringify(tmp)
				}, function(data) {
				});
			};

			scope.initFileUpload();
		}
	};
}).directive('adminuser', function(AdminService) {
	return {
		restrict : 'EA',
		scope : {
			user: '=',
			height: '='
		},
		replace : true,
		templateUrl : 'static/app/elements/admin-user.htm',
		link : function(scope, element, attrs) {
			scope.banks = [];
			scope.provinces = [];
			scope.height = attrs.height;
			for (var i = 0; i < scope.user.identifications.length; i++) {
				scope.user.identifications[i].idDateFormated = moment(scope.user.identifications[i].idDate, 'YYYY-MM-DD hh:mm:ss').format('DD/MM/YYYY');
			}
			scope.user.birthdayFormated = moment(data.birthday, 'YYYY-MM-DD hh:mm:ss').format('DD/MM/YYYY');
			AdminService.getProvince(function(provinces){
				scope.provinces = provinces;
				AdminService.getUserBank(scope.user.id, function(banks){
					scope.banks = banks;
				});
			});
			
			
			scope.changeProvince = function(province, bankid) {
				AdminService.getBankByProvince(province, function(bankData) {
					$scope.banks  = bankData;
				});
			};
		}
	};
}).directive('adminuserbank', function(AdminService) {
	return {
		restrict : 'EA',
		scope : {
			data: '=',
			provinces: '='
		},
		templateUrl : 'static/app/elements/admin-user-bank.htm',
		link : function(scope, element, attrs) {
			console.log(scope.data);
			AdminService.getBankByProvince(scope.data.bankProvince, function(bankData) {
				scope.banks  = bankData;
			});
			AdminService.getBankBranch(scope.data.bankProvince, scope.data.bankSys, function(branchesData) {
				scope.branches  = branchesData;
			});
		}
	};
}).directive('navigation', function(NavManager) {
	return {
		restrict : 'E',
		templateUrl : 'static/app/elements/navigation.htm',
		scope : false,
		link : function($scope) {
			$scope.user = NavManager.user;
		}
	};
}).directive('scrollOnClick', function() {
	return {
		restrict : 'A',
		link : function($scope, $elm) {
			$('.rightcolin').bind('scroll', function() {
				if ($(this).scrollTop() > 0) {
					$elm.fadeIn(400);
				} else {
					$elm.fadeOut(400);
				};
			});
			$elm.on('click', function() {
				$('.rightcolin').animate({
					scrollTop : 0
				}, "slow");
			});
		}
	};
}).directive('resize', function($window) {
	return function($scope, element) {
		var w = angular.element($window);
		$scope.getWidth = function() {
			return {
				'w' : w.width()
			};
		};

		$scope.$watch($scope.getWidth, function(newValue, oldValue) {
			$scope.IsDeviceResolution = newValue.w < 1200;
		}, true);

		w.bind('resize', function() {
			$scope.$apply();
		});
	};
}).directive('datepicker', function() {
	return {
		restrict : 'A',
		require : 'ngModel',
		link : function(scope, element, attrs, ngModelCtrl) {
			$(function() {
				element.datepicker({
					format : 'dd/mm/yyyy',
					onSelect : function(date) {
						scope.$apply(function() {
							ngModelCtrl.$setViewValue(date);
						});
					}
				}).on('show', function() {
					element.datepicker('setDate', moment(element.val(), 'DD/MM/YYYY').toDate());
					element.datepicker('update');
				});
			});
		}
	};
}).directive('ngEnter', function() {
	return function(scope, element, attrs) {
		element.bind("keydown keypress", function(event) {
			if (event.which === 13) {
				scope.$apply(function() {
					scope.$eval(attrs.ngEnter);
				});
				event.preventDefault();
			}
		});
	};
}).directive('sparkline', function($parse, $templateCache, $compile, utils, Stock, TcbsService) {
	return {
		restrict : 'EA',
		scope : {
			template : "@",
			template2 : "@",
			ticker : "="
		},
		link : function(scope, element, attrs, ngModel) {
			scope.$on('change-chart-window', function(e, args) {
				switch(args.dataWindow.id) {
				case 1:
					loadData("1W");
					break;
				case 2:
					loadData("1M");
					break;
				default:
					load24h();
					break;
				}

			});
			scope.added = false;
			var ngModelGet = $parse(attrs.ngModel);
			var ticker = scope.ticker;

			function load24h() {
				Stock.getPricesIn24h();

				Stock.data.get(ticker, {}).then(function(res) {
					var data = utils.optimizeData(res.data);
					renderChart(data);
					var len = res.data.length;
					var change = parseInt(res.data[len - 1].changePrice);
					var ratio = Math.round(change / (parseInt(res.data[len - 1].price) - change) * 10000) / 100;
					setPriceChange(change / 1000, ratio);
				});
			}

			function setPrice(price) {
				scope.price = parseInt(price) / 100;
				var template = $templateCache.get(scope.template2);
				element.parent().after($compile(template)(scope));
			}

			function setPriceChange(change, ratio) {
				scope.changePrice = change;
				scope.ratio = ratio;
				var template = $templateCache.get(scope.template);
				if (!scope.added) {
					element.parent().before($compile(template)(scope));
					scope.added = true;
				}
			}

			function loadData(dataWindow) {
				TcbsService.getStockHistory(ticker, dataWindow, function(pricing) {
					var priceData = [];
					for (var i = pricing.length - 1; i >= 0; i--) {
						priceData.push(parseInt(pricing[i]['closePrice']) / 100);
					}
					renderChart(priceData);
					var endPrice = pricing[0]['closePrice'];
					var startPrice = pricing[pricing.length - 1]['closePrice'];
					var change = endPrice - startPrice;
					setPriceChange(change / 100, Math.round((change / startPrice) * 10000) / 100);
					setPrice(endPrice);
				});
			}

			function renderChart(data) {
				if (scope.chart) {
					scope.chart.yAxis[0].update({
						max : Math.max.apply(null, data),
						min : Math.min.apply(null, data)
					});
					scope.chart.series[0].setData(data, 1);
					return;
				}
				scope.chart = new Highcharts.Chart({
					chart : {
						renderTo : $(element)[0],
						margin : [0, 0, 0, 0],
						backgroundColor : '#fff'
					},
					title : {
						text : ''
					},
					credits : {
						enabled : false
					},
					xAxis : {
						labels : {
							enabled : false
						}
					},
					yAxis : {
						min : Math.min.apply(Math, data),
						max : Math.max.apply(Math, data),
						maxPadding : 0,
						minPadding : 0,
						gridLineWidth : 0,
						endOnTick : false,
						labels : {
							enabled : false
						}
					},
					legend : {
						enabled : false
					},
					plotOptions : {
						series : {
							animation : true,
							fillColor : "#f2f2f2",
							color : '#c4161c',
							enableMouseTracking : false,
							lineWidth : 1.5,
							shadow : false,
							states : {
								hover : {
									lineWidth : 1
								}
							},
							marker : {
								radius : 0,
								states : {
									hover : {
										radius : 2
									}
								}
							}
						}
					},
					series : [{
						type : 'areaspline',
						data : data
					}]
				});
			}

			// load24h();
			// loadData("1M");
		}
	};
});
