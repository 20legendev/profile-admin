angular.module('tcbs.services').factory('LayoutManager', function($rootScope) {
	var Layout = this;

	Layout.header = {
		title : '',
		url : false,
		setTitle : function(title) {
			this.title = title;
		},
		setUrl : function(url) {
			this.url = url;
		},
		reset : function() {
			this.setTitle('');
			this.setUrl(false);
		}
	};

	/*
	$rootScope.$on('$stateChangeStart', function() {
		Layout.header.reset();
	});

	$rootScope.$on('$stateChangeSuccess', function(evt, $route) {
		var layout = $route.layout || {}, header = layout.header || false;
		if (header) {
			if (header.title) {
				Layout.header.setTitle(header.title);
			}
			if (header.url) {
				Layout.header.setUrl(header.url);
			}
		}
	});
	*/
	return Layout;
}); 