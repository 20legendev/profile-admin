angular.module('tcbs.services')
.factory('socket', function() {
    var socket;
 
    return {
     connect: function (url, options) {
        socket = io.connect(url, options);
    },
      on: function(eventName, callback){
        socket.on(eventName, callback);
      },
      
      emit: function(eventName, data) {
        socket.emit(eventName, data);
      }
    };
});
