angular.module('tcbs.services').factory('TcbsService', function(jsonModel, utils, $http) {
	var factory = {
		data : [],
		validateOtp : validateOtp,
		getMe : getMe,
		updateOpenRequest : updateOpenRequest,
		confirmOpen : confirmOpen,
		confirmContract: confirmContract
	};

	function getMe(cb, err) {
		$http.get('/api/user/me', {
		}).then(function(res) {
			if (res.status == 200 && res.data.status == 0) {
				if (cb)
					cb(res.data.data);
			} else {
				if (err)
					err(res.data);
			}
		}, function(errdata) {
			if (err)
				err(errdata);
		});
	}

	function updateOpenRequest(data, cb, err) {
		$http.post('/api/rm/update-open', data).then(function(res) {
			if (res.status == 200 && res.data.status == 0) {
				if (cb)
					cb();
			} else {
				if (err)
					err();
			}
		}, function(errData) {
			if (err)
				err();
		});
	}

	function confirmOpen(referenceid, cb, err) {
		$http.post('api/rm/confirm-open', {
			referenceid : referenceid
		}).then(function(res) {
			if (res.status == 200 && res.data.status == 0) {
				if (cb)
					cb();
			} else {
				if (err)
					err();
			}
		}, function(errData) {
			if (err)
				err();
		});
	}
	
	function confirmContract(referenceid, cb, err) {
		$http.post('api/rm/confirm-contract', {
			referenceid : referenceid
		}).then(function(res) {
			if (res.status == 200 && res.data.status == 0) {
				if (cb)
					cb();
			} else {
				if (err)
					err();
			}
		}, function(errData) {
			if (err)
				err();
		});
	}
	
	function validateOtp() {
		factory.data = jsonModel('/validate', 'otp');
	}

	return factory;
});
