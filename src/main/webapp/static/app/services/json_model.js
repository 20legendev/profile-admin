angular.module('tcbs.services').config(function($httpProvider) {
	// Intercept POST requests, convert to standard form encoding
	$httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
	$httpProvider.defaults.headers.put["Content-Type"] = "application/x-www-form-urlencoded";
	$httpProvider.defaults.headers.delete = {
		'Content-Type' : 'application/json'
	};

	$httpProvider.defaults.transformRequest.unshift(function(data, headersGetter) {
		var key, result = [];
		if ( typeof data === "string")
			return data;
		for (key in data) {
			if (data.hasOwnProperty(key))
				result.push(encodeURIComponent(key) + "=" + encodeURIComponent(data[key]));
		}
		return result.join("&");
	});
}).factory('jsonModel', function($http, $q, $window) {
	return function(uri, serviceName) {
		uri = Config.baseUrl + serviceName + uri;

		return {
			getUri : function() {
				return uri;
			},
			query : function(params) {
				var uri2 = uri;
				if (angular.isArray(params)) {
					uri2 += '?' + params.join('&');
					params = {};
				}
				return $http({
					method : 'GET',
					url : uri2,
					params : params
				}).then(function(response) {
					if (response['data'].msg == 'LOGIN_REQUIRED') {
						$window.location.reload();
					} else {
						return response['data'];
					}

				}, function(response) {
					return $q.reject(response);
				});
			},
			get : function(id, params) {
				return $http({
					method : 'GET',
					url : uri + '/' + id,
					params : params
				}).then(function(response) {
					if (response['data'].msg == 'LOGIN_REQUIRED') {
						$window.location.reload();
					} else {
						return response['data'];
					}
				}, function(response) {
					return $q.reject(response);
				});
			},
			post : function(data) {
				return $http({
					method : 'POST',
					url : uri,
					data : data
				}).then(function(response) {
					if (response['data'].msg == 'LOGIN_REQUIRED') {
						$window.location.reload();
					} else {
						return response['data'];
					}
				}, function(response) {
					return $q.reject(response);
				});
			},
			edit : function(id, data) {
				return $http({
					method : 'PUT',
					url : uri + '/' + id,
					data : data
				}).then(function(response) {
					if (response['data'].msg == 'LOGIN_REQUIRED') {
						$window.location.reload();
					} else {
						return response['data'];
					}
				}, function(response) {
					return $q.reject(response);
				});
			},
			remove : function(id) {
				return $http({
					method : 'DELETE',
					url : uri + '/' + id
				}).then(function(response) {
					if (response['data'].msg == 'LOGIN_REQUIRED') {
						$window.location.reload();
					} else {
						return response['data'];
					}
				}, function(response) {
					return $q.reject(response);
				});
			},
			request : function(path, options) {
				options = options || {};
				options.url = uri + ( path ? ('/' + path) : '');
				options.method = options.method || 'GET';
				options.headers = options.headers || {};
				options.headers['Content-Type'] = options.headers['Content-Type'] || 'application/json';
				return $http(options).then(function(response) {
					if (response['data'].msg == 'LOGIN_REQUIRED') {
						$window.location.reload();
					} else {
						return response;
					}
				}, function(response) {
					return $q.reject(response);
				});
			}
		};
	};
}); 