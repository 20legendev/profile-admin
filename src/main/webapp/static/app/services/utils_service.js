angular.module('tcbs.services').factory('utils', function() {
    return {
        nextElement : function(a, target, nameIndex) {
            for (var i = 0; i < a.length; i++) {
                if (a[i][nameIndex] == target) {
                    if (i < a.length - 1) {
                        return a[i + 1];
                    } else {
                        return a[0]
                    }
                }
            }
            return null;
        },
        prevElement : function(a, target, nameIndex) {
            for (var i = 0; i < a.length; i++) {
                if (a[i][nameIndex] == target) {
                    if (i === 0) {
                        return a[a.length - 1];
                    } else {
                        return a[i - 1]
                    }
                }
            }
            return null;
        },
        setCache: function(key, data, ttl){
            localStorage.setItem(key, data);
            localStorage.setItem(key + '.ttl', new Date().getTime() + ttl);
        },
        
        getCache: function(key){
            var data = localStorage.getItem(key);
            if(!data) return null;
            
            var time = localStorage.getItem(key + '.ttl');
            if(new Date(parseFloat(time)) >= new Date()){
                return data;
            }
            localStorage.removeItem(key);
            localStorage.removeItem(key + '.ttl');
            return null;
        },
        
        optimizeData: function(data){
            var result = [];
            angular.forEach(data, function(o,i){
                if ( i == 0 || o.price != data[i - 1].price) result.push(parseInt(o.price));
            });

            if (result.length > 25) {
                while (true){
                    var rand = Math.floor(Math.random() * result.length);  
                    if (rand != 0 && rand != result.length - 1){// && result[rand + 1] != result[rand - 1]){
                        result.splice(rand, 1);
                    }
                    if(result.length <= 25) break;
                }    
            }
            return result;
        }
    };
});
