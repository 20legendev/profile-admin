angular.module('tcbs.services').factory('NavManager', function(TcbsService, $rootScope, jsonModel) {

	var Nav = this;
	Nav.userData = {};
	Nav.user = {
		fullname : '',
		email : '',

		setFullname : function(fullname) {
			this.fullname = fullname;
		},
		setEmail : function(email) {
			this.email = email;
		}
	};
	TcbsService.getMe(function(data) {
		Nav.userData = data;
		Nav.user.setFullname(data.lastname + " " + data.firstname);
		Nav.user.setEmail(data.email);
	});
	return Nav;
});
