/**
 * Load states for application
 * more info on UI-Router states can be found at
 * https://github.com/angular-ui/ui-router/wiki
 */
angular.module('tcbs').config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
function($stateProvider, $urlRouterProvider, $locationProvider) {

	$urlRouterProvider.when('', '/').otherwise('/404');

	$stateProvider.state('home', {
		url : '/',
		templateUrl : 'static/app/components/home/views/home.view.htm',
		controller : 'homeController',
		controllerAs : 'ctrl',
		activemenu : 'home',
		layout : {
			header : {
				title : 'Tổng quan'
			}
		}
	}).state('pgd', {
		url : '/phong-giao-dich',
		templateUrl : 'static/app/components/pgd/views/home.view.htm',
		controller : 'pgdController',
		controllerAs : 'ctrl',
		layout : {
			header : {
				title : 'Phòng giao dịch'
			}
		}
	}).state('mycustomer', {
		url : '/khach-hang-cua-toi',
		templateUrl : 'static/app/components/my-customer/views/home.view.htm',
		controller : 'myCustomerController',
		controllerAs : 'ctrl',
		layout : {
			header : {
				title : 'Hộp thư'
			}
		}
	}).state('adminusermanager', {
		url : '/quan-ly-khach-hang',
		templateUrl : 'static/app/components/admin/views/user-manager.htm',
		controller : 'userManagerController',
		controllerAs : 'ctrl',
		layout : {
			header : {
				title : 'Hộp thư'
			}
		}
	}).state('permission', {
		url : '/permission',
		templateUrl : 'static/app/components/permission/views/permission.view.htm',
		controller : 'permissionController',
		controllerAs : 'ctrl',
		layout : {
			header : {
				title : 'Hộp thư'
			}
		}
	}).state('permission.group', {
		url : '/group',
		templateUrl : 'static/app/components/permission/views/ajax/group.htm',
		controller : 'permissionGroupController',
		controllerAs : 'ctrl',
		layout : {
			header : {
				title : 'Hộp thư'
			}
		}
	}).state('system', {
		url : '/system',
		templateUrl : 'static/app/components/permission/views/ajax/system.htm',
		controller : 'systemController',
		controllerAs : 'ctrl',
		activetab : 'push',
		layout : {
			header : {
				title : 'Hộp thư'
			}
		}
	}).state('transfer', {
		url : '/transfer',
		templateUrl : 'static/app/components/transfer/views/transfer.view.htm',
		controller : 'transferController',
		controllerAs : 'ctrl',
		activemenu : 'transfer',
		layout : {
			header : {
				title : 'Chuyển tiền'
			}
		}
	}).state('advisory', {
		url : '/advisory',
		templateUrl : 'static/app/components/advisory/views/advisory.view.htm',
		controller : 'advisoryController',
		controllerAs : 'ctrl',
		activemenu : 'advisory',
		layout : {
			header : {
				title : 'Tư vấn đầu tư'
			}
		}
	}).state('watchlist', {
		url : '/watchlist',
		templateUrl : 'static/app/components/watchlist/views/watchlist.view.htm',
		controller : 'watchlistController',
		controllerAs : 'ctrl',
		activemenu : 'watchlist',
		layout : {
			header : {
				title : 'Sản phấm'
			}
		}
	}).state('watchlist.stock', {
		url : '/stock',
		templateUrl : 'static/app/components/watchlist/views/ajax/stock.htm',
		controller : 'wlStockController',
		controllerAs : 'ctrl',
		activetab : 'stock',
		activemenu : 'watchlist',
		layout : {
			header : {
				title : 'Cổ phiếu'
			}
		}
	}).state('watchlist.bond', {
		url : '/bond',
		templateUrl : 'static/app/components/watchlist/views/ajax/bond.htm',
		controller : 'wlBondController',
		controllerAs : 'ctrl',
		activetab : 'bond',
		activemenu : 'watchlist',
		layout : {
			header : {
				title : 'Trái phiếu'
			}
		}
	}).state('watchlist.fund', {
		url : '/fund',
		templateUrl : 'static/app/components/watchlist/views/ajax/fund.htm',
		controller : 'wlFundController',
		controllerAs : 'ctrl',
		activetab : 'fund',
		activemenu : 'watchlist',
		layout : {
			header : {
				title : 'Quỹ đầu tư'
			}
		}
	}).state('stock', {
		url : '/stock',
		templateUrl : 'static/app/components/products/stock/views/stock.view.htm',
		controller : 'stockController',
		controllerAs : 'ctrl',
		activemenu : 'stock',
		layout : {
			header : {
				title : 'Cổ phiếu'
			}
		}
	}).state('stock.portfolio', {
		url : '/portfolio',
		templateUrl : 'static/app/components/products/stock/views/ajax/portfolio.htm',
		controller : 'portfolioStockController',
		controllerAs : 'ctrl',
		activetab : 'portfolio',
		activemenu : 'stock',
		layout : {
			header : {
				title : 'Cổ phiếu'
			}
		}
	}).state('stock.detail', {
		url : '/detail/{symbol:[A-Z]{0,4}}',
		templateUrl : 'static/app/components/products/stock/views/ajax/detail.htm',
		controller : 'detailStockController',
		controllerAs : 'ctrl',
		activetab : 'detail',
		activemenu : 'stock',
		layout : {
			header : {
				title : 'Cổ phiếu'
			}
		}
	}).state('stock.effective', {
		url : '/effective',
		templateUrl : 'static/app/components/products/stock/views/ajax/effective.htm',
		controller : 'effectiveStockController',
		controllerAs : 'ctrl',
		activetab : 'effective',
		activemenu : 'stock',
		layout : {
			header : {
				title : 'Cổ phiếu'
			}
		}
	}).state('stock.history', {
		url : '/history',
		templateUrl : 'static/app/components/products/stock/views/ajax/history.htm',
		controller : 'historyStockController',
		activetab : 'history',
		activemenu : 'stock',
		layout : {
			header : {
				title : 'Cổ phiếu'
			}
		}
	}).state('bond', {
		url : '/bond',
		templateUrl : 'static/app/components/products/bond/views/bond.view.htm',
		controller : 'bondController',
		controllerAs : 'ctrl',
		activemenu : 'bond',
		layout : {
			header : {
				title : 'Trái phiếu'
			}
		}
	}).state('bond.portfolio', {
		url : '/portfolio',
		templateUrl : 'static/app/components/products/bond/views/ajax/portfolio.htm',
		controller : 'portfolioBondController',
		controllerAs : 'ctrl',
		activetab : 'portfolio',
		activemenu : 'bond',
		layout : {
			header : {
				title : 'Trái phiếu'
			}
		}
	}).state('bond.detail', {
		url : '/detail/{tradingId:[0-9]{0,5}}',
		templateUrl : 'static/app/components/products/bond/views/ajax/detail.htm',
		controller : 'detailBondController',
		controllerAs : 'ctrl',
		activetab : 'detail',
		activemenu : 'bond',
		layout : {
			header : {
				title : 'Trái phiếu'
			}
		}
	}).state('bond.history', {
		url : '/history',
		templateUrl : 'static/app/components/products/bond/views/ajax/history.htm',
		controller : 'historyBondController',
		controllerAs : 'ctrl',
		activetab : 'history',
		activemenu : 'bond',
		layout : {
			header : {
				title : 'Trái phiếu'
			}
		}
	}).state('fund', {
		url : '/fund',
		templateUrl : 'static/app/components/products/fund/views/fund.view.htm',
		controller : 'fundController',
		controllerAs : 'ctrl',
		activemenu : 'fund',
		layout : {
			header : {
				title : 'Quỹ đầu tư'
			}
		}
	}).state('fund.portfolio', {
		url : '/portfolio',
		templateUrl : 'static/app/components/products/fund/views/ajax/portfolio.htm',
		controller : 'portfolioFundController',
		controllerAs : 'ctrl',
		activetab : 'portfolio',
		activemenu : 'fund',
		layout : {
			header : {
				title : 'Quỹ đầu tư'
			}
		}
	}).state('fund.detail', {
		url : '/detail',
		templateUrl : 'static/app/components/products/fund/views/ajax/detail.htm',
		controllerAs : 'ctrl',
		activetab : 'detail',
		activemenu : 'fund',
		layout : {
			header : {
				title : 'Quỹ đầu tư'
			}
		}
	}).state('fund.history', {
		url : '/history',
		templateUrl : 'static/app/components/products/fund/views/ajax/history.htm',
		controller : 'historyFundController',
		controllerAs : 'ctrl',
		activetab : 'history',
		activemenu : 'fund',
		layout : {
			header : {
				title : 'Quỹ đầu tư'
			}
		}
	}).state('profile', {
		url : '/profile',
		templateUrl : 'static/app/components/profile/views/profile.view.htm',
		controller : 'profileController',
		controllerAs : 'ctrl',
		activetab : 'profile',
		layout : {
			header : {
				title : 'Thông tin cá nhân'
			}
		}
	}).state('profile.detail', {
		url : '/detail',
		templateUrl : 'static/app/components/profile/views/ajax/detail.htm',
		controller : 'profileDetailController',
		controllerAs : 'ctrl',
		activetab : 'profile',
		layout : {
			header : {
				title : 'Thông tin cá nhân'
			}
		}
	}).state('profile.security', {
		url : '/security',
		templateUrl : 'static/app/components/profile/views/ajax/security.htm',
		controller : 'profileSecurityController',
		controllerAs : 'ctrl',
		activetab : 'security',
		layout : {
			header : {
				title : 'Thông tin cá nhân'
			}
		}
	}).state('product-detail', {
		url : '/product-detail/{id}',
		templateUrl : 'static/app/components/product-detail/views/product-detail.view.htm',
		controller : 'productDetailController',
		controllerAs : 'ctrl',
		layout : {
			header : {
				title : 'Sản phẩm đầu tư'
			}
		}
	}).state('404', {
		url : '/404',
		templateUrl : 'app/shared/404.htm'
	});
}]);
